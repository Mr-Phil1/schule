#include <Arduino.h>

#define LEDGREEN 12
#define LEDBLUE 11
#define LEDWHITE 10
#define LEDRED 9
#define LEDYELLOW 8

int analogPin = A3;
int val = 0;

//led activation
void activation(int pinNumber)
{
  pinMode(pinNumber, OUTPUT);
}

//put the led on
void lightOn(int ledNumber)
{
  digitalWrite(ledNumber, HIGH);
}

//put all led off
void allOut()
{
  digitalWrite(LEDGREEN, LOW);
  digitalWrite(LEDBLUE, LOW);
  digitalWrite(LEDWHITE, LOW);
  digitalWrite(LEDRED, LOW);
  digitalWrite(LEDYELLOW, LOW);
}

//light chain
void lightChain(int intput)
{
  if (intput <= 200)
  {
    lightOn(LEDGREEN);
  }
  else if (intput <= 400)
  {
    lightOn(LEDBLUE);
  }
  else if (intput <= 600)
  {
    lightOn(LEDWHITE);
  }
  else if (intput <= 800)
  {
    lightOn(LEDRED);
  }
  else if (intput <= 1000)
  {
    lightOn(LEDYELLOW);
  }
  delay(500);
  allOut();

}

void setup()
{
  activation(LEDGREEN);
  activation(LEDBLUE);
  activation(LEDWHITE);
  activation(LEDRED);
  activation(LEDYELLOW);
  Serial.begin(9600);
}

void loop()
{
  val = analogRead(analogPin);
  Serial.println(val);
  lightChain(val);
}