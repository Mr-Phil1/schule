---
title: Arbeitsbericht 
subtitle: LDR (Light Density Resistor) - LED-Steuerung
author: Mr. Phil
rights: Nah
language: de-AT
keywords: TINF; Linux;
titlepage: true
titlepage-color: "36A1F0"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---
# Einleitung
In diesem Bericht wird erklärt wie man für einen Arduino ein Programm schreibt, welches mit Hilfe von LEDs die Lichtstärke ausgeben kann.

## Aufgaben-Beschreibung
Verwende einen LDR (siehe Live Coding und Skript Spannungsteilerschaltung), um die Lichtstärke zu messen. Bei geringer Lichtstärke (Finger auf Sensor) soll LED 1 leuchten, bei starkem Licht soll LED2 leuchten. 

Der 3 Punkte Kandidat langweilt sich bei dieser Aufgabe und implementiert daher eine Lichterkette aus z.B. 5 LEDs, die abhängig von der Lichtstärke leuchtet und damit einen Rückschluss auf die Lichtintensität gibt. Damit der Code nicht zu unübersichtlich wird, lagert er die Logik für die Lichterkette (für die er mit Schleifen arbeitet) in eine Funktion aus. 

# Verwendete Technologien
Technologien-Name | Verwendete Version 
------------ | ------------- | -------------
Arduino | Arduino Mega 2560 (REV 3)
IDE  | Visual Studio Code mit dem PlattformIO Plugin

# Durchführung

## Grafische Darstellung

![Steckbrett Darstellung (erstellt mit Fritzing)](Sketch.png)


\pagebreak
## Arduino-Code

### main.cpp-Inhalt
```c++
#include <Arduino.h>

#define LEDGREEN 12
#define LEDBLUE 11
#define LEDWHITE 10
#define LEDRED 9
#define LEDYELLOW 8

int analogPin = A3;
int val = 0;

//led activation
void activation(int pinNumber)
{
  pinMode(pinNumber, OUTPUT);
}

//put the led on
void lightOn(int ledNumber)
{
  digitalWrite(ledNumber, HIGH);
}

//put all led off
void allOut()
{
  digitalWrite(LEDGREEN, LOW);
  digitalWrite(LEDBLUE, LOW);
  digitalWrite(LEDWHITE, LOW);
  digitalWrite(LEDRED, LOW);
  digitalWrite(LEDYELLOW, LOW);
}

//light chain
void lightChain(int input)
{
  if (input <= 200)
  {
    lightOn(LEDGREEN);
  }
  else if (input <= 400)
  {
    lightOn(LEDBLUE);
  }
  else if (input <= 600)
  {
    lightOn(LEDWHITE);
  }
  else if (input <= 800)
  {
    lightOn(LEDRED);
  }
  else if (input <= 1000)
  {
    lightOn(LEDYELLOW);
  }
  delay(500);
  allOut();

}

void setup()
{
  activation(LEDGREEN);
  activation(LEDBLUE);
  activation(LEDWHITE);
  activation(LEDRED);
  activation(LEDYELLOW);
  Serial.begin(9600);
}

void loop()
{
  val = analogRead(analogPin);
  Serial.println(val);
  lightChain(val);
}
```
\pagebreak
### main.cpp-Erklärung
**Funktions Erklärung:**

* **void activation(int pinNumber)**
  * Mit Hilfe dieser Funktion lässt sich vereinfacht der Pin-Platz einer LED aktivieren.
* **void lightOn(int ledNumber)**
  * Mit Hilfe dieser Funktion lässt sich vereinfacht eine LED einschalten.
* **void allOut()**
  * Mit Hilfe dieser Funktion lassen sich alle LEDs ausschalten.
* **void lightChain(int input)**
  * In dieser Funktion wird je nach Input Wert, eine andere LED eingeschaltet.
  * Verwendet wird bei 5 LEDs und einen Maximalwert von 1000, ein 200 Schritt.
* **void setup()**
  * Diese Funktion wird einmalig beim System-Start ausgeführt.
  * Hier bei wird mit Hilfe der *activation*-Funktion sämtliche LEDs aktiv geschalten.
  * Nach der LED-Aktivierung wird dann Baud-Rate (Einheit für die Symbolrate: 1 Baud = 1 Symbol pro Sekunde) für die serielle Datenübertragung auf 9600 bps (Bits per Seconds) festgelegt.
* **void loop()**
  * Diese Funktion wird dauerhaft ausgeführt.
  * Zunächst wird von einem Analog-Pin der Inputwert, welcher von dem LDR-Sensor kommt, in die Variable *val* gespeichert.
  * Dieser Wert wird zuerst auf den Serial-Monitor ausgegeben und dann der *lightChain*-Funktion mitgegeben.

# Diskussion


# Literaturverzeichnis
* [Arduina-Reference](https://www.arduino.cc/reference/en/)
  * [analogRead() Funktion](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/)