#include <Arduino.h>

#define blue 5
#define yellow 3

void setup()
{
  pinMode(blue, OUTPUT);
  pinMode(yellow, OUTPUT);
}
void loop()
{
  digitalWrite(blue, HIGH);
  delay(500);
  digitalWrite(blue, LOW);
  delay(1000); // wait for a second
  digitalWrite(yellow, HIGH);
  delay(500);
  digitalWrite(yellow, LOW);
}