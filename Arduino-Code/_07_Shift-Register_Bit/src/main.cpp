#include <Arduino.h>

byte dataPin = 11;
byte stcPin = 8;
byte shcPin = 12;

void progShiftReg(byte data)
{
  for (byte i = 0; i < 8; i++)
  {
    byte mask = 1 << i;
    byte masked_data = data & mask;
    byte writeData = masked_data >> i;
    digitalWrite(dataPin, writeData);

    digitalWrite(shcPin, HIGH);
    digitalWrite(shcPin, LOW);
  }
  digitalWrite(stcPin, HIGH);
  digitalWrite(stcPin, LOW);
}

void setup()
{
  pinMode(dataPin, OUTPUT);
  pinMode(stcPin, OUTPUT);
  pinMode(shcPin, OUTPUT);
  digitalWrite(dataPin, LOW);
  digitalWrite(stcPin, LOW);
  digitalWrite(shcPin, LOW);
  Serial.begin(9600);
}

void loop()
{
  int number = 128;
  for (byte i = 0; i <= number; i++)
  {
    progShiftReg(i);
    if (i < 100)
    {
      if (i < 10)
      {
        Serial.print("Number: 00");
        Serial.println(i);
      }
      else
      {
        Serial.print("Number: 0");
        Serial.println(i);
      }
    }
    else
    {
      Serial.print("Number: ");
      Serial.println(i);
    }

    delay(500);
  }
  Serial.println("-------------------------");
}
