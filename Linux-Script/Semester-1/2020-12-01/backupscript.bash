#!/bin/bash
# Copyright Mr-Phil
folderToBackup="$1"
backupFolder="$2"
backupFileName="$3"

clear
echo "Welcome to the Mr. Phil1 Backup-Script"
echo "--------------------------------------"
echo "folderToBackup: ${folderToBackup}"
echo "backupFolder:   ${backupFolder}"
echo "backupFileName: ${backupFileName}"
echo "Backup nun gestartet"
echo "--------------------------------------"
if [ ! -d $folderToBackup ]; then
  clear
  echo "--------------------------------------"
  echo "Fehlerhafte Ordner-Eingabe"
  echo "--------------------------------------"
  exit 1
fi

if [ ! -d ${backupFolder} ]; then
  mkdir ${backupFolder}
fi

tar -czf ${backupFolder}/`date +%Y-%m-%d_%H-%M.%S`_${backupFileName}.tar.gz ${folderToBackup}
echo "Das Backup vom "${backupFileName}" ist nun fertig gestellt"
echo "--------------------------------------------------------------------------"
# Copyright Mr-Phil
