#!/bin/bash

counter=0
echo "----------------------------"
echo "while schleife"
while [ $counter  -lt 10 ]; do
  echo "Hello world $counter"
  counter=$((counter+1))
done

echo "----------------------------"
echo "for schleife"
for (( i = 0; i < 10; i++ )); do

    echo "Hello world1 $i"
done
