#!/bin/bash

echo "----------------------------"
echo "for-each schleife"
for i in `ls /etc`; do
  echo "Datei: $i"
done

echo "----------------------------"
echo "for-each schleife"
for i in "hallo welt" Noch etwas; do
  echo "Datei: $i"
done

#echo "----------------------------"
#echo "for-each schleife"
#for i in `ls ./`; do
#  grep "for" $i
#done
