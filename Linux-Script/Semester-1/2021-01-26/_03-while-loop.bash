#!/bin/bash
echo "----------------------------"
cat names.txt | while read name; do
  if [[ $name = "Frau"* ]]; then
    echo "Sehr geehrte $name"
  else
    echo "Sehr geehrter $name"
  fi
done
echo "----------------------------"
