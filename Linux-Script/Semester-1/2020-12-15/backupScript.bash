#!/bin/bash
# Copyright Mr-Phil
######put the Arg in to my variables######
folderToBackup="$1"
backupFolder="$2"
backupFileName="$3"
backupDate=`date +%Y-%m-%d_%H-%M-%S`
##########################################


clear
echo "--------------------------------------------------------------------------"
echo "                  Welcome to the Mr. Phil1 Backup-Script"
echo "--------------------------------------------------------------------------"
if [[ "$3" == '' ]] ; then
  echo "  Sie haben zu wenigen Argumente mitgegeben!"
  echo "  Sie müssen folderToBackup, backupFolder und backupFileName angegeben"
  echo "--------------------------------------------------------------------------"
  exit
fi

if [ $# -eq 4 ] ; then
  echo "  Sie haben zu viele Argumente mitgegeben!"
  echo "  Sie müssen folderToBackup, backupFolder und backupFileName angegeben"
  echo "--------------------------------------------------------------------------"
  exit
fi
echo "  folderToBackup: ${folderToBackup}"
echo "  backupFolder:   ${backupFolder}"
echo "  backupFileName: `date +%Y-%m-%d_%H-%M-%S`_${backupFileName}.tar.gz"
echo "--------------------------------------------------------------------------"
if [ ! -d $folderToBackup ]; then
  echo "  Nicht vorhandener Sicherungs-Ordner"
  echo "  !! Der Ordner "${folderToBackup}" existiert nicht !!"
  echo "--------------------------------------------------------------------------"
  exit
fi

if [ ! -d ${backupFolder} ]; then
  echo "  Nicht vorhandener Backup-Ordner"
  echo "  Der Ordner "${backupFolder}" existiert nicht und wird erstellt!"
  mkdir ${backupFolder}/
  echo "--------------------------------------------------------------------------"
fi

echo "  Backup wird nun gestartet!"
echo "  Je nach Größe des folderToBackup kann dieser Prozess eine Weile dauern."
tar -czf ${backupFolder}/${backupDate}_${backupFileName}.tar.gz ${folderToBackup} 2>> ${backupFolder}/error-msg.txt; echo "" >>${backupFolder}/error-msg.txt
echo "  Das Backup von "${folderToBackup}" ist nun fertig gestellt."
echo "  Vielen Herzlichen Dank für Ihr Vertrauen."
echo "--------------------------------------------------------------------------"
if [ -s ${backupFolder}/${backupDate}_${backupFileName}.tar.gz ]; then
    rsync --numeric-ids -avz --stats ${backupFolder}/${backupDate}_${backupFileName}.tar.gz mr-phil@192.168.56.31:
else
    echo "  Nicht vorhandener *.tar.gz Datei"
    echo "  !! Die *.tar.gz Datei existiert nicht oder ist kleiner 0 Byte!!"
    echo "--------------------------------------------------------------------------"
  exit 1
fi
# Copyright Mr-Phil
