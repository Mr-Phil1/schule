# Netbeans-Projekte

## Info:
In diesem Repository sind sämtliche Java Programme hinterlegt, welche während des Schulverlaufs im IT-Kolleg Imst erstellt wurden. Sollte Sie noch Fragen bezüglich der Anforderungen bzw. der Haftung haben, so empfehle ich Ihnen dass [Lizenz-und-Schüleranforderung](Lizenz-und-Schüleranforderung.pdf) Dokument noch mal zu lesen.


### Ersteller (mit dem dazu gehörigen Ordner):
- Mr. Phil (Ordner: 1.Semester/)
- Professoren: (die kennzeichnung erfolgt in den jeweiligen Bereichen) [Ordner: 1.Semester/Prof/](/1. Semester/Prof/)
- Schüler: (die kennzeichnung erfolgt in den jeweiligen Bereichen) [Ordner: 1.Semester/Schuler/](/1. Semester/Schuler/)
---
#### Benutze Ressourcen:
* Java jdk 8_191
* NetBeans 8.2
---
#### Disclaimer:
Der Seitenbetreiber (Mr. Phil) übernimmt keinerlei Haftung für die onlinegestellten Codezeilen, außer für die, welche der Seitenbetreiber selbst geschrieben hat. Sämtliche Codezeilen, die von anderen Quellen (z.B. StackOverflow) stammen, müssen vom Ersteller mit einer Quellenangabe versehen werden.  Sollte eine der oben genannten Anforderungen nicht erfüllt sein, behält sich der Seitenbetreiber das Recht vor, das zur Verfügung gestellte Projekt nicht zu veröffentlichen.

Sollte es zu Forderungen dritte wegen unberechtigter Verwendung von Codezeilen kommen, so haftet der Seitenbetreiber nicht dafür, sondern immer der Ersteller der Codezeilen.

---
#### Wünsche + Anregungen
Sollten irgenwelche Wünsche und Anregungen bezüglich dieses Repository auftauchen sind diese bitte bei Mr. Phil anzubringen.

---
---
###### Dankesagung
1. An Prof. Land für die Genehmigung dass ich seinen geschrieben Code veröffentlichen darf.
