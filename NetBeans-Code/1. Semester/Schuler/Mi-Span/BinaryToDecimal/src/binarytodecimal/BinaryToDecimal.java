/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarytodecimal;

import java.util.Scanner;

/**
 *
 * @author Span Michael
 */
public class BinaryToDecimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Bitte geben Sie eine BinÃ¤rzahl ein: ");
        int binary = scan.nextInt();
        int length = Integer.toString(binary).length() - 1;
        int decimalPart = 0;
        int decimal = 0;
        double hoch = 0.0;
        while (length != -1) {
            decimalPart = Integer.parseInt("" + Integer.toString(binary).charAt(length)) * ((int) Math.pow(2, hoch));
            length--;
            hoch++;
            decimal += decimalPart;
        }
        scan.close();
        System.out.println("Ihre BinÃ¤rzahl ist " + decimal + " in Dezimalschreibweise.");
    }

}
