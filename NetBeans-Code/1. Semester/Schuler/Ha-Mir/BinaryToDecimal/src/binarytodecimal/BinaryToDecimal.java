/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarytodecimal;

import java.util.Scanner;

/**
 *
 * @author Mirle Hagen
 */
public class BinaryToDecimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //Variablen deklarieren
        String varDualzahlString = "0";     //Die Dualzahl als String
        int varLängeDualzahl = 0;           //Die Länge der Dualzahl als Integer
        int varPotenz = 0;                  //Die Potenz
        int varPositionNummer = 0;          //Die Stelle in der Dualzahl
        int varDezimalzahl = 0;             //Das Ergebnis, die Dezimalzahl
        int varEingabeFehler = 0;           //Der Fehlerspeicher bei Falscheingabe

        //Scanner wird intialisiert
        Scanner varScannerInput = new Scanner(System.in);

        //Begrüßung und erste Eingabe der Dualzahl
        System.out.println("***************************************");
        System.out.println("***       DUALZAHLENUMRECHNER       ***");
        System.out.println("***************************************");
        System.out.println("");
        System.out.println("Gib eine beliebige Dualzahl ein.");
        System.out.println("Es sind nur die Ziffern 1 und 0 erlaubt.");
        System.out.println("");
        System.out.print("Deine Eingabe bitte: ");
        varDualzahlString = varScannerInput.next();
        System.out.println("");

        //Prüfung ob nur erlaubte Zeichen eingegeben wurden
        //Bei unerlaubten Zeichen Aufforderung zur Neueingabe
        //Schleife Anfang
        do {
            //Fehlerspeicher in Schlefe zurücksetzen
            varEingabeFehler = 0;

            //Länge der Dualzahl ermitteln
            varLängeDualzahl = varDualzahlString.length();

            //Jede Stelle der Dualzahl auf Falscheingabe prüfen (ungleich 0 oder 1)
            for (int a = varLängeDualzahl; a > 0; a--) {
                varPositionNummer = varLängeDualzahl - a;
                //Bei Falscheingabe Fehlerspeicher auf 1 setzen
                if ((varDualzahlString.charAt(varPositionNummer) != '0') & (varDualzahlString.charAt(varPositionNummer) != '1')) {
                    varEingabeFehler = 1;
                }
            }
            //Erneute Eingabe der Dualzahl
            if (varEingabeFehler == 1) {
                System.out.println("Die Eingabe enthält andere Zeichen.");
                System.out.println("Es sind nur die Ziffern 1 und 0 erlaubt.");
                System.out.println("");
                System.out.print("Bitte noch einmal: ");
                varDualzahlString = varScannerInput.next();
                System.out.println("");
            }
        } while (varEingabeFehler == 1);    //Schleife Ende

        //Umrechnung der Dualzahl in die entsprechende Dezimalzahl
        for (int i = varLängeDualzahl; i > 0; i--) {
            varPositionNummer = varLängeDualzahl - i;       //Positionsnummer 0 entspricht erster Stelle von links gezählt
            varPotenz = i - 1;
            if (varDualzahlString.charAt(varPositionNummer) == '1') {
                varDezimalzahl = (int) (varDezimalzahl + (Math.pow(2, varPotenz)));
            }
        }

        //Ausgabe des Ergebnisses und Programm beenden
        System.out.println("Die Dualzahl " + varDualzahlString);
        System.out.println("entspricht der Dezimalzahl " + varDezimalzahl + ".");
        System.out.println("");
        System.out.println("Auf Wiedersehen...");
        varScannerInput.close();
        System.exit(0);
    }

}
