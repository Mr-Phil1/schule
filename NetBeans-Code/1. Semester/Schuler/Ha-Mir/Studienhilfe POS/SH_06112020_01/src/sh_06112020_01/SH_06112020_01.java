// Hagen Mirle, 3AKIF, Studienhilfe SH_06112020_01, 05.11.2020

//******************************************************************************

// Hinweis zu Kommentaren

        /*
        Der Kopf des Scriptes sollte Dinge wie Programmname und Programmbeschreibung,  Entwickler, Lizenzbestimmungen, etc. enthalten.
        */

        // Einzeilige Kommentare beginnen mit dem doppelten Slash.
        // Mit dem dopelten Slash kann auch am Ende einer Programmzeile kommentiert werden.

        /*
        Mehrzeilige Kommentare beginnen mit Slash und Stern
        und enden mit Stern und Slash.
        */

//******************************************************************************

// Das Komando package

        package sh_06112020_01;     // Das ist der Name des Ordners, in dem alle Java-Dateien zusammengefasst sind, die zu dem Projekt gehören.
                                    // Der Ordnername für das Projekt muss am Anfang immer mindestens einen Buchstaben stehen haben.
                                    // Der package Eintrag entfällt vollständig, wenn man z.B. nur eine Java-Datei hat und diese auf dem Desktop compilern will.

//******************************************************************************

// Import der benötigten Klassen

        import java.util.Scanner;       // Die Funktionalitäten für die Eingabe von Zeichen und Zeichenketten über die Kommandozeile wird importiert.

//******************************************************************************

// Das Kommando public class

        public class SH_06112020_01 {       //Der Name der Klasse, public steht für öffentlich.

//******************************************************************************

// Das Kommando public static void main(String[] args) {

        public static void main(String[] args) {                // Der Beginn der Mainmethode und der Anfang des eigentlichen Codeblocks.

//******************************************************************************

// Der Header zusammengefasst

        // Hier noch einmal der vollständige Body der *.java Datei als Mainmethode, ohne die oben beschriebenen Kommentarbereiche.
        // Jede Zeile ist ausführbarer Code, ist aber an dieser Stelle mit /* */ auskommentiert, damit dieses Script trotzdem noch fehlerfrei laufen kann.

        /*
        package sh_06112020_01;                             // Der Name des Ordners, in dem die Java-Dateien liegen. Kleinschreibung beachten!
        import java.util.Scanner;                           // Hier (wenn nötig) Import zusätzlicher Klassen nach dem Schema: import java.util.Scanner;
        public class SH_06112020_01 {                       // Der Name der Klasse (Dateiname). Groß- und Kleinschreibung wie beim Datei selbst, aber ohne .java.
            public static void main(String[] args) {        // Der Beginn der Mainmethode und danach folgt der eigentliche Codeblock.
                // Hier den eigenen Code eintragen          // Der eigentliche Programmcode.
            }                                               // Abschließende geschweifte Klammer für die Mainmethode.
        }                                                   // Abschließende geschweifte Klammer für die Klasse.
        */

//******************************************************************************

// Variablen

        // Variablen dürfen keine Namen bekommen, die von Java selbst als Schlüsselwörter reserviert sind. Eine Liste findet sich unter: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
        // Variablen dürfen nicht mit einer Ziffer anfangen.
        // Sonderzeichen Dollar und Unterstrich sind erlaubt, sollten aber nicht als erstes Zeichen verwendet werden. Andere Sonderzeichen sind nicht erlaubt!
        // Variablen müssen deklariert und ggf. initialisiert werden.
        // Die Deklaration und die Initialisierung kann zu Anfang des Programms erfolgen, oder erst wenn die Variable gebraucht wird.
        // Eine Deklaration ist die Kombination aus einem Datentyp und einem Variablenname (z.B.: int meineVariable).
        // Eine Variable darf im gesamten Programm nur einmal deklariert werden.
        // Steht nach dem Namen der Variable ein Gleichheitszeichen und ein Wert, so ist das die Zuweisung eines Wertes (Initialisierung).
        // Nicht initialisierte Variablen können je nach Verwendung zu Problemen im Code führen.

        //Beispiele:

        // Die fehlerhaften Beispiele wurden auskommentiert, sodass das der gesamte Code noch ausführbar ist

        System.out.println("");
        System.out.println("*** Variablen ***");

        int varA01;                                                                                                     // Eine Variable wird deklariert, aber es wird kein Wert zugewiesen.
        //System.out.println("1 Variable ohne Wert " + varA01);                                                         // Achtung, die Variable ist immernoch leer und das kann später zu Fehlern führen, wenn die leere Variable verwendet wird.

        int varA02, varB02, varC02;                                                                                     // Mehrere Variablen werden in einer Zeile deklariert, ohne Werte zuzuweisen. Das geht nur bei gleichem Typ. Hier sind alle Variablen vom Typ Integer.
        //System.out.println("3 Variablen ohne Wert " + varA02 + varB02 + varC02);                                      // Achtung, die Variable ist immernoch leer und das kann später zu Fehlern führen, wenn die leere Variable verwendet wird.

        int varA03 = 0;                                                                                                 // Eine Variable wird deklariert und ein Wert zugewiesen.
        System.out.println("1 Variable mit Wert " + varA03);                                                            // Die Variable enthält nun den Wert 0.

        int varA04 = 1, varB04 = 5;                                                                                     // Mehrere Variablen werden deklariert und unterschiedliche Werte zugewiesen.
        System.out.println("2 Variablen mit Werten " + varA04 + " und " + varB04);                                      // Geht nur bei gleichem Typ. Hier sind alle Variablen vom Typ Integer.

        int varA05, varB05 = 3;                                                                                         // Eine Mischung aus der zugewiesenen und nicht zugewiesenen Variante ist möglich.
        //System.out.println("1 Variable ohne Wert und 1 Variable mit Wert " + varA05 + varB05);                        // Achtung, die Variable ist immernoch leer und das kann später zu Fehlern führen, wenn die leere Variable verwendet wird.

        int varA06;                                                                                                     // Eine Variable wird als Integer deklariert.
        varA06 = 1 + 1;                                                                                                 // Und später wird damit gerechnet.
        System.out.println("1 Variable ohne Wert, aber nach der Zuweisung durch Berechnung mit Wert " + varA06);

        int varA07 = 2 + 2;                                                                                             // Die Variable wird erst deklariert, wenn sie gebraucht wird (direkt vor der eigentlichen Berechnung).
        System.out.println("1 Variable mit direkter Zuweisung durch Berechnung " + varA07);

//******************************************************************************

// Java Datentypen

        /*
        boolean	8 Bit	Wahrheitswert kann true oder false sein
        byte	8 Bit	ein Byte für kleine Zahlen
        char	16 Bit	Buchstaben
        short	16 Bit	kleine ganze Zahlen
        int 	32 Bit	ganze Zahlen
        long	64 Bit	große ganze Zahlen
        float	32 Bit	zahl mit Nachkommastellen
        double	64 Bit	große Zahl mit vielen Nachkommastellen
        */

//******************************************************************************

// Logische Operatoren

        // Bei logischen Operatoren geht es immer nur um wahr oder falsch, auch wenn eine mathematischer Operation im Ausdruck enthalten ist.
        // Der Rückgabewert kann nur ein wahr oder falsch sein, was dann entsprechend ausgewertet werden muss.

        /*
        Operator    Name            Beispiel                Funktion
        ------------------------------------------------------------------------
        &           Und             (varA = 1 & varB = 2)   wahr wenn beide Ausdrücke wahr sind
                                                            falsch wenn mindestens ein Ausdruck falsch ist
        |           Oder            (varA = 1 | varB = 2)   wahr wenn mindestens ein Ausdruck wahr ist
                                                            falsch wenn keine Ausdruck wahr ist
        !           Nicht           !(varA = 1 & varB = 2)  Das Ergebnis für den Ausdruck in der Klammer wird zuerst ermittelt
                                                            Der Operator ! bewirkt nun, dass aus wahr falsch wird und aus falsch wahr
        ^           Exklusiv Oder   (varA = 1 ^ varB = 2)   wahr nur dann, wenn beide Ausdrücke unterschiedlich sind
                                                            falsch wenn beide Ausdrücke gleich sind
        &&          Und SCE         (varA = 1 && varB = 2)  funktioniert wie das einzelne &, mit dem Unterschied, dass wenn schon
                                                            die Auswertung des ersten (linken) Ausdrucks ein eindeutiges Ergebnis ergibt,
                                                            die Prüfung des zweiten Ausdrucks nicht mehr durchgeführt wird
                                                            wahr wenn beide Ausdrücke wahr sind
                                                            falsch wenn schon der erste Ausdruck falsch ist
        ||          Oder SCE        (varA = 1 || varB = 2)  funktioniert wie das einzelne |, mit dem Unterschied, dass wenn schon
                                                            die Auswertung des ersten (linken) Ausdrucks ein eindeutiges Ergebnis ergibt,
                                                            die Prüfung des zweiten Ausdrucks nicht mehr durchgeführt wird
                                                            wahr wenn schon der erste Ausdruck wahr ist
                                                            falsch wenn kein Ausdruck wahr ist
        */

        // Beispiele:

        // Die hier verwendete IF-Anweisung dient nur zur Darstellung der Funktionalität der logischen Operatoren und wird hier nicht beschrieben.
        // Die betreffende Beschreibung befindet sich weiter unten.

        System.out.println("");
        System.out.println("*** Logische Operatoren ***");

        // UND
        boolean varA071 = true;
        boolean varB071 = true;
        if (varA071 & varB071) {                                                                    //Beschreibung siehe oben.
            System.out.println("Logischer Operator UND - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator UND - die Bedingung ist falsch.");
        }

        // ODER
        boolean varA072 = true;
        boolean varB072 = true;
        if (varA072 | varB072) {                                                                    //Beschreibung siehe oben.
            System.out.println("Logischer Operator ODER - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator ODER - die Bedingung ist falsch.");
        }

        // NICHT (mit UND)
        boolean varA073 = true;
        boolean varB073 = true;
        if (!(varA073 & varB073)) {                                                                 //Beschreibung siehe oben.
            System.out.println("Logischer Operator NICHT - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator NICHT - die Bedingung ist falsch.");
        }

        // EXCLUSIVE ODER
        boolean varA074 = true;
        boolean varB074 = true;
        if (varA074 ^ varB074) {                                                                    //Beschreibung siehe oben.
            System.out.println("Logischer Operator EXCLUSIVE ODER - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator EXCLUSIVE ODER - die Bedingung ist falsch.");
        }

        // UND SCE
        boolean varA075 = true;
        boolean varB075 = true;
        if (varA075 && varB075) {                                                                   //Beschreibung siehe oben.
            System.out.println("Logischer Operator UND SCE - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator UND SCE - die Bedingung ist falsch.");
        }

        // ODER SCE
        boolean varA076 = true;
        boolean varB076 = true;
        if (varA076 || varB076) {                                                                   //Beschreibung siehe oben.
            System.out.println("Logischer Operator ODER SCE - die Bedingung ist wahr.");
        } else {
            System.out.println("Logischer Operator ODER SCE - die Bedingung ist falsch.");
        }

//******************************************************************************

//IF-Anweisungen

        /*
        IF-Anweisungen sind Verzweigungen im Programm.
        Auslöser für die Verzweigung ist eine Bedingung.
        Das Ergebnis der Prüfung dieser Bedingung kann wahr oder falsch sein.
        Bei der "einfachen" IF-Anweisung wird der Code in den geschweiften Klammern ausgeführt,
        wenn die Prüfung der Bedingung wahr ergibt.
        Ergibt die Prüfung der Bedingung falsch, wird kein Code ausgeführt
        und der Teil in den geschweiften Klammern unbeachtet übergangen.
        Eine Möglichkeit Code auszuführen, wenn die Bedingung falsch ist,
        gibt es bei dieser Art der IF-Anweisung nicht.

        Syntax:

        if (Bedingung in runden Klammern) {
            //Code Block ausführen wenn Bedingung wahr ist;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** IF-Anweisung ***");

        int varA08 = 1;                                             // Variable wird deklariert und auf 1 gesetzt.
        if (varA08 == 1) {                                          // IF Beginn, Bedingung wird geprüft, Variable ist 1 und die Bedingung damit wahr.
            System.out.println("varA08 ist gleich " + varA08);      // Ausgabe wird ausgeführt, da Bedingung wahr ist.
        }                                                           // IF Ende.

        int varB08 = 2;                                             // Variable wird deklariert und auf 2 gesetzt.
        if (varB08 == 1) {                                          // IF Beginn, Bedingung wird geprüft, Variable ist nicht 1 und die Bedingung damit falsch.
            System.out.println("varB08 ist gleich " + varB08);      // Ausgabe wird nicht ausgeführt, da Bedingung falsch ist ist.
        }                                                           // IF Ende.

//******************************************************************************

//IF-ELSE-Anweisungen

        /*
        IF-ELSE- Anweisungen sind Verzweigungen im Programm.
        Auslöser für die Verzweigung ist eine Bedingung.
        Das Ergebnis der Prüfung dieser Bedingung kann wahr oder falsch sein.
        Bei der IF-ELSE-Anweisung wird der Code in den geschweiften Klammern
        hinter der IF-Anweisung ausgeführt, wenn die Prüfung der Bedingung wahr ergibt.
        Der Code in den geschweiften Klammern hinter der ELSE-Anweisung wird ausgeführt,
        wenn die Prüfung der Bedingung falsch ergibt.

        Syntax:

        if (Bedingung in runden Klammern) {
            //Code Block ausführen wenn Bedingung wahr ist;
        } else {
            //Code Block ausführen wenn Bedingung falsch ist;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** IF-ELSE-Anweisung ***");

        int varA09 = 1;                                                                                 // Variable wird deklariert und auf 1 gesetzt.
        //int varA09 = 2;                                                                               // Variable wird deklariert und auf 2 gesetzt.
        if (varA09 == 1) {                                                                              // IF Beginn, Bedingung wird geprüft, wenn Variable = 1 dann wahr, wenn nicht dann falsch.
            System.out.println("Die Bedingung ergibt wahr. Die Variable varA09 ist: " + varA09);        // Ausgabe wird ausgeführt, wenn Bedingung wahr ist.
        } else {                                                                                        // IF Ende und ELSE Beginn.
            System.out.println("Die Bedingung ergibt falsch. Die Variable varA09 ist: " + varA09);      // Ausgabe wird ausgeführt, wenn Bedingung falsch ist.
        }                                                                                               // ELSE Ende und gleichzeitig entgültiges Ende der IF-ELSE-Anweisung.

//******************************************************************************

// Geschachtelte IF-Anweisungen

        /*
        Schachteln bedeutet in dem Fall, dass in einer IF-Anweisung eine weitere IF-Anweisung
        steckt. Beide haben dabei in der Regel unterschiedliche Aufgaben, aber die innere
        IF-Anweisung ist abhängig von der äußeren, da sie nur ausgeführt wird, wenn die Bedingung
        der äußeren IF-Anweisung dazuführt, das zur Inneren verzweigt wird.
        Eine Abhängigkeit der äußeren IF-Anweisung von der inneren besteht nicht.

        Für die Funktionalität innerhalb der jeweiligen IF-Anweisung gelten die gleichen Regeln,
        wie für alleinstehende IF-Anweisungen.

        Syntax:

        if (1. Bedingung) {
            //Code Block  ausführen wenn Bedingung 1 wahr ist;
            if (2. Bedingung) {
                //Code Block ausführen wenn Bedingung 1 und 2 wahr sind;
            }
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** Geschachtelte IF-Anweisungen ***");

        int varA10 = 1;                                                                                                 // Die Variablen werden deklariert und gesetzt.
        //int varA10 = 2;
        int varB10 = 1;
        //int varB10 = 2;
        if (varA10 == 1) {                                                                                              // Die Bedingung der ersten IF-Anweisung wird geprüft.
            System.out.println("Der erste Ausdruck ist wahr, die äußere IF-Bedingung wird ausgeführt.");                // Ergibt die Bedingung wahr, wird dieser Codeblock ausgeführt. In diesem Codeblock ist ebenso die 2. IF-Anweisung enthalten.
            if (varB10 == 1) {                                                                                          // Die Bedingung der zweiten IF-Anweisung wird geprüft.
                System.out.println("    Der zweite Ausdruck ist wahr, die innere IF-Bedingung wird ausgeführt.");       // Ergibt die Bedingung wahr, wird dieser Codeblock ausgeführt.
            }                                                                                                           // Ende der inneren IF-Anweisung.
        }                                                                                                               // Ende der äußeren IF-Anweisung.

        /*
        Mit der Schachtelung kann man ebenso bei der IF-ELSE-Anweisung verfahren.
        Auch in den ELSE-Codeblock können eine oder mehrere IF-Anweisungen
        (oder IF-ELSE-Anweisungen) hineingeschachtelt werden. Jedoch kann in einen
        IF oder ELSE Block keine einzelner ELSE-Block geschachtelt werden.
        Zu ELSE gehört immer das IF darüber.
        */

//******************************************************************************

// IF-Anweisung mit ELSE-IF

        /*
        Eine IF-Anweisung mit ELSE-IF verhält sich im Grunde ebenso wie eine geschachtelte
        IF-ELSE-Anweisung, bei der die geschachtelte IF-Anweisung im ELSE-Codeblock steht.
        Der Unterschied besteht in der Schreibweise. Dabei wird die folgende
        IF-Bedingung nicht in den Codeblock zwischen die geschweiften Kalmmern geschrieben,
        sondern direkt hinter das ELSE, noch vor der geöffneten, geschwungenen Klammer.

        Es bestehen die selben Regeln für die Ausführung, wie sie für die IF-ELSE-Anweisung gelten.

        Syntax:

        if (Bedingung 1 in runden Klammern) {
            //Code Block ausführen wenn Bedingung 1 wahr ist;
        } else if (Bedingung 2 in runden Klammern){
            //Code Block ausführen wenn Bedingung 1 falsch und Bedingung 2 wahr ist;
        } else if (Bedingung 3 in runden Klammern){
            //Code Block ausführen wenn Bedingung 1 und 2 falsch und Bedingung 3 wahr ist;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** IF-Anweisung mit ELSE-IF ***");

        int varA090 = 1;                                                                                                            // Variable wird deklariert und auf 1 gesetzt.
        //int varA090 = 2;                                                                                                          // Variable wird deklariert und auf 2 gesetzt.
        int varA091 = 1;                                                                                                            // Variable wird deklariert und auf 1 gesetzt.
        //int varA091 = 2;                                                                                                          // Variable wird deklariert und auf 2 gesetzt.
        int varA092 = 1;                                                                                                            // Variable wird deklariert und auf 1 gesetzt.
        //int varA092 = 2;                                                                                                          // Variable wird deklariert und auf 2 gesetzt.
        if (varA090 == 1) {                                                                                                         // IF 1 Beginn, 1. Bedingung wird geprüft, wenn wahr wird der Codeblock ausgeführt, wenn falsch wird weitergegangen zu ELSE 1.
            System.out.println("Die Bedingung 1 ergibt wahr. Die Variable varA090 ist: " + varA090);                                // Ausgabe wird ausgeführt, wenn 1. Bedingung wahr ist.
        } else if (varA091 == 1) {                                                                                                  // IF 1 Ende und ELSE-IF 1 Beginn, ELSE wird nur ausgeführt, wenn Bedingung 1 Falsch ist, dann wird die 2. IF-Bedingung geprüft.
            System.out.println("Die Bedingung 1 ist falsch, Bedingung 2 ist wahr. Die Variable varA091 ist: " + varA091);           // Ausgabe wird ausgeführt, wenn 2. Bedingung wahr ist.
        } else if (varA092 == 1) {                                                                                                  // IF 2 Ende und ELSE-IF 2 Beginn, ELSE wird nur ausgeführt, wenn Bedingung 1 und 2 falsch sind, dann wird die 3. IF-Bedingung geprüft.
           System.out.println("Die Bedingung 1 und 2 sind falsch, Bedingung 3 ist wahr. Die Variable varA091 ist: " + varA091);     // Ausgabe wird ausgeführt, wenn 3. Bedingung wahr ist.
        }                                                                                                                           // Ende der IF-Anweisung mit ELSE-IF.

//******************************************************************************

// FOR-Schleife

        /*
        Mit der FOR-Schleife wird ein Code-Block solang immer wieder ausgeführt,
        bis der mit Startwert, Zielwert und Schrittweite definierte Vorgang
        vollstänndig abgelaufen ist. Da diese drei Werte vorgegeben werden müssen,
        kann eine FOR-Schleife nur dann verwendet werden, wenn diese Werte bekannt
        sind, bzw. spätestens zur Laufzeit mittels Variablen definiert werden können

        Syntax:

        for (Startwert; Zielwert; Schrittweite) {
            // Code Block ausführen solange Zielwert nicht erreicht ist;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** FOR-Schleife ***");

        int varA11;                                                                             // Die Schleifenvariable wird deklariert, kann aber auch direkt in der Schleife mit (int varA11 = 0; ...) erfolgen.
        for (varA11 = 0; varA11 < 3; varA11 = varA11 + 1) {                                     // FOR Beginn, varA11 = 0 startet bei 0, varA11 < 3 durchläuft Schleife solange wie varA11 kleiner 3 ist. Mit varA11 = varA11 + 1 wird bei jedem Schleifendurchlauf der Schleifenzähler um 1 erhöht.
            System.out.println("Der Zähler der FOR-Schleife (varA11): " + varA11);      // Der Codeblock wird solange wiederholt, bis die Bedingung varA11 < 3 nicht mehr erfüllt ist.
        }                                                                                       // FOR Ende.

//******************************************************************************

// Geschachtelte FOR-Schleife

        /*
        FOR-Schleifen können ebenso geschachtelt werden. Dabei wird als Erstes
        die erste Schleife der äußeren FOR-Schleife begonne und der Code im
        entsprechenden Code-Block ausgeführt. Da die innere FOR-Schleife in
        diesem Block steht, wird im nächsten Schritt die innere FOR-Schleife
        ausgeführt und zwar so lange, bis alle einzelnen Schleifen
        dieser inneren FOR-Schleife durchlaufen wurden. Danach läuft die erste
        Schleife der außeren FOR-Schleife weiter. Ist deren Code-Block vollständig
        abgearbeitet, beginnt die äußere FOR-Schleife mit der zweiten Schleife.
        Diese übergibt wiederum an die innere Schleife, welche dann wieder vollständig
        durchläuft und am Ende wieder an die äußere Schleife übergibt. Das geht
        solange weiter, bis die außere Schleife das in den Bedingungen definierte
        Ende erreicht hat. Danach wird auch die äußere Schleife verlassen.

        Für die Funktionalität innerhalb der jeweiligen FOR-Schleife gelten die gleichen Regeln,
        wie für alleinstehende FOR-Schleifen.

        Syntax:

        for (Startwert Schleife 1; Zielwert Schleife 1; Schrittweite Schleife 1) {
            // Code Block Schleife 1 ausführen solange Zielwert Schleife 1 nicht erreicht ist;
            for (Startwert Schleife 2; Zielwert Schleife 2; Schrittweite Schleife 2) {
                // Code Block Schleife 2 ausführen solange Zielwert Schleife 1 nicht erreicht ist;
            }
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** Geschachtelte FOR-Schleife ***");

        int varA12;                                                                                     // Die Schleifenvariable für die äußere Schleife wird deklariert, kann aber auch direkt in der Schleife mit (int varA12 = 0; ...) erfolgen.
        int varB12;                                                                                     // Die Schleifenvariable für die innere Schleife wird deklariert, kann aber auch direkt in der Schleife mit (int varB12 = 0; ...) erfolgen.
        for (varA12 = 0; varA12 < 2; varA12 = varA12 + 1) {                                             // FOR Beginn äußere Schleife, varA12 = 0 startet bei 0, varA12 < 2 durchläuft Schleife solange wie varA12 kleiner 2 ist. Mit varA12 = varA12 + 1 wird bei jedem Schleifendurchlauf der Schleifenzähler um 1 erhöht.
            System.out.println("Der externe Zähler der FOR-Schleife (varA12): " + varA12);              // Der Codeblock wird solange wiederholt, bis die Bedingung varA12 < 2 nicht mehr erfüllt ist.
            for (varB12 = 0; varB12 < 2; varB12 = varB12 + 1) {                                         // FOR Beginn innere Schleife, varB12 = 0 startet bei 0, varB12 < 2 durchläuft Schleife solange wie varB12 kleiner 2 ist. Mit varB12 = varB12 + 1 wird bei jedem Schleifendurchlauf der Schleifenzähler um 1 erhöht.
                System.out.println("    Der interne Zähler der FOR-Schleife (varB12): " + varB12);      // Der Codeblock wird solange wiederholt, bis die Bedingung varB12 < 2 nicht mehr erfüllt ist.
            }                                                                                           // FOR Ende und Übergabe an die äußere FOR-Schleife, wenn bei der inneren alle Schleifendurchläufe abgeschlossen sind.
        }                                                                                               // FOR End, wenn bei der außeren FOR-Schleife alle Schleifendurchläufe abgesachlossen sind.

//******************************************************************************

// WHILE-Schleife

        /*
        Mit der WHILE-Schleife wird ein Code-Block solang immer wieder ausgeführt,
        bis die im Kopf der Schleife definierte Bedingung falsch ist. Deswegen wird
        sie auch kopfgesteuerte Schleife genannt. Diese Besonderheit bewirkt aber
        auch, dass wenn die Bedingung schon vor dem ersten Schleifendurchlauf falsch
        ist, die WHILE-Schleife nicht ausgeführt, sondern übersprungen wird.

        Syntax:

        while(Bedingung mit einem Wert oder Schleifenzähler) {
            // Code-Block ausführen solange Bedingung wahr;
            // ggf. Schleifenzähler weiterzählen;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** WHILE-Schleife ***");

        int varA13 = 1;                                                             // Die Schleifenvariable wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varA13 = 4;                                                           // Die Schleifenvariable wird deklariert, Wert ist gleich der Bedingung, Schleife wird nicht ausgeführt.
        while (varA13 < 4) {                                                        // WHILE Begin und Bedingung der Schleife, wenn Bedingung wahr ergibt wird Schleife ausgeführt, wenn Bedingung falsch ergibt wird Schleife nicht ausgeführt und übersprungen.
            System.out.println("WHILE-Schleife Schleifendurchlauf " + varA13 );     // Ausführen des Codeblocks wenn Bedingung wahr ist.
            varA13 = varA13 + 1;                                                    // Innerhalb der Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
        }                                                                           // WHILE Ende, von hier wird immer zum Anfang der Schleife gesprungen und dann die Bedingung neu geprüft.

//******************************************************************************

// Geschachtelte WHILE-Schleife

        /*
        WHILE-Schleifen können ebenso geschachtelt werden. Dabei wird als Erstes
        die erste Schleife der äußeren WHILE-Schleife begonne und der Code im
        entsprechenden Code-Block ausgeführt. Da die Innere WHILE-Schleife in
        diesem Block steht, wird im nächsten Schritt die innere WHILE-Schleife
        ausgeführt und zwar so lange, bis alle einzelnen Schleifen
        dieser inneren WHILE-Schleife durchlaufen wurden. Danach läuft die erste
        Schleife der außeren WHILE-Schleife weiter. Ist deren Code-Block vollständig
        abgearbeitet, beginnt die äußere WHILE-Schleife mit der zweiten Schleife.
        Diese übergibt widerum an die innere Schleife, welche dann wieder vollständig
        durchläuft und am Ende wieder an die äußere Schleife übergibt. Das geht
        solange weiter, bis die außere Schleife das in den Bedingungen definierte
        Ende erreicht hat. Danach wird auch die äußere Schleife verlassen.

        Für die Funktionalität innerhalb der jeweiligen WHILE-Schleife gelten die gleichen Regeln,
        wie für alleinstehende WHILE-Schleifen.

        Syntax:

        while(Bedingung Schleife 1 mit einem Wert oder Schleifenzähler) {
            // Code-Block Schleife 1 ausführen solange Bedingung Schleife 1 wahr;
            while(Bedingung Schleife 2 mit einem Wert oder Schleifenzähler) {
                // Code-Block Schleife 2 ausführen solange Bedingung Schleife 2 wahr;
                // ggf. Schleifenzähler Schleife 2 weiterzählen;
            }
            // ggf. Schleifenzähler Schleife 1 weiterzählen;
            // Schleifenzähler Schleife 2 auf Anfangswert setzen;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** Geschachtelte WHILE-Schleife ***");

        int varA14 = 1;                                                                         // Die Schleifenvariable äußere Schleife wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varA14 = 3;                                                                       // Die Schleifenvariable äußere Schleife wird deklariert, Wert ist größer als Bedingung, Schleife wird nicht ausgeführt.
        int varB14 = 1;                                                                         // Die Schleifenvariable innere Schleife wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varB14 = 3;                                                                       // Die Schleifenvariable innere Schleife wird deklariert, Wert ist größer als Bedingung, Schleife wird nicht ausgeführt.
        while (varA14 < 3) {                                                                    // WHILE Begin und Bedingung der äußeren Schleife, wenn Bedingung wahr ergibt wird Schleife ausgeführt, wenn Bedingung falsch ergibt wird Schleife nicht ausgeführt und übersprungen.
            System.out.println("1. WHILE-Schleife Schleifendurchlauf " + varA14);               // Ausführen des Codeblocks der äußeren Schleife wenn Bedingung wahr ist.
            while (varB14 < 3) {                                                                // WHILE Begin und Bedingung der inneren Schleife, wenn Bedingung wahr ergibt wird Schleife ausgeführt, wenn Bedingung falsch ergibt wird Schleife nicht ausgeführt und übersprungen.
                System.out.println("    2. WHILE-Schleife Schleifendurchlauf " + varB14);       // Ausführen des Codeblocks der inneren Schleife wenn Bedingung wahr ist.
                varB14 = varB14 + 1;                                                            // Innerhalb der inneren Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
            }                                                                                   // WHILE Ende innere Schleife, von hier wird immer zum Anfang der Schleife gesprungen und dann die Bedingung neu geprüft.
            varA14 = varA14 + 1;                                                                // Innerhalb der äußeren Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
            varB14 = 1;                                                                         // Innerhalb der äußeren Schleife muss der Schleifenzähler der inneren Schleife zurück auf den Anfangswert gesetzt werden, in diesem Fall 1.
        }                                                                                       // WHILE Ende äußere Schleife, von hier wird immer zum Anfang der Schleife gesprungen und dann die Bedingung neu geprüft.

//******************************************************************************

// DO-WHILE-Schleife

        /*
        Mit der DO-WHILE-Schleife wird ein Code-Block solang immer wieder ausgeführt,
        bis die im Fuß der Schleife definierte Bedingung falsch ist. Deswegen wird
        sie auch fußgesteuerte Schleife genannt. Die Besonderheit ist, dass die Schleife
        trotzdem einmal ausgeführt wird, auch wenn die Bedingung schon vor dem Start
        auf einen Wert gesetzt wird, der als Ergebnis der Bedingung falsch zurück gibt.
        Dies passiert, da die Bedingung erst am Ende der Schleife ausgewertet wird.

        Syntax:

        do {
            // Code-Block ausführen solange Bedingung wahr;
            // ggf. Schleifenzähler weiterzählen;
        } while (Bedingung mit einem Wert oder Schleifenzähler);

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** DO-WHILE-Schleife ***");

        int varA15 = 1;                                                                             // Die Schleifenvariable wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varA15 = 4;                                                                           // Die Schleifenvariable wird deklariert, Wert ist gleich der Bedingung, Schleife wird nicht ausgeführt.
        do {                                                                                        // DO-WHILE Beginn, der folgende Code Block wird im ersten Schleifendurchlauf in jedem Fall ausgeführt.
            System.out.println("DO-WHILE-Schleife Wert der Schleifenzählervariable " + varA15);     // Ausführen des Codeblocks solange Prüfung am Ende wahr ist.
            varA15 = varA15 + 1;                                                                    // Innerhalb der Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
        } while (varA15 < 4);                                                                       // DO-WHILE Ende, es wird nur zum Anfang der Schleife gesprungen wenn die Bedingung wahr ist, anderenfalls wird die Schleife beendet.

//******************************************************************************

// Geschachtelte DO-WHILE-Schleife

        /*
        DO-WHILE-Schleifen können ebenso geschachtelt werden. Dabei wird als Erstes
        die erste Schleife der äußeren DO-WHILE-Schleife begonnen und der Code im
        entsprechenden Code-Block ausgeführt. Da die Innere DO-WHILE-Schleife in
        diesem Block steht, wird im nächsten Schritt die innere DO-WHILE-Schleife
        ausgeführt und zwar so lange, bis alle einzelnen Schleifen
        dieser inneren DO-WHILE-Schleife durchlaufen wurden. Danach läuft die erste
        Schleife der außeren DO-WHILE-Schleife weiter. Ist deren Code-Block vollständig
        abgearbeitet, beginnt die äußere DO-WHILE-Schleife mit der zweiten Schleife.
        Diese übergibt widerum an die innere Schleife, welche dann wieder vollständig
        durchläuft und am Ende wieder an die äußere Schleife übergibt. Das geht
        solange weiter, bis die außere Schleife das in den Bedingungen definierte
        Ende erreicht hat. Danach wird auch die äußere Schleife verlassen.

        Für die Funktionalität innerhalb der jeweiligen DO-WHILE-Schleife gelten die gleichen Regeln,
        wie für alleinstehende DO-WHILE-Schleifen.

        Syntax:

        do {
            // Code-Block Schleife 1 ausführen solange Bedingung wahr;
            do {
                // Code-Block Schleife 2 ausführen solange Bedingung wahr;
                // ggf. Schleifenzähler 2 weiterzählen;
            } while (Bedingung Schleife 2 mit einem Wert oder Schleifenzähler);
            // ggf. Schleifenzähler 1 weiterzählen;
            // Schleifenzähler Schleife 2 auf Anfangswert setzen;
        } while (Bedingung Schleife 1 mit einem Wert oder Schleifenzähler);

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** Geschachtelte DO-WHILE-Schleife ***");

        int varA16 = 1;                                                                                         // Die Schleifenvariable der äußeren Schleife wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varA16 = 3;                                                                                       // Die Schleifenvariable der äußeren Schleife wird deklariert, Wert ist gleich der Bedingung, Schleife wird nicht ausgeführt.
        int varB16 = 1;                                                                                         // Die Schleifenvariable der inneren Schleife wird deklariert, Wert ist kleiner als Bedingung, Schleife wird ausgeführt.
        //int varB16 = 3;                                                                                       // Die Schleifenvariable der inneren Schleife wird deklariert, Wert ist gleich der Bedingung, Schleife wird nicht ausgeführt.
        do {                                                                                                    // DO-WHILE Beginn äußere Schleife, der folgende Code Block wird im ersten Schleifendurchlauf in jedem Fall ausgeführt.
            System.out.println("1. DO-WHILE-Schleife Wert der Schleifenzählervariable " + varA16);              // Ausführen des Codeblocks solange Prüfung am Ende der äußeren Schleife wahr ist.
            do {                                                                                                // DO-WHILE Beginn, der folgende Code Block wird im ersten Schleifendurchlauf in jedem Fall ausgeführt.
                System.out.println("    2. DO-WHILE-Schleife Wert der Schleifenzählervariable " + varB16);      // Ausführen des Codeblocks solange Prüfung am Ende des inneren Codeblocks wahr ist.
                varB16 = varB16 + 1;                                                                            // Innerhalb der inneren Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
            } while (varB16 < 3);                                                                               // DO-WHILE Ende der inneren Schleife, es wird nur zum Anfang der inneren Schleife gesprungen wenn die Bedingung wahr ist, anderenfalls wird die Schleife beendet.
            varA16 = varA16 + 1;                                                                                // Innerhalb der äußeren Schleife muss der Schleifenzähler bei jedem Durchlauf entsprechend der Anforderung neu berechnet werden, in diesem Fall Erhöhung um 1.
            varB16 = 1;                                                                                         // Innerhalb der äußeren Schleife muss der Schleifenzähler der inneren Schleife zurück auf den Anfangswert gesetzt werden, in diesem Fall 1.
        } while (varA16 < 3);                                                                                   // DO-WHILE Ende der äußeren Schleife, es wird nur zum Anfang der äußeren Schleife gesprungen wenn die Bedingung wahr ist, anderenfalls wird die Schleife beendet.

//******************************************************************************

// SWITCH-Anweisung

        /*
        Die SWITCH-Anweisung dient dazu, mit relativ wenig Aufwand Werte auszuwerten
        und abhängig davon unterschiedlichen Code auszuführen. Dazu wird der SWITCH-Anweisung
        ein Wert übergeben, z.B. eine Benutzereingabe. Dieser Wert wird anschließend
        in den einzelnen case Bedingungen auf Übereinstimmung geprüft. Wenn die Bedingung
        wahr ist, wird der zugehörige Codeblock ausgeführt. Am Ende dieses Codeblocks
        wird die SWITCH-Anweisung mit der break Anweisung verlassen. Wird in keinem
        der case eine Übereinstimmung gefunden, wird der Codeblock unter default
        ausgeführt.

        Zu beachten ist, das die SWITCH-Anweisung nur mit den Datentypen char, short,
        int und String arbeiten kann. Ebenso sind Vergleichsoperationen nicht möglich.

        Syntax:

        switch(ausdruck) {
            case Bedingung 1:
                // Code-Block für Bedingung 1;
                break;
            case Bedingung 2:
                // Code-Block für Bedingung 2;
                break;
            case Bedingung 3:
                // Code-Block für Bedingung 3;
                break;
            default:
                // Code-Block für Default;
        }

        */

        //Beispiele:

        System.out.println("");
        System.out.println("*** SWITCH-Anweisung ***");

        int varA17 = 1;                                                 // Die Variable wird deklariert, der Wert 1 löst den case 1 aus.
        //int varA17 = 2;                                               // Die Variable wird deklariert, der Wert 2 löst den case 2 aus.
        //int varA17 = 3;                                               // Die Variable wird deklariert, der Wert 3 löst den case 3 aus.
        //int varA17 = 4;                                               // Die Variable wird deklariert, der Wert 4 löst default aus, da es keinen case für den Wert 4 gibt.
        switch(varA17) {                                                // Der SWITCH-Anweisung wird der Ausdruck übergeben.
            case 1:                                                     // Der erste Case prüft, ob der übergebene Wert eine 1 ist.
                System.out.println("SWITCH-Anweisung case 1");          // Hier steht der auszuführende Code für case 1.
                break;                                                  // Wenn case 1 zutreffend war, wird die SWITCH-Anweisung hier verlassen.
            case 2:                                                     // Der zweite Case prüft, ob der übergebene Wert eine 2 ist.
                System.out.println("SWITCH-Anweisung case 2");          // Hier steht der auszuführende Code für case 2.
                break;                                                  // Wenn case 2 zutreffend war, wird die SWITCH-Anweisung hier verlassen.
            case 3:                                                     // Der dritte Case prüft, ob der übergebene Wert eine 3 ist.
                System.out.println("SWITCH-Anweisung case 3");          // Hier steht der auszuführende Code für case 3.
                break;                                                  // Wenn case 3 zutreffend war, wird die SWITCH-Anweisung hier verlassen.
            default:                                                    // default prüft nichts, dient aber dazu Code ausführen zu können, wenn kein case zutreffend war.
                System.out.println("SWITCH-Anweisung default");         // Hier steht der auszuführende Code für default.
        }                                                               // Ende der SWITCH-Anweisung.

//******************************************************************************

//Fuß des Scripts

    }       // Geschweifte Klammer, in dem Fall das Ende der Mainmethode.
}           // Geschweifte Klammer, in dem Fall das Ende Klasse.

/*
Zusammengehörende Klammern findet man immer heraus,
indem man auf die Klammer klickt und schaut,
welche andere Klammer mit markiert wurde.
*/
