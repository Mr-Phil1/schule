/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scheresteinpapier;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Rene Deutschmann
 */
public class SchereSteinPapier {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Schere, Stein oder Papier?");
        Scanner scan = new Scanner(System.in);
        String spieler = scan.next();

        String[] texts = {"Schere", "Stein", "Papier"};
        String computer = (texts[new Random().nextInt(texts.length)]);
        System.out.println(computer);

        if (spieler.equals("Schere") && computer.equals("Schere")) {

            System.out.println("unentschieden");

        }
        if (spieler.equals("Schere") && computer.equals("Stein")) {

            System.out.println("Computer hat gewonnen");

        }
        if (spieler.equals("Schere") && computer.equals("Papier")) {

            System.out.println("Du hast gewonnen");

        }
        if (spieler.equals("Stein") && computer.equals("Stein")) {

            System.out.println("unentschieden");

        }
        if (spieler.equals("Stein") && computer.equals("Schere")) {

            System.out.println("Du hast gewonnen");

        }
        if (spieler.equals("Stein") && computer.equals("Papier")) {

            System.out.println("Computer hat gewonnen");

        }
        if (spieler.equals("Papier") && computer.equals("Papier")) {

            System.out.println("unentschieden");

        }
        if (spieler.equals("Papier") && computer.equals("Stein")) {

            System.out.println("Du hast gewonnen");

        }
        if (spieler.equals("Papier") && computer.equals("Schere")) {

            System.out.println("Computer hat gewonnen");

        }
    }
}
