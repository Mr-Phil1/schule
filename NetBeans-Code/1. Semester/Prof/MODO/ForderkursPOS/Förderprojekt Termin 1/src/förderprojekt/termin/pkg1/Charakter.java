
package förderprojekt.termin.pkg1;


public class Charakter {
    
    private String name;
    private int staerke;
    private int gesundheit;
    private int glueck;
    
    
    
    
    
    // Ich kann eine Methode OHNE Rückgabewert und OHNE Parameter definieren.
    public void methode1() {
        System.out.println("Methode 1: Hallo");
    }
    
    // Ich kann eine Methode MIT einem oder mehreren Parametern definieren.
    // Klassische Setter-Methode
    public void methode2(int zahl, double zahl2) {
        System.out.println("Methode 2: Die Summe ist = " + (zahl + zahl2));
    }
    
    // Ich kann eine Methode MIT Rückgabewert definieren.
    public String methode3(int zahl, double zahl2) {
        String s = "Methode 3: Die Summe ist = " + (zahl + zahl2);
        return s;
    }
    
    // Klassische Getter-Methoden
    public String getName() {
        return this.name;
    }
    
    // Klassische Setter-Methoden
    public void setName(String name) {
        this.name = name;
    }
    
    // Klassische Getter-Methoden
    public int getStaerke() {
        return this.staerke;
    }
    
    // Klassische Setter-Methoden
    public void setStaerke(int staerke) {
        this.staerke = staerke;
    }
}
