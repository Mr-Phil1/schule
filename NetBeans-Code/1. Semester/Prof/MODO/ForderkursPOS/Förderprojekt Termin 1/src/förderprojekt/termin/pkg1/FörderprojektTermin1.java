package förderprojekt.termin.pkg1;

import java.util.Scanner;

public class FörderprojektTermin1 {

    public static void main(String[] args) {
        
        long zahl1 = 9_000_000_000L / 4;   // ganzzahlige Division
        int zahl2 = (int) (9 / 4.0); // fließkomma-Division
        int zahl3 = (int) zahl1 / 4;
        System.out.println("zahl1 = " + zahl1 + " zahl2 = " + zahl2 + " zahl3 = " + zahl3);
        
        
        // zahl1 = 2.0  zahl2 = 2.25 zahl3 = 0.5
        // zahl1 = 2    zahl2 = 2    zahl3 = 0.0
        
        
        // int und double parsen
        String str1 = "100";
        int i = Integer.parseInt(str1);
        System.out.println("i = " + i / 2);
        
        String str2 = "2.5";
        double d = Double.parseDouble(str2);
        System.out.println("d = " + d * 3);
        
         // Wrapper-Klasse
         System.out.println(Boolean.parseBoolean("true"));
        
         
         // Unterschiede zwischen Methodenaufrufe über Objekte und Klassen
         Scanner scan = new Scanner(System.in);
         //int neueZahl = scan.nextInt();
         
         
         System.out.println(Math.floor(3.5));
         System.out.println("e = " + Math.E + " pi = " + Math.PI);
        
         FörderprojektTermin1.halloDu();
         
         FörderprojektTermin1 objekt = new FörderprojektTermin1();
         objekt.halloAnderer();
         
         
         
         
         Charakter c = new Charakter();
         c.methode1();          // kein Rückgabewert, kein Parameter
         c.methode2(600, 0.5);  // kein Rückgabewert, Parameter
         String s = c.methode3(300, 0.5);  // Rückgabewert, Parameter
         System.out.println("Rückgabewert = \"" + s + "\"");
         
         c.setName("Claudio"); // Setter-Methode
         c.setStaerke(80);
         System.out.println("Der Charakter heißt " + c.getName() + " und hat Staerke " + c.getStaerke()); // Getter-Methode
         
         Charakter c2 = new Charakter();
         c2.setName("Dominik"); // Setter-Methode
         c2.setStaerke(81);
         System.out.println("Der Charakter heißt " + c2.getName() + " und hat Staerke " + c2.getStaerke()); // Getter-Methode
         
         
         String substring = s.substring(5, 7);
         System.out.println("Substring: " + substring);
    }
    
    public static void halloDu() {
        System.out.println("Hallo Du!");
    }
    
    public void halloAnderer() {
        System.out.println("Hallo Anderer!");
    }
    
    
    
}


