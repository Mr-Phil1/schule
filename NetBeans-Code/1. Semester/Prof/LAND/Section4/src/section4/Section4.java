/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package section4;

import java.util.Random;

/**
 *
 * @author landerer
 */
public class Section4 {


    public static void main(String[] args) {
        int erg = addiere(2,3);
        System.out.println(erg);
        ergebnisAusgeben(erg,"Hallo, das ist der super Text!");
        System.out.println("Zufallszahl ist: " + gibZufallszahl(5));
        System.out.println(9%5);
        System.out.println("Parkschaden: " + parkschadenPassiert(20));
        String s ="ASDFASDF";
        System.out.println(s.toLowerCase());
    }
    
    public static int gibZufallszahl(int oG)
    {
        Random r = new Random();
        int zufall = r.nextInt(oG);
        return zufall;
    }
    
    public static boolean parkschadenPassiert(int wahrscheinlichkeit)
    {
        Random r = new Random();
        if(r.nextInt(101)<=wahrscheinlichkeit)
        {
            return true;
        } else
        {
            return false;
        }
    }
   
    public static int addiere(int a, int b)
    {
        int erg = a + b;
        return erg;
    }
    
    public static void ergebnisAusgeben(int erg, String text)
    {
        System.out.println("----" + text + "-----" + erg);
    }
}
