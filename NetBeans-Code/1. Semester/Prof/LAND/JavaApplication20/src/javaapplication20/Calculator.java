/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaapplication20;

/**
 *
 * @author landerer
 */
public class Calculator {
    
    // Datenfelder / Properties
    double ergebnis;
    
    // Methoden
    public double summe(double x, double y)
    {
        this.ergebnis = x + y;
        return this.ergebnis;
    }
    
    public double getErgebnis()
    {
        return this.ergebnis;
    }
    
    public String getSumme(String a, String b)
    {
        return a + b;
    }
}
