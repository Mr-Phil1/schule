/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication20;

/**
 *
 * @author landerer
 */
public class JavaApplication20 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calculator c = new Calculator();
        Calculator c2 = new Calculator();
        
        double erg = c.summe(112, 122);
        System.out.println("Ergebnis: " + erg);
        
        System.out.println("c2: " + c2.getSumme("A", "B"));
        
        System.out.println(c.getErgebnis());
        
    }
    
}
