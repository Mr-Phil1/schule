package lagerlisten;

import java.util.Collections;


public class LagerListen {


    public static void main(String[] args) {

        Artikel a1 = new Artikel("1","WinXP",199);
        a1.setLagermenge(100);
        Artikel a2 = new Artikel("2","Win2000",939);
        a2.setLagermenge(200);
        Artikel a3 = new Artikel("3","MacOXS",99);
        a3.setLagermenge(59);
          Artikel a4 = new Artikel("3","AacOXS",9999);
        a3.setLagermenge(59);
        
        Lager lager = new Lager();
        
        lager.artikelHinzufuegen(a2);
        lager.artikelHinzufuegen(a1);
        lager.artikelHinzufuegen(a3);
         lager.artikelHinzufuegen(a4);

        System.out.println(lager.lagerwert());
        lager.alleArtikelAusgeben();
        
        Collections.sort(lager.gibLagerliste());
        
        lager.alleArtikelAusgeben();
        
        Collections.sort(lager.gibLagerliste(),new ArtikelpreisVergleich());
        lager.alleArtikelAusgeben();
        
        Collections.sort(lager.gibLagerliste(), new ArtikelbezeichnungVergleich());
        lager.alleArtikelAusgeben();
        
        lager.loescheAlleArtikelmitBezeichnungWie("Win");
          
        lager.alleArtikelAusgeben();
        
        lager.loescheArtikelId("0");
        
        lager.alleArtikelAusgeben();
        lager.loescheArtikelId("3");

        lager.alleArtikelAusgeben();
        
        

    }
    
}
