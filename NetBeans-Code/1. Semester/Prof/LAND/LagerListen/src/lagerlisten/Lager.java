/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lagerlisten;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author landerer
 */
public class Lager {

    private ArrayList<Artikel> lagerliste;

    public Lager() {
        this.lagerliste = new ArrayList<>();
    }

    public void artikelHinzufuegen(Artikel artikel) {
        this.lagerliste.add(artikel);
    }

    public void alleArtikelAusgeben() {
        System.out.println("*************************************");
        if (this.lagerliste.size() == 0) {
            System.out.println("LEER");
        } else {

            for (Artikel artikel : this.lagerliste) {
                System.out.println(artikel);

            }
        }

    }

    public void loescheArtikelId(String id) {
        for (int i = 0; i < this.lagerliste.size(); i++) {
            if (this.lagerliste.get(i).getId().equals(id)) {
                this.lagerliste.remove(i);
                return;
            }
        }
    }

    public void loescheAlleArtikelmitBezeichnungWie(String bezeichnung) {
        Iterator<Artikel> it = this.lagerliste.iterator();
        while (it.hasNext()) {
            Artikel aktuellerArtikel = it.next();
            if (aktuellerArtikel.getName().contains(bezeichnung)) {
                it.remove();
            }
        }
    }

    public double lagerwert() {
        double lagerwert = 0;
        for (Artikel artikel : this.lagerliste) {
            lagerwert = lagerwert + artikel.getLagermenge() * artikel.getPreis();
        }

        return lagerwert;
    }
    
    public ArrayList<Artikel> gibLagerliste()
    {
        return this.lagerliste;
    }

}
