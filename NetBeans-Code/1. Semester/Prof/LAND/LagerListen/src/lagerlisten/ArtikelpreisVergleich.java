/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lagerlisten;

import java.util.Comparator;

/**
 *
 * @author landerer
 */
public class ArtikelpreisVergleich implements Comparator<Artikel>{

    @Override
    public int compare(Artikel o1, Artikel o2) {
        if(o1.getPreis()<o2.getPreis()) 
        {
            return -1;
        }
        else if(o1.getPreis()==o2.getPreis()) 
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

}
