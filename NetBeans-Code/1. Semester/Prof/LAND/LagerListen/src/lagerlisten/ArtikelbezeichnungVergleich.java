/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lagerlisten;

import java.util.Comparator;

/**
 *
 * @author landerer
 */
public class ArtikelbezeichnungVergleich implements Comparator<Artikel>{

    @Override
    public int compare(Artikel o1, Artikel o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
