/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication29;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class JavaApplication29 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        int zufallszahl = rand.nextInt(100);
        zufallszahl++;
        int versuche = 0;
        int tipp;

        do {

            System.out.println("Tipp 1-100 eingeben");
            tipp = scan.nextInt();

            versuche++;

            if (tipp > zufallszahl) {
                System.out.println("Tipp zu hoch!");

            } else if (tipp < zufallszahl) {
                System.out.println("Tipp zu nieder!");

            } else {
                System.out.println("Tipp ist richtig!");
                System.out.println("Anzahl an Versuchen: " + versuche);
            }

        } while (zufallszahl != tipp);
        scan.close();
    }
}
