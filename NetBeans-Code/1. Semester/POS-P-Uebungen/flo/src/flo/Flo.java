/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flo;

/**
 *
 * @author Philipp
 */
public class Flo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        byte myByte1, myByte2, myByte3;
        short myShort1;
        // myByte1 = 128;
        myShort1 = 128;
        System.out.println(myShort1);
        myByte2 = 127;
        System.out.println(myByte2);
        myByte3 = ++myByte2;
        System.out.println(myByte3);
        myByte3 = ++myByte3;
        System.out.println(myByte3);
        byte var1, var2, var3;
        short var4;
        //  var1 = 128;
        var4 = 128;
        System.out.println(var4);
        var2 = 127;
        System.out.println(var2);
        var3 = ++var2;
        System.out.println(var3);
        var3 = ++var3;
        System.out.println(var3);
        
    }
    
}
