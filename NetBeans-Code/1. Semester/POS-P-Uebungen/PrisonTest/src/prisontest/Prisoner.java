/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prisontest;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Prisoner {

    Scanner scan = new Scanner(System.in);
    private double grosse, verurteilteJahre;
    private String name, zelle;

    public Prisoner(String name, double grosse, int verurteilteJahre) {
        this.name = name;
        this.grosse = grosse;
        this.verurteilteJahre = verurteilteJahre;
    }

    public String getGefangenerName() {
        return this.name;
    }

    public double getVerurteilteJahre() {
        return this.verurteilteJahre;
    }

    public double getGefangenerGröße() {
        return this.grosse;
    }

    public String getGefangenenZelle() {
        return this.zelle;
    }

    private void setJahre(double jahre) {
        this.verurteilteJahre = jahre;
    }

    private void setGröße(double grosse) {
        this.grosse = grosse;
    }

    private String setZelle(String zelle) {
        return this.zelle = zelle;
    }

    private void neuAnlegen() {
        int verurteilteJahre;
        double grosse;
        String zelle, name;
        Texte.printErstanmeldungWillkomensText();
        Texte.printInputName();
        name = scan.next();
        Texte.printInputJahre();
        verurteilteJahre = scan.nextInt();
        Texte.printInputGroesse();
        grosse = scan.nextDouble();
        Texte.printInputZelle();
        zelle = scan.nextLine();
        this.setAlleWerte(verurteilteJahre, grosse, zelle);
    }

    private void datenAndern() {
        double grosse, verurteilteJahre;
        String zelle;
        Texte.printStriche();
        Texte.printInputJahre();
        verurteilteJahre = scan.nextInt();
        Texte.printInputGroesse();
        grosse = scan.nextDouble();
        Texte.printInputZelle();
        zelle = scan.next();
        Texte.printStriche();
        this.setAlleWerte(verurteilteJahre, grosse, zelle);
    }

    public void gefangenenVerwaltung() {
        Texte.printVerwaltungsText();
        char auswahl = scan.next().charAt(0);
        Texte.printStriche();
        switch (auswahl) {
            case 'n':
                this.neuAnlegen();
                break;
            case 'd':
                Texte.printWerteAusgeben(this.name, this.verurteilteJahre, this.grosse, this.zelle);
                break;
            case 'a':
                this.datenAndern();
                break;
            default:
                Texte.printUngueltigeEingabe();
                break;
        }

    }

    public void setAlleWerte(double jahre, double grosse, String zelle) {
        this.setJahre(jahre);
        this.setGröße(grosse);
        this.setZelle(zelle);
    }

    public void debug() {
        System.out.println("Debug: \n\tName: \t\t" + this.name +"\n\tGroesse: \t" + this.grosse + "\n\tJahre: \t\t" + this.verurteilteJahre + "\n\tZelle: \t\t" + this.zelle);
    }

}
