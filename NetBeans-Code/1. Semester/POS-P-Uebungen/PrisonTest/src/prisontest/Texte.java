/*
 * To change Texte license header, choose License Headers in Project Properties.
 * To change Texte template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prisontest;

import javax.swing.*;

/**
 *
 * @author Mr-Phil
 */
public class Texte {
//-----------------------------------------------------------------------
//Willkomenstexte

    public static void printWilkommensText() {
        Texte.printStriche();
        System.out.println("       Herzlich Willkommen bei dem Gefängnis Ihres Vertrauens");
        Texte.printStriche();
    }

    private static void printVerwaltungsWillkomensText() {
        System.out.println("    Herzlich Willkommen zu der Verwaltung eines Gefangenen");
    }

    public static void printErstanmeldungWillkomensText() {
        System.out.println("Herzlich Willkommen zu Erstanmeldung eines Gefangenen");
    }
//-----------------------------------------------------------------------
//Texte für die Verwalltungs-Klasse

    public static void printVerwaltungsText() {
        Texte.printZeilenUmbruch();
        Texte.printStriche();
        Texte.printVerwaltungsWillkomensText();
        System.out.println("    Möchten Sie einen Gefangenen neu anlegen?");
        System.out.println("    Möchten Sie die aktuellen Daten des Gefangenen ansehen?");
        System.out.println("    Möchten Sie die Daten des Gefangenen ändern?");
        Texte.printStriche();
        System.out.println("d: Daten ausgeben \t a: Daten ändern");
        System.out.print("Ihre Eingabe lautet: ");
    }

    public static void printGefangenerNeu() {
        Texte.printStriche();
        Texte.printVerwaltungsWillkomensText();
        Texte.printStriche();
    }

    public static void printDatenAenderung() {
        Texte.printZeilenUmbruch();
        Texte.printStriche();
        System.out.println("");
        Texte.printStriche();
    }

//Texte für die Input-Ausgeben    
    public static void printInputName() {
        System.out.print("Bitte geben Sie den Namen des Gefangen an: ");
    }

    public static void printInputJahre() {
        System.out.print("Bitte geben Sie die verurteilten Jahre des Gefangen an: ");
    }

    public static void printInputGroesse() {
        System.out.print("Bitte geben Sie die Körpergröße des Gefangen an: ");
    }

    public static void printInputZelle() {
        System.out.print("Bitte geben Sie die Zelle des Gefangen an: ");

    }
//-----------------------------------------------------------------------
//Texte für eine falsche Eingaben

    public static void printUngueltigeEingabe() {
        System.out.println("Sie haben einen falschen Wert eingegeben!");
    }

    public static void printVerabschiedung() {
        System.out.println("Auf Wiedersehen");
        System.out.println("Beehren Sie uns wieder einmal mit Ihrer Anwesenheit");
    }
//-----------------------------------------------------------------------
//Print Strich und Zeilenumbruch

    /**
     * Diese Funktion zeichent einen Stich von 64 -
     */
    public static void printStriche() {
        System.out.println("----------------------------------------------------------------");
    }

    public static void printZeilenUmbruch() {
        System.out.println("\n");
    }
//-----------------------------------------------------------------------
//Texte für die Ausgabe von Werten

    public static void printWerteAusgeben(String name, double verurteilteJahre, double grosse, String zelle) {
        System.out.println("Ausgabe: \n\tName: \t\t" + name + "\n\tJahre: \t\t" + verurteilteJahre + "\n\tGroesse: \t" + grosse + "\n\tZelle: \t\t" + zelle);
    }

//-----------------------------------------------------------------------
//Texte für die JOptionPane-Klasse
    /**
     *
     * @param confirmText Hier kann der Text für das JOptionPane-ConfirmDialog
     * Fenster mitgegeben werden.
     * @return Hier wird entweder der Wert 0 (für den JA-Button), der Wert 1(für
     * den NEIN-Button) oder der Wert 2 (für den ABBRECHEN-Button)
     * zurückgegeben.
     */
    public static int printBestaetigungsFenster(String confirmText) {
        int confimValue = JOptionPane.showConfirmDialog(null, confirmText);
        return confimValue;
    }

    /**
     *
     * @param inputText Hier kann der Text für das JOptionPane-InputDialog
     * Fenster mitgegeben werden.
     * @return Hier wirde ein String mit den Eingegeben Wert zurückgegeben.
     */
    public static String printInputFenster(String inputText) {
        String name = JOptionPane.showInputDialog(null, inputText);
        return name;
    }

    /**
     *
     * @param inputText Hier kann der Text für das JOptionPane-InputDialog
     * Fenster mitgegeben werden.
     * @return
     */
    public static int printInputFensterInt(String inputText) {
        String inputStr = JOptionPane.showInputDialog(null, inputText);
        int inputInt = Integer.parseInt(inputStr);
        return inputInt;
    }

    /**
     * Mit Hilfe dieser funktion kann man eine MessageBox erstellen mit einem
     * eigenem Text.
     *
     * @param messageText Hier kann der Text für das JOptionPane-MessageDialog
     * Fenster mitgegeben werden.
     */
    public static void printMessageFenster(String messageText) {
        JOptionPane.showMessageDialog(null, messageText);
    }
}
