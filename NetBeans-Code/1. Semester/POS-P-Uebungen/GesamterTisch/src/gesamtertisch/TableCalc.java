/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gesamtertisch;

/**
 *
 * @author Philipp
 */
public class TableCalc {

    public double steuer = 0.1;
    public double trinkgeld = 0.1;
    private double gesamterTischbetrag = 0;

    public double preisBrutto(double preis) {
        double brutto = (preis * (1 + (this.steuer + this.trinkgeld)));
        gesamterTischbetrag = gesamterTischbetrag + brutto;
        return brutto;
    }

    public double gibGesamterTisch() {
        return this.gesamterTischbetrag;
    }

    public double resetGesamterTischPreis() {
        return this.gesamterTischbetrag = 0;
    }

    public void ausgeben() {
        System.out.println("TableCalc: steuer: $" + this.steuer + ", trinkgeld: $" + this.trinkgeld + ", gesamterTischBetrag: $" + this.gesamterTischbetrag);
    }
}
