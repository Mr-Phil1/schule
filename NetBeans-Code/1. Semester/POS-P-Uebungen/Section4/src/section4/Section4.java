/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package section4;

import java.util.Random;

/**
 *
 * @author Philipp
 */
public class Section4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int var1, var2;
        var1 = addiere(2, 3);
        ergebnisAusgeben(var1, "Dies ist ein tolles Ergebnis: ");
        System.out.println("Zufallszahl ist: " + gibZufallszahl(1, 10));

        System.out.println("Parkschaden " + parkschadenPassiert(20));
    }

    public static int gibZufallszahl(int uG, int oG) {
        Random r = new Random();
        int zufall = r.nextInt(oG);
        return zufall;

    }

    public static boolean parkschadenPassiert(int wahrscheinlichkeit) {
        Random r = new Random();
        if (r.nextInt(101) <= wahrscheinlichkeit) {
            return true;
        } else {
            return false;
        }
    }

    public static int addiere(int a, int b) {
        int erg = a + b;
        return erg;
    }

    public static void ergebnisAusgeben(int erg, String text) {
        System.out.println("---- " + text + erg + " ----");
    }

}
