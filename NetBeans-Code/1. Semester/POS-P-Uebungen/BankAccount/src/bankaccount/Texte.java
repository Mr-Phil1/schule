/*
 * To change Texte license header, choose License Headers in Project Properties.
 * To change Texte template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import javax.swing.*;

/**
 *
 * @author Mr-Phil
 */
public class Texte {
//-----------------------------------------------------------------------
//Willkomenstexte

    public static void printWilkommensText() {
        Texte.printStriche();
        System.out.println("       Herzlich Willkommen bei der Bank Ihres Vertrauens");
        Texte.printStriche();
    }

    private static void printVerwaltungsWillkomensText() {
        System.out.println("    Herzlich Willkommen in der Verwaltung Ihrer Bank-Konten");
    }
//-----------------------------------------------------------------------
//Texte für die Verwalltungs-Klasse

    public static void printVerwaltungsText() {
        Texte.printZeilenUmbruch();
        Texte.printStriche();
        Texte.printVerwaltungsWillkomensText();
        System.out.println("    Möchten Sie einen Kunden neu anlegen?");
        System.out.println("    Möchten Sie die aktuellen Daten des Kunden ansehen?");
        System.out.println("    Möchten Sie die Daten des Kunden ändern?");
        Texte.printStriche();
        System.out.println("n: Kunden neuanlegen \t d: Daten ausgeben \t a: Daten ändern");
        System.out.print("Ihre Eingabe lautet: ");
    }

    public static void printKundeNeu() {
        Texte.printStriche();
        Texte.printVerwaltungsWillkomensText();
        Texte.printStriche();
    }

    public static void printDatenAenderung() {
        Texte.printZeilenUmbruch();
        Texte.printStriche();
        System.out.println("");
        Texte.printStriche();
    }
//-----------------------------------------------------------------------
//Texte für eine falsche Eingaben

    public static void printUngueltigeEingabe() {
        System.out.println("Sie haben einen falschen Wert eingegeben!");
    }

    public static void printVerabschiedung() {
        System.out.println("Auf Wiedersehen");
        System.out.println("Beehren Sie uns wieder einmal mit Ihrer Anwesenheit");
    }
//-----------------------------------------------------------------------
//Print Strich und Zeilenumbruch

    /**
     * Diese Funktion zeichent einen Stich von 64 -
     */
    public static void printStriche() {
        System.out.println("----------------------------------------------------------------");
    }

    public static void printZeilenUmbruch() {
        System.out.println("\n");
    }
//-----------------------------------------------------------------------
//Texte für die PIN-Klasse

    public static void printPinEingabe() {
        System.out.print("Bitte geben Sie Ihre PIN ein: ");
    }

    public static void printPinFalscheEingabe() {
        System.out.println("Ihre PIN ist ungueltig oder wurde falsch eingegeben.");
        System.out.print("Bitte geben Sie Ihre PIN noch einmal ein: ");
    }

    public static void printPinMehrfachFalsch() {
        System.out.println("Sie haben den PIN mehrfach falsch eingegeben");
    }

    public static void printGesperrtKarte() {
        Texte.printPinMehrfachFalsch();
        System.out.println("Ihre Karte ist nun gesperrt");
        System.out.println("Bitte wenden Sie sich diezbezüglich an Ihre Bank!");
        Texte.printVerabschiedung();
    }
//-----------------------------------------------------------------------
//Texte für die JOptionPane-Klasse

    /**
     *
     * @param confirmText Hier kann der Text für das JOptionPane-ConfirmDialog
     * Fenster mitgegeben werden.
     * @return Hier wird entweder der Wert 0 (für den JA-Button), der Wert 1(für
     * den NEIN-Button) oder der Wert 2 (für den ABBRECHEN-Button)
     * zurückgegeben.
     */
    public static int printBestaetigungsFenster(String confirmText) {
        int confimValue = JOptionPane.showConfirmDialog(null, confirmText);
        return confimValue;
    }

    /**
     *
     * @param inputText Hier kann der Text für das JOptionPane-InputDialog
     * Fenster mitgegeben werden.
     * @return Hier wirde ein String mit den Eingegeben Wert zurückgegeben.
     */
    public static String printInputFenster(String inputText) {
        String name = JOptionPane.showInputDialog(null, inputText);
        return name;
    }

    /**
     * Mit Hilfe dieser funktion kann man eine MessageBox erstellen mit einem
     * eigenem Text.
     *
     * @param messageText Hier kann der Text für das JOptionPane-MessageDialog
     * Fenster mitgegeben werden.
     */
    public static void printMessageFenster(String messageText) {
        JOptionPane.showMessageDialog(null, messageText);
    }
}
