/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Verwaltung {

    private String name, erstellungsdatum, geburtsdatum;
    private int pin;
    static Scanner scan = new Scanner(System.in);
    Kunden kunde1 = new Kunden(name, erstellungsdatum, geburtsdatum, pin);

    public void verwaltungAuswahl() {
        Texte.printVerwaltungsText();
        char auswahl = scan.next().charAt(0);
        switch (auswahl) {
            case 'n':
                Texte.printKundeNeu();
                this.verwaltungKundenNeu();
                break;
            case 'd':
                this.verwaltungKundenAusgeben();
                break;
            case 'a':
                Texte.printWilkommensText();
                Texte.printDatenAenderung();
                this.verwaltungKundenAendern();
                break;
            default:
                Texte.printUngueltigeEingabe();
                Texte.printVerabschiedung();
                break;
        }
    }

    private void verwaltungKundenNeu() {
        Texte.printWilkommensText();
        System.out.print("Name: ");
        this.name = scan.next();
        System.out.print("Erstelldatum: ");
        this.erstellungsdatum = scan.next();
        System.out.print("Geburtsdatum: ");
        this.geburtsdatum = scan.next();
        System.out.print("PIN: ");
        pin = scan.nextInt();
        kunde1.setAll(name, erstellungsdatum, geburtsdatum, pin);
    }

    private void verwaltungKundenAusgeben() {
        Texte.printWilkommensText();
        kunde1.debugKunde();
    }

    private void verwaltungKundenAendern() {
        Texte.printWilkommensText();

    }
}
