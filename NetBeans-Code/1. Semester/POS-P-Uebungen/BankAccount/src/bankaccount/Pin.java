/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Pin {

    private final Scanner scan = new Scanner(System.in);
    private boolean inputPinok = false, checkPIN = false, karteGesperrt;
    private int count, inputPin;
    private int pin;

    public void setPin(int pin) {
        this.pin = pin;
    }

    private boolean setKarteGesperrt() {
        return this.karteGesperrt = true;
    }

    public boolean checkPin() {
        int pinwert = this.inputPin();
        if (pinwert == this.pin) {
            inputPinok = true;
            return inputPinok;
        } else {
            Texte.printGesperrtKarte();
            this.setKarteGesperrt();
            return inputPinok;
        }
    }

    private int inputPin() {
        int countInput = 0;
        Texte.printPinEingabe();
        inputPin = scan.nextInt();
        while (inputPin != this.pin) {
            Texte.printPinFalscheEingabe();
            inputPin = scan.nextInt();
            countInput++;
            if (countInput == 2) {
                break;
            }
        }
        return inputPin;
    }

    public void debugPin() {
        Texte.printStriche();
        Texte.printMessageFenster("Debug-PIN:");
        System.out.println("Debug-PIN: \n\tPin: \t\t" + this.pin + "\n\tGespert: \t" + this.karteGesperrt + "\n\tPIN-OK: \t" + this.inputPinok);

    }
}
