/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

/**
 *
 * @author Mr-Phil
 */
public class Kunden {

    private String name, erstellungsdatum, geburtsdatum;
    Pin pin1 = new Pin();

    public Kunden(String name, String erstellungsdatum, String geburtsdatum, int pin) {
        this.name = name;
        this.erstellungsdatum = erstellungsdatum;
        this.geburtsdatum = geburtsdatum;
        pin1.setPin(pin);
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setErstellungsdatum(String erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }

    private void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public void setAll(String name, String erstellungsdatum, String geburtsdatum, int pin) {
        this.setName(name);
        this.setErstellungsdatum(erstellungsdatum);
        this.setGeburtsdatum(geburtsdatum);
        pin1.setPin(pin);

    }

    public void debugKunde() {
        System.out.println("Debug-Kunde: \n\tName: \t\t" + this.name + "\n\tKunde seit: \t" + this.erstellungsdatum + "\n\tGeburtsdatum: \t" + this.geburtsdatum);
    }
}
