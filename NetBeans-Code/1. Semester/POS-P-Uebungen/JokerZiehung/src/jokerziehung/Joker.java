/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jokerziehung;

import java.util.Random;

/**
 *
 * @author Philipp
 */
public class Joker {

    private int zahl, var1, var2, var3, var4, var5, var6;

    private int ziehungStart() {
        Random r = new Random();
        zahl = r.nextInt(11);
        return zahl;
    }

    private int ziehungZahl1() {
        var1 = this.ziehungStart();
        return this.var1;
    }

    private int ziehungZahl2() {
        var2 = this.ziehungStart();
        return this.var2;
    }

    private int ziehungZahl3() {
        var3 = this.ziehungStart();
        return this.var3;
    }

    private int ziehungZahl4() {
        var4 = this.ziehungStart();
        return this.var4;
    }

    private int ziehungZahl5() {
        var5 = this.ziehungStart();
        return this.var5;
    }

    private int ziehungZahl6() {
        var6 = this.ziehungStart();
        return this.var6;
    }

    /**
     * 
     * 
     */
    public void ausgeben() {
        System.out.println("Die Ziehung wird gestartet.");
        System.out.println("");
        System.out.println("");
        System.out.println("Die Ziehung hat folgende Zahlen gezogen: ");
        System.out.println("Joker Zahl 1 ist: " + ziehungZahl1());
        System.out.println("Joker Zahl 2 ist: " + ziehungZahl2());
        System.out.println("Joker Zahl 3 ist: " + ziehungZahl3());
        System.out.println("Joker Zahl 4 ist: " + ziehungZahl4());
        System.out.println("Joker Zahl 5 ist: " + ziehungZahl5());
        System.out.println("Joker Zahl 6 ist: " + ziehungZahl6());

    }

}
