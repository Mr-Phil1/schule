/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arcadegame;

/**
 *
 * @author Mr-Phil
 */
public class Card {

    private int creditBalance, ticketBalance;
    private final String cardNumber;

    public Card(String cardNumber) {
        this.cardNumber = cardNumber;
        this.creditBalance = 0;
        this.ticketBalance = 0;
    }

    public int getCreditBalance() {
        return this.creditBalance;
    }

    public int getTicketBalance() {
        return this.ticketBalance;
    }

    public void setCreditBalance(int creditBalance) {
        this.creditBalance = ticketBalance;
    }

    public void setTicketBalance(int ticketBalance) {
        this.ticketBalance = ticketBalance;
    }
}
