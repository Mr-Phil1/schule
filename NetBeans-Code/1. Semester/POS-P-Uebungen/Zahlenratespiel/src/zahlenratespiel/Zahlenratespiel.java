/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zahlenratespiel;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class Zahlenratespiel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int zufall, versuche = 0, tipp = 0;

        Random r = new Random();
        Scanner scan = new Scanner(System.in);
        zufall = r.nextInt(101);
        do {
            System.out.print("Bitte eine Zahl von 1 bis 100 eingeben: ");
            tipp = scan.nextInt();
            versuche++;
            if (tipp > zufall) {
                System.out.println("Ihr Tipp ist leider zu groß");
            } else {
                if (tipp < zufall) {
                    System.out.println("Ihr Tipp ist leider zu klein");
                } else {
                    System.out.println("Sie haben einen richtigen Tipp erziehlt.");
                    System.out.println("Die gesuchte Zahl war: " + zufall);
                    System.out.println("Und sie haben nur " + versuche + " Versuche gebraucht.");
                }
            }

        } while (tipp != zufall);
        {
        }
    }
}
