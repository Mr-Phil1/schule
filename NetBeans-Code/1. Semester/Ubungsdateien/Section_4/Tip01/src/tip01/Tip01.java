package tip01;

public class Tip01 {

    public static void main(String[] args) {
        //Find everyone's indivudlal total after tax(5%) and tip(15%)
        double tax, tip, total, var1, var2, var3, var4, var5, var6, var7, var8;
        tax = 0.05;
        tip = 0.15;
        var1 = 10 + (10 * (tax + tip));
        var2 = 12 + (12 * (tax + tip));
        var3 = 9 + (9 * (tax + tip));
        var4 = 8 + (8 * (tax + tip));
        var5 = 7 + (7 * (tax + tip));
        var6 = 15 + (15 * (tax + tip));
        var7 = 11 + (11 * (tax + tip));
        var8 = 30 + (30 * (tax + tip));
        System.out.println(
                "Die Preise sind: P1: " + var1 + ", P2: " + var2 + ", P3: " + var3 + ", P4: " + var4 + ", P5:" + var5 + ", P6:" + var6 + ", P7: " + var7 + ", P8: " + var8 + "."
        );
        /*This is what everyone owes before tax and tip:
        Person 1: $10
        Person 2: $12
        Person 3: $9
        Person 4: $8
        Person 5: $7
        Person 6: $15
        Person 7: $11
        Person 8: $30
         */
    }
}
