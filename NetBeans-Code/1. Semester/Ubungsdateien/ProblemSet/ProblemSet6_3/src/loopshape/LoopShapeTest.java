package loopshape;

import java.util.Scanner;

public class LoopShapeTest {

    public static void main(String[] args) {
        LoopShape loop = new LoopShape();
        int hohe, breite;
        Scanner scan = new Scanner(System.in);
        System.out.print("Bitte geben sie die Breite an: ");
        breite = scan.nextInt();
        System.out.print("Bitte geben sie die Höhe an: ");
        hohe = scan.nextInt();
        loop.createRectangle(breite, hohe);
        loop.createTriangle(5);
    }
}
