package loopshape;

public class LoopShape {

    public void createRectangle(int breite, int hoehe) {
        if (hoehe > 1 && breite > 1) {
            this.kopfZeichnen(breite);
            for (int i = 0; i < hoehe - 2; i++) {
                for (int j = 1; j <= breite; j++) {
                    if (j == 1 || j == breite) {
                        System.out.print("#");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println("");
            }
            this.kopfZeichnen(breite);
        } else {
            System.out.println("#");
        }

    }

    private void kopfZeichnen(int breite) {
        for (int i = 0; i < breite; i++) {
            System.out.print("#");
        }
        System.out.println("");
    }

    public void createTriangle(int leg) {
        //Draw an Isosceles Right Triangle

    }
}
