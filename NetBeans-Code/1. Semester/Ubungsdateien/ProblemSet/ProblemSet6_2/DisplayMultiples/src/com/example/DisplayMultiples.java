/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.Scanner;

public class DisplayMultiples {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        int anfang, ende, number, erg;
        do {
            System.out.print("Bitte geben Sie die den ersten Faktor der Multiplikation an: ");
            anfang = scan.nextInt();
            System.out.print("Bitte geben Sie die den letzten Faktor der Multiplikation an: ");
            ende = scan.nextInt();
        } while (anfang > ende);
        System.out.print("Bitte geben sie Ihre Zahl ein: ");
        number = scan.nextInt();
        for (int i = anfang; i <= ende; i++) {
            erg = number * i;
            System.out.println("Das Ergebnis von " + number + " mal " + i + " beträgt: " + erg);
        }
    }

}
