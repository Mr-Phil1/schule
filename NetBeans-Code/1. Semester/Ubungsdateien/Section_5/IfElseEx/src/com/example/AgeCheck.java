/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.Scanner;

/**
 *
 * @author anshenoy
 */
public class AgeCheck {

    public static void main(String[] args) {
        int myAge;   // I am 19; let me see if I can drive yet
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter your Age: ");
        myAge = scan.nextInt();
        if (myAge >= 16) {
            System.out.println("I'm old enough to have a driver's license!");
        } else {
            System.out.println("I'm not old enough yet... :*(");
        }
    }
}
