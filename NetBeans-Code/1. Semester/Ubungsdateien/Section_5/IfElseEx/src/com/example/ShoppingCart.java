package com.example;

public class ShoppingCart {

    public static void main(String[] args) {
        String custName = "Mary Smith";

        String itemDesc = "Shirt";

        // numeric fields
        double price = 21.99, total, tax = 1.04;
        int quantity = 1;
        boolean outOfStock, bquantity;
        int n = itemDesc.length();
        String message = custName + " wants to purchase " + quantity + " " + itemDesc;

        // Calculating total cost
        total = (price * quantity) * tax;
        outOfStock = quantity <= 0;
        bquantity = quantity <= 1;
        if (outOfStock) {
            System.out.println("The Item is out of stock!");
        } else {
            if (bquantity) {
                System.out.println(message+ ", the total cost is: "+total);
            } else {
                System.out.println(message + "s, the total cost is: "+total);
            }
            // Declare outOfStock variable and initialize it
            // Test quantity and modify message if quantity > 1.  
            // Test outOfStock and notify user in either case. 
        }
    }
}
