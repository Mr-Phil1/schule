/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conditionalex;

import java.util.Scanner;

/**
 *
 * @author anshenoy
 */
public class WatchMovie {

    public static void main(String args[]) {
        boolean gut, teuer;
        int rating, price;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the movie ticket price: ");
        price = scan.nextInt();
        System.out.print("Enter the movie rating: ");
        rating = scan.nextInt();
        teuer = price >= 12;
        gut = rating >= 5;
        if (gut && teuer) {
            System.out.println("I’m interested in watching the movie.");
        } else {
            System.out.println("I am not interested in watching the movie.");
        }
    }
}
