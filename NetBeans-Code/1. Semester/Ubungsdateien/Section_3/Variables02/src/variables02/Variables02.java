
package variables02;


public class Variables02 {

    public static void main(String[] args) {
        //RUN THE PROGRAM BETWEEN EACH STEP AND OBSERVE THE OUTPUT
        //Step 1) Change the print statements in lines 12 and 16 so that they print the value of x.
        //Step 2) Change the value of x to "kitty".
        // part to define the variables
        String x;
        boolean bool;
       // part to define the value
        x = "kitty";
        bool = true;
        
        
        System.out.println(x);
        System .out.println(x);
        
        //Step 3) Add a line of code that changes the value of x to "bunny".
        x = "bunnys";
        System.out.println(x);
        
    }
}
