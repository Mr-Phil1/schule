package casting01;

public class Casting01 {

    public static void main(String[] args) {
        //Declare and initialize a byte with a value of 128
        //Observe NetBeans' complaint
        byte var1, var2, var3;
        short var4;
        //  var1 = 128;
        var4 = 128;
        System.out.println(var4);
        var2 = 127;
        System.out.println(var2);
        var3 = ++var2;
        System.out.println(var3);
        var3 = ++var3;
        System.out.println(var3);

        //Declare and initialize a short with a value of 128
        //Create a print statement that casts this short to a byte
        //Declare and initialize a byte with a value of 127
        //Add 1 to this variable and print it
        //Add 1 to this variable again and print it again
    }
}
