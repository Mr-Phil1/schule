package chickens01;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Chickens01 {

    public static void main(String[] args) {
        int chickenCount, eggsPerChicken, totalEggs, mon, tue, wed;

        /**
         * System.out.print("Please enter the Number of the Eggs: "); Scanner
         * egg = new Scanner(System.in); *
         */
        String sEgg = JOptionPane.showInputDialog("Please enter the Number of the Eggs:");
        int eggs = Integer.parseInt(sEgg);
        /**
         * System.out.print("Please enter the Number of the Chicken: "); Scanner
         * chicken = new Scanner(System.in); int animal = chicken.nextInt();
         */
        String sAnimal = JOptionPane.showInputDialog("Please enter the Number of the Chicken:");
        int animal = Integer.parseInt(sAnimal);
        System.out.println("-------------------------------------");
        eggsPerChicken = eggs;
        chickenCount = animal;
        mon = chickenCount++;
        tue = chickenCount;
        wed = tue / 2;
        totalEggs = (mon + tue + wed) * eggsPerChicken;
        System.out.println("Mon = " + mon + " Chicken");
        System.out.println("Tue = " + tue + " Chicken");
        System.out.println("Wed = " + wed + " Chicken");
        System.out.println("-------------------------------------");
        System.out.println(totalEggs + " Eggs and " + (mon + tue + wed) + " Chicken in three Days.");
    }
}
