package variables03;

public class Variables03 {

    public static void main(String[] args) {

        boolean bool = true;
        int intVar3;
        int x, y;

        int intVar1 = 1;
        int intVar2 = 2;
        intVar3 = 3;

        double doubleVar1, doubleVar2, doubleVar3, doubleVar4;
        doubleVar1 = 1.1;
        doubleVar2 = 2.1;
        doubleVar3 = 3.1;
        doubleVar4 = 3.5;

        String stringVar1 = "1";
        String stringVar2 = "2";
        x = 25;
        x = 3+5; // 8
        x = x + 1; // 9
        x += 1; // 10
        x++; // 11
        
        y = (((25 - 5) * 4) / (2 - 10)) + 4;
/*
        //Don't edit these print statements
        System.out.println("bool = " + bool);
        System.out.println("intVar1 = " + intVar1);
        System.out.println("intVar2 = " + intVar2);
        System.out.println("intVar3 = " + intVar3);
        System.out.println("doubleVar1 = " + doubleVar1);
        System.out.println("doubleVar2 = " + doubleVar2);
        System.out.println("doubleVar3 = " + doubleVar3);
        System.out.println("doubleVar4 = " + doubleVar4);
        System.out.println("stringVar1 = " + (stringVar1 + 1));
        System.out.println("stringVar2 = " + (stringVar2 + 2));
*/
        System.out.println(x);
        System.out.println(y);
    }
}
