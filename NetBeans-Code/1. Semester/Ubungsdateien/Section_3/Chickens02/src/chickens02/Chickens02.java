package chickens02;

import java.util.Scanner;

public class Chickens02 {

    public static void main(String[] args) {
        double dailyAverage, monthlyAverage, monthlyProfit;
// Scanner int-Input with Egg Amount on Monday
        System.out.print("Please enter the Number of the Eggs on Monday: ");
        Scanner eggMon = new Scanner(System.in);
        int mon = eggMon.nextInt();
// Scanner Int-Input with Egg Amount on Monday
        System.out.print("Please enter the Number of the Eggs on Tuesday: ");
        Scanner eggTu = new Scanner(System.in);
        int tue = eggTu.nextInt();
// Scanner Int-Input with Egg Amount on Monday
        System.out.print("Please enter the Number of the Eggs on Wednesday: ");
        Scanner eggWe = new Scanner(System.in);
        int wed = eggWe.nextInt();
// Scanner Double-Input with the Dollar Value per Egg // On a German Keybord with ,  // On a English Keyboard with . //
        System.out.print("Please enter the Dollar Value per Egg: ");
        Scanner dollorAm = new Scanner(System.in);
        System.out.print("$");
        double dollar = dollorAm.nextDouble();
        dailyAverage = (mon + tue + wed) / 3.0;
        monthlyAverage = dailyAverage * 30;
        monthlyProfit = monthlyAverage * dollar;
        System.out.println("----------------------------------------------------------------------------------------------------------------------");
        System.out.print("Your Input: ");
        System.out.println("Eggs on Monday: " + mon + ", Eggs on Tuesday: " + tue + " and Eggs on Wednesday: " + wed + ". And the Value of the Dollar: $" + dollar + ".");
        System.out.println("----------------------------------------------------------------------------------------------------------------------");
        System.out.println("-------------------------------------");
        System.out.println("Daily Average:   " + dailyAverage);
        System.out.println("Monthly Average: " + monthlyAverage);
        System.out.println("Monthly Profit:  $" + monthlyProfit);
        System.out.println("-------------------------------------");

    }

}
