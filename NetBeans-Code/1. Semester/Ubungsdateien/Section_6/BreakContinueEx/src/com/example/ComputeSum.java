package com.example;

import java.util.Scanner;

public class ComputeSum {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = 0, sum = 0, i;

        System.out.println("Eingabe von zehn Zahlen: ");
        for (i = 1; i < 10; i++) {
            System.out.print("Eingabe der " + i + " .Zahl: ");
            num = scan.nextInt();
            if (num == 0) {
                break;
            }
            sum = sum + num;
        }
        System.out.println("Die Summe beträgt: " + sum);

    }
}
