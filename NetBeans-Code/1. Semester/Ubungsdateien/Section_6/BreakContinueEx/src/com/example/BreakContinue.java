package com.example;

public class BreakContinue {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {

            if (i % 2 == 0) {

            } else if (i != 7) {

                System.out.println("The number is " + i);

            } else {
                break;
            }

        }
    }

}
