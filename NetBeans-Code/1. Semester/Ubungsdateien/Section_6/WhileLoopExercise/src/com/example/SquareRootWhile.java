package com.example;

import java.util.Scanner;

public class SquareRootWhile {

    public static void main(String args[]) {
        double sroot;
        int number;
        System.out.print("Type a non-negative integer: ");
        Scanner console = new Scanner(System.in);
        number = console.nextInt();
        while (number <= 0) {
            System.out.print("Invalid number, try again: ");
            number = console.nextInt();
        }
        sroot = Math.sqrt(number);
        System.out.println("The square root of " + number + " is: " + sroot);

    }

}
