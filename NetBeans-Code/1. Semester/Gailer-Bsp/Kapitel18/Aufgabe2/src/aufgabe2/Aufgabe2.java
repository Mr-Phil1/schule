/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aufgabe2;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Aufgabe2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int oberg, unterg, input, sumo = 0, sumu = 0;
        Scanner scan = new Scanner(System.in);
        System.out.print("untergrenze: ");
        unterg = scan.nextInt();
        System.out.print("obergrenze: ");
        oberg = scan.nextInt();
        do {
            System.out.print("input: ");
            input = scan.nextInt();
            if (input > unterg && input < oberg) {
                sumu = sumu + input;
            } else {
                sumo = sumo + input;
            }
        } while (input != 0);
        System.out.println("innerhalb: " + sumu);
        System.out.println("ausserhalb: " + sumo);
    }

}
