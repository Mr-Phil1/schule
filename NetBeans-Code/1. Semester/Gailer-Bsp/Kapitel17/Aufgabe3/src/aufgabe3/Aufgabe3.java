/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aufgabe3;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Aufgabe3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int sterne;
        Scanner scan = new Scanner(System.in);
        System.out.print("Bitte anzahl: ");
        sterne = scan.nextInt();
        for (int i = sterne; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

}
