/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontrolle1;

/**
 *
 * @author Philipp
 */
public class Kontrolle1 {

    public static void main(String[] args) {
        double x, funktionswert;
        x = 2;
        funktionswert = 3 * x * x - 8 * x + 4;
        System.out.println("Bei x: " + x + " ergibt die Quadratgleichung den Wert " + funktionswert + ".");
        x = 2.0 / 3.0;
        funktionswert = 3 * x * x - 8 * x + 4;
        System.out.println("Bei x: " + x + " ergibt die Quadratgleichung den Wert " + funktionswert + ".");
        //Die Endergebnisse:
        //Bei x: 4.0 ergibt die Quadratgleichung den Wert 20.0.
        //Bei x: 3.0 ergibt die Quadratgleichung den Wert 7.0.
        //Bei x: -7.0 ergibt die Quadratgleichung den Wert 207.0.
        //Bei x: 2.0 ergibt die Quadratgleichung den Wert 0.0. | Richtig es kommt 0 raus, weil 3*2*2-8*2 = -4 rauskommt und dann +4 gerechnet wird.
        //Bei x: 0.0 ergibt die Quadratgleichung den Wert 4.0. | Falsch es kommt 4 raus, weil 3*0*0-8*0 = 0 rauskommt und dann +4 gerechnet wird.
    }

}
