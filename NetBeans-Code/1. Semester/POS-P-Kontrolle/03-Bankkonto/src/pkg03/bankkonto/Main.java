/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg03.bankkonto;

/**
 *
 * @author Mr-Phil
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bankkonto konto1 = new Bankkonto("Dominik", 00000123456L, 1224); // alles korrekt
        Bankkonto konto2 = new Bankkonto("Claudio", -1L, 1111); // negative Kontonummer
        Bankkonto konto3 = new Bankkonto("Melanie", 00000123451L, -1); // negativer PIN

        konto1.ausgeben();
        konto2.ausgeben();
        konto3.ausgeben();
        konto1.einzahlen(300);
        konto1.ausgeben();
        konto1.einzahlen(100);
        konto1.ausgeben();
        konto1.abheben(100);    // Kann abgehoben werden (Kontostand bleibt positiv)
        konto1.ausgeben();
        konto1.abheben(500);    // Kann nicht abgehoben werden (Kontostand würde negativ)
        konto1.ausgeben();
//         Testen der getter-Methoden
        String inhaber = konto1.getKontoinhaber();
        long kontonummer = konto1.getKontonummer();
        double kontostand = konto1.getKontostand();
        int pin = konto1.getPinCode();
//         Testen der setter-Methoden
        konto2.ausgeben();
        konto2.setKontoinhaber("Herta");
        konto2.setKontonummer(kontonummer + 3);
        konto2.setPinCode(1234);
        konto2.ausgeben();
    }

}
