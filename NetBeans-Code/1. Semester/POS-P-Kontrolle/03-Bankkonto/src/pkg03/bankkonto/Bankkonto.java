/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg03.bankkonto;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Bankkonto {

    Scanner scan = new Scanner(System.in);
    private String name;
    private long kontonummer;
    private int pin, kontostand;
    private boolean gesperrt = false;

    public Bankkonto(String name, long kontonummer, int pin) {
        this.name = name;
        this.kontonummer = kontonummer;
        this.pin = pin;
    }

    public String getKontoinhaber() {
        return this.name;
    }

    public long getKontonummer() {
        return this.kontonummer;
    }

    public int getKontostand() {
        return this.kontostand;
    }

    public int getPinCode() {
        return this.pin;
    }

    public String setKontoinhaber(String name) {
        return this.name = name;
    }

    public long setKontonummer(long kontonummer) {
        return this.kontonummer = kontonummer;
    }

    private boolean setKarteGesperrt() {
        return this.gesperrt = true;
    }

    public int setPinCode(int pin) {
        return this.pin = pin;
    }

    public boolean checkPin() {
        boolean inputPinok;
        int pinwert = this.inputPin();
        if (pinwert == this.pin) {
            return inputPinok = true;
        } else {
            System.out.println("Ihre Karte ist ab jetzt gesperrt");
            this.setKarteGesperrt();
            return inputPinok = false;
        }
    }

    private int inputPin() {
        int countInput = 0;
        System.out.print("Bitte geben Sie Ihren PIN-Code ein:");
        int inputPin = scan.nextInt();
        while (inputPin != this.pin) {
            System.out.println("Sie haben den PIN-CODE falsch eingegeben");
            System.out.print("Bitte geben Sie Ihren PIN-Code ein:");
            inputPin = scan.nextInt();
            countInput++;
            if (countInput == 2) {
                break;
            }
        }
        return inputPin;
    }

    public void setKontostand(int kontostand) {
        this.kontostand = kontostand;
    }

    public void einzahlen(int einzahlung) {
        int neu = this.kontostand + einzahlung;
        this.setKontostand(neu);
    }

    public void abheben(int abhebung) {
        if (this.checkPin() && !this.gesperrt) {
            if ((this.kontostand - abhebung) > 0) {
                int neu = this.kontostand - abhebung;
                this.setKontostand(neu);
            } else {
                System.out.println("Abhebungs leider nicht möglich");
            }
        } else {
            System.out.println("Sie sind nicht berechtig dazu.");
        }

    }

    public void ausgeben() {
        System.out.println("Ausgabe: \n\tKontoinhaber: " + this.name + "\n\tKontonummer: " + this.kontonummer + "\n\tAktueller Kontostand: " + this.kontostand);
    }

}
