/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fakultaet.rekusive;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class FakultaetRekusive {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        fakultaetInt f1 = new fakultaetInt();
        fakultaetLong f2 = new fakultaetLong();
        fakultaetBigInt f3 = new fakultaetBigInt();
        long inputLong, startTimeI, endTimeI, startTimeR, endTimeR, runTimeI1, runTimeR1, runTimeI2, runTimeR2, runTimeI3, runTimeR3;
        System.out.print("Bitte geben Sie eine Zahle ein: ");
        int inputInt = scan.nextInt();
        inputLong = inputInt;
        BigInteger inputBig = new BigInteger("10050");
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Berechnung mit Hilfe von Integer\n");
        startTimeI = System.currentTimeMillis();
        f1.printFakultaetIterative(inputInt);
        endTimeI = System.currentTimeMillis();
        startTimeR = System.currentTimeMillis();
        f1.printFakultaetRekusive(inputInt);
        endTimeR = System.currentTimeMillis();
        runTimeI1 = endTimeI - startTimeI;
        runTimeR1 = endTimeR - startTimeR;
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Berechnung mit Hilfe von Long\n");
        startTimeI = System.currentTimeMillis();
        f2.printFakultaetIterative(inputLong);
        endTimeI = System.currentTimeMillis();
        startTimeR = System.currentTimeMillis();
        f2.printFakultaetRekusive(inputLong);
        endTimeR = System.currentTimeMillis();
        runTimeI2 = endTimeI - startTimeI;
        runTimeR2 = endTimeR - startTimeR;
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Berechnung mit Hilfe von BigInteger\n");
        startTimeI = System.currentTimeMillis();
        f3.printFakultaetIterative(inputBig);
        endTimeI = System.currentTimeMillis();
        startTimeR = System.currentTimeMillis();
        f3.printFakultaetRekusive(inputBig);
        endTimeR = System.currentTimeMillis();
        runTimeI3 = endTimeI - startTimeI;
        runTimeR3 = endTimeR - startTimeR;
        System.out.println("-----------------------------------------------------------------");
        System.out.println("laufzeit von INT:\t\t Iterativ: " + runTimeI1 + "ms, Rekusive: " + runTimeR1 + "ms");
        System.out.println("laufzeit von LONG:\t\t Iterativ: " + runTimeI2 + "ms, Rekusive: " + runTimeR2 + "ms");
        System.out.println("laufzeit von BigIntege:\t\t Iterativ: " + runTimeI3 + "ms, Rekusive: " + runTimeR3 + "ms");
        System.out.println("-----------------------------------------------------------------");

    }

}
