/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fakultaet.rekusive;

/**
 *
 * @author Mr-Phil
 */
public class fakultaetInt {

    private int f, i;

    /**
     * Errechnet iterative die Fakultät einer gegebnen Zahl.
     *
     * @param n Zahl deren Fakultät berechnet wird
     * @return Fakultät der Zahl n
     */
    private int fakultaetIterative(int n) {

        if (n < 0) { // Argument n gültig?
            f = 0; // 1)
        } else {
            f = 1;
            for (i = 2; i <= n; i++) {
                f = f * i; // 2)
            }
        }
        return f;
    }

    /**
     * Errechnet rekursiv die Fakultät einer gegebnen Zahl.
     *
     * @param n Zahl deren Fakultät berechnet wird
     * @return Fakultät der Zahl n
     */
    private int fakultaetRekusive(int n) {
        if (n < 0) {
            f = 0;
        } else {
            if (n == 0) {
                f = 1; // Rekursionsbeginn 2)
            } else {
                f = n * fakultaetRekusive(n - 1); // Rekursionsschritt 1)
            }
        }
        return f;
    }

    public void printFakultaetIterative(int n) {
        System.out.println("\tDie Iterative-Fakultätsberehnung:");
        System.out.println("\t\tDie Fakultät von " + n + " beträgt: \t" + this.fakultaetIterative(n));
    }

    public void printFakultaetRekusive(int n) {
        System.out.println("\tDie Rekusive-Fakultätsberehnung:");
        System.out.println("\t\tDie Fakultät von " + n + " beträgt: \t" + this.fakultaetRekusive(n));
    }

}
