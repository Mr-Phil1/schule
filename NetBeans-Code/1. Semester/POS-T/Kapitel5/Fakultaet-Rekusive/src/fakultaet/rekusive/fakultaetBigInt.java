/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fakultaet.rekusive;

import java.math.BigInteger;

/**
 *
 * @author Mr-Phil
 */
public class fakultaetBigInt {

    private BigInteger bigF, bigN;
    private BigInteger bigI = new BigInteger("2");

    /**
     * Errechnet iterative die Fakultät einer gegebnen Zahl.
     *
     * @param n Zahl deren Fakultät berechnet wird
     * @return Fakultät der Zahl n
     */
    private BigInteger fakultaetIterative(int n) {
        if (n < 0) {
            bigF = BigInteger.ZERO;
        } else {
            bigF = BigInteger.ONE;
            for (int i = 2; i <= n; i++) {
                bigF = bigF.multiply(BigInteger.valueOf(i));
            }
        }
        return bigF;
    }

    /**
     * Errechnet rekursiv die Fakultät einer gegebnen Zahl.
     *
     * @param n Zahl deren Fakultät berechnet wird
     * @return Fakultät der Zahl n
     */
    private BigInteger fakultaetRekusive(BigInteger n) {
        if (n.compareTo(BigInteger.ZERO) < 0) {
            bigF = BigInteger.ZERO;
        } else {
            if (n.compareTo(BigInteger.ZERO) == 0) {
                bigF = BigInteger.ONE;
            } else {
                bigF = n.multiply(fakultaetRekusive(n.subtract(BigInteger.ONE)));
            }
        }
        return bigF;
    }

    public void printFakultaetIterative(BigInteger bigN) {
        int n = bigN.intValue();
        System.out.println("\tDie Iterative-Fakultätsberehnung:");
        System.out.println("\t\tDie Fakultät von " + bigN + " beträgt: \t" + this.fakultaetIterative(n));
    }

    public void printFakultaetRekusive(BigInteger n) {
        System.out.println("\tDie Rekusive-Fakultätsberehnung:");
        System.out.println("\t\tDie Fakultät von " + n + " beträgt: \t" + this.fakultaetRekusive(n));
    }
}
