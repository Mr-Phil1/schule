/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turmvonhanoi;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class TurmVonHanoi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        Transport.transport(25, 'L', 'M', 'R');
        Transport.printStrich();
        long endTime = System.currentTimeMillis();
        long runTime = (endTime - startTime);
        System.out.println("Gesamtlaufzeit: " + runTime);
        Transport.printStrich();
    }
}
