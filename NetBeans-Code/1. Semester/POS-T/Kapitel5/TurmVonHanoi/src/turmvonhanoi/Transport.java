/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turmvonhanoi;

/**
 *
 * @author Mr-Phil
 */
public class Transport {

    public static long counter = 0;

    public static void transport(int n, char l, char m, char r) {
        if (n >= 1) {
            transport(n - 1, l, r, m);
            counter++;
            Transport.printTextScheibe(counter, n, l, m);
            transport(n - 1, r, m, r);
        }
    }

    public static void printStrich() {
        System.out.println("------------------------------------------------");
    }

    private static void printTextScheibe(long counter, int n, char l, char m) {
        System.out.println("Durchlauf: " + counter + " \tTransportiere Scheibe " + n + " von Säule " + l + " nach Säule " + m + ".");
    }
}
