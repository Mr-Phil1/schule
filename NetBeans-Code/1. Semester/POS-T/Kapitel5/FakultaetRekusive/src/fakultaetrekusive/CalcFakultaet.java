/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fakultaetrekusive;

/**
 *
 * @author Mr-Phil
 */
public class CalcFakultaet {

    int fakultaet(int n) {
        int f;
        if (n < 0) {
            f = 0;
        } else {
            if (n == 0) {
                f = 1; // Rekursionsbeginn 2)
            } else {
                f = n * fakultaet(n - 1); // Rekursionsschritt 1)
            }
        }
        return f;
    }
}
