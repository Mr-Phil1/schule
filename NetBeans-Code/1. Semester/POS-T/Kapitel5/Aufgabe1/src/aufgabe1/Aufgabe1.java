/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aufgabe1;

/**
 *
 * @author Mr-Phil
 */
public class Aufgabe1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//  // Beispiel 1: (call by Reference) 
        Punkt a = new Punkt(10, 5);
        Punkt b = a;
        a.x = 10;
        System.out.println(b.x); // x=10
        b.y = 5;
        System.out.println(a.y); // y=5
    }
//
//  // Beispiel 2: (call by Reference)
//        Punkt a = new Punkt(47, 11); // Koordinaten gesetzt (x=47, y=11)
//        clear(a);
//        System.out.println(a.x); // ?
//    }
//
//    public static void clear(Punkt p) {
//        p.setLocation(0, 0);
//    }

//  // Beispiel 3: (call by Reference)
//
//    
//        Punkt a = new Punkt(15, 23); // Koordinaten gesetzt (x=15,y=23)
//        foobar(a);
//        System.out.println(a.x); // ?
//    }
//
//    public static void foobar(Punkt p) {
//        p = new Punkt();
//    }
}
