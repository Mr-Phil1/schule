/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonacci;

/**
 *
 * @author Mr-Phil
 */
public class CalcInt {


    private int fibNteZahleInt(int n) {
        if (n == 1 || n == 0) {
            return n;
        }
        return fibNteZahleInt(n - 1) + fibNteZahleInt(n - 2);
    }

    public void ausgebenNteZahlInt(int n) {
        System.out.println("Die Fibonacci Zahle an der " + n + ". Stelle beträgt: " + this.fibNteZahleInt(n));
    }

}
