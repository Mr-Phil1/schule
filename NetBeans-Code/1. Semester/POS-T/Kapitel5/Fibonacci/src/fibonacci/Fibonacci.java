/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonacci;

import java.math.BigInteger;

/**
 *
 * @author Mr-Phil
 */
public class Fibonacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BigInteger fibZahl1 = new BigInteger("6");
        BigInteger fibZahl2 = new BigInteger("19");
        BigInteger fibZahl3 = new BigInteger("28");
        BigInteger fibZahl4 = new BigInteger("36");
        BigInteger fibZahl5 = new BigInteger("38");
        CalcInt c1 = new CalcInt();
        calcBigInt c2 = new calcBigInt();
        System.out.println("N-te Stelle mit Integer");
        c1.ausgebenNteZahlInt(6);
        c1.ausgebenNteZahlInt(19);
        c1.ausgebenNteZahlInt(28);
        c1.ausgebenNteZahlInt(36);
        c1.ausgebenNteZahlInt(38);
        System.out.println("-------------------------------------------------------");
        System.out.println("N-te Stelle mit BigInteger");
        c2.ausgebenNteZahlInt(fibZahl1);
        c2.ausgebenNteZahlInt(fibZahl2);
        c2.ausgebenNteZahlInt(fibZahl3);
        c2.ausgebenNteZahlInt(fibZahl4);
        c2.ausgebenNteZahlInt(fibZahl5);
    }

}
