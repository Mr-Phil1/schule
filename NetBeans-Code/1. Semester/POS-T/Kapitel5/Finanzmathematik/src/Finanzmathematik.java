
/**
 * Beschreiben Sie hier die Klasse Finanzmathematik.
 *
 * @author (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Finanzmathematik {

    private static double Z, K;

    /**
     * Mit dieser Funktion werden die zu erwarteten Zinsen mit Hilfe des
     * Kapitals, dem Zinssatz und der Tage berechnet.
     *
     * @param kapital Hier wird das für die Zinsberechnung notwendige Kapital
     * mitgegeben
     *
     * @param zinssatz Hier wird der für die Zinsberechnung notwendige Zinssatz
     * mitgegeben
     *
     * @param tage Hier werden die für die Zinsberechnung notwendige Tage
     * mitgegeben (Man beachte dass 1 Jahr in der Finanzberechnung 360 Tage hat)
     * @return Hier werden die zu erwarteten Zinsen zurückgegeben.
     */
    public static double berechneZins(double kapital, double zinssatz, int tage) {
        Z = (kapital * zinssatz * tage) / (100 * 360);
        return Z;
    }

    /**
     * Mit dieser Funktion wird das zu erwartete Kapital mit Hilfe der Jahre,
     * dem Zinssatz und dem vorhanden Kapital berechnet.
     *
     * @param kapital Hier wird das vorhandene Kapital mitgegeben.
     * @param zinssatz Hier wird der Zinssatz mitgegeben.
     * @param jahre Hier werden die Jahre mitgegeben.
     * @return Das zu erwartete Kapital wird danach wieder zurück gegeben.
     */
    public static double berechneKapital(double kapital, double zinssatz, double jahre) {
        K = kapital;
        for (double i = 1; i <= jahre; i++) {
            K = K + K * (zinssatz / 100);
        }
        if (jahre % 1 == 0) {
        } else {
            K = K + K * (zinssatz / 100 * (jahre % 1));
        }
        return K;
    }
}
