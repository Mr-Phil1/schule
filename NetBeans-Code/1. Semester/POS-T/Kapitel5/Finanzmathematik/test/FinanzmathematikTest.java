
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Die Test-Klasse FinanzmathematikTest.
 *
 * @author (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class FinanzmathematikTest {

    @Test
    public void berechneZinsTest() {
        assertEquals(69.791, Finanzmathematik.berechneZins(25000, 3.35, 30), 0.1);
        assertEquals(418.75, Finanzmathematik.berechneZins(25000, 3.35, 180), 0.1);
        assertEquals(837.5, Finanzmathematik.berechneZins(25000, 3.35, 360), 0.1);
    }

    @Test
    public void berechneKapitalTest() {
        assertEquals(40982.384, Finanzmathematik.berechneKapital(25000, 3.35, 15.0), 0.1);
        assertEquals(41668.839, Finanzmathematik.berechneKapital(25000, 3.35, 15.5), 0.1);
        assertEquals(42355.294, Finanzmathematik.berechneKapital(25000, 3.35, 16.0), 0.1);
    }
}
