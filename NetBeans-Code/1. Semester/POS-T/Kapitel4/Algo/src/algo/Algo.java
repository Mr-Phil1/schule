/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class Algo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int ANZAHL = 5;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        Scanner scan = new Scanner(System.in);
        for (int i = 1; i <= ANZAHL; i++) {
            System.out.print("Eingabe Zahl " + i + " von " + ANZAHL + ": ");
            int eingabe = scan.nextInt();
            if (eingabe < min) {
                min = eingabe;
            }
            if (eingabe > max) {
                max = eingabe;
            }
        }
        scan.close();
        System.out.println("min: " + min);
        System.out.println("max: " + max);
    }

}
