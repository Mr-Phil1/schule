/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beamersteuerung;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class BeamerSteuerung {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /*
        System.out.print("Bitte eine Zahl zwischen 10 und 30 eingeben: ");
        double x = scanner.nextDouble();
        if (x >= 10 && x <= 30) {
            System.out.println("Zahl gültig!");
        } else {
            System.out.println("Zahl ungültig!");
            System.exit(0); 
         */
        System.out.print("Bitte Quelle wählen (1 ist PC, 2 ist HDMI): ");
        int wahl = scanner.nextInt();
        if (wahl == 1 || wahl == 2) {
            System.out.println("Quelle " + wahl + " gewählt");
            if (wahl == 1) {
                System.out.println("Quelle PC gewählt.");
            }
            if (wahl == 2) {
                System.out.println("Quelle HDMI gewählt.");
            }
        } else {
            System.out.println("Ungültige Quelle");
        }

    }
}

