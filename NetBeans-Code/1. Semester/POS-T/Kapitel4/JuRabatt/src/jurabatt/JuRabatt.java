/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jurabatt;

import java.util.Scanner;
//import javax.swing.JOptionPane;

/**
 *
 * @author Mr-Phil
 */
public class JuRabatt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int input;
        long kn;
        double rabatt, ju;
        char n = 'n', c;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.print("Bitte geben Sie Ihre Kundennummer an: ");
            kn = scan.nextLong();
            System.out.print("Bitte geben Sie Ihren Jahresumsatz an: € ");
            ju = scan.nextDouble();
            if (ju > 10000) {
                rabatt = ju * 0.03;
            } else {
                rabatt = ju * 0.02;
            }
            System.out.println("Die Gutschrift für Ihrer Kundennummer " + kn + " beträgt: " + rabatt + " €");
//            input = JOptionPane.showConfirmDialog(null, "Wünschen Sie noch eine Berechnung?"); // 0= ja, 1= nein, 2=cancle
            System.out.print("Wünschen Sie noch eine Berechnung? (j oder n) ");
            c = scan.next().charAt(0);
        } while (c != n);
//        } while (input != 1 && input != 2);
        System.out.println("Vielen Herzlichen Dank!");
    }

}
