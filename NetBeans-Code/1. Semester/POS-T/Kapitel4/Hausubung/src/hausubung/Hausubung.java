/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hausubung;

/**
 *
 * @author Philipp
 */
public class Hausubung {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean a, b, c, var1, var2, var3, var4, var5, var6, var7, var8, var9;
        a = true;
        b = true;
        c = true;
        System.out.println("Punkt 5:\n");
        System.out.println("------------------------------------------------");
        System.out.print("Erste Operator:\t\t");
        System.out.println("&  >> und");
        System.out.print("Zweiter Operator:\t");
        System.out.println("|  >> oder");
        System.out.print("Dritter Operator:\t");
        System.out.println("!  >> nicht");
        System.out.print("Vierter Operator:\t");
        System.out.println("^  >> exklusives Oder");
        System.out.println("------------------------------------------------ \n");
        System.out.println("Punkt 6_1: ");
        System.out.println("Wert a = \t" + a);
        System.out.println("Wert b = \t" + b);
        System.out.println("Wert c = \t" + c);
        System.out.println("------------------------------------------------");
        var1 = a || b;
        var2 = !(a || b);
        var3 = ! !c;
        var4 = !(a || b) && ! !c;
        System.out.println("!(a||b): \t" + var1);
        System.out.println("!(a||b): \t" + var2);
        System.out.println("!!c: \t\t" + var3);
        System.out.println("!(a||b)&&!!c: \t" + var4);
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------ \n");
        System.out.println("Punkt 6_2: ");
        System.out.println("Wert a = \t" + a);
        System.out.println("Wert b = \t" + b);
        System.out.println("Wert c = \t" + c);
        System.out.println("------------------------------------------------");
        var5 = a & !b;
        var6 = !b & c;
        var7 = a & !b & c;
        var8 = c ^ c;
        var9 = (a & !b & c) || (c ^ c);
        System.out.println("a & !b: \t\t\t" + var5);
        System.out.println("!b & c: \t\t\t" + var6);
        System.out.println("a & !b & c: \t\t\t" + var7);
        System.out.println("c ^ c: \t\t\t\t" + var8);
        System.out.println("(a & !b & c) || (c ^ c): \t" + var9);
        System.out.println("------------------------------------------------");

    }

}
