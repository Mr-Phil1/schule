/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luftsteuerungbayer;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class LuftsteuerungBayer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Temperatur:");
        Scanner s = new Scanner(System.in);
        int TEMP = s.nextInt();
        System.out.println("Humidität:");
        int RH = s.nextInt();
        boolean Lüfter;
        if (RH < 0 || RH >= 100) {
            System.out.println("Eingabefehler");

        } else {
            if (TEMP < 12 || TEMP >= 24 && TEMP =< 50 || RH > 65) {
                Lüfter = true;
            } else {
                Lüfter = false;

            }

            System.out.println("Fan.Isrunning=" + Lüfter);
        }

    }
}
