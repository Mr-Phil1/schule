/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logik;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class Logik {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double temp = scan.nextDouble();
        System.out.print("A:\t");
        boolean a = scan.nextBoolean();
        System.out.print("B:\t");
        boolean b = scan.nextBoolean();
        boolean aUndB = a && b;
        boolean aOderB = a || b;
        boolean nichtA = !a;

        System.out.println("A|B:\t" + aUndB);
        System.out.println("A||B:\t" + aOderB);
        System.out.println("!A:\t" + nichtA);

        if (temp > 50) {
            System.out.println("zu groß");
        } else {
            System.out.println("ok");
        }

    }
}
