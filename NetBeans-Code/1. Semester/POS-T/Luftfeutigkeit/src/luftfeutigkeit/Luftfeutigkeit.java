/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luftfeutigkeit;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class Luftfeutigkeit {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int hun = 100;
        Scanner scan = new Scanner(System.in);
        System.out.print("Bitte geben Sie die Temperatur ein: ");
        int temp = scan.nextInt();
        System.out.print("Bitte geben Sie die Luftfeutigkeit ein: ");
        int luft = scan.nextInt();
        if (luft < 0 || luft >= hun) {
            System.out.println("Eingabefehler");
        } else {
            if (temp < 12 || temp >= 24 && temp <= 50 || luft >= 65) {
                System.out.println("Der Lüfter wird eingeschalten.");
            } else {
                System.out.println("Der Lüfter wird ausgeschalten.");
            }

        }
    }
}
