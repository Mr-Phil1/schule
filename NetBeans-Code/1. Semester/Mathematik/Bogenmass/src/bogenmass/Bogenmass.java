/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bogenmass;

/**
 *
 * @author Philipp
 */
public class Bogenmass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int grad;
        double varWert, varSin, varCos, ergebnis, bogenmass;
        grad = 30;
        varWert = 0.5235;
        bogenmass = grad*Math.PI/180;
        varSin = Math.sin(varWert);
        varCos = Math.cos(varWert);
        ergebnis = Math.pow(varSin, 2) + Math.pow(varCos, 2);
        System.out.println("Sinus: \t\t"+varSin);
        System.out.println("Cosinus: \t" +varCos);
        System.out.println("Ergebnis: \t" +ergebnis);
        System.out.println("30 Crad in Bogenmass sind: " + bogenmass+".");
    }
    
}
