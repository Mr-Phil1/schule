/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmi;

import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class BMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double gewicht, groeße;
        int alter;
        char w = 'w', m = 'm';
        Rechner calc = new Rechner();
        Scanner scan = new Scanner(System.in);
        System.out.println("*************************************");
        System.out.println("*Herzlich Willkommen zum BMI-Rechner*");
        System.out.println("*************************************");
        System.out.print("Bitte geben Sie Ihr Alter an: ");
        alter = scan.nextInt();
        if (alter >= 19) {
            System.out.print("Bitte geben Sie Ihr Geschlecht aus (w oder m): ");
            char c = scan.next().charAt(0);
            if (c == m || c == w) {
                System.out.print("Bitte geben Sie Ihr Körpergewicht in kg an: ");
                gewicht = scan.nextDouble();
                System.out.print("Bitte geben Sie Ihre Körpergröße in cm an: "); // beim deu nicht . sonder ,
                groeße = scan.nextDouble();
                if (c == w) {
                    calc.ausgebenW(gewicht, groeße);
                } else {
                    calc.ausgebenM(gewicht, groeße);
                }
            } else {
                System.out.println("Bitte geben Sie das Geschlecht korrekt ein.");
            }
        } else {
            System.out.println("BMI kann nur für Erwachsene ab 19 Jahren berechnet werden.");
        }
    }
}
