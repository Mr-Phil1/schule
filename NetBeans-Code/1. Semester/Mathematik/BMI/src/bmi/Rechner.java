/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bmi;

/**
 *
 * @author Mr-Phil
 */
public class Rechner {
private double bmi;

    private double berechnungBmi(double kg, double cm) {
        double m = cm / 100;
        bmi = kg / Math.pow(m, 2);
        return bmi;
    }

    /**
     *
     * @param kg Geben Sie Ihr Gewicht in kg an.
     * @param cm Geben Sie Ihre Größe in cm an. Im Anschluss wird dann Ihr BMI
     * berrechntet mit:
     *
     * <pre> {@code
     * double m = cm / 100;
     * bmi = kg / Math.pow(m, 2);
     * return bmi;} </pre>
     */
    public void ausgebenM(double kg, double cm) {
        bmi = this.berechnungBmi(kg, cm);
        if (bmi < 20) {
            System.out.println("BMI: " + bmi + "(Untergewicht)");
        } else if (bmi < 26) {
            System.out.println("BMI: " + bmi + "(Normalgewicht)");
        } else if (bmi < 31) {
            System.out.println("BMI: " + bmi + "(Übergewicht)");
        } else if (bmi <= 40) {
            System.out.println("BMI: " + bmi + "(Adipositas)");
        } else {
            System.out.println("BMI : " + bmi + "(starke Adipositas)");
        }
    }

    /**
     *
     * @param kg Geben Sie Ihr Gewicht in kg an.
     * @param cm Geben Sie Ihre Größe in cm an. Im Anschluss wird dann Ihr BMI
     * berrechntet mit:
     *
     * <pre> {@code
     * double m = cm / 100;
     * bmi = kg / Math.pow(m, 2);
     * return bmi;} </pre>
     */
    public void ausgebenW(double kg, double cm) {
        bmi = this.berechnungBmi(kg, cm);
        if (bmi < 19) {
            System.out.println("BMI: " + bmi + "(Untergewicht)");
        } else if (bmi < 25) {
            System.out.println("BMI: " + bmi + "(Normalgewicht)");
        } else if (bmi < 31) {
            System.out.println("BMI: " + bmi + "(Übergewicht)");
        } else if (bmi <= 40) {
            System.out.println("BMI: " + bmi + "(Adipositas)");
        } else {
            System.out.println("BMI: " + bmi + "(starke Adipositas)");
        }
    }
}
