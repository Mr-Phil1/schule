/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euklidischenalgorithmus;

/**
 *
 * @author Mr-Phil
 */
public class Calc {

    private int rechnen(int a, int b) {
        while (b != 0) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    public void ausgeben(int a, int b) {
        System.out.println("Der größte gemeinsame Teiler von " + a + " und " + b + " beträgt: " + this.rechnen(a, b));
    }
}
