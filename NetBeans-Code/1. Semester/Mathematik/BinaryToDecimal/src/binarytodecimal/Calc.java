/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarytodecimal;

/**
 *
 * @author Philipp
 */
public class Calc {

    private long rest, stelle, dezimal;

    private long convert(long binary) {
        while (binary != 0) {
            rest = binary % 10;
            binary = binary / 10;
            dezimal += rest * Math.pow(2, stelle);
            stelle = stelle +1;
        }
        return dezimal;
        
    }

    public void ausgeben(long binary) {
        System.out.println("Ihre Binärzahl war: " + binary + ", in Dezimal ist das: " + convert(binary));
    }
}
