/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ganzzahldivision;

import java.util.Scanner;

/**
 *
 * @author Philipp
 */
public class GanzzahlDivision {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int y, x, r, q = 0;
        Scanner scan = new Scanner(System.in);
        System.out.print("Bitte geben Sie den Dividend ein: ");
        x = scan.nextInt();
        System.out.print("Bitte geben Sie den Divisor ein: ");
        y = scan.nextInt();
        while (x >= y) {
            q++;
            x -= y;
        }
        r = x;
        scan.close();
        System.out.println("Ihr Quotient ist " + q + ", und Ihr Rest ist " + r + ".");
    }

}
