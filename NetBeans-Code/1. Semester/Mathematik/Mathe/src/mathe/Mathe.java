/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathe;

/**
 *
 * @author Philipp
 */
public class Mathe {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Methoden einfügen
        Pythagoras py1 = new Pythagoras();
        Kreis kr1 = new Kreis();
        Fakultaet f1 = new Fakultaet();
        Euklidischen ggt1 = new Euklidischen();
        // Pythagoras
        py1.ausgebenLC(9, 16);
        // Kreis
        kr1.ausgebenFlacheR(10);
        kr1.ausgebenFlacheD(20);
        //Fakutält
        f1.ausgeben(10);
        f1.ausgeben(9);
        f1.ausgeben(8);
        f1.ausgeben(7);
        f1.ausgeben(6);
        f1.ausgeben(5);
        f1.ausgeben(4);
        f1.ausgeben(3);
        f1.ausgeben(2);
        //Euklidischen
        ggt1.ausgebenGgt(0, 30);
    }

}
