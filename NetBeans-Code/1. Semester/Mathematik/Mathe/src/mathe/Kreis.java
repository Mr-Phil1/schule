/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathe;

/**
 *
 * @author Philipp
 */
public class Kreis {

    private double pi = Math.PI;
    private double flache;

    /**
     *
     * @param radius Der Durchmesser in mm.
     * @return Die Fläche des Kreise in mm².
     */
    private double flacheR(double radius) {
        flache = (pi * (radius * radius));
        return this.flache;
    }

    /**
     *
     * @param r Gibt den Flächeninhalt des Kreises mit dem Radius r zurück.
     */
    public void ausgebenFlacheR(double r) {
        System.out.println("Der Flacheninhalt ist: " + this.flacheR(r) + " mm²");
    }

    /**
     *
     * @param durchmesser Der Durchmesser in mm.
     * @return Die Fläche des Kreise in mm².
     */
    private double flacheD(double durchmesser) {
        flache = (pi * (durchmesser * durchmesser)) / 4;
        return this.flache;
    }

    /**
     *
     * @param d Gibt den Flächeninhalt des Kreises mit dem Durchmesser d zurück.
     */
    public void ausgebenFlacheD(double d) {
        System.out.println("Der Flacheninhalt ist: " + this.flacheD(d) + " mm²");
    }
}
