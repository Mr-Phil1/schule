/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathe;

/**
 *
 * @author Philipp
 */
public class Pythagoras {

    private double seiteC;
    private double langeC;

    /**
     *
     * @param langeA
     * @param langeB
     * @return Die Hypothenuse mit den Zahlen von langeA, langeB
     */
    private double langeC(double langeA, double langeB) {
        langeC = Math.sqrt(langeA * langeA + langeB * langeB);
        return langeC;
    }
/**
 * 
 * @param langeA
 * @param langeB 
 */
    public void ausgebenLC(double langeA, double langeB) {
        System.out.println("Lange von C: " + this.langeC(langeA, langeB));
    }

    private double seiteVonC(double seiteA, double seiteB) {
        seiteC = Math.pow(seiteA, 2) + Math.pow(seiteB, 2);
        return this.seiteC;
    }
/**
 * 
 * @param seiteA
 * @param seiteB 
 */
    public void ausgebenSC(double seiteA, double seiteB) {
        System.out.println("Seite von C: " + this.seiteVonC(seiteA, seiteB));
    }
}
