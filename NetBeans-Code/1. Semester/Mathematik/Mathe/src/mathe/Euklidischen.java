/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathe;

/**
 *
 * @author Mr-Phil
 */
public class Euklidischen {

    private int rechnenGgt(int a, int b) {
        if (a != 0) {
            while (b != 0) {
                if (a > b) {
                    a = a - b;
                } else {
                    b = b - a;
                }
            }
        }

        return a;
    }

    public void ausgebenGgt(int a, int b) {
        System.out.println("Der größte gemeinsame Teiler von " + a + " und " + b + " beträgt: " + this.rechnenGgt(a, b));
    }
}
