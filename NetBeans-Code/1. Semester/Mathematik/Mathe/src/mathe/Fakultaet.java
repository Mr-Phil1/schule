/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathe;

/**
 *
 * @author Philipp
 */
public class Fakultaet {

    private int erg = 1, i, f;

    /**
     *
     * @param n Berechnet die Fakultät von n
     * @return Gibt den berechneten Wert zurück.
     */
    private int calc(int n) {
        if (n < 0) {
            f = 0; // 1)
        } else {
            f = 1;
            for (i = 2; i <= n; i++) {
                f = f * i; // 2)
            }
        }
        return f;
    }
//
//    /**
//     *
//     * @param n Berechnet die Fakultät von n
//     * @return Gibt den berechneten Wert zurück.
//     */
//    private int calc(int n) {
//        for (i = 1; i == n; i++) {
//            erg = erg * i;
//        }
//        return erg;
//    }

    /**
     * @param n Berechnet die Fakultät von n      <pre> {@code void ausgeben(int n) {
     *  System.out.println("Ihre Zahl ist " + this.calc(n) + ".");}
     * }</pre>
     *
     * <pre> {@code
     *private int calc(int n) {
     * for (i = 1; i <= n; i++) {
     * erg = erg * i;
     * }
     * return erg;
     * }}</pre>
     */
    public void ausgeben(int n) {
        System.out.println("Ihre Zahl ist " + this.calc(n) + ".");
    }

}
