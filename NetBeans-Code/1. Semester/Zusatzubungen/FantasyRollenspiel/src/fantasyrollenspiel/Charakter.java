/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fantasyrollenspiel;

/**
 *
 * @author Mr-Phil
 */
public class Charakter {

    private int starke, gesundheit, glueck, check = 15;
    private String name;

    /**
     *
     * @return Gibt den Wert der Stärke zurück
     */
    public int getValueStarke() {
        return this.starke;
    }

    /**
     *
     * @return Gibt den Wert der Gesundheit zurück
     */
    public int getValueGesundheit() {
        return this.gesundheit;
    }

    /**
     *
     * @return Gibt den Wert des Glückes zurück
     */
    public int getValueGlueck() {
        return this.glueck;
    }

    public String getValueName() {
        return this.name;
    }

    private void setValueStarke(int starke) {
        this.starke = starke;
    }

    private void setValueGesundheit(int gesund) {
        this.gesundheit = gesund;
    }

    private void setValueGlueck(int glueck) {
        this.glueck = glueck;
    }

    public String setValueName(String name) {
        return this.name = name;
    }

    /**
     * Setzt alle Werte für Stärke, Glück und Gesundheit, überprüft ob diese
     * Werte die Gesammtzahl 15 nicht überschreiten, wenn die Gesammtzahl doch
     * den Wert überschreitet werden alle Werte auf 5 gesetzt.
     *
     * @param starke Gibt den Wert der Stärke durch
     * @param gesund Gibt den Wert der Gesundheit durch
     * @param glueck Gibt den Wert des Glücks durch
     */
    public void setAll(int starke, int gesund, int glueck) {
        int checkAll = starke + gesund + glueck;
        if (checkAll > check) {
            this.starke = 5;
            this.glueck = 5;
            this.gesundheit = 5;
        } else {
            this.setValueGesundheit(gesund);
            this.setValueGlueck(glueck);
            this.setValueStarke(starke);
        }
    }

    /**
     * Dient nur zum Debugen
     */
    public void debugSetValue() {
        System.out.println("Debug: \n\tName: \t\t" + this.name + "\n\tStärke: \t" + this.starke + "\n\tGesundheit: \t" + this.gesundheit + "\n\tGlück: \t\t" + this.glueck);
    }

}
