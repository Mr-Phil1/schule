/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fantasyrollenspiel;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Mr-Phil
 */
public class Spiel {

    private int starke, gluck, gesund, ten = 10;
    private final Random r1 = new Random();
    private final Scanner scan = new Scanner(System.in);
    private final Charakter c1 = new Charakter();
    private final Charakter c2 = new Charakter();

    /**
     * Generiert alle Charakter
     */
    private void charakterGenerieren() {
        this.spielerGenerieren();
        this.gegnerGenerieren();
    }

    /**
     * Generiert die Charakter für den Spieler
     */
    private void spielerGenerieren() {
        System.out.print("Bitte geben Sie einen Namen an: ");
        String name = scan.next();
        c1.setValueName(name);
        do {
            System.out.print("Bitte geben Sie einen Wert für die Stärke an: ");
            starke = scan.nextInt();
            System.out.print("Bitte geben Sie einen Wert für die Gesundheit an: ");
            gesund = scan.nextInt();
            System.out.print("Bitte geben Sie einen Wert für das Glück an: ");
            gluck = scan.nextInt();
        } while (starke > ten || gesund > ten || gluck > ten);
        c1.setAll(starke, gesund, gluck);
    }

    /**
     * Generiert die Charakter für den Gegener
     */
    private void gegnerGenerieren() {
        String name = "FredFinger";
        c2.setValueName(name);
        do {
            starke = r1.nextInt(11);
            gesund = r1.nextInt(11);
            gluck = r1.nextInt(11);
        } while (starke > ten || gesund > ten || gluck > ten);
        c2.setAll(starke, gesund, gluck);
    }

    /**
     * Dient für das Starten des Kampfes
     */
    public void derKampf() {
        this.charakterGenerieren();
        c1.debugSetValue();
        c2.debugSetValue();
    }
}
