package nebenlaeufigkeit.Race_Conditions;
import java.util.LinkedList;

public class _02_Race_2_join{
    public static void main(String[] args) {

        final int THREADS_N = 10;

        LinkedList<Thread> tList = new LinkedList<>();
        Runner run = new Runner();
        for(int i = 0; i < THREADS_N; i++){
            Thread t = new Thread(run);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner implements Runnable {

    final int COUNT_TIMES = 1000;

    @Override
    public void run() {
        for(int i = 0; i < COUNT_TIMES; i++){
            count();
        }
    }

    synchronized void count(){
        Storage.counter ++;
    }
}
