package nebenlaeufigkeit.Race_Conditions;

public class _01_Race_1 {
    public static void main(String[] args) {
        final int COUNT_TIMES = 1000;
        Runnable runner = () -> {
            for (int i = 0; i < COUNT_TIMES; i++) {
                Storage.counter++;
            }
        };

        final int THREADS_N = 10;
        for (int i = 0; i < THREADS_N; i++) {

        }
    }
}

class Storage {
    public static long counter;
}
