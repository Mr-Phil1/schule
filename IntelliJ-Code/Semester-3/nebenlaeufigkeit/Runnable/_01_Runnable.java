package nebenlaeufigkeit.Runnable;

public class _01_Runnable {
    public static void main(String[] args) {
        int job_runs = 10000;
        Thread t1 = new Thread(new Job(job_runs), "Job 1");
        Thread t2 = new Thread(new Job(job_runs), "Job 2");
        t1.start();
        t2.start();
    }
}

class Job implements Runnable {

    private static long counter;
    int runs;
    String threadName;

    public Job(int runs) {
        this.runs = runs;
    }

    @Override
    public void run() {
        for (int i = 0; i < runs; i++) {
            threadName = Thread.currentThread().getName();
            incrementCounter();
            System.out.println(threadName + ": " + i);
        }
        System.out.println("The global counter is: " + counter);
        System.out.println("----------------------------------");
    }

    static synchronized void incrementCounter() {
        counter++;
    }

    private void strich(int lange) {
        for (int i = 0; i < lange; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

}
