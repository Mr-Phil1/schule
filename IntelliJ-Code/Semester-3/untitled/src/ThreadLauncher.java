public class ThreadLauncher {
    public static void main(String[] args) {
        Thread t1 = new Thread(new PrintDate("#"));
        //Thread t2 = new Thread(new CounterCommand());
        Thread t2 = new Thread(new PrintDate("*"));

        t1.run();
        t2.run();
    }
}

class PrintDate implements Runnable {
    String symb;
    public PrintDate (String symb){
        this.symb = symb;
    }
    @Override
    public void run() {
        for(int i = 0; i < 10; i++){
            System.out.print(symb+" ");
            System.out.println(new java.util.Date());
        }
    }
}