package ProjektSem1.Mathe;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Mathe.Bereiche.Euklidischen;
import ProjektSem1.Mathe.Bereiche.Fakultaet;
import ProjektSem1.Mathe.Bereiche.Kreis;
import ProjektSem1.Mathe.Bereiche.Pythagoras;
import ProjektSem1.Mathe.Texte.BereichTitle;
import ProjektSem1.Mathe.Texte.TexteMathe;
import ProjektSem1.Texte.Texte;


public class MatheMenu {
    private int menuValue;
    private boolean ende;
    private String regex = "[0-9]+[.]*[0-9]*";
    private Pythagoras pythagoras = new Pythagoras();
    private Kreis kreis = new Kreis();
    private Fakultaet fakultaet = new Fakultaet();
    private Euklidischen euklidischen = new Euklidischen();


    public void Pythagoras() {
        while (!ende) {
            Texte.printStriche();
            BereichTitle.BereichsTitlePythagoras();
            TexteMathe.MenuTextSubPythagoras();
            menuValue = Input.inputInt(regex);
            switch (menuValue) {
                case 1: // Berechnung der Hypothenusenlänge von C
                    Texte.printStriche();
                    TexteMathe.menuTextEingabe("Längen");
                    pythagoras.ausgebenLC(Input.inputDouble(regex), Input.inputDouble(regex));
                    break;
                case 2: // Berechnung des Fläche vom C
                    Texte.printStriche();
                    TexteMathe.menuTextEingabe("Seiten");
                    pythagoras.ausgebenSC(Input.inputDouble(regex), Input.inputDouble(regex));
                    break;
                case 3: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 3");
                    break;
            }
        }
        ende = false;
    }

    public void Kreis() {
        while (!ende) {
            Texte.printStriche();
            BereichTitle.BereichsTitleKreis();
            TexteMathe.MenuTextSubKreis();
            menuValue = Input.inputInt(regex);
            switch (menuValue) {
                case 1: // Berechnung des Flächeninhalts mit Hilfe des Radius
                    Texte.printStriche();
                    TexteMathe.MenuTextKreis(1);
                    kreis.ausgebenFlacheR(Input.inputDouble(regex));
                    break;
                case 2: // Berechnung des Flächeninhalts mit Hilfe des Durchmessers
                    Texte.printStriche();
                    TexteMathe.MenuTextKreis(2);
                    kreis.ausgebenFlacheD(Input.checkDouble(regex));
                    break;
                case 3: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 3");
                    break;
            }
        }
        ende = false;
    }

    public void Fakultaet() {
        while (!ende) {
            Texte.printStriche();
            BereichTitle.BereichsTitleFakultaet();
            TexteMathe.MenuTextSubFakultaet();
            menuValue = Input.inputInt(regex);
            switch (menuValue) {
                case 1: // Berechnung der Fakultät
                    Texte.printStriche();
                    fakultaet.printFakultaetIterative(TexteMathe.MenuEingabeInt("eine", "[0-9]+"));
                    break;
                case 2: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 2");
                    break;
            }
        }
        ende = false;
    }

    public void Euklidischen() {
        while (!ende) {
            Texte.printStriche();
            BereichTitle.BereichsTitleEuklidischen();
            TexteMathe.MenuTextSubEuklidische();
            menuValue = Input.inputInt(regex);
            switch (menuValue) {
                case 1: // Größter gemeinsamen Teiler berechnen,
                    Texte.printStriche();
                    euklidischen.ausgebenGgt(TexteMathe.MenuEingabeInt("die 1.", "[0-9]+"), TexteMathe.MenuEingabeInt("die 2.", "[0-9]+"));
                    break;
                case 2: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 2");
                    break;
            }
        }
        ende = false;
    }
}
