package ProjektSem1.Mathe;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Mathe.Texte.TexteMathe;
import ProjektSem1.Texte.Texte;


public class Mathe {
    private int menuValue;
    private boolean ende;
    MatheMenu matheMenu = new MatheMenu();


    public Mathe() {
        while (!ende) {
            BereichsLogos.BereichsLogoMathe();
            TexteMathe.MenuTextMain();
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Pythagoras
                    matheMenu.Pythagoras();
                    break;
                case 2: // Kreis
                    matheMenu.Kreis();
                    break;
                case 3: // Fakultaet
                    matheMenu.Fakultaet();
                    break;
                case 4: // Euklidischen
                    matheMenu.Euklidischen();
                    break;
                case 5: // beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                    break;
            }
        }
        ende = false;
    }
}
