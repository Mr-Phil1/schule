package ProjektSem1.Mathe.Bereiche;

import ProjektSem1.Texte.Texte;

public class Pythagoras {
    private double seiteC;
    private double langeC;

    /**
     * @param langeA
     * @param langeB
     * @return Die Hypothenuse mit den Zahlen von langeA, langeB
     */
    private double langeC(double langeA, double langeB) {
        langeC = Math.sqrt(langeA * langeA + langeB * langeB);
        return langeC;
    }

    /**
     * @param langeA
     * @param langeB
     */
    public void ausgebenLC(double langeA, double langeB) {
        System.out.println("Durch ihre Eingeben für die Längen A " + langeA + " und B " + langeB + " ergibt sich für die Länge C folgender Wert: " + Texte.patternDoubleText(this.langeC(langeA, langeB),"#,###.##")+" cm.");
    }

    private double seiteVonC(double seiteA, double seiteB) {
        seiteC = Math.pow(seiteA, 2) + Math.pow(seiteB, 2);
        return this.seiteC;
    }

    /**
     * @param seiteA
     * @param seiteB
     */
    public void ausgebenSC(double seiteA, double seiteB) {
        System.out.println("Durch ihre Eingeben für die Seiten A " + seiteA + " und B " + seiteB + " ergibt sich für die Seite C folgender Wert: " +Texte.patternDoubleText(this.seiteVonC(seiteA, seiteB),"#,###.##")+" cm.");
    }
}
