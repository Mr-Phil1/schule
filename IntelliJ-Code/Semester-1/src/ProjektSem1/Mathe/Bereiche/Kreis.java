package ProjektSem1.Mathe.Bereiche;

import ProjektSem1.Texte.Texte;

public class Kreis {
    private double pi = Math.PI;
    private double flache;

    /**
     * @param radius Der Durchmesser in cm.
     * @return Die Fläche des Kreise in cm².
     */
    private double flacheR(double radius) {
        flache = (pi * (radius * radius));
        return this.flache;
    }

    /**
     * @param r Gibt den Flächeninhalt des Kreises mit dem Radius r zurück.
     */
    public void ausgebenFlacheR(double r) {
        System.out.println("Der Flacheninhalt ist: " + Texte.patternDoubleText(this.flacheR(r), "#,###.##") + " cm²");
    }

    /**
     * @param durchmesser Der Durchmesser in cm.
     * @return Die Fläche des Kreise in cm².
     */
    private double flacheD(double durchmesser) {
        flache = (pi * (durchmesser * durchmesser)) / 4;
        return this.flache;
    }

    /**
     * @param d Gibt den Flächeninhalt des Kreises mit dem Durchmesser d zurück.
     */
    public void ausgebenFlacheD(double d) {
        System.out.println("Der Flacheninhalt ist: " + Texte.patternDoubleText(this.flacheD(d), "#,###.##") + " cm²");
    }
}
