package ProjektSem1.Mathe.Bereiche;

import java.math.BigInteger;

public class Fakultaet {
    private int erg = 1, i, f;
    private BigInteger bigF, bigN;
    private BigInteger bigI = new BigInteger("2");

    /**
     * @param n Berechnet die Fakultät von n
     * @return Gibt den berechneten Wert zurück.
     */
    private int calc(int n) {
        if (n < 0) {
            f = 0; // 1)
        } else {
            f = 1;
            for (i = 2; i <= n; i++) {
                f = f * i; // 2)
            }
        }
        return f;
    }

    /**
     * @param n Berechnet die Fakultät von n
     *          <pre> {@code void ausgeben(int n) {
     *                 System.out.println("Ihre Zahl ist " + this.calc(n) + ".");}
     *               }</pre> <pre> {@code
     *              private int calc(int n) {
     *            for (i = 1; i <= n; i++) {
     *                 erg = erg * i;}
     *              return erg;}}</pre>
     */
    public void ausgeben(int n) {
        System.out.println("Die Fakultät von " + n + " entspricht: " + this.calc(n) + ".");
    }


    /**
     * Errechnet iterative die Fakultät einer gegebnen Zahl.
     *
     * @param n Zahl deren Fakultät berechnet wird
     * @return Fakultät der Zahl n
     */
    private BigInteger fakultaetIterative(int n) {
        if (n < 0) {
            bigF = BigInteger.ZERO;
        } else {
            bigF = BigInteger.ONE;
            for (int i = 2; i <= n; i++) {
                bigF = bigF.multiply(BigInteger.valueOf(i));
            }
        }
        return bigF;
    }

    public void printFakultaetIterative(int n) {
        System.out.println("\tDie Fakultät von " + n + " beträgt: " + this.fakultaetIterative(n));
    }

}
