package ProjektSem1.Mathe.Texte;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;

public class TexteMathe {
    public static void MenuTextMain() {
        Texte.fortfahren();
        System.out.println("\t1) Pythagoras-Berechnungen  \t2) Kreis-Berechnungen  \t\t3) Fakultaet-Berechnungen  \t\t4) Euklidische-Berechnungen  \t\t5) Programm beenden");
    }

    public static void MenuTextSubPythagoras() {
        Texte.fortfahren();
        System.out.println("\t1) Berechnung der Hypothenusenlänge von C  \t2) Berechnung des Fläche vom C  \t3) Ende");
    }

    public static void MenuTextSubKreis() {
        Texte.fortfahren();
        System.out.println("\t1) Berechnung des Flächeninhalts mit Hilfe des Radius  \t2) Berechnung des Flächeninhalts mit Hilfe des Durchmessers  \t3) Ende");
    }

    public static void MenuTextSubFakultaet() {
        Texte.fortfahren();
        System.out.println("\t1) Berechnung der Fakultät  \t2) Ende");
    }

    public static void MenuTextSubEuklidische() {
        Texte.fortfahren();
        System.out.println("\t1) Größter gemeinsamen Teiler berechnen  \t2) Ende");
    }

    public static void menuTextEingabe(String eingabe) {
        System.out.println("Bitte geben Sie im Anschluss nun die Werte für die " + eingabe + " A und B ein. (in cm)");
    }

    public static void MenuTextKreis(int value) {
        System.out.println("Bitte geben Sie den " + (value == 1 ? "Radius" : "Durchmesser") + " für die Berechnung in in cm ein.");
    }

    public static int  MenuEingabeInt(String position, String regex) {
        System.out.println("Bitte geben Sie nun " + position + " Zahl ein.");
        return Input.inputInt(regex);
    }

}
