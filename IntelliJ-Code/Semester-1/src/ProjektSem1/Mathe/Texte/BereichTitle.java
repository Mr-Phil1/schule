package ProjektSem1.Mathe.Texte;

public class BereichTitle {
    public static void BereichsTitlePythagoras() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+\n" +
                " |P|y|t|h|a|g|o|r|a|s|\n" +
                " +-+-+-+-+-+-+-+-+-+-+\n\n");
    }

    public static void BereichsTitleKreis() {
        System.out.print(" +-+-+-+-+-+\n" +
                " |K|r|e|i|s|\n" +
                " +-+-+-+-+-+\n\n");
    }

    public static void BereichsTitleFakultaet() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+\n" +
                " |F|a|k|u|l|t|a|e|t|\n" +
                " +-+-+-+-+-+-+-+-+-+\n\n");
    }

    public static void BereichsTitleEuklidischen() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |E|u|k|l|i|d|i|s|c|h|e|n|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+\n\n");
    }

}
