package ProjektSem1.Umrechner.Texte.Waehrung;

import ProjektSem1.FileIO.FileRead;
import ProjektSem1.Texte.Texte;

public class MenuText {


    public static void MenuTextMainWaehrung() {
        Texte.fortfahren();
        System.out.println("\t1) Währung-Umrechnen  \t2) Info  \t3) Programm beenden");
    }

    public static void MenuTextEpoche() {
        Texte.fortfahren();
        System.out.println("\t1) Modern-Währung  \t2) Antik-Währung  \t3) Programm beenden");
    }

    public static void MenuTextUmrechnerAntik() {
        System.out.println("Mit welcher Währung möchten Sie rechnen?");
        System.out.println("\t\t01) Öster.-Schilling  \t\t02) Belgischer-Franc  \t\t03) Zypern-Pfund  \t\t\t04) Deutsche-Mark  \t\t\t\t05) Estnische Krone " +
                "\n \t\t06) Spanische Peseta  \t\t07) Finnmark  \t\t\t\t08) Französischer Franc \t09) Griechische Drachme  \t\t10) Irisches Pfund " +
                "\n \t\t11) Italienische Lira  \t\t12) Litauische Litas  \t\t13) Luxemburgischer Franc  \t14) Lettischer Lats  \t\t\t15) Maltesische Lira " +
                " \n \t\t16) Holländischer Gulden  \t17) Portugiesischer Escudo  18) Slowakische Krone  \t\t19) Slowenischer Tolar  \t\t20) Ende");
    }

    public static void MenuTextUmrechnerModern() {
        System.out.println("Mit welcher Währung möchten Sie rechnen?");
        System.out.println("\t\t1) Schweizer-Franken \t\t2) US-Dollar \t\t3) Japan-Yen \t\t4) Ende");
    }

    public static void MenuTextInfo() {
        Texte.fortfahren();
        System.out.println("\t\t1) Quellen anzeigen  \t\t2) Ende");
    }


    public static void MenuTextSub(String waehrung) {
        Texte.fortfahren();
        System.out.println("\t1) Kurs Ausgaben  \t2) Umrechnung von Euro in " + waehrung + "  \t3) Umrechnung von " + waehrung + " in Euro  \t4) Kurs ändern  \t5) Ende");
    }

    public static void MenuTextSubKurs(String waehrung) {
        Texte.fortfahren();
        System.out.println("\t1) Kurs Ändern Euro -> " + waehrung + " \t2) Kurs Ändern " + waehrung + " -> Euro \t3) Auf Referenz-Kurs zurücksetzen  \t4) Kurs anzeigen  \t5) Ende");
    }

    public static void MenuTextSubKursAntik(String waehrung) {
        Texte.fortfahren();
        System.out.println("\t1) Kurs Ändern Euro <-> " + waehrung + " \t2) Auf Referenz-Kurs zurücksetzen  \t3) Kurs anzeigen  \t4) Ende");
    }
    public static void ausgabeKurs(String filePath, String fileName, String fileExt, String waehrung) {
        System.out.println("Der Wechselkurs " + waehrung + " mit Stand " +
                FileRead.FileLastModifiedTime(filePath, fileName, fileExt) + " beträgt: "
                + Texte.patternDoubleText(FileRead.FileReadDouble(filePath, fileName, fileExt), "###,###.######") + ".");
    }

    public static void neuerKursGesetzt(String kurs, String waehrung) {
        System.out.println("Der neu eingegeben Kurs von " + kurs + " " + waehrung + " wurde gesetzt.");
    }

    public static void referenzKursSetzten() {
        System.out.println("Der Referenz Kurs wurde gesetzt.");
    }

    public static void FileErstelltText(String filePath, String fileName, String fileExt) {
        System.out.println("Die Kurs-Datei wurde unter \"" + FileRead.FilePath(filePath, fileName, fileExt) + "\" erstellt");
    }

    public static void BereichsLogoUmrechnerWaehrung() {
        Texte.printStriche();
        System.out.println("$$\\      $$\\   $\\ $\\   $$\\                                                                                       $$\\                                     \n" +
                "$$ | $\\  $$ |  \\_|\\_|  $$ |                                                                                      $$ |                                    \n" +
                "$$ |$$$\\ $$ | $$$$$$\\  $$$$$$$\\   $$$$$$\\  $$\\   $$\\ $$$$$$$\\   $$$$$$\\   $$$$$$$\\  $$$$$$\\   $$$$$$\\   $$$$$$$\\ $$$$$$$\\  $$$$$$$\\   $$$$$$\\   $$$$$$\\  \n" +
                "$$ $$ $$\\$$ | \\____$$\\ $$  __$$\\ $$  __$$\\ $$ |  $$ |$$  __$$\\ $$  __$$\\ $$  _____|$$  __$$\\ $$  __$$\\ $$  _____|$$  __$$\\ $$  __$$\\ $$  __$$\\ $$  __$$\\ \n" +
                "$$$$  _$$$$ | $$$$$$$ |$$ |  $$ |$$ |  \\__|$$ |  $$ |$$ |  $$ |$$ /  $$ |\\$$$$$$\\  $$ |  \\__|$$$$$$$$ |$$ /      $$ |  $$ |$$ |  $$ |$$$$$$$$ |$$ |  \\__|\n" +
                "$$$  / \\$$$ |$$  __$$ |$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ | \\____$$\\ $$ |      $$   ____|$$ |      $$ |  $$ |$$ |  $$ |$$   ____|$$ |      \n" +
                "$$  /   \\$$ |\\$$$$$$$ |$$ |  $$ |$$ |      \\$$$$$$  |$$ |  $$ |\\$$$$$$$ |$$$$$$$  |$$ |      \\$$$$$$$\\ \\$$$$$$$\\ $$ |  $$ |$$ |  $$ |\\$$$$$$$\\ $$ |      \n" +
                "\\__/     \\__| \\_______|\\__|  \\__|\\__|       \\______/ \\__|  \\__| \\____$$ |\\_______/ \\__|       \\_______| \\_______|\\__|  \\__|\\__|  \\__| \\_______|\\__|      \n" +
                "                                                               $$\\   $$ |                                                                                \n" +
                "                                                               \\$$$$$$  |                                                                                \n" +
                "                                                                \\______/                                                                                 ");
    }


    //Quelle ASCII-Art: https://patorjk.com/software/taag/#p=testall&f=Digital&t=Auf%20Wiedersehen
}
