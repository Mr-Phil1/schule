package ProjektSem1.Umrechner.Texte.Waehrung;

public class BereichsTitleEpoche {
    public static void BereichsTitleEpochen() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |E|p|o|c|h|e|n|-|A|u|s|w|a|h|l|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n\n");
    }

    public static void BereichsTitleModerne() {
        System.out.print(" +-+-+-+-+-+-+-+\n" +
                " |M|o|d|e|r|n|e|\n" +
                " +-+-+-+-+-+-+-+\n\n");
    }

    public static void BereichsTitleAntike() {
        System.out.print(" +-+-+-+-+-+-+\n" +
                " |A|n|t|i|k|e|\n" +
                " +-+-+-+-+-+-+\n\n");
    }
}
