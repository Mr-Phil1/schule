package ProjektSem1.Umrechner.Texte.Waehrung;

public class BereichsTitleModerne {
    public static void BereichsTitleSchweizerFranken() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |S|c|h|w|e|i|z|e|r|-|F|r|a|n|k|e|n|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    public static void BereichsTitleJapanYen() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+\n" +
                " |J|a|p|a|n|-|Y|e|n|\n" +
                " +-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    public static void BereichsTitleUsDollar() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+\n" +
                " |U|S|-|D|o|l|l|a|r|\n" +
                " +-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    //Quelle ASCII-Art: https://patorjk.com/software/taag/#p=testall&f=Digital&t=Auf%20Wiedersehen
}
