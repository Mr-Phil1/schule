package ProjektSem1.Umrechner.Texte.Waehrung;

public class BereichsTitleAntik {
    public static void BereichsTitleSchilling() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+\n" +
                        " |S|c|h|i|l|l|i|n|g|\n" +
                        " +-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleBelgischerFranc() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |B|e|l|g|i|s|c|h|e|r|-|F|r|a|n|c|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleZypernPfund() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |Z|y|p|e|r|n|-|P|f|u|n|d|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleDeutscheMark() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |D|e|u|t|s|c|h|e|-|M|a|r|k|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleEtnischeKrone() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |E|t|n|i|s|c|h|e|-|K|r|o|n|e|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleSpanischePeseta() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |S|p|a|n|i|s|c|h|e|-|P|e|s|e|t|a|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleFinnmark() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+\n" +
                        " |F|i|n|n|m|a|r|k|\n" +
                        " +-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleFranzoesischerFranc() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |F|r|a|n|z|o|e|s|i|s|c|h|e|r|-|F|r|a|n|c|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleGriechischeDrachme() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |G|r|i|e|c|h|i|s|c|h|e|-|D|r|a|c|h|m|e|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleIrischesPfund() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |I|r|i|s|c|h|e|s|-|P|f|u|n|d|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleItalienischeLira() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |I|t|a|l|i|e|n|i|s|c|h|e|-|L|i|r|a|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    public static void BereichsTitleLitauischeLitas() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |L|i|t|a|u|i|s|c|h|e|-|L|i|t|a|s|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    public static void BereichsTitleLuxemburgischerFranc() {
        System.out.print(" +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                " |L|u|x|e|m|b|u|r|g|i|s|c|h|e|r|-|F|r|a|n|c|\n" +
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                "\n");
    }

    public static void BereichsTitleLettischerLats() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |L|e|t|t|i|s|c|h|e|r|-|L|a|t|s|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleMaltesischeLira() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |M|a|l|t|e|s|i|s|c|h|e|-|L|i|r|a|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleHolländischerGulden() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |H|o|l|l|a|e|n|d|i|s|c|h|e|r|-|G|u|l|d|e|n|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitlePortugiesischerEscudo() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |P|o|r|t|u|g|i|e|s|i|s|c|h|e|r|-|E|s|c|u|d|o|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleSlowakischeKrone() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |S|l|o|w|a|k|i|s|c|h|e|-|K|r|o|n|e|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }

    public static void BereichsTitleSlowenischerTolar() {
        System.out.print(
                " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        " |S|l|o|w|e|n|i|s|c|h|e|r|-|T|o|l|a|r|\n" +
                        " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n" +
                        "\n");
    }


    //Quelle ASCII-Art: https://patorjk.com/software/taag/#p=testall&f=Digital&t=Auf%20Wiedersehen
}
