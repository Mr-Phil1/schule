package ProjektSem1.Umrechner.Texte.Waehrung;

import ProjektSem1.Texte.Texte;

public class Eingabe {

    /*public static void eingabeEuro() {
        System.out.print("Bitte geben Sie Ihren Euro Betrag für die Umrechnung ein: ");
    }*/

    public static void eingabeWaehrung(String waehrung) {
        System.out.println("Bitte geben Sie Ihren " + waehrung + " Betrag für die Umrechnung ein.");
    }

    public static void eingabeKurs(String waehrung) {
        System.out.print("Bitte geben Sie denn aktuellen " + waehrung + "-Kurs ein (Unbedingt zu beachten: Verwendung eines \".\" kein \",\"). \n");
        Texte.printStriche();
    }
}
