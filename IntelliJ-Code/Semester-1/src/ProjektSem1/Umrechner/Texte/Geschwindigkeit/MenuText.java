package ProjektSem1.Umrechner.Texte.Geschwindigkeit;

import ProjektSem1.Texte.Texte;

public class MenuText {


    public static void MenuTextMainGeschwindigkeit() {
        Texte.fortfahren();
        System.out.println("\t1) Geschwindigkeit-Umrechnen  \t2) Programm beenden");
    }

    public static void MenuTextSubTemperatur() {
        Texte.fortfahren();
        System.out.println("\t1) km\\h-mph \t2) mph-km\\h \t3) Programm beenden");
    }
    public static void EingabeGeschwindigkei(String text){
        System.out.println("Bitte geben Sie den "+text+" Betrag für die Umrechnung ein.");
    }

    public static void BereichsLogoUmrechnerGeschwindigkeit() {
        Texte.printStriche();
        System.out.println(" $$$$$$\\                                $$\\                     $$\\                 $$\\ $$\\           $$\\                 $$\\   $$\\     \n" +
                "$$  __$$\\                               $$ |                    \\__|                $$ |\\__|          $$ |                \\__|  $$ |    \n" +
                "$$ /  \\__| $$$$$$\\   $$$$$$$\\  $$$$$$$\\ $$$$$$$\\  $$\\  $$\\  $$\\ $$\\ $$$$$$$\\   $$$$$$$ |$$\\  $$$$$$\\  $$ |  $$\\  $$$$$$\\  $$\\ $$$$$$\\   \n" +
                "$$ |$$$$\\ $$  __$$\\ $$  _____|$$  _____|$$  __$$\\ $$ | $$ | $$ |$$ |$$  __$$\\ $$  __$$ |$$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$ |\\_$$  _|  \n" +
                "$$ |\\_$$ |$$$$$$$$ |\\$$$$$$\\  $$ /      $$ |  $$ |$$ | $$ | $$ |$$ |$$ |  $$ |$$ /  $$ |$$ |$$ /  $$ |$$$$$$  / $$$$$$$$ |$$ |  $$ |    \n" +
                "$$ |  $$ |$$   ____| \\____$$\\ $$ |      $$ |  $$ |$$ | $$ | $$ |$$ |$$ |  $$ |$$ |  $$ |$$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |  $$ |$$\\ \n" +
                "\\$$$$$$  |\\$$$$$$$\\ $$$$$$$  |\\$$$$$$$\\ $$ |  $$ |\\$$$$$\\$$$$  |$$ |$$ |  $$ |\\$$$$$$$ |$$ |\\$$$$$$$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |  \\$$$$  |\n" +
                " \\______/  \\_______|\\_______/  \\_______|\\__|  \\__| \\_____\\____/ \\__|\\__|  \\__| \\_______|\\__| \\____$$ |\\__|  \\__| \\_______|\\__|   \\____/ \n" +
                "                                                                                            $$\\   $$ |                                  \n" +
                "                                                                                            \\$$$$$$  |                                  \n" +
                "                                                                                             \\______/                                   \n");
    }


    //Quelle ASCII-Art: https://patorjk.com/software/taag/#p=testall&f=Digital&t=Auf%20Wiedersehen
}
