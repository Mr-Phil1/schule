package ProjektSem1.Umrechner.Texte.Temperatur;

import ProjektSem1.Texte.Texte;

import java.text.DecimalFormat;

public class MenuText {


    public static void MenuTextMainTemperatur() {
        Texte.fortfahren();
        System.out.println("\t1) Temperatur-Umrechnen \t2) Programm beenden");
    }

    public static void MenuTextSubTemperatur() {
        Texte.fortfahren();
        System.out.println("\t1) Celsius-Fahrenheit \t2) Fahrenheit-Celsius \t3) Programm beenden");
    }

    public static String patternTemp(double betrag) {
        DecimalFormat myFormatter = new DecimalFormat("#,###.#");
        return myFormatter.format(betrag);
    }

    public static void BereichsLogoUmrechnerTemperatur() {
        Texte.printStriche();
        System.out.println("$$$$$$$$\\                                                              $$\\                         \n" +
                "\\__$$  __|                                                             $$ |                        \n" +
                "   $$ | $$$$$$\\  $$$$$$\\$$$$\\   $$$$$$\\   $$$$$$\\   $$$$$$\\  $$$$$$\\ $$$$$$\\   $$\\   $$\\  $$$$$$\\  \n" +
                "   $$ |$$  __$$\\ $$  _$$  _$$\\ $$  __$$\\ $$  __$$\\ $$  __$$\\ \\____$$\\\\_$$  _|  $$ |  $$ |$$  __$$\\ \n" +
                "   $$ |$$$$$$$$ |$$ / $$ / $$ |$$ /  $$ |$$$$$$$$ |$$ |  \\__|$$$$$$$ | $$ |    $$ |  $$ |$$ |  \\__|\n" +
                "   $$ |$$   ____|$$ | $$ | $$ |$$ |  $$ |$$   ____|$$ |     $$  __$$ | $$ |$$\\ $$ |  $$ |$$ |      \n" +
                "   $$ |\\$$$$$$$\\ $$ | $$ | $$ |$$$$$$$  |\\$$$$$$$\\ $$ |     \\$$$$$$$ | \\$$$$  |\\$$$$$$  |$$ |      \n" +
                "   \\__| \\_______|\\__| \\__| \\__|$$  ____/  \\_______|\\__|      \\_______|  \\____/  \\______/ \\__|      \n" +
                "                               $$ |                                                                \n" +
                "                               $$ |                                                                \n" +
                "                               \\__|                                                                \n");
    }


    //Quelle ASCII-Art: https://patorjk.com/software/taag/#p=testall&f=Digital&t=Auf%20Wiedersehen
}
