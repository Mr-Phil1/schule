package ProjektSem1.Umrechner;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Geschwindigkeit.Geschwindigkeit;
import ProjektSem1.Umrechner.Bereiche.Temperatur.Temperatur;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Waehrung;
import ProjektSem1.Umrechner.Texte.MenuText;


public class Umrechner {
    public Umrechner() {
        Waehrung waehrung;
        Temperatur temperatur;
        Geschwindigkeit geschwindigkeit;
        boolean ende = false;
        while (!ende) {
            BereichsLogos.BereichsLogoUmrechner();
            MenuText.MenuTextMain();
            int menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Geschwindigkeit
                    geschwindigkeit = new Geschwindigkeit();
                    break;
                case 2: // Temperatur
                    temperatur = new Temperatur();
                    break;
                case 3: // Waehrung
                    waehrung = new Waehrung();
                    break;
                case 4: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 4");
                    break;
            }
        }
    }
}

