package ProjektSem1.Umrechner.Bereiche.Temperatur;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Texte.Temperatur.MenuText;

public class Calc {
    private boolean ende;

    public Calc() {
        while (!ende) {
            Texte.printStriche();
            BereichsLogos.BereichsTitleAuswahl();
            MenuText.MenuTextSubTemperatur();
            int menuvalue = Input.inputInt("[0-9]+");
            switch (menuvalue) {
                case 1:
                    Texte.printStriche();
                    System.out.println("Celsius-Fahrenheit");
                    this.ausgabe("°C", "[0-9]+[.]*[0-9]*", "#,###.##");
                    break;
                case 2:
                    Texte.printStriche();
                    System.out.println("Fahrenheit-Celsius");
                    this.ausgabe("F", "[0-9]+[.]*[0-9]*", "#,###.##");
                    break;
                case 3:
                    Texte.printStriche();
                    ende = true;
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1-3");
                    break;
            }
        }
        ende = false;
    }

    private void ausgabe(String einheit, String regex, String pattern) {
        double input = Input.inputDouble(regex);
        if (einheit.equals("°C")) {
            System.out.println("Die Umrechnung von " + input + "°C ergibt " + Texte.patternDoubleText(this.CelsiusFahrenheit(input), pattern) + "°F.");
        } else {
            System.out.println("Die Umrechnung von " + input + "°F ergibt " + Texte.patternDoubleText(this.FahrenheitCelsius(input), pattern) + "°C.");
        }
    }

    private double CelsiusFahrenheit(double input) {
        return (input * 1.8) + 32;
    }

    private double FahrenheitCelsius(double input) {
        return (input - 32) * 0.55555555556;
    }
}

