package ProjektSem1.Umrechner.Bereiche.Geschwindigkeit;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Texte.Geschwindigkeit.MenuText;

public class Geschwindigkeit {

    private boolean ende;

    public Geschwindigkeit() {
        while (!ende) {
            MenuText.BereichsLogoUmrechnerGeschwindigkeit();
            MenuText.MenuTextMainGeschwindigkeit();
            int menuvalue = Input.inputInt("[0-9]+");
            switch (menuvalue) {
                case 1:
                    Calc calc = new Calc();
                    break;
                case 2:
                    Texte.printStriche();
                    ende = true;
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1-2");
                    break;
            }
        }
        ende = false;
    }
}
