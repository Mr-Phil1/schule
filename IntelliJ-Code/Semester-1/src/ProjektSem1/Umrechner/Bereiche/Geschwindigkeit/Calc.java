package ProjektSem1.Umrechner.Bereiche.Geschwindigkeit;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Texte.Geschwindigkeit.MenuText;

public class Calc {
    private String kmh = "km\\h", mph = "mph";
    private boolean ende;
    private double mphKmh = 0.62, kmhMph =1.609344;

    public Calc() {
        while (!ende) {
            Texte.printStriche();
            BereichsLogos.BereichsTitleAuswahl();
            MenuText.MenuTextSubTemperatur();
            int menuvalue = Input.inputInt("[0-9]+");
            switch (menuvalue) {
                case 1:
                    Texte.printStriche();
                    MenuText.EingabeGeschwindigkei("km\\h");
                    this.ausgabe("mph", "[0-9]+[.]*[0-9]*", "#,###.##");
                    break;
                case 2:
                    Texte.printStriche();
                    MenuText.EingabeGeschwindigkei("mph");
                    this.ausgabe("km\\h", "[0-9]+[.]*[0-9]*", "#,###.##");
                    break;
                case 3:
                    Texte.printStriche();
                    ende = true;
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1-2");
                    break;
            }
        }
        ende = false;
    }


    private void ausgabe(String einheit, String regex, String pattern) {
        double input = Input.inputDouble(regex);
        if (einheit.equals("km\\h")) {
            System.out.println("Die Umrechnung von " + input + " " + this.mph + " ergibt " + Texte.patternDoubleText(this.KmhMph(input), pattern) + " " + this.kmh+".");
        } else {
            System.out.println("Die Umrechnung von " + input + " " + this.kmh +" ergibt " + Texte.patternDoubleText(this.MphKmh(input), pattern) + " " + this.mph+".");
        }
    }

    private double KmhMph(double input) {
        return input *this.getKmhMph();
    }

    private double MphKmh(double input) {
        return input * this.getMphKmh();
    }

    private double getMphKmh() {
        return mphKmh;
    }

    private double getKmhMph() {
        return kmhMph;
    }
}
