package ProjektSem1.Umrechner.Bereiche.Waehrung.Antik.Menu;

import ProjektSem1.FileIO.FileCreate;
import ProjektSem1.FileIO.FileRead;
import ProjektSem1.FileIO.FileWrite;
import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Calc;
import ProjektSem1.Umrechner.Texte.Waehrung.BereichsTitleAntik;
import ProjektSem1.Umrechner.Texte.Waehrung.Eingabe;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;


public class FI_Menu {
    private String filePath, fileName, fileExt, waehrung;
    final String kurs = "5.94573";
    private int menuValue;
    private boolean subEnde;
    private Calc calc;
    private FileWrite fileWrite;
    private FileCreate fileCreate;


    public FI_Menu(String filePath, String fileName, String fileExt, String waehrung) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.fileExt = fileExt;
        this.waehrung = waehrung;
        if (!FileRead.FileExists(this.filePath, this.fileName, this.fileExt)) {
            Texte.printStriche();
            fileCreate = new FileCreate(this.filePath, this.fileName, this.fileExt, this.kurs); // EuroCH_Franken
            MenuText.FileErstelltText(this.filePath, this.fileName, this.fileExt);
        }
    }

    public void menu() {
        while (!subEnde) {
            calc = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName, this.fileExt), this.waehrung);
            Texte.printStriche();
            BereichsTitleAntik.BereichsTitleFinnmark();
            MenuText.MenuTextSub(this.waehrung);
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1:
                    MenuText.ausgabeKurs(this.filePath, this.fileName, this.fileExt, this.waehrung+" <-> Euro");
                    break;
                case 2:
                    Eingabe.eingabeWaehrung("Euro");
                    calc.umerchnenZuWaehrung(Input.inputDouble("[0-9]+[.]*[0-9]*"), "Euro");
                    break;
                case 3:
                    Eingabe.eingabeWaehrung(this.waehrung);
                    calc.umerchnenVonWaehrung(Input.inputDouble("[0-9]+[.]*[0-9]*"), "Euro");
                    break;
                case 4:
                    while (!subEnde) {
                        Texte.printStriche();
                        MenuText.MenuTextSubKursAntik(this.waehrung);
                        menuValue = Input.inputInt("[0-9]+");
                        switch (menuValue) {
                            case 1:
                                Texte.printStriche();
                                Eingabe.eingabeKurs(this.waehrung+" <-> Euro");
                                fileWrite = new FileWrite(this.filePath, this.fileName, this.fileExt, Input.checkStr("[0-9]+[.]*[0-9]*"));
                                calc = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName, this.fileExt), this.waehrung);
                                MenuText.neuerKursGesetzt(calc.kursAusgabe(), this.waehrung+" <-> Euro");
                                break;
                            case 2:
                                fileWrite = new FileWrite(this.filePath, this.fileName, this.fileExt, this.kurs);
                                Texte.printStriche();
                                MenuText.referenzKursSetzten();
                                break;
                            case 3:
                                Texte.printStriche();
                                MenuText.ausgabeKurs(this.filePath, this.fileName, this.fileExt, this.waehrung+" <-> Euro");
                                break;
                            case 4:
                                Texte.printStriche();
                                subEnde = true;
                                Texte.MenuTextSubEnde();
                                break;
                            default:
                                Texte.printStriche();
                                Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 4");
                                break;
                        }
                    }
                    subEnde = false;
                    break;
                case 5:
                    subEnde = true;
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                    break;
            }
        }
        subEnde = false;
    }
}
