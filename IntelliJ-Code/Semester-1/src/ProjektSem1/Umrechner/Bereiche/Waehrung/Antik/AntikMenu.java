package ProjektSem1.Umrechner.Bereiche.Waehrung.Antik;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Antik.Menu.*;
import ProjektSem1.Umrechner.Texte.Waehrung.BereichsTitleEpoche;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;

public class AntikMenu {
    private boolean ende;
    private String filePath = "Umrechner/Bereiche/Waehrung/Antik/Kurs/", fileExt = ".txt";


    public AntikMenu() {
        // Einbindung der verschidenen Menüs
        AT_Menu at_menu = new AT_Menu(this.filePath, "AT_Schilling", this.fileExt, "Schilling");
        BE_Menu be_menu = new BE_Menu(this.filePath, "BE_Franc", this.fileExt, "Franc");
        CY_Menu cy_menu = new CY_Menu(this.filePath, "CY_Pfund", this.fileExt, "Pfund");
        DE_Menu de_menu = new DE_Menu(this.filePath, "DE_Mark", this.fileExt, "Mark");
        EE_Menu ee_menu = new EE_Menu(this.filePath, "EE_Krone", this.fileExt, "Krone");
        ES_Menu es_menu = new ES_Menu(this.filePath, "ES_Peseta", this.fileExt, "Peseta");
        FI_Menu fi_menu = new FI_Menu(this.filePath, "FI_Finnmark", this.fileExt, "Finmark");
        FR_Menu fr_menu = new FR_Menu(this.filePath, "FR_Franc", this.fileExt, "Franc");
        GR_Menu gr_menu = new GR_Menu(this.filePath, "GR_Drachme", this.fileExt, "Drachme");
        IE_Menu ie_menu = new IE_Menu(this.filePath, "IE_Pfund", this.fileExt, "Pund");
        IT_Menu it_menu = new IT_Menu(this.filePath, "IT_Lira", this.fileExt, "Lira");
        LT_Menu lt_menu = new LT_Menu(this.filePath, "LT_Litas", this.fileExt, "Litas");
        LU_Menu lu_menu = new LU_Menu(this.filePath, "LU_Franc", this.fileExt, "Franc");
        LV_Menu lv_menu = new LV_Menu(this.filePath, "LV_Lats", this.fileExt, "Lats");
        MT_Menu mt_menu = new MT_Menu(this.filePath, "MT_Lira", this.fileExt, "Lira");
        NL_Menu nl_menu = new NL_Menu(this.filePath, "NL_Gulden", this.fileExt, "Gulden");
        PT_Menu pt_menu = new PT_Menu(this.filePath, "PT_Escudo", this.fileExt, "Escudo");
        SK_Menu sk_menu = new SK_Menu(this.filePath, "SK_Krone", this.fileExt, "Krone");
        SI_Menu si_menu = new SI_Menu(this.filePath, "SI_Tolar", this.fileExt, "Tolar");
        // Anfang, "CH_Franken", this.fileExt,
        while (!ende) {
            Texte.printStriche();
            BereichsTitleEpoche.BereichsTitleAntike();
            MenuText.MenuTextUmrechnerAntik();
            int menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Schilling
                    at_menu.menu();
                    break;
                case 2: // Belgischer-Franc
                    be_menu.menu();
                    break;
                case 3: // Zypern-Pfund
                    cy_menu.menu();
                    break;
                case 4: // Deutsche-Mark
                    de_menu.menu();
                    break;
                case 5: // Etnische-Krone
                    ee_menu.menu();
                    break;
                case 6: // Spanische-Peseta
                    es_menu.menu();
                    break;
                case 7: // Finnmark
                    fi_menu.menu();
                    break;
                case 8: // Französischer-Franc
                    fr_menu.menu();
                    break;
                case 9: // Griechische-Drachme
                    gr_menu.menu();
                    break;
                case 10: // Irisches-Pfund
                    ie_menu.menu();
                    break;
                case 11: // Italienische-Lira
                    it_menu.menu();
                    break;
                case 12: // Litauische-Litas
                    lt_menu.menu();
                    break;
                case 13: // Luxemburgischer-Franc
                    lu_menu.menu();
                    break;
                case 14: // Lettischer-Lats
                    lv_menu.menu();
                    break;
                case 15: // Maltesische-Lira
                    mt_menu.menu();
                    break;
                case 16: // Holländischer-Gulden
                    nl_menu.menu();
                    break;
                case 17: // Portugiesischer-Escudo
                    pt_menu.menu();
                    break;
                case 18: // Slowakische-Krone
                    sk_menu.menu();
                    break;
                case 19: // Slowenischer-Tolar
                    si_menu.menu();
                    break;
                case 20: // Ende
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 20");
                    break;
            }
        }
        ende = false;
    }
}
