package ProjektSem1.Umrechner.Bereiche.Waehrung;


import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;


public class Waehrung {
    public Waehrung() {
        Epochen epochen = new Epochen();
        boolean ende = false;
        while (!ende) {
            MenuText.BereichsLogoUmrechnerWaehrung();
            MenuText.MenuTextMainWaehrung();
            int menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Umrechner
                    epochen.waehrung();
                    break;
                case 2: // Info
                    epochen.info();
                    break;
                case 3: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 3");
                    break;
            }
        }
    }
}
