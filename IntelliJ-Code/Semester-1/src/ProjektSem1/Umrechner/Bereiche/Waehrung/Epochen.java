package ProjektSem1.Umrechner.Bereiche.Waehrung;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Antik.AntikMenu;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Modern.ModernMenu;
import ProjektSem1.Umrechner.Texte.Waehrung.BereichsTitleEpoche;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;


public class Epochen {
    private int menuValue;
    private boolean ende;
    private String Quelle;

    public void waehrung() {
        // Einbindung der verschidenen Menüs
        ModernMenu modernMenu;
        AntikMenu antikMenu;
        // Anfang
        while (!ende) {
            Texte.printStriche();
            BereichsTitleEpoche.BereichsTitleEpochen();
            MenuText.MenuTextEpoche();
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Modern-Currency
                    modernMenu = new ModernMenu();
                    break;
                case 2: // Antik-Currency
                    antikMenu = new AntikMenu();
                    break;
                case 3: // Ende
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 3");
                    break;
            }
        }
        ende = false;
    }

    public void info() {
        while (!ende) {
            Texte.printStriche();
            BereichsLogos.BereichsTitleQuellen();
            MenuText.MenuTextInfo();
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Quelle
                    Texte.printStriche();
                    System.out.println("Es wurden folgende Quellen zur Hilfe genommen: ");
                    System.out.println("\t* OeNB: \t\t\t\t\t\t\thttps://www.oenb.at/zinssaetzewechselkurse/zinssaetzewechselkurse?mode=wechselkurse");
                    System.out.println("\t* Bundesministerium für Finanzen: \thttps://formulare.bmf.gv.at/service/formulare/inter-Steuern/pdfd/2019/L17b.pdf");
                    break;
                case 2: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 2");
                    break;
            }
        }
        ende = false;
    }
}

