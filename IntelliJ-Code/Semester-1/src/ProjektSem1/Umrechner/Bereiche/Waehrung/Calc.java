package ProjektSem1.Umrechner.Bereiche.Waehrung;


import ProjektSem1.Texte.Texte;

public class Calc {
    private double kurs;

    private String waehrung, pattern = "###,###.######";

    public Calc(double kurs, String waehrung) {
        this.kurs = kurs;
        this.waehrung = waehrung;
    }

    private double vonWaehrung(double betrag) {
        return round2Dezi((betrag / getKurs()));
    }

    private double zuWaehrung(double betrag) {
        return round2Dezi((betrag * getKurs()));
    }


    private double round2Dezi(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    public void umerchnenVonWaehrung(double betrag, String waehrung) {
        System.out.println("Die Umrechung von " + Texte.patternDoubleText(betrag, this.pattern) + " " + this.waehrung + " in " + waehrung + " mit folgendem Kurs " +
                "" + Texte.patternDoubleText(this.getKurs(), this.pattern) + " ergibt: " + Texte.patternDoubleText(this.vonWaehrung(betrag), this.pattern) + " " + waehrung + ".");
    }

    public void umerchnenZuWaehrung(double betrag, String waehrung) {
        System.out.println("Die Umrechung von " + Texte.patternDoubleText(betrag,this.pattern) + " " + waehrung + " in " + this.waehrung + " mit folgendem Kurs " +
                "" + Texte.patternDoubleText(this.getKurs(),this.pattern) + " ergibt: " + Texte.patternDoubleText(this.zuWaehrung(betrag), this.pattern) + " " + this.waehrung + ".");
    }

    /**
     *  @param betrag Währungsbetrag
     * @param waehrung1 von Währung
     * @param waehrung2 zu Währung
     * @param waehrung3 end Währung
     */
    public void umerchnenModerne(double betrag, String waehrung1, String waehrung2, String waehrung3) {
        System.out.println("Die Umrechung von " + Texte.patternDoubleText(betrag, this.pattern) + " " + waehrung1 + " in " + waehrung2 + " mit folgendem Kurs" +
                " " + Texte.patternDoubleText(this.getKurs(), this.pattern) + " ergibt: " + Texte.patternDoubleText(this.zuWaehrung(betrag), this.pattern) + " " + waehrung3 + ".");
    }

    private double getKurs() {
        return kurs;
    }

    public String kursAusgabe() {
        return Texte.patternDoubleText(this.getKurs(), this.pattern);
    }
}
