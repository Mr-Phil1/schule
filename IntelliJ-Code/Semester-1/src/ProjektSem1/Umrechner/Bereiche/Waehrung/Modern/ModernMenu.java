package ProjektSem1.Umrechner.Bereiche.Waehrung.Modern;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Modern.Menu.CH_Menu;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Modern.Menu.JP_Menu;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Modern.Menu.US_Menu;
import ProjektSem1.Umrechner.Texte.Waehrung.BereichsTitleEpoche;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;


public class ModernMenu {
    private int menuValue;
    private boolean ende= false;
    private String filePath = "Umrechner/Bereiche/Waehrung/Modern/Kurs/", fileExt = ".txt";

    public ModernMenu() {
        // Einbindung der verschidenen Menüs
        CH_Menu ch_menu = new CH_Menu(this.filePath, "EuroCH_Franken", "CH_FrankenEuro", this.fileExt, "Franken");
        US_Menu us_menu = new US_Menu(this.filePath, "EuroUS_Dollar", "US_DollarEuro", this.fileExt, "US-Dollar");
        JP_Menu jp_menu = new JP_Menu(this.filePath, "EuroJP_Yen", "JP_YenEuro", this.fileExt, "Yen");

        // Anfang
        while (!ende) {
            Texte.printStriche();
            BereichsTitleEpoche.BereichsTitleModerne();
            MenuText.MenuTextUmrechnerModern();
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // Schweizer-Franken
                    ch_menu.menu();
                    break;
                case 2: // US-Dollar
                    us_menu.menu();
                    break;
                case 3: // Japan-Yen
                    jp_menu.menu();
                    break;
                case 4: // Ende
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 4");
                    break;
            }
        }
        ende = false;
    }
}

