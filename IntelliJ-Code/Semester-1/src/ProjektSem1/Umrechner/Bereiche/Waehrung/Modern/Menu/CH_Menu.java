package ProjektSem1.Umrechner.Bereiche.Waehrung.Modern.Menu;

import ProjektSem1.FileIO.FileCreate;
import ProjektSem1.FileIO.FileRead;
import ProjektSem1.FileIO.FileWrite;
import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Bereiche.Waehrung.Calc;
import ProjektSem1.Umrechner.Texte.Waehrung.BereichsTitleModerne;
import ProjektSem1.Umrechner.Texte.Waehrung.Eingabe;
import ProjektSem1.Umrechner.Texte.Waehrung.MenuText;

public class CH_Menu {
    private String filePath, fileName1, fileName2, fileExt, waehrung;
    final String kursEuroFremd = "1.11", kursFremdEuro = "0.898957", euro = "Euro";
    private int menuValue;
    private boolean subEnde;
    private Calc calc1, calc2;
    private FileCreate fileCreate1, fileCreate2;
    private FileWrite fileWrite;

    public CH_Menu(String filePath, String fileName1, String fileName2, String fileExt, String waehrung) {
        this.filePath = filePath;
        this.fileName1 = fileName1;
        this.fileName2 = fileName2;
        this.fileExt = fileExt;
        this.waehrung = waehrung;
        if (!FileRead.FileExists(this.filePath, this.fileName1, this.fileExt)) {
            Texte.printStriche();
            fileCreate1 = new FileCreate(this.filePath, this.fileName1, this.fileExt, this.kursEuroFremd); // EuroCH_Franken
            MenuText.FileErstelltText(this.filePath, this.fileName1, this.fileExt);
        }
        if (!FileRead.FileExists(this.filePath, this.fileName2, this.fileExt)) {
            Texte.printStriche();
            fileCreate2 = new FileCreate(this.filePath, this.fileName2, this.fileExt, this.kursFremdEuro); // CH_FrankenEuro
            MenuText.FileErstelltText(this.filePath, this.fileName2, this.fileExt);
        }
    }

    public void menu() {
        while (!subEnde) {
            calc1 = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName1, this.fileExt), this.waehrung);
            calc2 = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName2, this.fileExt), this.waehrung);
            Texte.printStriche();
            BereichsTitleModerne.BereichsTitleSchweizerFranken();
            MenuText.MenuTextSub(this.waehrung);
            menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1:
                    Texte.printStriche();
                    MenuText.ausgabeKurs(this.filePath, this.fileName1, this.fileExt, this.euro + " zu " + this.waehrung);
                    MenuText.ausgabeKurs(this.filePath, this.fileName2, this.fileExt, this.waehrung + " zu " + this.euro);
                    break;
                case 2:
                    Texte.printStriche();
                    Eingabe.eingabeWaehrung(this.euro);
                    calc1.umerchnenModerne(Input.inputDouble("[0-9]+[.]*[0-9]*"), this.euro, this.waehrung, this.waehrung);
                    break;
                case 3:
                    Texte.printStriche();
                    Eingabe.eingabeWaehrung(this.waehrung);
                    calc2.umerchnenModerne(Input.inputDouble("[0-9]+[.]*[0-9]*"), this.waehrung, this.euro, this.euro);
                    break;
                case 4:
                    while (!subEnde) {
                        Texte.printStriche();
                        MenuText.MenuTextSubKurs(this.waehrung);
                        menuValue = Input.inputInt("[0-9]+");
                        switch (menuValue) {
                            case 1:
                                Texte.printStriche();
                                Eingabe.eingabeKurs(this.euro + " zu " + this.waehrung);
                                fileWrite = new FileWrite(this.filePath, this.fileName1, this.fileExt, Input.checkStr("[0-9]+[.]*[0-9]*"));
                                calc1 = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName1, this.fileExt), this.waehrung);
                                MenuText.neuerKursGesetzt(calc1.kursAusgabe(), this.euro+" pro "+this.waehrung);
                                break;
                            case 2:
                                Texte.printStriche();
                                Eingabe.eingabeKurs(this.waehrung + " zu " + this.euro);
                                fileWrite = new FileWrite(this.filePath, this.fileName2, this.fileExt, Input.checkStr("[0-9]+[.]*[0-9]*"));
                                calc2 = new Calc(FileRead.FileReadDouble(this.filePath, this.fileName2, this.fileExt), this.waehrung);
                                MenuText.neuerKursGesetzt(calc2.kursAusgabe(), this.waehrung+" pro "+this.euro);
                                break;
                            case 3:
                                fileWrite = new FileWrite(this.filePath, this.fileName1, this.fileExt, this.kursEuroFremd);
                                fileWrite = new FileWrite(this.filePath, this.fileName2, this.fileExt, this.kursFremdEuro);
                                Texte.printStriche();
                                MenuText.referenzKursSetzten();
                                break;
                            case 4:
                                Texte.printStriche();
                                MenuText.ausgabeKurs(this.filePath, this.fileName1, this.fileExt, this.euro + " zu " + this.waehrung);
                                MenuText.ausgabeKurs(this.filePath, this.fileName2, this.fileExt, this.waehrung + " zu " + this.euro);
                                break;
                            case 5:
                                Texte.printStriche();
                                subEnde = true;
                                Texte.MenuTextSubEnde();
                                break;
                            default:
                                Texte.printStriche();
                                Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                                break;
                        }
                    }
                    subEnde = false;
                    break;
                case 5:
                    Texte.printStriche();
                    subEnde = true;
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                    break;
            }
        }
        subEnde = false;
    }
}
