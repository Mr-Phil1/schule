package ProjektSem1.BMI;

import ProjektSem1.Texte.Texte;

public class TexteBMI {
    /**
     * Menütext für das Starten bzw. für das Beenden des BMI-Bereich
     */
    public static void MenuTextMain() {
        Texte.fortfahren();
        System.out.println("\t1) BMI-berechnen \t2) Programm beenden");
    }

    public static void MenuTextSubEingabeGeschlecht() {
        System.out.println("Bitte geben Sie Ihr Geschlecht ein: ");
    }

    /**
     * Menütext für die Geschlechter Auswahl bzw. für das Beenden.
     */
    public static void MenuTextSubBerechnenBmi() {
        MenuTextSubEingabeGeschlecht();
        System.out.println("\tm) männlich \tw) weiblich \tb) beenden");
        Texte.eingabeMenu();
    }

    public static void InfoTextBmi(double cm, double kg) {
        System.out.print("Der BMI für Ihre Körpergröße " + (cm / 100) + "m und Ihr Körpergewicht " + kg + "kg ergibt: ");
    }

}
