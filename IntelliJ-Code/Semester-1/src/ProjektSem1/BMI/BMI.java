package ProjektSem1.BMI;

import ProjektSem1.FileIO.Input;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;


public class BMI {
    private boolean ende;
    private BmiRechner bmiRechner = new BmiRechner();

    public BMI() {
        while (!ende) {
            BereichsLogos.BereichsLogoBmi();
            TexteBMI.MenuTextMain();
            int menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1: // BMI-berechnen
                    this.berechnenBmi();
                    break;
                case 2: // beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                    break;
            }
        }
        ende = false;
    }

    private void berechnenBmi() {
        while (!ende) {
            BereichsLogos.BereichsLogoBmi();
            TexteBMI.MenuTextSubBerechnenBmi();
            char c = Input.inputChar();
            switch (c) {
                case 'm': // männlich
                    Texte.printStriche();
                    bmiRechner.ausgebenM(eingaben("ihre Körpergröße in cm"), eingaben("ihr Körpergewicht"));
                    break;
                case 'w': // weiblich
                    Texte.printStriche();
                    bmiRechner.ausgebenW(eingaben("ihre Körpergröße in cm"), eingaben("ihr Körpergewicht"));
                    break;
                case 'b': // beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuTextSubEnde();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Buchstaben", "\"m\", \"w\" oder \"b\"");
                    break;
            }
        }
        ende = false;
    }

    /**
     * Hier gibt der User die geforderten Werte in die Konsole ein.
     *
     * @param eingabeText Hier kann man
     * @return Gibt den eingebenden Double-Wert zurück.
     */
    private double eingaben(String eingabeText) {
        Texte.MenuTextEingabe(eingabeText);
        return Input.inputDouble("[0-9]+[.]*[0-9]*");
    }
}

