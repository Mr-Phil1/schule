package ProjektSem1.BMI;

import java.text.DecimalFormat;

public class BmiRechner {
    private double bmi;

    private double berechnungBmi(double cm, double kg) {
        double m = cm / 100;
        bmi = kg / Math.pow(m, 2);
        return bmi;
    }

    /**
     * @param cm Geben Sie Ihre Größe in cm an. Im Anschluss wird dann Ihr BMI
     *           berrechntet mit:
     *
     *           <pre> {@code
     *                                         double m = cm / 100;
     *                                         bmi = kg / Math.pow(m, 2);
     *                                         return bmi;} </pre>
     * @param kg Geben Sie Ihr Gewicht in kg an.
     */
    public void ausgebenM(double cm, double kg) {
        bmi = this.berechnungBmi(cm, kg);
        TexteBMI.InfoTextBmi(cm, kg);
        if (bmi < 20) {
            System.out.println(pattern(bmi) + " (Untergewicht)");
        } else if (bmi < 26) {
            System.out.println(pattern(bmi) + " (Normalgewicht)");
        } else if (bmi < 31) {
            System.out.println(pattern(bmi) + " (Übergewicht)");
        } else if (bmi <= 40) {
            System.out.println(pattern(bmi) + " (Adipositas)");
        } else {
            System.out.println(pattern(bmi) + " (starke Adipositas)");
        }
    }

    /**
     * @param cm Geben Sie Ihre Größe in cm an. Im Anschluss wird dann Ihr BMI
     *           berrechntet mit:
     *
     *           <pre> {@code
     *                                         double m = cm / 100;
     *                                         bmi = kg / Math.pow(m, 2);
     *                                         return bmi;} </pre>
     * @param kg Geben Sie Ihr Gewicht in kg an.
     */
    public void ausgebenW(double cm, double kg) {
        bmi = this.berechnungBmi(cm, kg);
        TexteBMI.InfoTextBmi(cm, kg);
        if (bmi < 19) {
            System.out.println(pattern(bmi) + " (Untergewicht)");
        } else if (bmi < 25) {
            System.out.println(pattern(bmi) + " (Normalgewicht)");
        } else if (bmi < 31) {
            System.out.println(pattern(bmi) + " (Übergewicht)");
        } else if (bmi <= 40) {
            System.out.println(pattern(bmi) + " (Adipositas)");
        } else {
            System.out.println(pattern(bmi) + " (starke Adipositas)");
        }
    }

    private static String pattern(double value) {
        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        return myFormatter.format(value);
    }
}
