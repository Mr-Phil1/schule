package ProjektSem1;

import ProjektSem1.BMI.BMI;
import ProjektSem1.FileIO.Input;
import ProjektSem1.Mathe.Mathe;
import ProjektSem1.Texte.BereichsLogos;
import ProjektSem1.Texte.Texte;
import ProjektSem1.Umrechner.Umrechner;


public class Auswahl {
    public String urheber = "Mr. Phil";

    public Auswahl() {
        BMI bmi;
        Umrechner umrechner;
        Mathe mathe;
        boolean ende = false;
        while (!ende) {
            BereichsLogos.BereichsLogoMain();
            Texte.MenuTextMain();
            int menuValue = Input.inputInt("[0-9]+");
            switch (menuValue) {
                case 1:
                    bmi = new BMI();
                    break;
                case 2: // Mathe
                    mathe = new Mathe();
                    break;
                case 3: // Umrechner
                    umrechner = new Umrechner();
                    break;
                case 4: // Info
                    Texte.printStriche();
                    this.info();
                    break;
                case 5: // Beenden
                    ende = true;
                    Texte.printStriche();
                    Texte.MenuAufWiedersehen();
                    Texte.printStriche();
                    break;
                default:
                    Texte.printStriche();
                    Texte.MenuTextFehlerhafteEingabe("Zahlen", "1 bis 5");
                    break;
            }
        }
    }

    private String getUrheber() {
        return urheber;
    }

    private void info() {
        System.out.println("Der Ersteller dieses Programmes ist: " + getUrheber()+".");
    }
}
