package ProjektSem1.Texte;


import java.text.DecimalFormat;

public class Texte {
    /**
     * Menütext für die Hauptmenü Auswahl
     */
    public static void MenuTextMain() {
        fortfahren();
        System.out.println("\t1) BMI \t\t2) Mathe \t3) Umrechner \t4) Info \t5) Programm beenden");
    }

    /**
     * Eingabe bitte:
     */
    public static void eingabeMenu() {
        System.out.print("Ihre Eingabe bitte: ");
    }

    public static void MenuAufWiedersehen() {
        System.out.println("\n\n\n\n\n\n $$$$$$\\             $$$$$$\\        $$\\      $$\\ $$\\                 $$\\                                         $$\\                           \n" +
                "$$  __$$\\           $$  __$$\\       $$ | $\\  $$ |\\__|                $$ |                                        $$ |                          \n" +
                "$$ /  $$ |$$\\   $$\\ $$ /  \\__|      $$ |$$$\\ $$ |$$\\  $$$$$$\\   $$$$$$$ | $$$$$$\\   $$$$$$\\   $$$$$$$\\  $$$$$$\\  $$$$$$$\\   $$$$$$\\  $$$$$$$\\  \n" +
                "$$$$$$$$ |$$ |  $$ |$$$$\\           $$ $$ $$\\$$ |$$ |$$  __$$\\ $$  __$$ |$$  __$$\\ $$  __$$\\ $$  _____|$$  __$$\\ $$  __$$\\ $$  __$$\\ $$  __$$\\ \n" +
                "$$  __$$ |$$ |  $$ |$$  _|          $$$$  _$$$$ |$$ |$$$$$$$$ |$$ /  $$ |$$$$$$$$ |$$ |  \\__|\\$$$$$$\\  $$$$$$$$ |$$ |  $$ |$$$$$$$$ |$$ |  $$ |\n" +
                "$$ |  $$ |$$ |  $$ |$$ |            $$$  / \\$$$ |$$ |$$   ____|$$ |  $$ |$$   ____|$$ |       \\____$$\\ $$   ____|$$ |  $$ |$$   ____|$$ |  $$ |\n" +
                "$$ |  $$ |\\$$$$$$  |$$ |            $$  /   \\$$ |$$ |\\$$$$$$$\\ \\$$$$$$$ |\\$$$$$$$\\ $$ |      $$$$$$$  |\\$$$$$$$\\ $$ |  $$ |\\$$$$$$$\\ $$ |  $$ |\n" +
                "\\__|  \\__| \\______/ \\__|            \\__/     \\__|\\__| \\_______| \\_______| \\_______|\\__|      \\_______/  \\_______|\\__|  \\__| \\_______|\\__|  \\__|\n");
    }

    /**
     * Gibt 148 Striche aus
     */
    public static void printStriche() {
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    /**
     * Wie möchten Sie nun fortfahren?
     */
    public static void fortfahren() {
        System.out.println("Wie möchten Sie nun fortfahren? ");
    }


   /* public static void MenuTextSub() {
        fortfahren();
        System.out.println("1) Kurs Ausgabe, 2) Umrechnung von Euro in diese Währung, 3) Umrechnung von dieser Währung in Euro, 4) Ende");
    }*/

    /**
     * Sie kommen nun wieder eine Ebene höher
     */
    public static void MenuTextSubEnde() {
        System.out.println("Sie kommen nun wieder eine Ebene höher.");
    }

    /**
     * Hier wird bei einer fehlerhaften Menüeingabe eine Meldung erzeugt mit den maximal möglichen Bereichen.
     *
     * @param type  Hier wird der Type des richtigen Wertes definiert.
     * @param range Hier wird die Range des richtigen Wertes definiert.
     */
    public static void MenuTextFehlerhafteEingabe(String type, String range) {
        System.out.println("\tSie haben einen falschen Wert eingegeben. Versuchen sie es erneut. Es werden nur folgende " + type + " akzeptiert: " + range + ".");
    }

    public static String patternDoubleText(double betrag, String pattern) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        return myFormatter.format(betrag);
    }

    /**
     * Funktion für eine entsprechende Textausgabe
     *
     * @param Text Hier kann man den gewünschten Text mitgeben.
     *             <pre> {@code
     *                                                                  System.out.println("Bitte geben Sie nun " + Text + " ein.");
     *                                                                     eingabeMenu();} </pre>
     */
    public static void MenuTextEingabe(String Text) {
        System.out.println("Bitte geben Sie nun " + Text + " ein.");
    }
}