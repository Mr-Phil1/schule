package ProjektSem1.FileIO;

import ProjektSem1.Texte.Texte;

import java.util.Scanner;

public class Input {
    private static Scanner scan = new Scanner(System.in);

    /**
     * @param regex
     * @return Liefert den eingegeben Wert als int zurück.
     */
    public static int inputInt(String regex) {
        return Input.checkInt(regex);
    }

    public static int intInput() {
        return scan.nextInt();
    }

    /**
     * @param regex
     * @return Liefert den eingegeben Wert als double zurück.
     */
    public static double inputDouble(String regex) {
        return Double.parseDouble(Input.checkerStr(regex));
    }

    /**
     * @return Liefert den eingegeben Wert als String zurück.
     */
    public static String inputString() {
        return scan.next();
    }

    /**
     * @return Liefert den eingegeben Wert als char zurück.
     */
    public static char inputChar() {
        return scan.next().charAt(0);
    }

    private static int checkInt(String regex) {
        return Integer.parseInt((Input.checkerStr(regex)));
    }

    private static String checkerStr(String regex) {
        String checkStr;
        do {
            Texte.eingabeMenu();
            checkStr = Input.inputString();
        } while (!(checkStr.matches(regex)));
        return checkStr;
    }

    public static String checkStr(String regex) {
        return Input.checkerStr(regex);
    }

    public static double checkDouble(String regex) {
        return Double.parseDouble(Input.checkerStr(regex));
    }
}