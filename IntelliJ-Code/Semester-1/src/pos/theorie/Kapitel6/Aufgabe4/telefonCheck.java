package pos.theorie.Kapitel6.Aufgabe4;

public class telefonCheck {
    private String patternNum = "[0-9]*", patternSlash = "[/]",
            patternNumber = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";

    /**
     *
     * @param nummer hhhh
     */
    public void check(String nummer) {
        System.out.println("----------------------------------------------");
        System.out.println(nummer);
        nummer = nummer.replaceAll(patternSlash, "");
        System.out.println(nummer);
        if (nummer.matches(patternNumber)) {
            System.out.println("richtig");

        } else {
            System.out.println("falsch");
        }
        System.out.println("----------------------------------------------");
    }
}