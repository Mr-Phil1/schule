package pos.theorie.Kapitel6.Aufgabe4;

public class regularExpressions {
    public static void main(String[] args) {
        eMailCheck checkMail = new eMailCheck();
        telefonCheck checkTele = new telefonCheck();
        String telefonNr1, telefonNr2, email;
        telefonNr1 = "0664/1234567";
        telefonNr2 = "+43/677/1234567";
        email = "phmaar@tsn.aot";
        checkMail.check(email);
        checkTele.check(telefonNr1);
        checkTele.check(telefonNr2);

    }
}
