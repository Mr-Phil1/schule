package pos.theorie.Kapitel6.Aufgabe4;

public class eMailCheck {
    private String patternAT = "\".*\\.[a-z][a-z]+$", pattern="[a-zA-Z0-9]+@(.+)$";
    public void check(String email){
        if (email.matches(pattern)) {
            System.out.println("Die E-Mail ist eine gültige.at Adresse.");
        } else {
            System.out.println("Die E-Mail ist keine gültige .at Adresse.");
        }
    }
}
