package pos.theorie.Kapitel5.Aufgabe6.FibonacciCatcoder;

public class calcInt {
    private int fibNteZahleInt(int n) {
        if (n == 1 || n == 0) {
            return n;
        }
        return fibNteZahleInt(n - 1) + fibNteZahleInt(n - 2);
    }

    public void ausgebenNteZahlInt(int n) {
        System.out.println("Die Fibonacci Zahle an der " + n + ". Stelle beträgt: " + this.fibNteZahleInt(n));
    }

}
