package pos.theorie.Kapitel5.Aufgabe6.FibonacciCatcoder;

import java.math.BigInteger;

public class main {
    public static void main(String[] args) {
        BigInteger fibZahl1 = new BigInteger("6");
        BigInteger fibZahl2 = new BigInteger("19");
        BigInteger fibZahl3 = new BigInteger("28");
        BigInteger fibZahl4 = new BigInteger("36");
        BigInteger fibZahl5 = new BigInteger("38");
        calcInt c1 = new calcInt();
        calcBigInt c2 = new calcBigInt();
        System.out.println("N-te Stelle mit Integer");
        c1.ausgebenNteZahlInt(6);
        c1.ausgebenNteZahlInt(19);
        c1.ausgebenNteZahlInt(36);
        c1.ausgebenNteZahlInt(38);
        System.out.println("-------------------------------------------------------");
        System.out.println("N-te Stelle mit BigInteger");
        c2.ausgebenNteZahlInt(fibZahl1);
        c2.ausgebenNteZahlInt(fibZahl2);
        c2.ausgebenNteZahlInt(fibZahl3);
        c2.ausgebenNteZahlInt(fibZahl4);
        c2.ausgebenNteZahlInt(fibZahl5);
    }
}
