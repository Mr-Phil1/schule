package pos.theorie.Kapitel5.Aufgabe6.FibonacciCatcoder;

import java.math.BigInteger;

public class calcBigInt {
    private BigInteger bigTwo = new BigInteger("2");

    private BigInteger fibNteZahleBigInt(BigInteger n) {
        if (n.compareTo(BigInteger.ZERO) == 0 || n.compareTo(BigInteger.ONE) == 0) {
            return n;
        }
        return fibNteZahleBigInt(n.subtract(BigInteger.ONE)).add(fibNteZahleBigInt(n.subtract(bigTwo)));
    }

    public void ausgebenNteZahlInt(BigInteger n) {
        System.out.println("Die Fibonacci Zahle an der " + n + ". Stelle beträgt: " + this.fibNteZahleBigInt(n));
    }
}
