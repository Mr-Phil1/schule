package pos.theorie.epicChallenge.lesenSchreiben;

import java.io.*;

public class DateiSchreiben {
    private static final String FILENAME ="src/pos/theorie/epicChallenge/input.txt";


    public static void main(String[] args) {
        try {
            File file = new File(FILENAME);
            FileWriter fileWriter =new FileWriter(file);
            BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);
            bufferedWriter.write("Inhalt 1: \n");
            bufferedWriter.write("Inhalt 2: \n");
            bufferedWriter.write("Inhalt 3: \n");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
