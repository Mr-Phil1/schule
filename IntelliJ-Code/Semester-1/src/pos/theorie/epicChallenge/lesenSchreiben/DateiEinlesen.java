package pos.theorie.epicChallenge.lesenSchreiben;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DateiEinlesen {
    private static final String FILENAME ="src/pos/theorie/epicChallenge/file.txt";

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader(FILENAME);
            BufferedReader bufferedReader= new BufferedReader(fileReader);
            String line=bufferedReader.readLine();
            System.out.println(line);
            while (line !=null){
                System.out.println(line);
                line=bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }
}
