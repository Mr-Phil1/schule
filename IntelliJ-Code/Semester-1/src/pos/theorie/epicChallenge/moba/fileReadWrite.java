package pos.theorie.epicChallenge.moba;

import java.io.*;

public class fileReadWrite {
    public void read(String FILENAME){
        try {
            FileReader fileReader = new FileReader(FILENAME);
            BufferedReader bufferedReader= new BufferedReader(fileReader);
            String line=bufferedReader.readLine();
            System.out.println(line);
            while (line !=null){
                System.out.println(line);
                line=bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void write(String FILENAME){
        try {
            File file = new File(FILENAME);
            FileWriter fileWriter =new FileWriter(file);
            BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);
            bufferedWriter.write("Inhalt 1:");
            bufferedWriter.write("Inhalt 2:");
            bufferedWriter.write("Inhalt 3:");
            

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
