package pos.theorie.epicChallenge.quantumBreakdown;

import pos.theorie.epicChallenge.lesenSchreiben.einlesen;

import java.util.ArrayList;
import java.util.List;

public class CodingContest {
    public static void main(String[] args) {
        ArrayList<String> lines = einlesen.loadFile("src/pos/theorie/epicChallenge/quantumBreakdown/level1_example.in");
        List<FlightEntry> flightEntries=new ArrayList<>();
        int n = Integer.parseInt(lines.get(0));
        for (int i = 1; i <= n; i++) {
            String line = lines.get(i);
            String[] tokens = line.split(",");
            int timestamp= Integer.parseInt(tokens[0]);
            float latitude = Float.parseFloat(tokens[1]);
            float longitude = Float.parseFloat(tokens[2]);
            float altitude = Float.parseFloat(tokens[3]);
            flightEntries.add(new FlightEntry(timestamp, latitude, longitude, altitude));
            System.out.println(new FlightEntry(timestamp, latitude, longitude, altitude));
        }
    }

}
