package pos.theorie.epicChallenge.quantumBreakdown;

public class FlightEntry {

    private int timestamp;
    private float latitude;
    private float longitude;
    private float altitude;

    public FlightEntry(int timestamp, float latitude, float longitude, float altitude) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    @Override
    public String toString() {
        return "FlightEntry{" +
                "timestamp=" + timestamp +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                '}';
    }

}
