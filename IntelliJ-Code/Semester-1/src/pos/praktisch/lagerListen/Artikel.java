package pos.praktisch.lagerListen;

public class Artikel implements Comparable<Artikel> {

    private String id;
    private String name;
    private int lagermenge;
    private double preis;

    public Artikel(String id, String name, double preis) {
        this.id = id;
        this.name = name;
        this.preis = preis;
    }

    public void setId(String id) {
        if (id != null && id.length() > 0) {
            this.id = id;
        }
    }

    public String getId() {
        return this.id;
    }

    public void setLagermenge(int lagermenge) {
        if (lagermenge >= 0) {
            this.lagermenge = lagermenge;
        }
    }

    public void mengeUmEinsErhoehen() {
        this.lagermenge++;
    }

    public int getLagermenge() {
        return this.lagermenge;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (name != null && name.length() > 3) {
            this.name = name;
        }
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public double getPreis() {
        return preis;
    }


    @Override
    public String toString() {
        return "Artikel{" + "id=" + id + ", name=" + name + ", lagermenge=" + lagermenge + ", preis=" + preis + '}';
    }

    @Override
    public int compareTo(Artikel o) {
        return this.getId().compareTo(o.getId());
    }
}

