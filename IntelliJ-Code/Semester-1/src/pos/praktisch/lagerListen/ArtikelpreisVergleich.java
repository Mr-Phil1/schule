package pos.praktisch.lagerListen;
import java.util.Comparator;
public class ArtikelpreisVergleich implements Comparator<Artikel>{

    @Override
    public int compare(Artikel o1, Artikel o2) {
        if(o1.getPreis()<o2.getPreis())
        {
            return -1;
        }
        else if(o1.getPreis()==o2.getPreis())
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }



}
