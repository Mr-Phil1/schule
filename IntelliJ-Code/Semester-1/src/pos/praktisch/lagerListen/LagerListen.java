package pos.praktisch.lagerListen;

import java.util.Collections;

public class LagerListen {
    public static void main(String[] args) {
        Lager lager = new Lager();
        Artikel a1 = new Artikel("1","WinXP",199);
        Artikel a2 = new Artikel("2","Win2000",939);
        Artikel a3 = new Artikel("3","MacOXS",99);
        Artikel a4 = new Artikel("4","AacOXS",9999);
        a1.setLagermenge(100);
        a2.setLagermenge(200);
        a3.setLagermenge(59);
        a3.setLagermenge(59);


        lager.artikelHinzufuegen(a2);
        lager.artikelHinzufuegen(a1);
        lager.artikelHinzufuegen(a3);
        lager.artikelHinzufuegen(a4);

        System.out.println(lager.lagerwert());
        lager.alleArtikelAusgeben();

        Collections.sort(lager.gibLagerliste());

        lager.alleArtikelAusgeben();

        Collections.sort(lager.gibLagerliste(),new ArtikelpreisVergleich());
        lager.alleArtikelAusgeben();

        Collections.sort(lager.gibLagerliste(), new ArtikelbezeichnungVergleich());
        lager.alleArtikelAusgeben();

        lager.loescheAlleArtikelmitBezeichnungWie("Win");

        lager.alleArtikelAusgeben();

        lager.loescheArtikelId("0");

        lager.alleArtikelAusgeben();
        lager.loescheArtikelId("3");

        lager.alleArtikelAusgeben();

    }
}
