package pos.praktisch.lagerListen;
import java.util.Comparator;

public class ArtikelbezeichnungVergleich implements Comparator<Artikel>{

    @Override
    public int compare(Artikel o1, Artikel o2) {
        return o1.getName().compareTo(o2.getName());
    }


}
