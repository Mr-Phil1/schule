package POS.Theorie.Kapitel4.Division;

public class Berechnung {

    public Berechnung() {

    }

    public int[] berechnungArr(int dividen, int divisor) {
        int[] ergebnis = new int[2];
        ergebnis[0] = dividen / divisor;
        ergebnis[1] = dividen % divisor;
        return ergebnis;
    }

    public Ergebnis berechnungObj(int dividen, int divisor) {
        Ergebnis erg = new Ergebnis();
        erg.setQuotient(dividen / divisor);
        erg.setRest(dividen % divisor);
        return erg;
    }

}
