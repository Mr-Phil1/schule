package POS.Theorie.Kapitel4.Division;

public class Main {
    public static void main(String[] args) {
        Ergebnis ergebnis;
        int dividen = 9, divisor = 2;
        int[] ergebnisInt;
        Berechnung berechnung = new Berechnung();
        ergebnisInt = berechnung.berechnungArr(dividen, divisor);
        for (int i = 0; i < ergebnisInt.length; i++) {
            System.out.print(ergebnisInt[i] + " ");
        }
        System.out.println();
        ergebnis = berechnung.berechnungObj(dividen, divisor);
        System.out.println(ergebnis);

    }
}
