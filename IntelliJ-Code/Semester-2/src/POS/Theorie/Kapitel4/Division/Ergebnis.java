package POS.Theorie.Kapitel4.Division;

import java.util.Arrays;

public class Ergebnis {
    private int quotient, rest;
    private int[] ergebnis = new int[2];

    public int getQuotient() {
        return quotient;
    }

    public void setQuotient(int quotient) {
        this.quotient = quotient;
    }

    public int getRest() {
        return rest;
    }

    public void setRest(int rest) {
        this.rest = rest;
    }


    @Override
    public String toString() {
        return "Ergebnis{" +
                "quotient=" + quotient +
                ", rest=" + rest +
                '}';
    }
}
