package POS.Theorie.Kapitel4.OoVsProzendural;

public class Kreis {
    private Koordinate mitte;
    private int radius;

    public Kreis(int radius) {
        this.radius = radius;
        mitte = new Koordinate();
    }

    public void setKoordinate(int x, int y) {
        mitte.setKooridnate(x, y);
    }

    public Koordinate getMitte() {
        return mitte;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Kreis{" +
                "mitte=" + mitte +
                ", radius=" + radius +
                '}';
    }
}
