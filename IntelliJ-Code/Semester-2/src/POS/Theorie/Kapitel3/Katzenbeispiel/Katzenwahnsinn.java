package POS.Theorie.Kapitel3.Katzenbeispiel;

import java.util.ArrayList;
import java.util.List;

public class Katzenwahnsinn {
    private static List<Katze> tierschuetzer = new ArrayList<>();

    public static void main(String[] args) {
        long free, total, millisekunden = 5000;
        int anzahl = 10000000, numBytes = 1024, strichLange = 30;
        strich("Garbage-Kollektor:", strichLange);
        speicherText(numBytes);
        for (int i = 0; i < 4; i++) {
            machKatzen1(anzahl);
            speicherText(numBytes);
            warten(millisekunden);
        }
        strich("ArrayList:", strichLange);
        speicherText(numBytes);
        for (int i = 0; i < 4; i++) {
            machKatzen2(anzahl);
            speicherText(numBytes);
            warten(millisekunden);
        }
        tierschuetzer.clear();
        strich("Normal mit Zuffalsnamen:", strichLange);
        speicherText(numBytes);
        for (int i = 0; i < 4; i++) {
            machKatzen3(anzahl);
            speicherText(numBytes);
            warten(millisekunden);
        }
        tierschuetzer.clear();
        strich("ArrayList mit Zuffalsnamen:", strichLange);
        speicherText(numBytes);
        for (int i = 0; i < 4; i++) {
            machKatzen4(anzahl);
            speicherText(numBytes);
            warten(millisekunden);
        }
    }

    private static void machKatzen1(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            Katze katze = new Katze("Katze ");
        }
    }

    private static void machKatzen2(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            tierschuetzer.add(new Katze("Katze " + i));
        }

    }

    private static void machKatzen3(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            Katze katze = new Katze(Zufall.zufallsString(7) + i);
        }

    }

    private static void machKatzen4(int anzahl) {
        for (int i = 0; i < anzahl; i++) {
            tierschuetzer.add(new Katze(Zufall.zufallsString(7) + i));
        }

    }

    private static void warten(long millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private static void speicherText(int numBytes) {
        long total = Runtime.getRuntime().totalMemory() / (numBytes * numBytes);
        long free = Runtime.getRuntime().freeMemory() / (numBytes * numBytes);
        System.out.println("Genutzter Speicher: " + (total - free) + " MB");
    }

    private static void strich(String text, int lange) {
        for (int i = 0; i < lange; i++) {
            System.out.print("-");
        }
        System.out.println();
        System.out.println(text);
    }
}
