package POS.Theorie.Kapitel3.Katzenbeispiel;

import java.nio.charset.Charset;
import java.util.Random;

public class Zufall {
    public static String zufallsString(int lange) {
        byte[] array = new byte[lange];
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        return generatedString;
    }
}
