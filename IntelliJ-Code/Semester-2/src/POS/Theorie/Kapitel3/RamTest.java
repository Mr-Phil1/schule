package POS.Theorie.Kapitel3;

public class RamTest {
    public static void main(String[] args) {

        Runtime runtime = Runtime.getRuntime();
        System.out.println("runtime.availableProcessors() = " + runtime.availableProcessors());
        System.out.println("runtime.freeMemory() = " + runtime.freeMemory()/1073742000);
        System.out.println("runtime.maxMemory() = " + runtime.maxMemory()/1073742000);
        System.out.println("runtime.totalMemory() = " + runtime.totalMemory()/1073742000);

        int s = 1000;
        long c = 0;
        int[][][] a = new int[s][s][s];
        for (int x = 0; x < s; x++) {
            for (int y = 0; y < s; y++) {
                for (int z = 0; z < s; z++) {
                    a[x][y][z] = 1;
                    c++;
                }
            }
        }
        System.out.println(c); // Anzahl Elemente

        System.out.println("runtime.availableProcessors() = " + runtime.availableProcessors());
        System.out.println("runtime.freeMemory() = " + runtime.freeMemory()/1073742000);
        System.out.println("runtime.maxMemory() = " + runtime.maxMemory()/1073742000);
        System.out.println("runtime.totalMemory() = " + runtime.totalMemory()/1073742000);
    }
}
