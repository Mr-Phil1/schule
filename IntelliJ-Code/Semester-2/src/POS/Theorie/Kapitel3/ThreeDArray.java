package POS.Theorie.Kapitel3;

public class ThreeDArray {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        System.out.println("runtime.availableProcessors() = " + runtime.availableProcessors());
        System.out.println("runtime.freeMemory() = " + runtime.freeMemory()/1073742000);
        System.out.println("runtime.maxMemory() = " + runtime.maxMemory()/1073742000);
        System.out.println("runtime.totalMemory() = " + runtime.totalMemory()/1073742000);

    }

}

