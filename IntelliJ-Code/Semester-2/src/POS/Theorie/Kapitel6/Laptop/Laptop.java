package POS.Theorie.Kapitel6.Laptop;

public class Laptop extends Komponente {
    private double preis;
    private Gehause gehaeuse;
    private Mainboard mainboard;
    private Bildschirm bildschirm;
    private Festplatte festplatte;

    /**
     *
     * @param hersteller
     * @param produktNummer
     * @param modellBezeichnung
     * @param preis
     * @param gehaeuse
     * @param mainboard
     * @param bildschirm
     * @param festplatte
     */
    public Laptop(String hersteller, String produktNummer, String modellBezeichnung, double preis,
                  Gehause gehaeuse, Mainboard mainboard, Bildschirm bildschirm, Festplatte festplatte) {
        super(hersteller, produktNummer, modellBezeichnung);
        this.preis = preis;
        this.gehaeuse = gehaeuse;
        this.mainboard = mainboard;
        this.bildschirm = bildschirm;
        this.festplatte = festplatte;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "preis=" + preis +
                ", gehaeuse=" + gehaeuse +
                ", mainboard=" + mainboard +
                ", bildschirm=" + bildschirm +
                ", festplatte=" + festplatte +
                "} " + super.toString();
    }
}
