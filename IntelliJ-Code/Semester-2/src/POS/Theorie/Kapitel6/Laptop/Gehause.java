package POS.Theorie.Kapitel6.Laptop;

public class Gehause extends Komponente {
    private String farbe, material;
    private double breite, tiefe, hoehe;

    /**
     *
     * @param hersteller
     * @param produktNummer
     * @param modellBezeichnung
     * @param farbe
     * @param material
     * @param breite
     * @param tiefe
     * @param hoehe
     */
    public Gehause(String hersteller, String produktNummer, String modellBezeichnung, String farbe, String material, double breite, double tiefe, double hoehe) {
        super(hersteller, produktNummer, modellBezeichnung);
        this.farbe = farbe;
        this.material = material;
        this.breite = breite;
        this.tiefe = tiefe;
        this.hoehe = hoehe;
    }

    @Override
    public String toString() {
        return "Gehause{" +
                "farbe='" + farbe + '\'' +
                ", material='" + material + '\'' +
                ", breite=" + breite +
                ", tiefe=" + tiefe +
                ", hoehe=" + hoehe +
                "} " + super.toString();
    }
}
