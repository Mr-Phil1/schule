package POS.Theorie.Kapitel6.Laptop;

public abstract class Komponente {
    private String hersteller, produktNummer, modellBezeichnung;

    public Komponente(String hersteller, String produktNummer, String modellBezeichnung) {
        this.hersteller = hersteller;
        this.produktNummer = produktNummer;
        this.modellBezeichnung = modellBezeichnung;
    }

    public Komponente() {
    }

    @Override
    public String toString() {
        return "Komponente{" +
                "hersteller='" + hersteller + '\'' +
                ", produktNummer='" + produktNummer + '\'' +
                ", modellBezeichnung='" + modellBezeichnung + '\'' +
                "}";
    }
}
