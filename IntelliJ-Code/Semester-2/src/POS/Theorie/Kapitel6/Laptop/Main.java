package POS.Theorie.Kapitel6.Laptop;

public class Main {
    public static void main(String[] args) {
        Gehause gehaeuse = new Gehause("Tuxedo", "1501", "Gehaeuse", "matt-Schwarz", "Aluminium", 20.0, 20.0, 20.0);
        Prozessor prozessor = new Prozessor("AMD", "4800H", "Ryzen 7");
        Ram ram = new Ram("Samsung", "CL22", "DDR4");
        Mainboard mainboard = new Mainboard("Tuxedo", "1502", "Mainboard", 2, ram, prozessor);
        Bildschirm bildschirm = new Bildschirm("Tuxedo", "1503", "Full-HD_IPS-Matt");
        Festplatte festplatte = new Festplatte("Samsung", "970 EVO Plus", "SSD");

        Laptop laptop = new Laptop("Tuxedo", "PULSE1501", "Pulse-15", 1734.45, gehaeuse, mainboard, bildschirm, festplatte);
        System.out.println(laptop);
    }
}
