package POS.Theorie.Kapitel6.Laptop;

public class Festplatte extends Komponente {
    private Lesekopf lesekopf;
    private Schreibkopf schreibkopf;

    /**
     * @param hersteller
     * @param produktNummer
     * @param modellBezeichnung
     */
    public Festplatte(String hersteller, String produktNummer, String modellBezeichnung) {
        super(hersteller, produktNummer, modellBezeichnung);
        if (lesekopf == null) {
            this.lesekopf = new Lesekopf();
        }
        this.schreibkopf = new Schreibkopf();
    }

    @Override
    public String toString() {
        return "Festplatte{" +
                "lesekopf=" + lesekopf +
                ", schreibkopf=" + schreibkopf +
                "} " + super.toString();
    }
}
