package POS.Theorie.Kapitel6.Laptop;

public class Mainboard extends Komponente {
    private int steckplaetzte;
    private Ram ram;
    private Prozessor prozessor;


    public Mainboard(String hersteller, String produktNummer, String modellBezeichnung, int steckplaetzte, Ram ram, Prozessor prozessor) {
        super(hersteller, produktNummer, modellBezeichnung);
        this.steckplaetzte = steckplaetzte;
        this.ram = ram;
        this.prozessor = prozessor;

    }

    @Override
    public String toString() {
        return "Mainboard{" +
                "steckplaetzte=" + steckplaetzte +
                ", ram=" + ram +
                ", prozessor=" + prozessor +
                "} " + super.toString();
    }
}
