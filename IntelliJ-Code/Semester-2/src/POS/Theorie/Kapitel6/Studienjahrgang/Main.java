package POS.Theorie.Kapitel6.Studienjahrgang;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Lehrer neun = new Lehrer("Neuner Dominik", new Date());
        Student mrphil = new Student("Mr Phil", new Date(), "12345");
        Student kutom = new Student("Kutz Thomas", new Date(), "12346");
        Jahrgang jahrgang4Akif = new Jahrgang("4AKIF");

        neun.addFach("dbi");
        neun.addFach("pos-t");
        neun.addFach("tinf-elekktro");
        neun.addFach("tinf-web");

        neun.addJahrgang(jahrgang4Akif.getJahrgang());

        jahrgang4Akif.addStudent(mrphil);
        jahrgang4Akif.addStudent(kutom);

        System.out.println(jahrgang4Akif);
        System.out.println(neun);
        System.out.println(mrphil);
    }
}
