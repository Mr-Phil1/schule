package POS.Theorie.Kapitel6.Studienjahrgang;

import java.util.Date;

public class Student extends Mensch {
    private String matrikelNummer;

    public Student(String name, Date geburtstag, String matrikelNummer) {
        super(name, geburtstag);
        this.matrikelNummer = matrikelNummer;
    }

    @Override
    public String toString() {
        return "Student{" +
                "matrikelNummer='" + matrikelNummer + '\'' +
                "} " + super.toString();
    }
}
