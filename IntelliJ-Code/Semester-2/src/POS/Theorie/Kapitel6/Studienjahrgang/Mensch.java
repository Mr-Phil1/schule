package POS.Theorie.Kapitel6.Studienjahrgang;

import java.util.Date;

public class Mensch {
    protected String name;
    protected Date geburtstag;

    public Mensch(String name, Date geburtstag) {
        this.name = name;
        this.geburtstag = geburtstag;
    }

    @Override
    public String toString() {
        return "Mensch{" +
                "name='" + name + '\'' +
                ", geburtstag=" + geburtstag +
                '}';
    }
}
