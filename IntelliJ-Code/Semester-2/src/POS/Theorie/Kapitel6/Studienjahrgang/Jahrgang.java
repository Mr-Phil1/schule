package POS.Theorie.Kapitel6.Studienjahrgang;

import java.util.ArrayList;
import java.util.List;

public class Jahrgang {
    private String stammklasse;
    private List<Student> studenten;

    public Jahrgang(String stammklasse) {
        this.stammklasse = stammklasse;
        this.studenten = new ArrayList<>();
    }

    public void addStudent(Student student) {
        this.studenten.add(student);
    }

    public List<Student> getJahrgang() {
        return this.studenten;
    }

    @Override

    public String toString() {
        return "Jahrgang{" +
                "stammklasse='" + stammklasse + '\'' +
                ", studenten=" + studenten +
                '}';
    }
}
