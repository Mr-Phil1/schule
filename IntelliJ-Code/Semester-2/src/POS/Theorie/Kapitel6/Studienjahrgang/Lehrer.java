package POS.Theorie.Kapitel6.Studienjahrgang;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Lehrer extends Mensch {
    private List<Student> unterricht;
    private List<String> faecher;

    public Lehrer(String name, Date geburtstag) {
        super(name, geburtstag);
        this.unterricht = new ArrayList<>();
        this.faecher = new ArrayList<>();
    }

    public void addFach(String fach) {
        this.faecher.add(fach);
    }

    public void addUnterricht(Student student) {
        this.unterricht.add(student);
    }

    public void addJahrgang(List<Student> students) {
        this.unterricht = students;
    }

    @Override
    public String toString() {
        return "Lehrer{" +
                "unterricht=" + unterricht +
                ", faecher=" + faecher +
                "} " + super.toString();
    }
}
