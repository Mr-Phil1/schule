package POS.Theorie.Kapitel6.Geometrie;

public class Main {
    private static int length = 66;
    private static char dashSymbol = '-';

    public static void main(String[] args) {
        GeoTester geoTester = new GeoTester();
        AFigur kreis1 = new Kreis(false, "Kreis1", 3.0);
        AFigur kreis3 = new Kreis(false, "Kreis3", 9.0);
        AFigur kreis2 = new Kreis(false, "Kreis2", 5.0);
        AFigur dreieck1 = new Dreieck(true, "Dreieck1", 5.0);
        AFigur dreieck2 = new Dreieck(true, "Dreieck2", 7.0);
        AFigur dreieck3 = new Dreieck(true, "Dreieck3", 12.0);

        geoTester.addFigur(kreis1);
        geoTester.addFigur(kreis2);
        geoTester.addFigur(kreis3);
        geoTester.addFigur(dreieck1);
        geoTester.addFigur(dreieck2);
        geoTester.addFigur(dreieck3);

        printDash(length, '#');
        System.out.println("Ausgabe aller Figuren:");
        geoTester.alleFigurenAusgeben();
        printDash(length, dashSymbol);

        System.out.println("Ausgabe aller Dreiecke:");
        tabOutput(geoTester.getDreieck());
        tabOutput(geoTester.ausgabeDreieck());
        printDash(length, dashSymbol);

        System.out.println("Ausgabe aller Kreise:");
        tabOutput(geoTester.getKreise());
        tabOutput(geoTester.ausgabeKreis());
        printDash(length, dashSymbol);

        System.out.println("Ausgabe der Gesamt-Flächen:");
        tabOutput(geoTester.getFlaeche(2));
        printDash(length, dashSymbol);

        System.out.println("Ausgabe des Gesamt-Umfanges:");
        tabOutput(geoTester.getUmfang(2));
        printDash(length, dashSymbol);

        printDash(length, '#');
    }

    public static void printDash(int length, char symbol) {
        for (int i = 0; i < length; i++) {
            System.out.print(symbol);
        }
        System.out.println();
    }

    public static void tabOutput(Object text) {
        System.out.println("\t" + text);
    }
}