package POS.Theorie.Kapitel6.Geometrie;

public interface FigurHelper {
    double getFlaeche();

    double getUmfang();
}

