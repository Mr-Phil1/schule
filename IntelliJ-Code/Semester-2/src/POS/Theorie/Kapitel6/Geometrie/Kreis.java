package POS.Theorie.Kapitel6.Geometrie;

public class Kreis extends AFigur implements FigurHelper {

    private String bezeichnung;
    private double radius;

    public Kreis(boolean eckig, String bezeichnung, double radius) {
        super(eckig);
        this.bezeichnung = bezeichnung;
        this.radius = radius;
    }


    @Override
    public double getFlaeche() {
        return Math.PI * Math.sqrt(radius);
    }

    @Override
    public double getUmfang() {
        return Math.PI * (radius * radius);
    }

    @Override
    public String toString() {
        return "Kreis{" +
                "bezeichnung='" + bezeichnung + '\'' +
                ", radius=" + radius +
                "} " + super.toString();
    }
}
