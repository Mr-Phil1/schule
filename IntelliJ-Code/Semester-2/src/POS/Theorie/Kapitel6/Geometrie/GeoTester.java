package POS.Theorie.Kapitel6.Geometrie;

import java.util.ArrayList;
import java.util.List;

public class GeoTester {
    private List<AFigur> figuren;
    private double umfang, flaeche;

    public GeoTester() {
        figuren = new ArrayList<>();
    }

    public void addFigur(AFigur figur) {
        this.figuren.add(figur);
    }

    public double berechneUmfang(int places) {
        for (AFigur figur : this.figuren) {
            if (!figur.isEckig()) {
                Kreis kreis = (Kreis) figur;
                umfang += kreis.getUmfang();
            } else {
                Dreieck dreieck = (Dreieck) figur;
                umfang += dreieck.getUmfang();
            }
        }
        return round(umfang, places);
    }

    public double berechneFlaeche(int places) {
        for (AFigur figur : this.figuren) {
            if (!figur.isEckig()) {
                Kreis kreis = (Kreis) figur;
                flaeche += kreis.getFlaeche();
            } else {
                Dreieck dreieck = (Dreieck) figur;
                flaeche += dreieck.getFlaeche();
            }
        }
        return round(flaeche, places);
    }

    public double getUmfang(int places) {
        if (umfang != 0) {
            return umfang;
        } else {
            return berechneUmfang(places);
        }
    }

    public double getFlaeche(int places) {
        if (flaeche != 0) {
            return flaeche;
        } else {
            return berechneFlaeche(places);
        }
    }

    public void alleFigurenAusgeben() {
        for (AFigur figur : figuren) {
            Main.tabOutput(figur.toString());
            Main.printDash(75, '+');
        }

    }

    public List<Dreieck> getDreieck() {
        List<Dreieck> dreieck = new ArrayList<>();
        for (AFigur aFigur : figuren) {
            if (aFigur.isEckig()) {
                dreieck.add((Dreieck) aFigur);
            }
        }
        return dreieck;
    }

    public List<Kreis> getKreise() {
        List<Kreis> kreis = new ArrayList<>();
        for (AFigur aFigur : figuren) {
            if (!aFigur.isEckig()) {
                kreis.add((Kreis) aFigur);
            }
        }
        return kreis;
    }

    public String ausgabeKreis() {
        String tmp = "";
        for (Kreis kreis : getKreise()) {
            tmp += "\n\t";
            tmp += kreis.toString();
        }
        return tmp;
    }

    public String ausgabeDreieck() {
        String tmp = "";
        for (Dreieck dreieck : getDreieck()) {
            tmp += "\n\t";
            tmp += dreieck.toString();
        }
        return tmp;
    }

    private double round(double value, int places) {
        if (places < 0) {
            return value;
        } else {

            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        }
    }
}
