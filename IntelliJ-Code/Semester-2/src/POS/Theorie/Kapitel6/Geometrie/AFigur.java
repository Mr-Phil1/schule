package POS.Theorie.Kapitel6.Geometrie;

public abstract class AFigur {
    private boolean eckig;

    public AFigur(boolean eckig) {
        this.eckig = eckig;
    }

    public boolean isEckig() {
        return eckig;
    }

    public void setEckig(boolean eckig) {
        this.eckig = eckig;
    }

    @Override
    public String toString() {
        return "AFigur{" +
                "eckig=" + eckig +
                '}';
    }
}

