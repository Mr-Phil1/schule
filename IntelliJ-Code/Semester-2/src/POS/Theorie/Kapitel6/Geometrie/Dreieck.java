package POS.Theorie.Kapitel6.Geometrie;

public class Dreieck extends AFigur implements FigurHelper {
    private String bezeichnung;
    private double laengeSeite;

    public Dreieck(boolean eckig, String bezeichnung, double laengeSeite) {
        super(eckig);
        this.bezeichnung = bezeichnung;
        this.laengeSeite = laengeSeite;
    }

    @Override
    public double getFlaeche() {
        double seite = getUmfang() / 2;
        return Math.sqrt(seite * (seite - laengeSeite) * (seite - laengeSeite) * (seite - laengeSeite));
    }

    @Override
    public double getUmfang() {
        return laengeSeite * 3;
    }

    @Override
    public String toString() {
        return "Dreieck{" +
                "bezeichnung='" + bezeichnung + '\'' +
                ", laengeSeite=" + laengeSeite +
                "} " + super.toString();
    }
}
