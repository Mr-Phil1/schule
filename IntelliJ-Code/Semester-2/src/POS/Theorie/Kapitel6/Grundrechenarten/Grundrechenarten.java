package POS.Theorie.Kapitel6.Grundrechenarten;

public interface Grundrechenarten {
    Bruch addieren(Bruch bruch);

    Bruch subrahieren(Bruch bruch);

    Bruch multiplizieren(Bruch bruch);

    Bruch dividieren(Bruch bruch) throws NullPointerException;
}
