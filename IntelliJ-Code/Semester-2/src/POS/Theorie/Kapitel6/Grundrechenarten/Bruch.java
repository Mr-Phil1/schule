package POS.Theorie.Kapitel6.Grundrechenarten;

public class Bruch implements Grundrechenarten {
    public static final Bruch NULL = new Bruch(0, 0);
    public static final Bruch EIN_HALB = new Bruch(1, 2);
    public static final Bruch EINS = new Bruch(1, 1);
    private int zaehler, nenner;
    private Bruch ergebnis;

    private Bruch() {

    }

    public Bruch(int zaehler, int nenner) {
        this.setNenner(nenner);
        this.setZaehler(zaehler);
    }

    public int getZaehler() {
        return zaehler;
    }

    public void setZaehler(int zaehler) {
        this.zaehler = zaehler;
    }

    public int getNenner() {
        return nenner;
    }

    public void setNenner(int nenner) {
        this.nenner = nenner;
    }

    @Override
    public Bruch addieren(Bruch bruch) {
        ergebnis = new Bruch();
        if (this.nenner == bruch.getNenner()) {
            ergebnis.setZaehler(this.zaehler + bruch.getZaehler());
            ergebnis.setNenner(this.nenner);
        } else {
            ergebnis.setZaehler(this.zaehler * bruch.getNenner() + this.zaehler * this.nenner);
            ergebnis.setNenner(this.nenner * bruch.getNenner());
        }
        return ergebnis;
    }

    @Override
    public Bruch subrahieren(Bruch bruch) {
        ergebnis = new Bruch();
        if (this.nenner == bruch.getNenner()) {
            ergebnis.setZaehler(this.zaehler - bruch.getZaehler());
            ergebnis.setNenner(this.nenner);
        } else {
            ergebnis.setZaehler(this.zaehler * bruch.getNenner() - this.zaehler * this.nenner);
            ergebnis.setNenner(this.nenner * bruch.getNenner());
        }
        return ergebnis;
    }

    @Override
    public Bruch multiplizieren(Bruch bruch) {
        ergebnis = new Bruch();
        ergebnis = new Bruch();
        ergebnis.setNenner(this.nenner * bruch.getNenner());
        ergebnis.setZaehler(this.zaehler * bruch.getZaehler());
        return ergebnis;
    }


    @Override
    public Bruch dividieren(Bruch bruch) throws NullPointerException {
        if (nullPruefung(this, bruch)) {
            ergebnis.setZaehler(this.zaehler * bruch.getNenner());
            ergebnis.setNenner(this.nenner * bruch.getZaehler());
            return ergebnis;
        } else {
            throw new NullPointerException("Division durch 0 ist nicht möglich");
        }
    }

    private boolean nullPruefung(Bruch eins, Bruch zwei) {
        if (eins.getNenner() != 0 && zwei.getNenner() != 0 && eins.getZaehler() != 0 && zwei.getZaehler() != 0) {
            return true;
        } else {
            return false;
        }
    }

    public String ausgabe() {
        return "Ergebnis: " + zaehler + "/" + nenner;
    }
}
