package POS.Theorie.Kapitel6.Grundrechenarten;

public class Bruchrechnen {
    public static void main(String[] args) {
        Bruch bruch = new Bruch(1, 2);
        try {
            System.out.println(bruch.addieren(Bruch.EIN_HALB).ausgabe());
            System.out.println(bruch.subrahieren(Bruch.EINS).ausgabe());
            System.out.println(bruch.multiplizieren(Bruch.EINS).ausgabe());
            System.out.println(bruch.dividieren(Bruch.NULL).ausgabe());
        } catch (NullPointerException nullPointerException) {
            System.out.println(nullPointerException.getMessage());
        }
    }
}
