package POS.Theorie.Kapitel6.MyListElementInterface;

public interface MyList {
    void add(int value);

    void add(int value, int index);

    int get(int index);

    int remove(int index);

    int size();
}
