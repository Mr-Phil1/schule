package POS.Theorie.Kapitel6.MyListElementInterface;

import java.util.Random;

public class Check {
    public static void main(String[] args) {
        Random random = new Random();
        MyListElement myListElement = new MyListElement();
        for (int i = 0; i < 10; i++) {
            myListElement.add(random.nextInt(100) + 1);
        }
        System.out.println("Size of myListElement:\n\t" + myListElement.size());
        printDash(55);
        System.out.println("Entries in myListElement:\n\t" + myListElement);
        printDash(55);
       try {
           myListElement.add(10, 7);
           System.out.println(myListElement);
           printDash(55);
           System.out.println(myListElement.remove(5));
           printDash(55);
           System.out.println(myListElement);
           printDash(55);
           System.out.println(myListElement.get(2));
           printDash(55);
       }catch (NullPointerException nullPointerException){
           System.out.println(nullPointerException.getMessage());
       }
    }
    private static void printDash(int lenght){
        for (int i = 0; i < lenght; i++) {
            System.out.print("-");
        }
        System.out.println();
    }
}

