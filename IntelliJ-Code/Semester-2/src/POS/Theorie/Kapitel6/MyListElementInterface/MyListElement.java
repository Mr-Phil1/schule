package POS.Theorie.Kapitel6.MyListElementInterface;

public class MyListElement implements MyList {
    private Integer value;
    private MyListElement nextElement;
    private int tmpValue, index;

    public MyListElement() {
    }

    public MyListElement(Integer value) {
        this.value = value;
    }


    @Override
    public void add(int value) {
        if (this.value == null) {
            this.value = value;
        } else {
            if (nextElement == null) {
                this.nextElement = new MyListElement(value);
            } else {
                this.nextElement.add(value);
            }
        }
    }

    @Override
    public void add(int value, int index) {
        if (index <= size()) {
            if (index == 0) {
                this.value = value;
            } else {
                index--;
                this.nextElement.add(value, index);
            }
        } else {
            add(value);
        }
    }

    @Override
    public int get(int index) {
        tmpValue = 0;
        MyListElement tmp = this;
        for (int i = 0; i <= value; i++) {
            value = tmp.value;
            tmp = tmp.nextElement;
        }
        return value;
    }

    @Override
    public int remove(int index) {
        if (index <= size()) {
            if (index == 0) {
                tmpValue = this.value;
                this.value = null;
                return tmpValue;
            } else {
                index--;
                tmpValue = this.nextElement.remove(index);
            }
        }
        return tmpValue;
    }

    @Override
    public int size() {
        MyListElement tmp = this;
        for (index = 0; tmp != null; index++) {
            tmp = tmp.nextElement;
        }
        return index;
    }

    @Override
    public String toString() {
        MyListElement tmp = this;
        StringBuilder sb = new StringBuilder();
        while (tmp != null) {
            sb.append(tmp.value + " ");
            tmp = tmp.nextElement;
        }
        return sb.toString();
    }



}
