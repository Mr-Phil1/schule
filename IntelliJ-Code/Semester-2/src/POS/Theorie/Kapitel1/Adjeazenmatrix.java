package POS.Theorie.Kapitel1;

public class Adjeazenmatrix {
    public static void main(String[] args) {
        int [][] adjazenmatrix = new int[6][6];

        adjazenmatrix[0][0]=1;  // Kante zwischen Knoten 1 und 1
        adjazenmatrix[0][1]=1;  // Kante zwischen Knoten 1 und 2
        adjazenmatrix[0][4]=1;  // Kante zwischen Knoten 1 und 5

        adjazenmatrix[1][0]=1;  // Kante zwischen Knoten 2 und 1
        adjazenmatrix[1][2]=1;  // Kante zwischen Knoten 2 und 3
        adjazenmatrix[1][4]=1;  // Kante zwischen Knoten 2 und 5

        for (int zeile = 0; zeile < adjazenmatrix.length; zeile++) {
            for (int spalte = 0; spalte < adjazenmatrix[zeile].length; spalte++) {
                System.out.print(adjazenmatrix[zeile][spalte]+" ");
            }
            System.out.println();
        }

    }
}
