package POS.Theorie.Kapitel2.Aufgabe4;

public class Fibonacci {
    public static void main(String[] args) {
        // Berechnung berechnung = new Berechnung(50);
        System.out.println("Bis zur wievielten Stelle möchten Sie die Fibonacci-Zahlen ausgeben?");
        Berechnung berechnung = new Berechnung(Eingabe.inputInt("Ihre Eingabe bitte: ", "[0-9]+") + 1);
        berechnung.ausgebenInt();
        berechnung.ausgebenLong();
    }
}
