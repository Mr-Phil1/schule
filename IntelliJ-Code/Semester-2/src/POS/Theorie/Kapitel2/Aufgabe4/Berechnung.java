package POS.Theorie.Kapitel2.Aufgabe4;


public class Berechnung {
    private int[] fibInt;
    private long[] fibLong;
    private int stelle, summeInt, vorgangerInt = 1, nachfolgerInt = 1;
    private long summeLong, vorgangerLong = 1, nachfolgerLong = 1;
    private final String TITLE_LONG = "Long", TITLE_INT = "Integer";

    public Berechnung(int n) {
        this.setStelle(n);
        fibInt = new int[n];
        fibLong = new long[n];
    }


    public int getStelle() {
        return stelle;
    }

    private void setStelle(int stelle) {
        this.stelle = stelle;
    }

    public void ausgebenInt() {
        this.ausgabeStellen(TITLE_INT);
        this.einsetzenInt(this.getStelle());
        for (int i = 0; i < this.fibInt.length; i++) {
            if (i < 10) {
                //this.ausgabeZahl(i, 'i');
                System.out.println("\tDie Fibonacci-Zahl an der 0" + i + ". Stelle ist: " + this.fibInt[i]);
            } else {
                //this.ausgabeZahl(i, 'i');
                System.out.println("\tDie Fibonacci-Zahl an der " + i + ". Stelle ist: " + this.fibInt[i]);
            }
        }
    }


    public void testEingabenInt() {
        for (int i = 0; i < this.fibInt.length; i++) {
            this.fibInt[i] = i;
        }
    }


    private int berechnenInt(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        this.summeInt = this.vorgangerInt + this.nachfolgerInt;
        this.vorgangerInt = this.nachfolgerInt;
        this.nachfolgerInt = this.summeInt;
        return this.summeInt;
    }


    private void einsetzenInt(int n) {
        for (int i = 0; i < n; i++) {
            this.fibInt[i] = this.berechnenInt(i);
        }
    }

    public void ausgebenLong() {
        this.ausgabeStellen(TITLE_LONG);
        this.einsetzenLong(this.getStelle());
        for (int i = 0; i < this.fibLong.length; i++) {
            if (i < 10) {
                // this.ausgabeZahl(i, 'l');
                System.out.println("\tDie Fibonacci-Zahl an der 0" + i + ". Stelle ist: " + this.fibLong[i]);
            } else {
                //  this.ausgabeZahl(i, 'l');
                System.out.println("\tDie Fibonacci-Zahl an der " + i + ". Stelle ist: " + this.fibLong[i]);
            }
        }
        this.strich();
    }

    private long berechnenLong(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        this.summeLong = this.vorgangerLong + this.nachfolgerLong;
        this.vorgangerLong = this.nachfolgerLong;
        this.nachfolgerLong = this.summeLong;
        return this.summeLong;
    }

    private void einsetzenLong(int n) {
        for (int i = 0; i < n; i++) {
            this.fibLong[i] = this.berechnenLong(i);
        }
    }

    public void testEingabenLong() {
        for (int i = 0; i < this.fibLong.length; i++) {
            this.fibLong[i] = i;
        }
    }

    private void strich() {
        System.out.println("----------------------------------------------------------------------------------------");
    }

    private void ausgabeStellen(String title) {
        this.strich();
        System.out.println("Es werden nun die ersten " + this.getStelle() + " Fibonacci-Stellen als " + title + " ausgegeben: ");
    }

    private void ausgabeZahl(int n, char type) {
        if (type == 'i') {
            System.out.println("\tDie Fibonacci-Zahl an der " + n + ". Stelle ist: " + this.fibInt[n]);
        } else if (type == 'l') {
            System.out.println("\tDie Fibonacci-Zahl an der " + n + ". Stelle ist: " + this.fibLong[n]);
        }
    }

}
