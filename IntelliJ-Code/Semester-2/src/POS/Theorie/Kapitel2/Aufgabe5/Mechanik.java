package POS.Theorie.Kapitel2.Aufgabe5;

public class Mechanik {
    private final char WASSER = 'W';
    private final char ZERSTOERER = 'Z';
    private final char FLUGZEUGTRAEGER = 'F';
    private final char SCHIFF = 'S';
    private int breite, lange;
    private Character[][] spielfeld;
    private Integer[][] testArray;

    public Mechanik(int breite, int lange) {
        this.spielfeld = new Character[breite][lange];
        this.setLange(lange);
        this.setBreite(breite);
        this.initialisiereSpielfeld(breite, lange);

    }

    private void setBreite(int breite) {
        this.breite = breite;
    }

    private void setLange(int lange) {
        this.lange = lange;
    }

    private int getBreite() {
        return breite;
    }

    private int getLange() {
        return lange;
    }

    public void spielStarten() {
        this.platziereSchiff(this.getBreite(), this.getLange(), 4);
        this.platziereFlugzeutraeger(this.getBreite(), this.getLange(), 5);
        this.platziereZerstoerer(this.getBreite(), this.getLange(), 6);
        this.strich(this.getLange());
        this.ausgebenSpielfeld();
        this.strich(this.getLange());
    }

    private void initialisiereSpielfeld(int breite, int lange) {
        for (int i = 0; i < breite; i++) {
            for (int j = 0; j < lange; j++) {
                this.spielfeld[i][j] = this.WASSER;
            }
        }
    }

    public void platziereSchiff(int breite, int lange, int grosse) {
        int schiffBreite = Zufall.gibZuallsWert(breite);
        int schiffLange = Zufall.gibZuallsWert(lange);
        for (int i = 0; i < grosse; i++) {
            if (lange <= schiffLange + i) {
                this.spielfeld[schiffBreite][(schiffLange + i) % this.getBreite()] = this.SCHIFF;

            } else {
                this.spielfeld[schiffBreite][schiffLange + i] = this.SCHIFF;
            }
        }
    }

    private void platziereZerstoerer(int breite, int lange, int grosse) {
        int schiffBreite = Zufall.gibZuallsWert(breite);
        int schiffLange = Zufall.gibZuallsWert(lange);
        for (int i = 0; i < grosse; i++) {
            if (lange <= schiffLange + i) {
                this.spielfeld[schiffBreite][(schiffLange + i) % this.getBreite()] = this.ZERSTOERER;
            } else {
                this.spielfeld[schiffBreite][schiffLange + i] = this.ZERSTOERER;
            }
        }
    }

    private void platziereFlugzeutraeger(int breite, int lange, int grosse) {
        int schiffBreite = Zufall.gibZuallsWert(breite);
        int schiffLange = Zufall.gibZuallsWert(lange);
        for (int i = 0; i < grosse; i++) {
            if (lange <= schiffLange + i) {
                this.spielfeld[schiffBreite][(schiffLange + i) % this.getBreite()] = this.FLUGZEUGTRAEGER;
            } else {
                this.spielfeld[schiffBreite][schiffLange + i] = this.FLUGZEUGTRAEGER;
            }
        }
    }

    public void ausgebenSpielfeld() {
        for (int i = 0; i < this.spielfeld.length; ++i) {
            for (int j = 0; j < this.spielfeld[i].length; ++j) {
                System.out.print(this.spielfeld[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }


    private void strich(int lange) {
        for (int i = 0; i < lange; i++) {
            System.out.print("--");
        }
        System.out.println();
    }

    private void testArrayBefuellen(int zeile, int spalte, int zahl) {
        testArray = new Integer[zeile][spalte];
        for (int i = 0; i < zeile * spalte; ++i) {

            this.testArray[i % zeile][i / spalte] = zahl;

            // System.out.println();
        }
    }

    private void ausgebenTestArryModulo() {
        for (int i = 0; i < testArray.length; i++) {
            for (int j = 0; j < testArray[i].length; j++) {
                System.out.print(testArray[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public void testArry(int zeile, int spalte, int zahl) {
        if (zeile != 0 || spalte != 0) {

            this.testArrayBefuellen(zeile, spalte, zahl);
        }else {
            this.testArrayBefuellen(5, 5, 5);
        }
        this.ausgebenTestArryModulo();
    }

}
