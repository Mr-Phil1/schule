package POS.Theorie.Kapitel2.Aufgabe6;

public class MyListElement {
    private Integer value;
    private MyListElement next;

    public MyListElement(int value) {
        this.value = value;
    }

    public void add(int newValue) {
        if (this.value == null) {
            this.value = newValue;
        } else {
            if (this.next == null) {
                this.next = new MyListElement(newValue);
            } else {
                this.next.add(newValue);
            }
        }
    }

    public String toString() {
        MyListElement tmp = this;
        StringBuilder sb = new StringBuilder();
        while (tmp != null) {
            sb.append(tmp.value+" ");
            tmp = tmp.next;
        }
        return sb.toString();
    }


}
