package POS.Theorie.Kapitel2.Aufgabe6;

public class Main {
    public static void main(String[] args) {
        MyListElement element = new MyListElement(5);
        element.add(7);
        element.add(0);
        element.add(6);
        for (int i = 1; i < 20; i++) {
            element.add(i*2);
        }

        System.out.println(element.toString());
    }

}
