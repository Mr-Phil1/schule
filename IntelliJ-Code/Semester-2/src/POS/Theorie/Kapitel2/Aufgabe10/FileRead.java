package POS.Theorie.Kapitel2.Aufgabe10;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class FileRead {
    private static java.io.File file;
    private static Scanner scan;
    private static String valueStr;
    private static Double valueDouble;
    private static int valueInt;


    /**
     * @param filePath Der Speicherort für die einzulesenden Datei
     * @param fileName Der Name für die einzulesenden Datei
     * @param fileExt  Die Endung für die einzulesenden Datei
     * @return Gibt den Zeitpunkt der letzten Veränderung in millisekund als long-Wert zurück
     */
    private static long FileLastModified(String filePath, String fileName, String fileExt) {
        file = new java.io.File(filePath + fileName + fileExt);
        return file.lastModified();
    }

    public static boolean FileExists(String filePath, String fileName, String fileExt) {
        file = new java.io.File(filePath + fileName + fileExt);
        return file.exists();
    }

    public static String FilePath(String filePath, String fileName, String fileExt) {
        file = new java.io.File(filePath + fileName + fileExt);
        return file.getAbsolutePath();
    }

    /**
     * @param filePath Der Speicherort für die einzulesenden Datei
     * @param fileName Der Name für die einzulesenden Datei
     * @param fileExt  Die Endung für die einzulesenden Datei
     * @return Gibt den Zeitpunkt der letzten Veränderung als formatierten UTC String zurück.
     */
    public static String FileLastModifiedTime(String filePath, String fileName, String fileExt) {
        LocalDateTime localDateTime = new Timestamp(FileLastModified(filePath, fileName, fileExt)).toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(formatter);
    }

    /**
     * @return
     */
    private static DateTimeFormatter dateTimeFormat() {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return formatter;
    }

    /**
     * @param filePath Der Speicherort für die einzulesenden Datei
     * @param fileName Der Name für die einzulesenden Datei
     * @param fileExt  Die Endung für die einzulesenden Datei
     * @return Gibt den in der Datei gespeicherten Wert als String zurück
     */
    public static String FileReadStr(String filePath, String fileName, String fileExt) {
        try {
            scan = new Scanner(file = new java.io.File(filePath + fileName + fileExt));
            while (scan.hasNextLine()) {
                valueStr = scan.nextLine();
            }
            scan.close();
            return valueStr;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valueStr;
    }

    /**
     * @param filePath Der Speicherort für die einzulesenden Datei
     * @param fileName Der Name für die einzulesenden Datei
     * @param fileExt  Die Endung für die einzulesenden Datei
     * @return Gibt den in der Datei gespeicherten Wert als Double zurück
     */
    public static double FileReadDouble(String filePath, String fileName, String fileExt) {
        try {
            scan = new Scanner(file = new java.io.File(filePath + fileName + fileExt));
            while (scan.hasNextLine()) {
                valueDouble = Double.parseDouble(scan.nextLine());
            }
            scan.close();
            return valueDouble;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valueDouble;
    }

    /**
     * @param filePath Der Speicherort für die einzulesenden Datei
     * @param fileName Der Name für die einzulesenden Datei
     * @param fileExt  Die Endung für die einzulesenden Datei
     * @return Gibt den in der Datei gespeicherten Wert als int zurück
     */
    public static int FileReadInt(String filePath, String fileName, String fileExt) {
        try {
            scan = new Scanner(file = new java.io.File(filePath + fileName + fileExt));
            while (scan.hasNextLine()) {
                valueInt = Integer.parseInt(scan.nextLine());
            }
            scan.close();
            return valueInt;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valueInt;
    }

}
