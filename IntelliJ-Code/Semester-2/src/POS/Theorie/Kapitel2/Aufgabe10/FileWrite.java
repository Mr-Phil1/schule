package POS.Theorie.Kapitel2.Aufgabe10;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class FileWrite {

    private static File file;
    private static FileWriter writer;
    private static LocalDateTime dateTime = LocalDateTime.now();
    private static String timeDate = dateTime.getYear() + "-" + dateTime.getMonthValue() + "-" + dateTime.getDayOfMonth() + "_" + dateTime.getHour() + "-" + dateTime.getMinute() + "_";

    /**
     * @param filePath  Der zukünftige Speicherort für die Datei
     * @param fileName  Der zukünftige Name für die Datei
     * @param fileExt   Die zukünftige Endung für die Datei
     * @param writeStr1 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @throws IOException
     */
    public FileWrite(String filePath, String fileName, String fileExt, String writeStr1) {
        try {
            file = new File(filePath + fileName + fileExt);
            writer = new FileWriter(file);
            //System.out.println("Speicherort der Datei: " + file.getAbsolutePath());
            writer.append(writeStr1);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param filePath  Der zukünftige Speicherort für die Datei
     * @param fileName  Der zukünftige Name für die Datei
     * @param fileExt   Die zukünftige Endung für die Datei
     * @param writeStr1 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr2 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @throws IOException
     */
    public FileWrite(String filePath, String fileName, String fileExt, String writeStr1, String writeStr2) {
        try {
            file = new File(filePath + fileName + fileExt);
            writer = new FileWriter(file);
            // System.out.println("Speicherort der Datei: " + file.getAbsolutePath());
            writer.append(writeStr1);
            writer.append(writeStr2);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param filePath  Der zukünftige Speicherort für die Datei
     * @param fileName  Der zukünftige Name für die Datei
     * @param fileExt   Die zukünftige Endung für die Datei
     * @param writeStr1 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr2 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr3 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @throws IOException
     */
    public FileWrite(String filePath, String fileName, String fileExt, String writeStr1, String writeStr2, String writeStr3) {
        try {
            file = new File(filePath + fileName + fileExt);
            writer = new FileWriter(file);
            //System.out.println("Speicherort der Datei: " + file.getAbsolutePath());
            writer.append(writeStr1);
            writer.append(writeStr2).append(writeStr3);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param filePath  Der zukünftige Speicherort für die Datei
     * @param fileName  Der zukünftige Name für die Datei
     * @param fileExt   Die zukünftige Endung für die Datei
     * @param writeStr1 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr2 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr3 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr4 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @throws IOException
     */
    public FileWrite(String filePath, String fileName, String fileExt, String writeStr1, String writeStr2, String writeStr3, String writeStr4) {
        try {
            file = new File(filePath + fileName + fileExt);
            writer = new FileWriter(file);
            //System.out.println("Speicherort der Datei: " + file.getAbsolutePath());
            writer.append(writeStr1);
            writer.append(writeStr2).append(writeStr3).append(writeStr4);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param filePath  Der zukünftige Speicherort für die Datei
     * @param fileName  Der zukünftige Name für die Datei
     * @param fileExt   Die zukünftige Endung für die Datei
     * @param writeStr1 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr2 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr3 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @param writeStr4 Der zukünftige Inhalt der in der Datei gespeichert werden soll
     * @throws IOException
     */
    public static void writeFileStr4(String filePath, String fileName, String fileExt, String writeStr1, String
            writeStr2, String writeStr3, String writeStr4) {
        try {
            file = new File(filePath + timeDate + fileName + fileExt);
            writer = new FileWriter(file);
            // System.out.println("Speicherort der Datei: " + file.getAbsolutePath());
            writer.append(writeStr1);
            writer.append(writeStr2).append(writeStr3).append(writeStr4);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeFileStr1(String filePath, String fileName, String fileExt, String writeStr1) {
        try {
            file = new File(filePath + fileName + fileExt);
            writer = new FileWriter(file);
            writer.append(writeStr1);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //writeStr,
    //                pathNameR = "FileWrite\\src\\", dirName2 = "2021\\", fileName2 = "filename", fileExt2 = ".txt",
}

