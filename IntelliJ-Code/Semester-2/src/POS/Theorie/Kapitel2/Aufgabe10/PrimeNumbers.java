package POS.Theorie.Kapitel2.Aufgabe10;


import java.util.Stack;

public class PrimeNumbers {
    public static void main(String[] args) {

        DeletablePrime prime = new DeletablePrime();
        System.out.println(prime.reduce("4125673"));
        System.out.println(prime.reduce("41256793"));
        System.out.println(prime.reduce("337424981"));
        System.out.println(prime.reduce("537430451"));
        System.out.println(prime.reduce("200899998"));
        System.out.println(prime.reduce("537499093"));
        System.out.println(prime.reduce("2147483059"));
        System.out.println(prime.reduce("410256793"));
        System.out.println(prime.reduce("567629137"));
        System.out.println(prime.reduce("46216567629137"));
        /*

        int number1 = 4125673, number2 = 567629137, test = 4567, number;
        String numberStr, filePath = "Semester-2/src/POS/Theorie/Kapitel2/Aufgabe10/", fileName = "primeNumber", fileExt = ".txt";
    // number=test;
       // number=number2;
        Stack<Integer> stack = null;
        FileCreate fileCreate;

        if (!FileRead.FileExists(filePath, fileName, fileExt)) {
            fileCreate = new FileCreate(filePath, fileName, fileExt);
        }
        numberStr=Integer.toString(number);
        if (!FileRead.FileExists(filePath, numberStr, fileExt)) {
            fileCreate = new FileCreate(filePath, numberStr, fileExt);
        }
        // prime.setPrimeNumber("4125673");
        //stack = prime.test();
        long start = System.currentTimeMillis();
        for (int i = 0; i < number; i++) {
            prime.setPrimeNumber(Integer.toString(i));
            stack = prime.test();
        }
        long end = System.currentTimeMillis();
        double diff = (end - start) / 1000.0;
        System.out.println();
        System.out.println(stack);
        System.out.println();
        FileWrite.writeFileStr1(filePath, numberStr, fileExt, stack.toString());
        System.out.println("Runtime: " + diff + " s");
        System.out.println("Runtime: " + diff / 60.0 + " min");
        // System.out.println(prime.sortStack(stack));*/

    }

}
