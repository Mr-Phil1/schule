package POS.Theorie.Kapitel2.Aufgabe10;

import java.math.BigInteger;
import java.util.Stack;

public class DeletablePrimeNumbers {
    public static void main(String[] args) {
        int counter = 0;
        String s = "46216567629137";
        Stack<String> stack = new Stack<>();
        if (new BigInteger(s).isProbablePrime(100)) {
            stack.push(s);
        }
        while (!stack.isEmpty()) {
            s = stack.pop();
            if (s.length() == 1) {
                counter++;
            } else {
                for (int i = 0; i < s.length(); i++) {
                    String tmp = s.substring(0, i) + s.substring(i + 1);
                    if (new BigInteger(tmp).isProbablePrime(100)) {
                        stack.push(tmp);
                    }

                }
            }
        }
        System.out.println(counter);
    }

}
