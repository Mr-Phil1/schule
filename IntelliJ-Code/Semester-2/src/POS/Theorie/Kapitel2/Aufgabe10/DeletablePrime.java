package POS.Theorie.Kapitel2.Aufgabe10;

import java.math.BigInteger;
import java.util.Stack;

public class DeletablePrime {
    private int counter =0;
    private BigInteger big;
    private String primeNumber;
    private Stack<Integer> prime;

    public DeletablePrime() {
        this.prime = new Stack<>();
    }
    public int reduce(String number){
        if (counter!=0){
            counter=0;
        }
        String s = number;
        Stack<String> stack = new Stack<>();
        if (new BigInteger(s).isProbablePrime(100)) {
            stack.push(s);
        }
        while (!stack.isEmpty()) {
            s = stack.pop();
            if (s.length() == 1) {
               counter++;
            } else {
                for (int i = 0; i < s.length(); i++) {
                    String tmp = s.substring(0, i) + s.substring(i + 1);
                    if (new BigInteger(tmp).isProbablePrime(100)) {
                        stack.push(tmp);
                    }

                }
            }
        }
        return counter;
    }

    public Stack test() {
        this.big = new BigInteger(this.getPrimeNumber());
        if (this.big.isProbablePrime(100)) {
            this.primeNumber = this.big.toString();
            if (this.primeNumber.length() != 1) {
                for (int i = 0; i < this.primeNumber.length(); i++) {
                    String tmp = this.primeNumber.substring(0, i) + this.primeNumber.substring(i + 1);
                    //System.out.println("\t" + tmp);
                    this.big = new BigInteger(tmp);
                    if (big.isProbablePrime(100)) {
                        //stack push
                        this.prime.push(Integer.parseInt(tmp));
                    } else {
                        primeNumberText(tmp);
                    }

                }
            }
        } else {
            primeNumberText(this.getPrimeNumber());

        }
        return this.prime;
        //return this.sortStack(this.prime);
    }

    public String getPrimeNumber() {
        return primeNumber;
    }

    public void setPrimeNumber(String primeNumber) {
        this.primeNumber = primeNumber;
    }

    public Stack<Integer> sortStack(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return null;
        }
        Stack<Integer> sortedStack = new Stack<>();
        int element = 0;
        while (!stack.isEmpty()) {
            if (stack.peek() <= (element = stack.pop())) {
                if (sortedStack.isEmpty()) {
                    sortedStack.push(element);
                } else {
                    while ((!sortedStack.isEmpty()) && sortedStack.peek() > element) {
                        stack.push(sortedStack.pop());
                    }
                    sortedStack.push(element);
                }
            }
        }

        return sortedStack;
    }

    private void primeNumberText(String value) {
        System.out.println("This is no PrimeNumber: "+value);
    }
}
