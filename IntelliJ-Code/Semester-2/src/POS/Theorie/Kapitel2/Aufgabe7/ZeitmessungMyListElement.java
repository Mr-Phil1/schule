package POS.Theorie.Kapitel2.Aufgabe7;

public class ZeitmessungMyListElement {
    private Integer value;
    private int length, groesse1 = 1000, groesse2 = 10000, groesse3 = 100000;
    private long start, stop, diff;
    private final String text = " Elemente";
    private MyListElement next;

    public ZeitmessungMyListElement(String title) {
        System.out.println(title + ":");
        ZeitmessungMyListElement myListElement1 = new ZeitmessungMyListElement(groesse1);
        ZeitmessungMyListElement myListElement2 = new ZeitmessungMyListElement(groesse2);
        ZeitmessungMyListElement myListElement3 = new ZeitmessungMyListElement(groesse3);
        myListElement1.strich();
        System.out.println(groesse1 + text);
        myListElement1.measurement(0);
        myListElement1.measurement(groesse1 / 2);
        myListElement1.measurement(groesse1 - 1);
        myListElement1.strich();
        System.out.println(groesse2 + text);
        myListElement2.measurement(0);
        myListElement2.measurement(groesse2 / 2);
        myListElement2.measurement(groesse2 - 1);
        myListElement1.strich();
       System.out.println(groesse3 + text);
        myListElement3.measurement(0);
        myListElement3.measurement(groesse3 / 2);
        myListElement3.measurement(groesse3 - 1);
        myListElement1.strich();
    }

    public ZeitmessungMyListElement(int value) {
        this.next =new MyListElement(value);
        this.length = value;
        this.init();
    }



    public void init() {
        for (int i = 0; i < this.length; i++) {
            this.next.add(Zufall.zufall());
        }
    }



    public long measurement(int index) {
        this.start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
           int tmp = this.next.get(index);
        }
        this.stop = System.nanoTime();
        this.diff = this.stop - this.start;
        System.out.println("\t" + index + " : " + this.diff / 1000 + " ns");
        return this.diff;
    }


    private void strich() {
        System.out.println("----------------------------------");
    }

}
