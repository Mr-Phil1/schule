package POS.Theorie.Kapitel2.Aufgabe7;

import java.util.LinkedList;

public class ZeitmessungList {
    private LinkedList<Integer> linkedList;
    private int length, groesse1 = 1000, groesse2 = 10000, groesse3 = 100000;
    private long start, stop, diff;
    private final  String text = " Elemente";



    public ZeitmessungList(String title) {
        System.out.println(title+":");
        ZeitmessungList zeitmessungList1 = new ZeitmessungList(groesse1);
        ZeitmessungList zeitmessungList2 = new ZeitmessungList(groesse2);
        ZeitmessungList zeitmessungList3 = new ZeitmessungList(groesse3);
        zeitmessungList1.strich();
        System.out.println(groesse1 + text);
        zeitmessungList1.measurement(0);
        zeitmessungList1.measurement(groesse1 / 2);
        zeitmessungList1.measurement(groesse1 - 1);
        zeitmessungList1.strich();
        System.out.println(groesse2 + text);
        zeitmessungList2.measurement(0);
        zeitmessungList2.measurement(groesse2 / 2);
        zeitmessungList2.measurement(groesse2 - 1);
        zeitmessungList1.strich();
        System.out.println(groesse3 + text);
        zeitmessungList3.measurement(0);
        zeitmessungList3.measurement(groesse3 / 2);
        zeitmessungList3.measurement(groesse3 - 1);
        zeitmessungList1.strich();
    }

    public ZeitmessungList(int length) {
        this.length = length;
        this.linkedList = new LinkedList<>();
        this.init();
    }

    public void init() {
        for (int i = 0; i < this.length; i++) {
            this.linkedList.add(Zufall.zufall());

        }
    }

    public long measurement(int index) {
        this.start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            int tmp = this.linkedList.get(index);
        }
        this.stop = System.nanoTime();
        this.diff = this.stop - this.start;
        System.out.println("\t" + index + " : " + this.diff / 1000 + " ns");
        return this.diff;
    }


    private void strich() {
        System.out.println("----------------------------------");
    }
}
