package POS.Theorie.Kapitel2.Aufgabe7;

public class Zeitmessung {
    public static void main(String[] args) {
        double start, stop, diff;
        start = System.currentTimeMillis();
        ZeitmessungList list = new ZeitmessungList("LinkedList");
        ZeitmessungArray array = new ZeitmessungArray("Array");
        ZeitmessungArrayList arrayList = new ZeitmessungArrayList("ArrayList");
        ZeitmessungMyListElement myListElement = new ZeitmessungMyListElement("MyListElement");
        stop = System.currentTimeMillis();
        diff = stop - start;
        System.out.println("Runtime: " + diff + " ms");
        System.out.println("Runtime: " + diff / 1000 + " s");

    }
}
