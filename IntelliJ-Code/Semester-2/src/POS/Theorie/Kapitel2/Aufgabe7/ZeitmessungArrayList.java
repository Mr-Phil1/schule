package POS.Theorie.Kapitel2.Aufgabe7;

import java.util.ArrayList;

public class ZeitmessungArrayList {
    private ArrayList<Integer> arrayList;
    private int length, groesse1 = 1000, groesse2 = 10000, groesse3 = 100000;
    private long start, stop, diff;
    private final  String text = " Elemente";


    public ZeitmessungArrayList(String title) {
        System.out.println(title+":");
        ZeitmessungArrayList zeitmessungArrayList1 = new ZeitmessungArrayList(groesse1);
        ZeitmessungArrayList zeitmessungArrayList2 = new ZeitmessungArrayList(groesse2);
        ZeitmessungArrayList zeitmessungArrayList3 = new ZeitmessungArrayList(groesse3);
        zeitmessungArrayList1.strich();
        System.out.println(groesse1+text);
        zeitmessungArrayList1.measurement(0);
        zeitmessungArrayList1.measurement(groesse1/2);
        zeitmessungArrayList1.measurement(groesse1-1);
        zeitmessungArrayList1.strich();
        System.out.println(groesse2+text);
        zeitmessungArrayList2.measurement(0);
        zeitmessungArrayList2.measurement(groesse2/2);
        zeitmessungArrayList2.measurement(groesse2-1);
        zeitmessungArrayList1.strich();
        System.out.println(groesse3+text);
        zeitmessungArrayList3.measurement(0);
        zeitmessungArrayList3.measurement(groesse3/2);
        zeitmessungArrayList3.measurement(groesse3-1);
        zeitmessungArrayList1.strich();
    }

    public ZeitmessungArrayList(int length) {
        this.length = length;
        this.arrayList=new ArrayList<Integer>();
        this.init();
    }

    public void init() {
        for (int i = 0; i < this.length; i++) {
            this.arrayList.add(Zufall.zufall());

        }
    }

    public long measurement(int index) {
        this.start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            int tmp = this.arrayList.get(index);
        }
        this.stop = System.nanoTime();
        this.diff = this.stop - this.start;
        System.out.println("\t" + index + " : " + this.diff / 1000 + " ns");
        return this.diff;
    }


    private void strich() {
        System.out.println("----------------------------------");
    }
}
