package POS.Theorie.Kapitel2.Aufgabe7;

public class ZeitmessungArray {
    private int[] array;
    private int length, groesse1 = 1000, groesse2 = 10000, groesse3 = 100000;
    private long start, stop, diff;
    private final  String text = " Elemente";




    public ZeitmessungArray(String title) {
        System.out.println(title+":");
        ZeitmessungArray zeitmessungArray1 = new ZeitmessungArray(groesse1);
        ZeitmessungArray zeitmessungArray2 = new ZeitmessungArray(groesse2);
        ZeitmessungArray zeitmessungArray3 = new ZeitmessungArray(groesse3);
        zeitmessungArray1.strich();
        System.out.println(groesse1 + text);
        zeitmessungArray1.measurement(0);
        zeitmessungArray1.measurement(groesse1 / 2);
        zeitmessungArray1.measurement(groesse1 - 1);
        zeitmessungArray1.strich();
        System.out.println(groesse2 + text);
        zeitmessungArray2.measurement(0);
        zeitmessungArray2.measurement(groesse2 / 2);
        zeitmessungArray2.measurement(groesse2 - 1);
        zeitmessungArray1.strich();
        System.out.println(groesse3 + text);
        zeitmessungArray3.measurement(0);
        zeitmessungArray3.measurement(groesse3 / 2);
        zeitmessungArray3.measurement(groesse3 - 1);
        zeitmessungArray1.strich();
    }

    public ZeitmessungArray(int length) {
        this.length = length;
        this.array = new int[length];
        this.init();
    }

    public void init() {
        for (int i = 0; i < this.length; i++) {
            this.array[i] = Zufall.zufall();

        }
    }

    public long measurement(int index) {
        this.start = System.nanoTime();
        for (int i = 0; i < 1000; i++) {
            int tmp = this.array[index];
        }
        this.stop = System.nanoTime();
        this.diff = this.stop - this.start;
        System.out.println("\t" + index + " : " + this.diff / 1000 + " ns");
        return this.diff;
    }


    private void strich() {
        System.out.println("----------------------------------");
    }
}
