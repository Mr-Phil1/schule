package POS.Theorie.Kapitel2.Aufgabe3;

import java.util.Scanner;

public class Einagbe {
    private static Scanner scan = new Scanner(System.in);

    /**
     * @param regex
     * @return Liefert den eingegeben Wert als int zurück.
     */
    public static int inputInt(String text,String regex) {
        return Einagbe.checkInt(text,regex);
    }

    public static int intInput() {
        return scan.nextInt();
    }

    /**
     * @param regex
     * @return Liefert den eingegeben Wert als double zurück.
     */
    public static double inputDouble(String text,String regex) {
        return Double.parseDouble(Einagbe.checkerStr(text, regex));
    }

    /**
     * @return Liefert den eingegeben Wert als String zurück.
     */
    public static String inputString() {
        return scan.next();
    }

    /**
     * @return Liefert den eingegeben Wert als char zurück.
     */
    public static char inputChar() {
        return scan.next().charAt(0);
    }

    private static int checkInt(String text,String regex) {
        return Integer.parseInt((Einagbe.checkerStr(text, regex)));
    }

    private static String checkerStr(String text, String regex) {
        String checkStr;
        do {
            System.out.print(text);
            checkStr = Einagbe.inputString();
        } while (!(checkStr.matches(regex)));
        return checkStr;
    }

    public static String checkStr(String text,String regex) {

        return Einagbe.checkerStr(text, regex);
    }

    public static double checkDouble(String text,String regex) {
        return Double.parseDouble(Einagbe.checkerStr(text, regex));
    }






}
