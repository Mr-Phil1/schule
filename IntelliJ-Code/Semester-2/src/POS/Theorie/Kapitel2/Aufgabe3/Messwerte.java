package POS.Theorie.Kapitel2.Aufgabe3;

import java.util.ArrayList;

public class Messwerte {
    private ArrayList<Double> messwerte;
    private boolean eingabe;
    private double eingabewert, platzhalter, minWert, maxWert, durchschnittWert;
    private String weiter;

    public Messwerte() {
        messwerte = new ArrayList<>();
        eingabe = false;
        minWert = Double.MAX_VALUE;
        maxWert = Double.MIN_VALUE;
    }

    public void werteEingabe() {
        do {
            eingabewert = Einagbe.inputDouble("Bitte geben Sie Ihre gemessenen Temperaturwerte ein: ", "[\\-]*[0-9]+[.]*[0-9]*");
            messwerte.add(eingabewert);
            System.out.println("Möchten Sie noch weitere Messwerte eingeben? ");
            weiter = Einagbe.checkStr("Für Ja (j) oder Nein (n) eingaben: ", "[J|N|j|n]").toLowerCase();
            this.strich();
            if (weiter.equals("n")) {
                eingabe = true;
            }
        } while (!eingabe);
    }

    private void strich() {
        System.out.println("------------------------------------------------------------");
    }

    public void werteAusgeben() {
        for (int i = 0; i < messwerte.size(); i++) {
            System.out.println("\tDer " + i + ". Messwert ist: " + messwerte.get(i));
        }
        this.strich();
    }

    public void minAusgeben() {
        for (int i = 0; i < messwerte.size(); i++) {
            platzhalter = messwerte.get(i);
            if (minWert > platzhalter) {
                minWert = platzhalter;
            }
        }
        System.out.println("\tDer Minimalwert ist: " + minWert);
        this.strich();
    }

    public void maxAusgeben() {
        for (int i = 0; i < messwerte.size(); i++) {
            platzhalter = messwerte.get(i);
            if (maxWert < platzhalter) {
                maxWert = platzhalter;
            }
        }
        System.out.println("\tDer Maximalwert ist: " + maxWert);
        this.strich();
    }

    public void durchschnittAusgeben() {
        int counter = 0;
        for (int i = 0; i < messwerte.size(); i++) {
            platzhalter = platzhalter + messwerte.get(i);
            counter++;
        }
        durchschnittWert = platzhalter / counter;
        System.out.println("\tDer Durchschnitt ist: " + durchschnittWert);
        this.strich();
    }

}
