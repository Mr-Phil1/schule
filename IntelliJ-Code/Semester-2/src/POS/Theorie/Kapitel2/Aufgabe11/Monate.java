package POS.Theorie.Kapitel2.Aufgabe11;

import java.text.DecimalFormat;
import java.util.HashMap;

public class Monate {
    private HashMap<String, Integer> monat;
    private final int tage = 30;
    private int summe = 0, jahr;
    private double anzahl = 0;
    private final String textMonat = "Der Monat ", textHat = " hat ", textTage = " Tage.", tab = "\t";

    public Monate(int jahreszahl) {
        monat = new HashMap<>();
        this.jahr = jahreszahl;
        this.init();
    }

    private void init() {
        this.monat.put("Jänner", tage + 1);
        if (this.getJahr() % 4 == 0) {
            this.monat.put("Feber", tage - 1);
        } else {
            this.monat.put("Feber", tage - 2);
        }
        this.monat.put("März", tage + 1);
        this.monat.put("April", tage);
        this.monat.put("Mai", tage + 1);
        this.monat.put("Juni", tage);
        this.monat.put("Juli", tage + 1);
        this.monat.put("August", tage + 1);
        this.monat.put("September", tage);
        this.monat.put("Oktober", tage + 1);
        this.monat.put("November", tage);
        this.monat.put("Dezember", tage + 1);
    }

    private int summeAllerTage() {
        this.monat.forEach((key, value) -> {
            this.summe += value;
            this.anzahl++;
        });
        return this.summe;
    }

    private int tageAusgeben(String monat) {
        return this.monat.get(monat);
    }

    private double mittelwertAllerTage() {
        if (this.summe == 0 || this.anzahl == 0) {
            this.summeAllerTage();
        }
        return this.summe / this.anzahl;
    }

    public void tageAusgebenMitText(String monat) {
        System.out.println(tab + monat + " : " + this.tageAusgeben(monat));
    }

    public void monatMitTageAusgeben() {
        for (String monate : this.monat.keySet()) {
            System.out.println(tab + textMonat + monate + textHat + this.monat.get(monate) + textTage);
        }
    }

    public void monatMitTageAusgebenDebug() {
        this.monat.forEach((key, value) -> {
            System.out.println(tab + textMonat + key + textHat + value + textTage);
        });
    }

    public void summeAllerTageText() {
        System.out.println(tab + this.summeAllerTage());
    }

    public void mittelwertAllerTageText() {
        System.out.println(tab + this.patternDoubleText(this.mittelwertAllerTage(), "###,###.##"));
    }

    public static void strich() {
        System.out.println("----------------------------------------------");
    }

    public int getJahr() {
        return this.jahr;
    }

    private String patternDoubleText(double betrag, String pattern) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        return myFormatter.format(betrag);
    }

}
