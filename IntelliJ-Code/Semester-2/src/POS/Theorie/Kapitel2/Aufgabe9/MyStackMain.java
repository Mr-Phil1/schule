package POS.Theorie.Kapitel2.Aufgabe9;

public class MyStackMain {
    public static void main(String[] args) {
        MyStack stack = new MyStack(5);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(7);
        System.out.println(stack.peek());
        MyStack.strich();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        System.out.println(stack.peek());
        MyStack.strich();


    }
}
