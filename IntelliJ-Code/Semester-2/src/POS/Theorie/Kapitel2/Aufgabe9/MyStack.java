package POS.Theorie.Kapitel2.Aufgabe9;

public class MyStack {
    private int[] data;
    private int zaehler;

    public MyStack(int size) {
        data = new int[size];
        this.zaehler = 0;
    }

    public int peek() {
        if (zaehler - 1 < 0) {
            this.stackInfo("empty!");
            return -1;
        } else {
            return data[zaehler - 1];
        }
    }

    public int pop() {
        if (zaehler > 0) {
            int tmp = data[zaehler - 1] = 0;
            zaehler--;
            return tmp;
        } else {
            this.stackInfo("empty!");
            return -1;
        }
    }

    public void push(int wert) {
        if (zaehler < data.length) {
            data[zaehler] = wert;
            zaehler++;
        } else {
            this.stackInfo("full!");
        }
    }

    private void stackInfo(String text) {
        System.out.println("The Stack is " + text);
    }

    public static void strich() {
        System.out.println("------------------------------------------");
    }
}
