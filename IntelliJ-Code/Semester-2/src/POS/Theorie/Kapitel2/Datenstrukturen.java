package POS.Theorie.Kapitel2;

import java.math.BigInteger;
import java.util.ArrayList;

public class Datenstrukturen {
    public static void main(String[] args) {
     /*   stringArray();
        strich();
        intArray();
        strich();
        doubleArrayList();
        strich();*/

        String s = "4567";
        BigInteger big = new BigInteger(s);

        if (big.isProbablePrime(100)){
            s=big.toString();
            if (s.length() != 1 && (s.charAt(0)!='1'))
            for (int i = 0; i < s.length(); i++) {
                String tmp =s.substring(0,i)+s.substring(i+1);
                System.out.println(tmp);
                big=new BigInteger(tmp);
                if (big.isProbablePrime(100)){
                    //stack push
                    System.out.println(tmp);
                }
            }
        }
    }

    private static void stringArray() {
        System.out.println("String-Array:");
        String[] woerter = new String[5];
        woerter[0] = "ABC";
        woerter[4] = "EFG";
        System.out.println(woerter[0]);
        System.out.println(woerter[2]);
        System.out.println(woerter.length);
    }

    private static void strich() {
        System.out.println("-----------------------");
    }

    private static void doubleArrayList() {
        System.out.println("double-ArrayList:");
        ArrayList<Double> messwerte = new ArrayList<>();
        messwerte.add(45.6);
        messwerte.add(12.6);
        messwerte.add(0, 12.9);
        for (int i = 0; i < messwerte.size(); i++) {
            System.out.println("Messwert: " + i + " ist: " + messwerte.get(i));
        }
        System.out.println(messwerte.size());
        System.out.println(messwerte.get(1));
        messwerte.remove(1);
        System.out.println(messwerte.size());
        messwerte.clear();
        System.out.println("Messwerte is empty: " + messwerte.isEmpty());
    }

    private static void intArray() {
        System.out.println("int-Array:");
        int[] zahlen = new int[10];
        zahlen[2] = 55;
        System.out.println(zahlen[2]);
        System.out.println(zahlen.length);
    }

    private static void spielFeld() {
        System.out.println("int-Array:");
        int[] zahlen = new int[10];
        zahlen[2] = 55;
        System.out.println(zahlen[2]);
        System.out.println(zahlen.length);
    }
}
