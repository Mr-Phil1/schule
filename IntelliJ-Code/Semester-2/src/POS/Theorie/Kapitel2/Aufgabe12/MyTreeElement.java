package POS.Theorie.Kapitel2.Aufgabe12;

public class MyTreeElement {
    private final char x;
    private MyTreeElement right, left;
    private int z = 0;

    public MyTreeElement(char x) {
        this.x = x;
    }

    public void add(char y) {
        if (y <= x) {
            if (left == null) {
                left = new MyTreeElement(y);
            } else {
                left.add(y);
            }
        } else {
            if (right == null) {
                right = new MyTreeElement(y);
            } else {
                right.add(y);
            }
        }
    }

    public void count(char y) {
        if (y == x) {
            z++;
        } else if (y <= x) {
            left.count(y);
        } else {
            right.count(y);
        }
        System.out.println("Ebene: " + z);
    }
}

