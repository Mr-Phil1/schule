package POS.Theorie.Kapitel5.BubbleSort;

public class Algorithmus {
    long schritte;
    long start, ende, zeit;
    public static long schritteGesamt;

    public Algorithmus() {
    }

    void bubble_sort(int A[], int n) {
        int temp;
        for (int k = 0; k < n - 1; k++) {

            for (int i = 0; i < n - k - 1; i++) {
                if (A[i] > A[i + 1]) {
                    // here swapping of positions is being done.
                    temp = A[i];
                    A[i] = A[i + 1];
                    A[i + 1] = temp;
                }
            }
        }
    }

    public int[] bubbleSort(int[] a) {
        for (int i = a.length; i > 1; i--) {
            for (int j = 0; j < i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                    schritte++;
                }
            }
        }
        schritteGesamt += schritte;

        return a;
    }

    public long sort(int[] array) {
      Algorithmus mBs = new Algorithmus();
         start = System.nanoTime();
        mBs.bubbleSort(array);
         ende = System.nanoTime();
         zeit = ende - start;
        return zeit;
    }

    public String toString(int[] a) {
        String s = "";
        for (int i = 0; i < a.length; i++) {
            s += a[i] + ", ";
        }
        return s;
    }

    public long getSchritte() {
        return schritteGesamt;
    }

}
