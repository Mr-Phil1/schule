package POS.Theorie.Kapitel5.BubbleSort;

import java.util.Random;

public class Main {
    static int elemente = 100_000;
    static int durchlauf = 1000;
    static long start, ende, zeit, random, best, worst;

    public static void main(String[] args) {
        Algorithmus algorithmus = new Algorithmus();
        for (int i = 0; i < durchlauf; i++) {
            random += algorithmus.sort(fillRandom());
            best += algorithmus.sort(fillBest());
            worst += algorithmus.sort(fillWorst());
        }

        printStrich(45);
        ausgabe("Random", elemente, algorithmus.getSchritte(), durchlauf, (random / elemente));
        printStrich(45);
        ausgabe("Best", elemente, algorithmus.getSchritte(), durchlauf, (best / elemente));
        printStrich(45);
        ausgabe("Worst", elemente, algorithmus.getSchritte(), durchlauf, (worst / elemente));
    }

    public static int[] fillRandom() {
        Random rand = new Random();
        int arrayRandom[] = new int[elemente];
        for (int i = 0; i < elemente; i++) {
            arrayRandom[i] = rand.nextInt(1000);
        }
        return arrayRandom;
    }

    public static int[] fillBest() {
        int arrayBest[] = new int[elemente];
        for (int i = 0; i < elemente; i++) {
            arrayBest[i] = i;
        }
        return arrayBest;
    }

    public static int[] fillWorst() {
        int arrayWorst[] = new int[elemente];
        for (int i = 0; i < elemente; i++) {
            arrayWorst[i] = elemente - i;
        }
        return arrayWorst;
    }

    public static double convertToMiliSec(long ns) {
        return ns / 1_000.0;
    }

    public static void printStrich(int lenght) {
        for (int i = 0; i < lenght; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static void ausgabe(String title, int element, long schritt, int durchlauf, long time) {
        System.out.println(title + "-Case: Berechnung mit " + element + " Elementen" + "\n\tSchritte: " + schritt / durchlauf + "\n\tZeit: " + convertToMiliSec((time / durchlauf)) + " ms\n");
    }
}
