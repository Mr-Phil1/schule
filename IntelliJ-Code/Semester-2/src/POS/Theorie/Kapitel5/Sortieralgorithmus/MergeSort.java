package POS.Theorie.Kapitel5.Sortieralgorithmus;

public class MergeSort {
    public int zaehler, sizeOfFirstArray, sizeOfSecondArray, varArrayLeft = 0, varArrayRight = 0, k;
    public int[] leftArray, rightArray;

    public int getZaehler() {
        return zaehler;
    }

    public int[] merge(int array[], int start, int middle, int end) {
        if (array == null && array.length < 2) {
            return null;
        }

        sizeOfFirstArray = (middle - start) + 1;
        sizeOfSecondArray = end - middle;
        k = start;
        leftArray = new int[sizeOfFirstArray];
        rightArray = new int[sizeOfSecondArray];

        for (int i = 0; i < sizeOfFirstArray; i++) {
            leftArray[i] = array[start + i];
        }
        for (int j = 0; j < sizeOfSecondArray; j++) {
            rightArray[j] = array[middle + 1 + j];
        }

        while (varArrayLeft < sizeOfFirstArray && varArrayRight < sizeOfSecondArray) {
            if (leftArray[varArrayLeft] <= rightArray[varArrayRight]) {
                array[k] = leftArray[varArrayLeft];
                varArrayLeft++;
            } else {
                array[k] = rightArray[varArrayRight];
                varArrayRight++;
            }
            k++;
        }
        return array;
    }

    public long sort(int[] array, int left, int right) {
        long start = System.nanoTime();
        if (left < right) {
            int middle = left + (right - left) / 2;

            sort(array, left, middle);
            zaehler++;

            sort(array, middle + 1, right);
            zaehler++;

            merge(array, left, middle, right);

            zaehler++;
        }

        return System.nanoTime() - start;

    }

}
