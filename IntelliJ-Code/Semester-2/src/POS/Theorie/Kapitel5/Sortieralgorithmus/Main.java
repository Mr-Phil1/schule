package POS.Theorie.Kapitel5.Sortieralgorithmus;

import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {

        int elemente = 10_000_000;
        int durchlaeufe = 1_000_000;
        long bestTime = 0, averageTime = 0, worstTime = 0, start, end;

        MergeSort sort1 = new MergeSort();
        MergeSort sort2 = new MergeSort();
        MergeSort sort3 = new MergeSort();
        start = System.nanoTime();
        for (int i = 0; i < durchlaeufe; i++) {
            bestTime += sort1.sort(best(elemente), 0, elemente - 1);
            averageTime += sort2.sort(average(elemente), 0, elemente - 1);
            worstTime += sort3.sort(worst(elemente), 0, elemente - 1);
        }
        end = System.nanoTime();
        printStrich(45);
        ausgabe("Best", elemente, sort1.getZaehler(), durchlaeufe, bestTime);
        printStrich(45);
        ausgabe("Average", elemente, sort2.getZaehler(), durchlaeufe, averageTime);
        printStrich(45);
        ausgabe("Worst", elemente, sort3.getZaehler(), durchlaeufe, worstTime);
        printStrich(45);
        System.out.println("Gesamtzeit: " + convertToSec(end - start) + " s");
    }

    public static int[] best(int elemente) {
        int[] bestCase = new int[elemente];

        for (int i = 0; i < elemente; i++) {
            bestCase[i] = i;
        }
        return bestCase;
    }

    public static int[] average(int elemente) {
        int[] averageCase = new int[elemente];

        for (int i = 0; i < elemente; i++) {
            averageCase[i] = ThreadLocalRandom.current().nextInt(0, elemente);
        }
        return averageCase;
    }

    public static int[] worst(int elemente) {
        int[] worstCase = new int[elemente];

        for (int i = 0; i < elemente; i++) {
            worstCase[i] = elemente;
            elemente--;
        }
        return worstCase;
    }

    public static double convertToMiliSec(long ns) {
        return ns / 1_000_000.0;
    }

    public static double convertToSec(long ns) {
        return ns / 1_000_000_000.0;
    }
    public static void printStrich(int lenght) {
        for (int i = 0; i < lenght; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static void ausgabe(String title, int element, int schritt, int durchlauf, long time) {
        System.out.println(title+"-Case: Berechnung mit " + element + " Elementen" + "\n\tSchritte: " + schritt / durchlauf + "\n\tZeit: " + convertToMiliSec((time / durchlauf)) + " ms\n");
    }
}
