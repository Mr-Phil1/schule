package POS.Theorie.Kapitel5.LineareSucheVsBaum;
//import theorie.pos.MyTreeElement;

import pos.theorie.sose.k6.MyTreeElement;

import java.util.ArrayList;
import java.util.Random;


public class Suche {
    static int gefunden=0;
    public static void main(String[] args) {
//ArrayList mit 100 zufälligen Zahlen
        ArrayList<Integer> arrayList = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 100; i++) {
            arrayList.add(random.nextInt(10001));
        }
        //Test
        //alist.add(5208);
        System.out.println("ArrayList: ");
        System.out.println(arrayList);

        //MyTreeElement mit 100 zufälligen Zahlen
        MyTreeElement mytree = new MyTreeElement();

        for (int i = 0; i < 100; i++) {
            mytree.add(random.nextInt(10001));
        }
        System.out.println();
        System.out.println("MyTreeElement: ");
        System.out.println(mytree);
        System.out.println();
        //ArrayList lineare Suche
        //System.out.println(lineareSuche(5208, alist));

        double gesamtWertArrayList=0;
        for (int i = 0; i < 1000; i++) {
            int tmp = random.nextInt(10001);
            System.out.println("Runde "+i+":");
            int tmp1 = lineareSuche(tmp, arrayList);
            schritteAusgabe(tmp1, tmp);
            gesamtWertArrayList += tmp1;
        }
        System.out.println();
        System.out.println("ArrayList: ");
        System.out.println("\tSchritte insgesamt: "+gesamtWertArrayList);
        System.out.println("\tGefundene Runden: "+gefunden);
        System.out.println("\tMittelwert: "+gesamtWertArrayList/gefunden);

        //MyTreeElement
        System.out.println();
        int tmpTree = random.nextInt(10001);
        System.out.println("MyTreeElement: ");
        System.out.println("\tSize: "+mytree.count(tmpTree));
        schritteAusgabe(mytree.count(tmpTree), tmpTree);
    }

    //Methoden
    //ArrayList lineare Suche
    public static int lineareSuche(int gesucht, ArrayList<Integer> arraylist) {

        ArrayList<Integer> tmpList = arraylist;
        int i = 0;

        while (i < tmpList.size()) {
            if (tmpList.get(i) == gesucht) {
                gefunden++;
                return i + 1;
            }
            i++;
        }
        return 0;
    }

    public static void schritteAusgabe(int value,int gesucht) {

        if (value == 0) {

            System.out.println("\tNicht vorhanden!");
        } else {
            System.out.println("\t" + value +" Schritte bis zum gesuchten Wert: "+gesucht);
        }

    }
}
