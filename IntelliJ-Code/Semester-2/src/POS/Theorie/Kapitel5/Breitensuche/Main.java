package POS.Theorie.Kapitel5.Breitensuche;




import pos.theorie.sose.k6.MyNode;

import java.util.Stack;

public class Main {
    public static Stack<MyNode> stack = new Stack<>();

    public static void main(String[] args) {
        MyNode node=new MyNode(1);

        MyNode node1 = new MyNode(11);
        MyNode node2 = new MyNode(9);
        MyNode node3 = new MyNode(1);
        MyNode node4 = new MyNode(15);
        MyNode node5 = new MyNode(5);
        MyNode node6 = new MyNode(7);
        MyNode node7 = new MyNode(3);
        MyNode node8 = new MyNode(14);
        MyNode node9 = new MyNode(2);
        MyNode node10 = new MyNode(6);
        MyNode node11 = new MyNode(13);
        MyNode node12 = new MyNode(4);
        MyNode node13 = new MyNode(12);
        MyNode node14 = new MyNode(8);
        MyNode node15 = new MyNode(10);

        node1.add(node2);
        node2.add(node3);
        node3.add(node4);
        node1.add(node5);
        node5.add(node6);
        node6.add(node7);
        node5.add(node8);
        node1.add(node9);
        node9.add(node10);
        node15.add(node11);
        node11.add(node1);
        node13.add(node13);
        node12.add(node14);

        node.add(dfs(node1,5));
        System.out.println(node.getNumber());
    }

    public static MyNode dfs(MyNode start, int goal) {
        if (start.getNumber() == goal) {
            return start;
        } else {
            stack.addAll(start.getChildren()); //weist alle Kinderknoten dem Stack zu
            //System.out.println(stack); //nur für debugging
            while (!stack.empty()) {
                start = stack.pop(); //Referenzierung auf den letzen Knoten im Stack
                return dfs(start, goal); //rekursiever Aufruf mit dem neuen Knoten
            }
        }
        return null;
    }
}
