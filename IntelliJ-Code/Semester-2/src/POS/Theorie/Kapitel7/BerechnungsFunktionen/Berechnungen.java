package POS.Theorie.Kapitel7.BerechnungsFunktionen;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Berechnungen {
    static Function<Integer, Integer> verdoppeln = x -> x * 2;
    static UnaryOperator<Double> wurzel = Math::sqrt;  // Andere Schreibweise für: [x -> Math.sqrt(x)]
    static BinaryOperator<Double> sum = Double::sum; // Andere Schreibweise für: [(a,b)) -> a+b]

    public static void main(String[] args) {
        int a = verdoppeln.apply(25);
        double b = wurzel.apply(25.0);
        double c = sum.apply(10.284, 845.9857);
        printDash(35, '#');
        System.out.println("Teil-01");
        System.out.println("\tVerdoppelt:\t\t" + a + "\n" + "\tWurzel:\t\t\t" + b + "\n" + "\tSumme:\t\t\t" + c);
        printDash(35, '#');
        System.out.println("Teil-02");
        List<Integer> zahlen = Arrays.asList(5, 10, 15, 20, 25);
        Stream<Integer> stream = zahlen.stream().map(x -> verdoppeln.apply(x));
        stream.forEach(zahl -> System.out.println("\t"+zahl));

        printDash(25, '-');

        List<Double> zahlen2 = Arrays.asList(4., 9., 16., 25., 36.);
        Stream<Double> stream2 = zahlen2.stream().map(x -> wurzel.apply(x));
        stream2.forEach(zahl -> System.out.println("\t"+zahl));

        printDash(25, '-');
        List<Double> zahlen3 = Arrays.asList(5., 10., 15., 20.);
        Stream<Double> stream3 = zahlen3.stream().map(x -> sum.apply(x, 5.));
        stream3.forEach(zahl -> System.out.println("\t"+zahl));
        printDash(35, '#');

    }

    private static void printDash(int length, char symbol) {
        for (int i = 0; i < length; i++) {
            System.out.print(symbol);
        }
        System.out.println();
    }
}
