package POS.Theorie.Kapitel7.StreamTest;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        long mapZaehlung = 0, filterZaehlung = 0;
        List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");
        System.out.println("\nMap:");
        mapZaehlung = myList
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .count();

        System.out.println("\nFilter:");
        filterZaehlung = myList
                .stream()
                .map(String::toUpperCase)
                .sorted()
                .filter(s -> s.startsWith("c"))
                .count();

        System.out.println("Filter -> Map: " + filterZaehlung + " Schritte"); // 0
        System.out.println("Map -> Filter: " + mapZaehlung + " Schritte"); // 2
    }
}
