package POS.Theorie.Kapitel7.AutoVergleich;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Funktionen {
    public static void main(String[] args) {

        Integer i1 = 4;
        Integer i2 = 5;

        char c1 = 'G';
        char c2 = 'J';

        if (c1 < c2) {

        }

        String s1 = "EBC";
        String s2 = "D";

        // geht nicht für referenztypen
        //if (s1 > s2) {
        //}

        System.out.println(s1.compareTo(s2));


        List<Integer> zahlen = Arrays.asList(4, 5, 6, 7, 5, 6, 4, 7);

        Collections.sort(zahlen, (zahl1, zahl2) -> zahl2.compareTo(zahl1));

//        Collections.sort(zahlen, new IntegerAbsteigendComparator());

        System.out.println(zahlen);

        Function<Integer, Function<Integer, Integer>> sub = ii1 -> ii2 -> ii1 - ii2;
        System.out.println(sub.apply(3).apply(9));

        UnaryOperator<Integer> add1 = n -> n + 1;
        BinaryOperator<Integer> sum = (a, b) -> a + b;

        Function<Integer, Double> add2 = x -> x + 1.0;
        double d = add1.apply(6);


        List<String> namen = new ArrayList<>();
        namen.add("Schrödinger");
        namen.add("Bissingen");
        namen.add("Bossingen");
        namen.add("Bussingen");
        namen.add("Schickelmickel");

        // Filter aller Strings mit ö
        Stream<String> str = namen.stream();
        Stream<String> langeNamen = str.filter(name -> name.contains("ö"));
        langeNamen.forEach(name -> System.out.println(name));

        // Filter aller Strings mit ö, mit Methodenreernz
        namen.stream().filter(name -> name.contains("ö")).forEach(name -> System.out.println(name));

        // Map aller Werte
        zahlen = Arrays.asList(4, 9, 16, 25, 36, 49);
        Stream<Double> wurzeln = zahlen.stream().map(zahl -> Math.sqrt(zahl));
        wurzeln.forEach(zahl -> System.out.println(zahl));
        System.out.println(zahlen);

        // Reduce (hat immer zwei Parameter und ein Ergebnis)
        Optional<Integer> opt = zahlen.stream().reduce((a, b) -> a + b);
        System.out.println(opt);
        System.out.println(opt.isPresent());
        System.out.println(opt.get());

        Integer maximum = zahlen.stream().reduce(Integer::max).get();


    }

}
