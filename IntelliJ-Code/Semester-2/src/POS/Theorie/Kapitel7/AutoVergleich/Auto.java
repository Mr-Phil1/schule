package POS.Theorie.Kapitel7.AutoVergleich;

import java.util.Comparator;


public class Auto implements Comparator<Auto> {

    /**
     * leistung in ps (0-x)
     */
    private int leistung;

    public Auto(int leistung) {
        this.leistung = leistung;
    }

    /**
     * vergleicht zwei autos anhand der leistung
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Auto o1, Auto o2) {
        // einzeiliger kommentar

        /*
         *
         */

        return o1.leistung - o2.leistung;
    }

}
