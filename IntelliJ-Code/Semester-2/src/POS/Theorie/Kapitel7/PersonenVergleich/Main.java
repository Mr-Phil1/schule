package POS.Theorie.Kapitel7.PersonenVergleich;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("John Doe", 38, Geschlecht.MALE));
        personList.add(new Person("Jane Doe", 40, Geschlecht.FEMALE));
        personList.add(new Person("Max Mustermann", 25, Geschlecht.MALE));

        Comparator<Person> comparator = (zahl1, zahl2) -> (String.valueOf(zahl2.getAlter())).compareTo(String.valueOf(zahl1.getAlter()));
        Collections.sort(personList, comparator);
        personList.forEach(System.out::println);

        double avgMaleAge = personList
                .stream()
                .filter(geschlecht -> geschlecht.getGeschlecht() == (Geschlecht.MALE))
                .mapToInt(person -> person.getAlter())
                .average()
                .getAsDouble();

        System.out.println(avgMaleAge);
    }
}
