package POS.Theorie.Kapitel7.PersonenVergleich;

public class Person {
    private Geschlecht geschlecht;
    private String name;
    private int alter;

    public Person(String name, int alter,Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
        this.name = name;
        this.alter = alter;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public String getName() {
        return name;
    }

    public int getAlter() {
        return alter;
    }

    @Override
    public String toString() {
        return "Person{" +
                "geschlecht=" + geschlecht +
                ", name='" + name + '\'' +
                ", alter=" + alter +
                '}';
    }
}
