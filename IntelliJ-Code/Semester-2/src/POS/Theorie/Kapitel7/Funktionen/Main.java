package POS.Theorie.Kapitel7.Funktionen;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<Integer,Integer> add1 = x -> x + 1;
        System.out.println(add1.apply(3));
        Function<Integer, Function<Integer,Integer>> makeAdder = x -> y -> x + y;
        Function<Integer, Integer> add2 = makeAdder.apply(2);

        System.out.println(add2.apply(3));
    }
}
