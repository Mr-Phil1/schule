package POS.Praktisch.Kompetenzkontrolle.Kontrolle1;

import java.util.Random;

public class Zufall {
    private Random random = new Random();

    /**
     * Erzeugt einen Zufallsstring der angegebenen Länge mit Kleinbuchstaben von a bis z.
     *
     * @param length Länge des gewünschten Zufallsstrings.
     * @return Zufallsstring der gewünschten Länge mit Characters aus a bis z.
     */
    public String generateRandomString(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = length;
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }


    public boolean generateRandomBoolean() {
        int x = random.nextInt(10) + 1;
        if (x <= 7) {
            return true;
        } else {
            return false;
        }
    }
}
