package POS.Praktisch.Kompetenzkontrolle.Kontrolle1;

public class Kontrolle {
    private static Aufgabenergebnisse level1 = new Aufgabenergebnisse("\"Aufgabe 1 - Objektinteraktion\"");
    private static Aufgabenergebnisse level2 = new Aufgabenergebnisse("\"Aufgabe 2 - Kollektionsklassen\"");
    private static Aufgabenergebnisse level3 = new Aufgabenergebnisse("\"Zufällige Liste für 4 Einträge");
    private static Zufall zufall = new Zufall();

    public static void main(String[] args) {
        //Level 4
        System.out.println("ANZAHL ERGEBNISLISTEN: " + Aufgabenergebnisse.getInstanzenAnzahl());
        // Level 1
        Texte.strich();
        System.out.println(level1.getAufgabenName());
        System.out.println();
        beufellenLevel1();
        ausgebenLevel1();
        // Level 2
        Texte.strich();
        System.out.println(level2.getAufgabenName());
        System.out.println();
        beufellenLevel2();
        ausgebenLevel2();
        System.out.println();
        ergebnissLevel2("Melanie Osl");
        loeschenLevel2("Elmar Märk");
        System.out.println();
        ausgebenLevel2();
        // Level 3
        Texte.strich();
        System.out.println(level3.getAufgabenName());
        System.out.println();
        beufellenLevel3();
        ausgebenLevel3();
    }

    /**
     * Methode für das kollogiale Befüllen die HashMap des Opjektes level1
     */
    private static void beufellenLevel1() {
        level1.eintragHinzufuegen("Claudio Landerer", false);
        level1.eintragHinzufuegen("Melanie Osl", true);
        level1.eintragHinzufuegen("Hassan Cosgun", false);
        level1.eintragHinzufuegen("Erwin Zangerle", true);
        level1.eintragHinzufuegen("Alois Merk", true);
        level1.eintragHinzufuegen("Anton Haslauer", false);
    }

    private static void ausgebenLevel1() {
        level1.listeAusgeben();
    }

    /**
     * Methode für das kollogiale Befüllen die HashMap des Opjektes level2
     */
    private static void beufellenLevel2() {
        level2.eintragHinzufuegen("Elmar Märk", true);
        level2.eintragHinzufuegen("Melanie Osl", true);
        level2.eintragHinzufuegen("Alexander Hauser", false);
        level2.eintragHinzufuegen("Otto Siess", true);
        level2.eintragHinzufuegen("Edwin Müller", false);
    }

    private static void loeschenLevel2(String name) {
        System.out.println("Der Wert des gelöschten Studenten (" + name + ") war: " + level2.eintragLoeschen(name));
    }

    private static void ergebnissLevel2(String name) {
        level2.gibErgebnisSchueler(name);
    }

    private static void ausgebenLevel2() {
        level2.listeAusgeben();
    }

    private static void ausgebenLevel3() {
        level3.listeAusgeben();
    }

    /**
     * Methode für das kollogiale Befüllen die HashMap des Objektes level3
     */
    private static void beufellenLevel3() {
        level3.listeMitZufaelligenWertenFuellen(10);
        level3.listeMitZufaelligenWertenFuellen(10);
        level3.listeMitZufaelligenWertenFuellen(10);
        level3.listeMitZufaelligenWertenFuellen(10);

    }

}
