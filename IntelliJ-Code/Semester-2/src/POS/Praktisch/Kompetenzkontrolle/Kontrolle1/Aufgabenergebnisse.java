package POS.Praktisch.Kompetenzkontrolle.Kontrolle1;

import java.util.HashMap;

public class Aufgabenergebnisse {
    private final String BESTANDEN = "bestanden";
    private final String NICHT_BESTANDEN = "nicht bestanden";
    private final String BEREICHSTITLE = "AUFGABENERGEBNISSE für ";
    private static int instanzenAnzahl;
    private String aufgabenName;
    private HashMap<String, Boolean> studenten;
    private Zufall zufall = new Zufall();

    /**
     * Hier wird der Konstuktor eines jeden Aufgabenergebnisse-Objekt erzeugen
     *
     * @param title Hier wird der Namen für das erzeugte Objekt mitgegeben
     */
    public Aufgabenergebnisse(String title) {
        this.aufgabenName = title;
        this.studenten = new HashMap<>();
        instanzenAnzahl++;
    }

    public static int getInstanzenAnzahl() {
        return instanzenAnzahl;
    }

    public String getAufgabenName() {
        return BEREICHSTITLE + aufgabenName;
    }

    public void eintragHinzufuegen(String name, boolean erfolgreich) {
        this.studenten.put(name, erfolgreich);
    }


    public void listeAusgeben() {
        this.studenten.forEach((key, value) -> {
                    if (value == true) {
                        System.out.println(key + ", " + this.BESTANDEN);
                    } else {
                        System.out.println(key + ", " + this.NICHT_BESTANDEN);
                    }
                }
        );
    }

    /**
     * Diese Methode löscht aus einer HashMap mit dem mitgegeben Namen einen Studenten, gibt aber davor seinen Wert aus.
     *
     * @param name Hier wird der Namen des zu löschenden Studenten mitgegeben
     */
    public boolean eintragLoeschen(String name) {
        return this.studenten.remove(name);
    }

    /**
     * Diese Methode gibt den gespeicherten Wert eines Students aus
     *
     * @param name Hier wird der Namen des auszugebenden Studenten mitgegeben
     */
    public void gibErgebnisSchueler(String name) {
        System.out.println(name + ": " + this.studenten.get(name));
    }

    /**
     * Mit dieser Methode wird ein neuer Student in die HashMap eingetragen mit einem zufälligen String und einem zufälligen Boolschen-Wert.
     *
     * @param length Hier wird die Länge für den String mitgegeben
     */
    public void listeMitZufaelligenWertenFuellen(int length) {
        this.studenten.put(zufall.generateRandomString(length), zufall.generateRandomBoolean());
    }

}
