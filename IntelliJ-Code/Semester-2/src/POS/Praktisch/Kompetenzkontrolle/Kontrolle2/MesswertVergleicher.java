package POS.Praktisch.Kompetenzkontrolle.Kontrolle2;

import java.util.Comparator;

public class MesswertVergleicher implements Comparator<Messwert> {

    @Override
    public int compare(Messwert o1, Messwert o2) {
        if (o1 == null && o2 == null) return 0;
        else if (o1 == null && o2 != null) return -1;
        else if (o1 != null && o2 == null) return 1;
        else {
            if (o1.getMesswert() < o2.getMesswert()) return -1;
            else if (o1.getMesswert() == o2.getMesswert()) return 0;
            else return 1;
        }
    }

}
