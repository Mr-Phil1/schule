package POS.Praktisch.Kompetenzkontrolle.Kontrolle2;


public class Main {
    private static Messwert messwert = new Messwert("Haiming", 17, 4);
    private static Messwert messwert1 = new Messwert("Landeck", 12, 3);
    private static Messwert messwert2 = new Messwert("Imst", 20, 3);
    private static Messwert messwert3 = new Messwert("Imsterberg", 8, 2);
    private static Messwert messwert4 = new Messwert("Roppen", 8, 2);
    private static Statistik statistik = new Statistik(5);

    public static void main(String[] args) {


        statistik.messwertSetzenAnPosition(messwert, 0);
        statistik.messwertSetzenAnPosition(messwert1, 1);
        statistik.messwertSetzenAnPosition(messwert2, 2);
        statistik.messwertSetzenAnPosition(messwert3, 3);
        statistik.messwertSetzenAnPosition(messwert4, 4);
        System.out.println(statistik.toString());
        Text.strich();
        System.out.println(statistik.messwertLoeschenAnPosition(1));
        System.out.println(statistik.messwertLoeschenAnPosition(99));
        System.out.println(statistik.toString());
        Text.strich();
        statistik.anzahlMesswerteAusgeben();
        statistik.messwertHinzufuegeneAusgeben(messwert);
        System.out.println(statistik.toString());
        statistik.messwertHinzufuegeneAusgeben(messwert);
        statistik.mminTemperaturAusgeben();
        statistik.durchschnittTemperaturZuStundeAusgeben(3);
        statistik.anzahlWarmeNaechteFuerStationAusgeben("Imst");
        statistik.temperaturWerteInCelsiusAusgeben();
        Text.strich();
        statistik.sotieren();
        System.out.println(statistik.toString());


    }
}
