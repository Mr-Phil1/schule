package POS.Praktisch.Kompetenzkontrolle.Kontrolle2;

import java.util.Arrays;

public class Statistik {
    private Messwert[] messwerte;


    public Statistik(int anzahl) {
        this.messwerte = new Messwert[anzahl];

    }

    public Statistik(Messwert[] messwerte) {
        this.messwerte = messwerte;
    }

    public void messwertSetzenAnPosition(Messwert messwert, int indexposition) {
        if (indexposition < this.messwerte.length) {
            this.messwerte[indexposition] = messwert;
        } else {
            System.out.println("fail");
        }

    }

    public Messwert messwertLoeschenAnPosition(int indexposition) {
        Messwert tmp;
        if (indexposition < this.messwerte.length && this.messwerte[indexposition] != null) {
            tmp = new Messwert(this.messwerte[indexposition].getName(), this.messwerte[indexposition].getMesswert(), this.messwerte[indexposition].getZeitpunkt());
            this.messwerte[indexposition] = null;
            return tmp;
        } else {
            System.out.println("Löschen nicht möglich!");
        }
        return null;
    }


    @Override
    public String toString() {
        return "Statistik{" +
                "messwerte=" + Arrays.toString(messwerte) +
                '}';
    }


    public int anzahlMesswerte() {
        int anzahl = 0;
        for (int i = 0; i < this.messwerte.length; i++) {
            if (messwerte[i] != null) {
                anzahl++;
            }
        }
        return anzahl;
    }

    public void anzahlMesswerteAusgeben() {
        System.out.println("Die Anzahl aller Messwerte beträgt: " + this.anzahlMesswerte());
    }

    public boolean messwertHinzufuegen(Messwert messwert) {
        boolean frei = false;
        for (int i = 0; i < this.messwerte.length; i++) {
            if (messwerte[i] == null) {
                messwerte[i] = messwert;
                frei = true;
            } else {
                frei = false;
            }
        }
        return frei;
    }

    public void messwertHinzufuegeneAusgeben(Messwert messwert) {
        if (messwertHinzufuegen(messwert)) {
            System.out.println("Der Messwert konnte hinzugefügt werden.");
        } else {

            System.out.println("Der Messwert konnte nicht hinzugefügt werden.");
        }
    }

    public double minTemperatur() {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < this.messwerte.length; i++) {
            if (messwerte[i].getMesswert() < min) {
                min = messwerte[i].getMesswert();
            }
        }
        return min;
    }

    public void mminTemperaturAusgeben() {
        System.out.println("Die Minimal Temperatur beträgt: " + this.minTemperatur());
    }

    public double durchschnittTemperaturZuStunde(int stunde) {
        int counter = 0;
        double durschnitt = 0;
        for (int i = 0; i < this.messwerte.length; i++) {
            if (messwerte[i].getZeitpunkt() == stunde) {
                durschnitt += messwerte[i].getMesswert();
                counter++;
            }
        }
        return durschnitt / counter;
    }

    public void durchschnittTemperaturZuStundeAusgeben(int stunde) {
        System.out.println("Die Durchschnitts Temperatur zur Stunde " + stunde + " beträgt: " + this.durchschnittTemperaturZuStunde(stunde));
    }

    /**
     * Bei dieser Methode wird dasMesswerte-Array nach einer bestimmten Station durchsucht und zum Schluss bekommt man die gesammt Anzahl der warmen Nächtezurück.
     *
     * @param stationsname Hier wird der Namen der gewünschten Station mitgegeben
     * @return Als Rückgaben bekommt man  die Anzahl der gesamtmen warmen Nächte
     */
    public int anzahlWarmeNaechteFuerStation(String stationsname) {
        int anzahl = 0;
        for (Messwert m : this.messwerte) {
            if (m != null && m.getName().equals(stationsname)
                    && m.getZeitpunkt() == 3
                    && m.getMesswert() >= 20) {
                anzahl++;
            }
        }
        return anzahl;
    }

    public void anzahlWarmeNaechteFuerStationAusgeben(String stationsname) {
        System.out.println("Die " + stationsname + " hat insgesamt " + this.anzahlWarmeNaechteFuerStation(stationsname) + " warme Nächte gehabt.");
    }

    public double[] temperaturWerteInCelsius() {
        double[] werte = new double[anzahlMesswerte()];
        int zaehler = 0;
        for (int i = 0; i < werte.length; i++) {
            if (this.messwerte[i] != null) {
                werte[zaehler] = this.messwerte[i].getMesswert();
                zaehler++;
            }
        }
        return werte;
    }

    public void temperaturWerteInCelsiusAusgeben() {
        System.out.println(Arrays.toString(this.temperaturWerteInCelsius()));
    }

    public void sotieren() {
        Arrays.sort(this.messwerte, new MesswertVergleicher());
    }
/**
 * Die Wahrscheinlichkeit für eine Codeduplikation konnte auf Grund einer anfänglichen Denkblockade nicht 100% vermieden werden.
 * Ich habe versucht meinen Codequalität auf meinen üblichen Level zu halten, konnte dies aber nicht 100% Garantieren.
 * Dort wo einen Auslagerung möglich war habe ich den code ausgelager.
 *
 * MfG Maar Philipp 2021-03-23 um 15:26
 */


}
