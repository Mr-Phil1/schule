package POS.Praktisch.Kompetenzkontrolle.Kontrolle2;

public class Messwert {

    private String name;
    private double messwert;
    private int zeitpunkt;

    public Messwert(String name, double messwert, int zeitpunkt) {
        this.name = name;
        this.messwert = messwert;
        this.zeitpunkt = zeitpunkt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name!=null)this.name = name;
    }

    public double getMesswert() {
        return messwert;
    }

    public void setMesswert(double messwert) {
        this.messwert = messwert;
    }

    public int getZeitpunkt() {
        return zeitpunkt;
    }

    public void setZeitpunkt(int zeitpunkt) {
        if(zeitpunkt > 0 && zeitpunkt < 24) this.zeitpunkt = zeitpunkt;
    }

    public String toString() {
        return "Messwert{" + "name=" + name + ", messwert=" + messwert + ", zeitpunkt=" + zeitpunkt + '}';
    }

}
