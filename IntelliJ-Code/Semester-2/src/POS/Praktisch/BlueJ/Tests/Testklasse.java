package POS.Praktisch.BlueJ.Tests;

public class Testklasse {
    public static void main(String[] args) {
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        for (int i = 1; i < 10000; i++) {
            System.out.println(i);
        }
        endTime = System.currentTimeMillis();
        System.out.println("Die Gesamtzeit beträgt: " + (endTime - startTime) + "ms");
    }
}
