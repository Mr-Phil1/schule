package POS.Praktisch.BlueJ.Ende.Tausch;

public class Tausche {
    public static void main(String[] args) {
        Tausche tausche=new Tausche();
        tausche.tausche(5,6);
    }
    public void tausche(int a, int b){
        int tmp = a;
        a = b;
        b = tmp;
        System.out.println(a+" "+b);

    }
}
