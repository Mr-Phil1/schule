package POS.Praktisch.Produkte;

public class Produkt  {
    private String nummer;
    private String name;
    private double preis;

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public Produkt(String nummer, String name, double preis) {
        this.nummer = nummer;
        this.name = name;
        this.preis = preis;
    }

    public String getNummer() {
        return nummer;
    }

    public void setNummer(String nummer) {
        this.nummer = nummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nummer='" + nummer + '\'' +
                ", name='" + name + '\'' +
                ", preis=" + preis +
                '}';
    }




}
