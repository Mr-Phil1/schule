package POS.Praktisch.Produkte;

public class Main {


    public static void main(String[] args) {
        Lager lager = new Lager(5);
        lager.produktAufnehmen(new Produkt("123", "Windows", 12.0));
        lager.produktAufnehmen(new Produkt("125", "Linux", 0));
        lager.produktAufnehmen(new Produkt("124", "Linux", 999));
        System.out.println("Produktliste::");
        lager.gibProduktMitNammeWie("Windows");
        lager.ausgeben();
        System.out.println(lager.lagerwert());
        lager.sortieren();
        lager.ausgeben();
    }
}
