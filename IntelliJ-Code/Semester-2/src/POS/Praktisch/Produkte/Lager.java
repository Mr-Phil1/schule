package POS.Praktisch.Produkte;

import java.util.ArrayList;
import java.util.Arrays;

public class Lager {
    private Produkt[] produktliste;

    public Lager(int anzahlProdukte) {
        produktliste = new Produkt[anzahlProdukte];
    }

    public void produktAufnehmen(Produkt produkt) {
        for (int i = 0; i < this.produktliste.length; i++) {
            if (this.produktliste[i] == null) {
                this.produktliste[i] = produkt;
                return;
            }
        }
    }

    public Produkt gibProduktMitNummer(String nummer) {
        for (Produkt produkt : this.produktliste) {
            if (produkt.getNummer().equals(nummer)) return produkt;
        }
        return null;
    }

    public Produkt[] gibProduktMitNammeWie(String name) {
        ArrayList<Produkt> interne = new ArrayList<>();
        for (Produkt produkt : this.produktliste) {
            if (produkt != null && produkt.getName().contains(name)) {
                interne.add(produkt);
            }
        }
       /*
       Produkt[] resultat = new Produkt[interne.size()];
        for (int i = 0; i < resultat.length; i++) {
            resultat[i]=interne.get(i);
        }*/
        return interne.toArray(new Produkt[interne.size()]);

    }

    public double lagerwert() {
        double summe = 0;
        for (Produkt p : this.produktliste
        ) {
            if (p != null) {
                summe += p.getPreis();
            }
        }
        return summe;
    }

    public void produktLoeschenAnPosiotion(int indexpositiom) {
        this.produktliste[indexpositiom] = null;
    }

    public void ausgeben() {
        for (Produkt p : produktliste) {
            if (p != null) {
                System.out.println(p);
            }
        }
    }

    public void sortieren() {
        Arrays.sort(produktliste);
    }

}
