package POS.Praktisch.Polymophie.Modul1;

public class App {
    public static void main(String[] args) {
        Medienbibliothek bib = new Medienbibliothek();

        VHS heman = new VHS("He-Man and the Masters of the Universe", 120.99, "Büro, UG", 120, "Ausgezeichnet");
        Cartridge zeldaGold = new Cartridge("The Adventures of Zelda", 300.00, "Keller, UG", true, "Nintendo", Konsolentyp.NES);
        Buch herr = new Buch("Herr der Ringe", 49.99, "Wohnzimmer, OG", "1234565432123");
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Meine hinzugefuegten Medien
        Cartridge pokemonSapphire = new Cartridge("Pokemon Sapphire", 50, "Wohnzimmer", true, "GameFreak", Konsolentyp.GBA);
        Cartridge pokemonSapphire3ds = new Cartridge("Pokemon Sapphire", 50, "Wohnzimmer", true, "GameFreak", Konsolentyp.DreiDS);
        Buch dTaS = new Buch("Dem Tod auf der Spur", 19.99, "Schlafzimmer", "978-3-548-37713-1");

        bib.mediumHinzufügen(heman);
        bib.mediumHinzufügen(zeldaGold);
        bib.mediumHinzufügen(herr);
        //-------------------------------------------
        bib.mediumHinzufügen(pokemonSapphire);
        bib.mediumHinzufügen(dTaS);
        bib.mediumHinzufügen(pokemonSapphire3ds);
        //-------------------------------------------

        bib.alleMedienAusgeben();

        System.out.println("");
        System.out.println("--- alle VHS in der Liste ---");
        for (VHS vhs : bib.alleVHSalsListe()) {
            vhs.anzeigen();
        }

        //-------------------------------------------
        bib.mediumSuchenAusgeben("Pokemon Sapphire");
        bib.mediumSuchenAusgeben("");
        bib.mediumSuchenAusgeben(null);
    }
}
