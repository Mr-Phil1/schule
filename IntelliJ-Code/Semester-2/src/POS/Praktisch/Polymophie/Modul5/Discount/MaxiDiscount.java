package POS.Praktisch.Polymophie.Modul5.Discount;

public class MaxiDiscount extends Rabattstragie{
    public MaxiDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    @Override
    public double getReduierterPreis(double preis) {
        return preis*0.7;
    }

}
