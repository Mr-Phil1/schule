package POS.Praktisch.Polymophie.Modul5.Discount;

public class ChristmasDiscount extends Rabattstragie {

    public ChristmasDiscount(String bezeichnung) {
        super(bezeichnung);
        System.out.println("*** Frohe Weihnachten ***");
    }

    @Override
    public double getReduierterPreis(double preis) {
        return preis * 0.5;
    }
}
