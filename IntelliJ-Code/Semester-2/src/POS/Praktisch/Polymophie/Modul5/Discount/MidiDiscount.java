package POS.Praktisch.Polymophie.Modul5.Discount;

public class MidiDiscount extends Rabattstragie{
    public MidiDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    public double getReduierterPreis(double preis) {
        return preis*0.85;
    }
}
