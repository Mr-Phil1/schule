package POS.Praktisch.Polymophie.Modul5.Discount;

public class SpezialDiscount extends Rabattstragie{
    public SpezialDiscount(String bezeichnung) {
        super(bezeichnung);
        System.out.println("*** Herzlichen Glückwunsch ***");
    }

    @Override
    public double getReduierterPreis(double preis) {
        return preis*0.25;
    }
}
