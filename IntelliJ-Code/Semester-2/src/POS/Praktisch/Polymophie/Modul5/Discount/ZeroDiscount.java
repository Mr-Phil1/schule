package POS.Praktisch.Polymophie.Modul5.Discount;

public class ZeroDiscount extends Rabattstragie{
    public ZeroDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    public double getReduierterPreis(double preis) {
        return preis;
    }
}
