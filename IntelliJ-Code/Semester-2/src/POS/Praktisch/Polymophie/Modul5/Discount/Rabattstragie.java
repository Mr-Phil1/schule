package POS.Praktisch.Polymophie.Modul5.Discount;

public class Rabattstragie {

    private String bezeichnung;

    public Rabattstragie(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public double getReduierterPreis(double preis) {
        return preis;
    }

    @Override
    public String toString() {
        return bezeichnung;
    }
}
