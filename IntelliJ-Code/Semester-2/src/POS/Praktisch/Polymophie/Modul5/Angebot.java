package POS.Praktisch.Polymophie.Modul5;

import POS.Praktisch.Polymophie.Modul5.Discount.*;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Angebot {
    //Datenfelder
    private Rabattstragie rabattstragie;
    private String flugnummer;
    private GregorianCalendar datum;
    private double regularPreis;

    private SimpleDateFormat datumsFormatierer = new SimpleDateFormat("YYYY-MMMM-dd");
    private DecimalFormat format = new DecimalFormat("#,##");

    //Konstruktor
    public Angebot(String flugnummer, GregorianCalendar datum, double regularPreis) {
        this.flugnummer = flugnummer;
        this.datum = datum;
        this.regularPreis = regularPreis;
        rabattWaehlen();
    }

    //Methoden
    // 0= Jänner, 1=Feber, 2=März, 3=April, 9= Oktober, 11=Dezember
    public void rabattWaehlen() {
        if (!getFlugnummer().contains("999") && getDatum().get(Calendar.MONTH) == 1 || getDatum().get(Calendar.MONTH) == 2) {
            this.rabattstragie = new MidiDiscount("Mittlerer Preisnachlass von -15% ");
        } else if (!getFlugnummer().contains("999") && getDatum().get(Calendar.MONTH) == 0 || getDatum().get(Calendar.MONTH) == 3
                || getDatum().get(Calendar.MONTH) == 9) {
            this.rabattstragie = new MaxiDiscount("Maximaler Preisnachlass von -30% ");
        } else if (!getFlugnummer().contains("999") && getDatum().get(Calendar.MONTH) == 11) {
            this.rabattstragie = new ChristmasDiscount("Weihnachts-Preisnachlass von -50% ");
        } else if (getFlugnummer().contains("111") || getFlugnummer().contains("222") || getFlugnummer().contains("333")
                || getFlugnummer().contains("444") || getFlugnummer().contains("555")) {
            this.rabattstragie = new SpezialDiscount(("Glücks-Rabatt anhand der passenden Flugnummer \"" + getFlugnummer() + "\" von -75% "));
        } else if (getFlugnummer().contains("999")) {
            this.rabattstragie = new ZeroDiscount("Kein Rabatt gewährt!");
        } else {
            this.rabattstragie = new ZeroDiscount("Während diesem Zeitraums wird kein Rabatt gewährt!");
        }
    }

    public double getReduzierterPreis() {
        return this.rabattstragie.getReduierterPreis(this.regularPreis);
    } //Polymorpher-Aufruf

    public Rabattstragie getRabattstragie() {
        return rabattstragie;
    }

    public void setRabattstragie(Rabattstragie rabattstragie) {
        this.rabattstragie = rabattstragie;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public void setFlugnummer(String flugnummer) {
        this.flugnummer = flugnummer;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum = datum;
    }

    public double getRegularPreis() {
        return regularPreis;
    }

    public void setRegularPreis(double regularPreis) {
        this.regularPreis = regularPreis;
    }

    @Override
    public String toString() {
        return "Angebot -> " +
                " Rabattstragie: " + rabattstragie +
                " | Flugnummer: " + flugnummer +
                " | Datum: " + datumsFormatierer.format(datum.getTime()) +
                " | Regulärer Preis: " + formatPreis(getRegularPreis());
    }

    //Anpassung an jegliche Währungen der Welt z.B.: €, Pfund, Yen...
    public String formatPreis(double preis) {
        return NumberFormat.getCurrencyInstance(Locale.CHINA).format(preis);
    }

    public void anzeigen() {
        System.out.println(this);
        System.out.println("\tRegulärer Preis: " + formatPreis(this.getRegularPreis()));
        System.out.println("\tReduzierter Preis: " + formatPreis(this.getReduzierterPreis()));
        printStrich(165);
    }

    public void printStrich(int value) {
        for (int i = 0; i < value; i++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
