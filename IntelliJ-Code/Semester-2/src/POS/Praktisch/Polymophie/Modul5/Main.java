package POS.Praktisch.Polymophie.Modul5;

import java.util.GregorianCalendar;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        double rangeMin = 50.0, rangeMax = 2000.0;
        for (int i = 0; i < 12; i++) {
            Angebot angebot = new Angebot(
                    String.valueOf(111),
                    new GregorianCalendar(2021, i, new Random().nextInt(31) + 1),
                    rangeMin + (rangeMax - rangeMin) * new Random().nextDouble()
            );
            angebot.anzeigen();
        }
        System.out.println();
        System.out.println();
    }
}

