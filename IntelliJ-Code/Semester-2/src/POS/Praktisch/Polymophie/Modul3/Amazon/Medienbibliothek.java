package POS.Praktisch.Polymophie.Modul3.Amazon;

import java.util.ArrayList;

public class Medienbibliothek {
    private ArrayList<Medium> mediumsListe;

    public Medienbibliothek() {
        mediumsListe = new ArrayList<>();
    }

    public void mediumHinzufuegen(Medium medium) {
        mediumsListe.add(medium);
    }

    public void mediumAnzeigen() {
        for (Medium m : mediumsListe) {
            m.anzeigen();
        }
    }

    public void mediumAnzeigenMitTitel(String titel) {
        for (Medium m : mediumsListe) {
            if (m.getTitel().contains(titel)) {
                m.anzeigen();
            }else {
                System.out.println("Medium ["+titel+"] ist nicht vorhanden.");
            }
        }
    }
}
