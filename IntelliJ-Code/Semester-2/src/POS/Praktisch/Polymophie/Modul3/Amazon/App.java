package POS.Praktisch.Polymophie.Modul3.Amazon;

public class App {
    public static void main(String[] args) {
        Medienbibliothek medienbibliothek = new Medienbibliothek();
        Urheber lucBesson = new Urheber("Luc", "Besson", "tobis");
        Urheber mKöelling = new Urheber("Michael", "Koelling", "Pearson");

        Genre comedy = new Genre("Comedy");
        Genre lernUnterlagen = new Genre("Lernunterlagen");

        Filme taxi1 = new Filme("Taxi", "1998", "Tolle Unterhaltung", lucBesson, comedy, false, false, 85);
        eBooks blueJ = new eBooks("Java lernen mit BlueJ", "2017", "JAVA Lernen", mKöelling, lernUnterlagen, "978-3-86326-961-6", 672, 6);

        taxi1.anzeigen();
        blueJ.anzeigen();
        System.out.println("******************************************************");

        medienbibliothek.mediumHinzufuegen(taxi1);
        medienbibliothek.mediumHinzufuegen(blueJ);

        medienbibliothek.mediumAnzeigen();
        System.out.println("******************************************************");
        medienbibliothek.mediumAnzeigenMitTitel("Java");


    }
}
