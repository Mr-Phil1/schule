package POS.Praktisch.Polymophie.Modul3.Amazon;

public class eBooks extends Medium {
    private String isbn;
    private int seiten, auflage;
    private Urheber autor;

    public eBooks(String titel, String erscheinungsdatum, String kommentar, Urheber autor, Genre genre, String isbn, int seiten, int auflage) {
        super(titel, erscheinungsdatum, kommentar, genre);
        this.isbn = isbn;
        this.seiten = seiten;
        this.auflage = auflage;
        this.autor = autor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getSeiten() {
        return seiten;
    }

    public void setSeiten(int seiten) {
        this.seiten = seiten;
    }

    public int getAuflage() {
        return auflage;
    }

    public void setAuflage(int auflage) {
        this.auflage = auflage;
    }

    public Urheber getAutor() {
        return autor;
    }

    public void setAutor(Urheber autor) {
        this.autor = autor;
    }

    @Override
    public void anzeigen() {
        super.anzeigen();
        System.out.println("[eBook] Autor -> " + this.getAutor().toString() + ", Buchseiten -> " + this.getSeiten()+
                ", Auflage -> " +this.getAuflage() +
                ", ISBN -> " +this.getIsbn());
    }
}
