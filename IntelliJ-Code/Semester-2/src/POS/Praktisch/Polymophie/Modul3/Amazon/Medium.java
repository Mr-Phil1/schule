package POS.Praktisch.Polymophie.Modul3.Amazon;

public class Medium {
    private String titel, erscheinungsdatum, kommentar;
    private Genre genre;

    public Medium(String titel, String erscheinungsdatum, String kommentar, Genre genre) {
        this.titel = titel;
        this.erscheinungsdatum = erscheinungsdatum;
        this.kommentar = kommentar;
        this.genre = genre;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getErscheinungsdatum() {
        return erscheinungsdatum;
    }

    public void setErscheinungsdatum(String erscheinungsdatum) {
        this.erscheinungsdatum = erscheinungsdatum;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }


    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    private void printDash(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public void anzeigen() {
        printDash(130);
        System.out.println("[Medium] Title -> " + this.getTitel() + ", Erscheinungsdatum -> " + this.getErscheinungsdatum() +
                ", Kommentar -> " + this.getKommentar() +
                ", Genre -> " + this.getGenre());
    }
}
