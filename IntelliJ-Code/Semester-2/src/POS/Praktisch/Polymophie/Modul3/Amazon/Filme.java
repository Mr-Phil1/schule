package POS.Praktisch.Polymophie.Modul3.Amazon;

public class Filme extends Medium {
    private boolean uhd, hd;
    private int spielzeit;
    private Urheber regisseur;

    public Filme(String titel, String erscheinungsdatum, String kommentar, Urheber regisseur, Genre genre, boolean uhd, boolean hd, int spielzeit) {
        super(titel, erscheinungsdatum, kommentar, genre);
        this.uhd = uhd;
        this.hd = hd;
        this.spielzeit = spielzeit;
        this.regisseur=regisseur;
    }

    public boolean isUhd() {
        return uhd;
    }

    public void setUhd(boolean uhd) {
        this.uhd = uhd;
    }

    public boolean isHd() {
        return hd;
    }

    public void setHd(boolean hd) {
        this.hd = hd;
    }

    public int getSpielzeit() {
        return spielzeit;
    }

    public void setSpielzeit(int spielzeit) {
        this.spielzeit = spielzeit;
    }

    public Urheber getRegisseur() {
        return regisseur;
    }

    public void setRegisseur(Urheber regisseur) {
        this.regisseur = regisseur;
    }

    public void anzeigen()  //Überschreiben der Mutterklassen Methode
    {
        super.anzeigen();   //Aufruf der Medium Methode anzeigen
        System.out.println("[FILM] Regisseur -> " + this.getRegisseur().toString() + ", Spielzeit -> " + this.getSpielzeit()+
                " min, UHD-Qualitaet -> " +this.isUhd() +
                ", HD-Qualitaet -> " +this.isHd());
    }
}
