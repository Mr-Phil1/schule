package POS.Praktisch.Polymophie.Modul3.SocialNetwork;

import java.util.ArrayList;

public class NewsFeed {
    private ArrayList<NachrichtenEinsendung> nachrichten;
    private ArrayList<FotoEinsendung> fotos;

    /**
     * Erzeuge einen leeren Newsfeed.
     */
    public NewsFeed()
    {
        nachrichten = new ArrayList<NachrichtenEinsendung>();
        fotos = new ArrayList<FotoEinsendung>();
    }

    /**
     * Füge dem Newsfeed eine eingesendete Nachricht hinzu.
     *
     * @param nachricht  der eingesendete Text, der hinzugefügt werden soll.
     */
    public void erfasseNachricht(NachrichtenEinsendung nachricht)
    {
        nachrichten.add(nachricht);
    }

    /**
     * Füge dem Newsfeed ein eingesendetes Foto hinzu.
     *
     * @param foto  das eingesendete Foto, das hinzugefügt werden soll.
     */
    public void erfasseFoto(FotoEinsendung foto)
    {
        fotos.add(foto);
    }

    /**
     * Gib den Newsfeed aus auf die Kommandozeile. Derzeit werden die Details des Newsfeed auf die
     * Konsole ausgegeben. (Noch zu tun: ersetze diesen Code durch die Anzeige im
     * Webbrowser.)
     */
    public void zeigen()
    {
        // alle Text-Einsendungen anzeigen
        for(NachrichtenEinsendung nachricht : nachrichten) {
            nachricht.anzeigen();
            System.out.println();   // leere Zeile zwischen den Einsendungen
        }

        // alle Fotos anzeigen
        for(FotoEinsendung foto : fotos) {
            foto.anzeigen();
            System.out.println();   // leere Zeile zwischen den Einsendungen
        }
    }
}
