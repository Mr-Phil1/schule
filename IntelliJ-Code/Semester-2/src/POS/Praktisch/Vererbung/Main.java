package POS.Praktisch.Vererbung;

public class Main {
    public static void main(String[] args) {
        try {
            Koordinate k1 = new Koordinate(120, 3);
            POI pointOfInterest = new POI();

            Tankstelle tankstelle = new Tankstelle("Esso Imst", new Koordinate(0, 0), true, true, true, true);
            System.out.println(tankstelle);

            Berggipfel berggipfel = new Berggipfel("Hohe Munde", new Koordinate(23, 47), 2890.0);
            System.out.println(berggipfel);

            tankstelle.setName("AGIP Imst");
            System.out.println(tankstelle);

            POI meinErsterPointOfInterest = new POI("mein POI", new Koordinate(23, 23));
            POI meinZweiterPointOfInterest = new Berggipfel("Venet", new Koordinate(23, 23), 2890);
            Berggipfel meinBerggipfel = (Berggipfel) meinErsterPointOfInterest;

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Bitte gben Sie nur Koordinaten von Länge -180 bis 180 und Breite -90 ung 90 ein.");
        } catch (ClassCastException classCastException) {
            System.out.println(classCastException.getMessage());
        }
    }
}
