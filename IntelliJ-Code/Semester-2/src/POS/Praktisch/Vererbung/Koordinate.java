package POS.Praktisch.Vererbung;

public class Koordinate {
    private final double laenge, breite;

    public Koordinate(double laenge, double breite) {
        if (laenge >= -180 && laenge <= 180 && breite >= -90 && breite <= 90) {
            this.laenge = laenge;
            this.breite = breite;
        } else {
            throw new IllegalMonitorStateException();

        }
    }

    public double getBreite() {
        return breite;
    }

    public double getLaenge() {
        return laenge;
    }

    @Override
    public String toString() {
        return "Koordinate{" +
                "laenge=" + laenge +
                ", breite=" + breite +
                '}';
    }
}
