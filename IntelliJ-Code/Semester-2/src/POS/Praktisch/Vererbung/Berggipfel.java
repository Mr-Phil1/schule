package POS.Praktisch.Vererbung;

public class Berggipfel extends POI {
    private double hoehe;

    public Berggipfel(String name, Koordinate koordinate, double hoehe) {
        super(name, koordinate);
        this.hoehe = hoehe;
    }

    public double getHoehe() {
        return hoehe;
    }

    public void setHoehe(double hoehe) {
        this.hoehe = hoehe;
    }

    @Override
    public String toString() {
        return super.toString() + ", Berggipfel{" +
                "hoehe=" + hoehe +
                '}';
    }
}
