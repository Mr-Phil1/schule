package POS.Praktisch.Vererbung;

public class POI {
    private String name;
    private Koordinate koordinate;

    public POI() {
        this("UNDEFINED", new Koordinate(0, 0));
    }

    public POI(String name, Koordinate koordinate) {
        this.setName(name);
        this.setKoordinate(koordinate);
    }

    public void setName(String name) {
        if (name != null && !name.equals(" ")) {
            this.name = name;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void setKoordinate(Koordinate koordinate) {
        if (koordinate != null) {
            this.koordinate = koordinate;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "POI{" +
                "name='" + name + '\'' +
                ", koordinate=" + koordinate +
                '}';
    }
}
