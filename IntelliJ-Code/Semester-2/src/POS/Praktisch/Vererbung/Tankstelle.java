package POS.Praktisch.Vererbung;

public class Tankstelle extends POI {

    private boolean diesel, benz95, benz98, erdgas;

    public Tankstelle() {
        super();
    }

    public Tankstelle(String name, Koordinate koordinate, boolean diesel, boolean benz95, boolean benz98, boolean erdgas) {
        super(name, koordinate);
        this.diesel = diesel;
        this.benz95 = benz95;
        this.benz98 = benz98;
        this.erdgas = erdgas;

    }

    public boolean isDiesel() {
        return diesel;
    }

    public boolean isBenz95() {
        return benz95;
    }

    public boolean isBenz98() {
        return benz98;
    }

    public boolean isErdgas() {
        return erdgas;
    }

    @Override
    public String toString() {
        return super.toString() + ", Tankstelle{" +
                "diesel=" + diesel +
                ", benz95=" + benz95 +
                ", benz98=" + benz98 +
                ", erdgas=" + erdgas +
                '}';
    }
}
