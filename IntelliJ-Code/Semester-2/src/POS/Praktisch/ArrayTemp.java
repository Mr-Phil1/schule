package POS.Praktisch;

public class ArrayTemp {
    public static void main(String[] args) {
        int[] tagestemperaturen = new int[24];
        int[] tagestemperaturen2 = {22,33,4,5,34,5,4,3,2,1};
        for(int i=0;i<tagestemperaturen2.length;i++)
        {
            System.out.println("Temperatur " + i + ": " + tagestemperaturen2[i]);
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
        tagestemperaturen2[3] = tagestemperaturen2[3]+1;
        for(int i=0;i<tagestemperaturen2.length;i++)
        {
            System.out.println("Temperatur " + i + ": " + tagestemperaturen2[i]);
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
        for(int x : tagestemperaturen2)
        {
            System.out.println(x);
        }
    }
}
