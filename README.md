# Schulische-Projekte

## Info:
In diesem Repository werden sämtliche Programme hinterlegt, welche während des Schulverlaufs des IT-Kolleg Imst erstellt wurden. Sollte Sie noch Fragen bezüglich der Anforderungen bzw. der Haftung haben, so empfehle ich Ihnen dass [Lizenz-und-Schüleranforderung](https://gitlab.com/Mr-Phil1/schule/-/blob/master/NetBeans-Code/Lizenz-und-Sch%C3%BCleranforderung.pdf) Dokument noch mal zu lesen.


### Ordnerstruktur:
- Sämtliche Projekte von **NetBeans** erstellt sind im Verzeichnis [NetBeans-Code](NetBeans-Code) zu finden.
- Sämtliche Scripte für **Linux** sind im Verzeichnis [Linux](Linux-Script) zu finden.
- Sämtliche Projekte von **BlueJ** erstellt sind im Verzeichnis [BlueJ-Code](BlueJ-Code) zu finden.
- Sämtliche Projekte von **IntelliJ** erstellt sind im Verzeichnis [IntelliJ-Code](IntelliJ-Code) zu finden.
- Sämtliche Projekte für **Docker** sind im Verzeichnis [Docker-Script](Docker-Script) zu finden.
- Sämtliche Projekte für **Arduino** sind im Verzeichnis [Arduino-Code](Arduino-Code) zu finden.
---
#### Disclaimer:
Der Seitenbetreiber (Mr. Phil) übernimmt keinerlei Haftung für die onlinegestellten Codezeilen, außer für die, welche der Seitenbetreiber selbst geschrieben hat. Sämtliche Codezeilen, die von anderen Quellen (z.B. StackOverflow) stammen, müssen vom Ersteller mit einer Quellenangabe versehen werden.  Sollte eine der oben genannten Anforderungen nicht erfüllt sein, behält sich der Seitenbetreiber das Recht vor, das zur Verfügung gestellte Projekt nicht zu veröffentlichen.

Sollte es zu Forderungen dritte wegen unberechtigter Verwendung von Codezeilen kommen, so haftet der Seitenbetreiber nicht dafür, sondern immer der Ersteller der Codezeilen.

---
