
/**
 * Beschreiben Sie hier die Klasse Spieler.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spieler
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String name;
    private int tragkraft;
    private Raum aktuellerRaum;

    /**
     * Konstruktor f�r Objekte der Klasse Spieler
     */
    public Spieler(String name, int tragkraft)
    {
        // Instanzvariable initialisieren
        this.name=name;
        this.tragkraft=tragkraft;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter f�r eine Methode
     * @return        die Summe aus x und y
     */
    public void setzeRaum(Raum raum)
    {
        // tragen Sie hier den Code ein
        this.aktuellerRaum=raum;
    }

    public void setTragkraft(int value){
        this.tragkraft=value;
    }

    public void erhoheTragkraft(int wert){
        this.setTragkraft(wert);
    }

    public String getName(){
        return this.name;
    }

    public int getTragkraft(){
        return this.tragkraft;
    }

    public void status(){
        System.out.println("Spielername: "+this.getName()
            +", hat eine Tragkraft von "+this.getTragkraft()+"kg.\n"
            +this.aktuellerRaum.gegenstand.gibBeschreibung());
    }
}
