import java.util.HashMap;
import java.util.Set;

/**
 * Diese Klasse modelliert R�ume in der Welt von Zuul.
 * 
 * Diese Klasse ist Teil der Anwendung "Die Welt von Zuul".
 * "Die Welt von Zuul" ist ein sehr einfaches textbasiertes 
 * Adventure-Game.
 * 
 * Ein "Raum" repr�sentiert einen Ort in der virtuellen Landschaft des
 * Spiels. Ein Raum ist mit anderen R�umen �ber Ausg�nge verbunden.
 * M�gliche Ausg�nge liegen im Norden, Osten, S�den und Westen.
 * F�r jede Richtung h�lt ein Raum eine Referenz auf den 
 * benachbarten Raum.
 * 
 * @author  Michael K�lling und David J. Barnes
 * @version 2016.02.29
 */
public class Raum 
{
    public String beschreibung;
    public Raum nordausgang;
    public Raum suedausgang;
    public Raum ostausgang;
    public Raum westausgang;
    public GegenstandListe gegenstand;
    private HashMap<String, Raum> ausgaenge;

    /**
     * Erzeuge einen Raum mit einer Beschreibung. Ein Raum
     * hat anfangs keine Ausg�nge. Eine Beschreibung hat die Form 
     * "in einer K�che" oder "auf einem Sportplatz".
     * @param beschreibung  die Beschreibung des Raums
     */
    public Raum(String beschreibung) 
    {
        this.beschreibung = beschreibung;
        ausgaenge=new HashMap<>();
        gegenstand=new GegenstandListe();

    }

    public void setzeAusgang(String richtung, Raum nachbar){
        ausgaenge.put(richtung,nachbar);
    }

    public void setzeGegenstand(String name ,String beschreibungGegenstand, int gewicht){
        gegenstand.erstelleGegenstand(name,beschreibungGegenstand, gewicht);
    }

    public Raum gibAusgang(String richtung) 
    {
        return ausgaenge.get(richtung);
    }

    /**
     * @return  die Beschreibung dieses Raums
     */
    public String gibBeschreibung()
    {
        return beschreibung;
    }

    public String gibAusgaengeAlsString()
    {
        String ausgabeString= "Ausg�nge:";
        Set<String> keys=ausgaenge.keySet();
        for(String ausgang : keys){
            ausgabeString+= " "+ausgang;
        }
        return ausgabeString;
    }

    public String gibLangeBeschreibung()
    {
        if(gegenstand.getBeschreibung() !=null){
            return "Sie sind "+ beschreibung+".\n"+gibAusgaengeAlsString()+".\n"+ gegenstand.gibBeschreibung();
        }
        else{return "Sie sind "+ beschreibung+".\n"+gibAusgaengeAlsString();}
    }

}
