
/**
 * Beschreiben Sie hier die Klasse Spieler.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spieler
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String name;
    private Raum aktuellerRaum;

    /**
     * Konstruktor f�r Objekte der Klasse Spieler
     */
    public Spieler(String name)
    {
        // Instanzvariable initialisieren
        this.name=name;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter f�r eine Methode
     * @return        die Summe aus x und y
     */
    public void setzeRaum(Raum raum)
    {
        // tragen Sie hier den Code ein
        this.aktuellerRaum=raum;
    }

    public String getName(){
        return this.name;
    }
}
