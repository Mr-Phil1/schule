import java.util.HashMap;
/**
 * Beschreiben Sie hier die Klasse Gegenstand.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Gegenstand
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int gegenstandGewicht;
    private String gegenstandBeschreibung;
    private HashMap<String, Integer> gegenstand;

    /**
     * Konstruktor f�r Objekte der Klasse Gegenstand
     */
    public Gegenstand()
    {
        gegenstand = new HashMap<>();
    }
    public void erstelleGegenstand(String beschreibung, int gewicht)
    {
        this.gegenstandBeschreibung = beschreibung;
        this.gegenstandGewicht = gewicht;
        gegenstand.put(beschreibung, gewicht);
        
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter f�r eine Methode
     * @return        die Summe aus x und y
     */
    public int getGewicht()
    {
  
        return this.gegenstandGewicht;
    }
    public String getBeschreibung()
    {
        return this.gegenstandBeschreibung;
    }
    public String gibBeschreibungGegenstand()
    {
        return "Gegenstand: "+this.getBeschreibung()+" und wiegt " +this.getGewicht()+" kg";
    }
}
