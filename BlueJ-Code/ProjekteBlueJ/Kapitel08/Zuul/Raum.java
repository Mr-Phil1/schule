
/**
 * Diese Klasse modelliert R�ume in der Welt von Zuul.
 * 
 * Diese Klasse ist Teil der Anwendung "Die Welt von Zuul".
 * "Die Welt von Zuul" ist ein sehr einfaches textbasiertes 
 * Adventure-Game.
 * 
 * Ein "Raum" repr�sentiert einen Ort in der virtuellen Landschaft des
 * Spiels. Ein Raum ist mit anderen R�umen �ber Ausg�nge verbunden.
 * M�gliche Ausg�nge liegen im Norden, Osten, S�den und Westen.
 * F�r jede Richtung h�lt ein Raum eine Referenz auf den 
 * benachbarten Raum.
 * 
 * @author  Michael K�lling und David J. Barnes
 * @version 2016.02.29
 */
import java.util.HashMap;
import java.util.Set;

public class Raum 
{
    private String beschreibung;
    private HashMap<String,Raum> ausgaenge;
    private Raum nordausgang, ostausgang, suedausgang, westausgang;
    private Gegenstand gegenstand;
    /**
     * Erzeuge einen Raum mit einer Beschreibung. Ein Raum
     * hat anfangs keine Ausg�nge. Eine Beschreibung hat die Form 
     * "in einer K�che" oder "auf einem Sportplatz".
     * @param beschreibung  die Beschreibung des Raums
     */
    public Raum(String beschreibung) 
    {
        this.beschreibung = beschreibung;
        ausgaenge = new HashMap<>();
        gegenstand = new Gegenstand();
    }

    /**
     * Definiere die Ausg�nge dieses Raums. Jede Richtung
     * f�hrt entweder in einen anderen Raum oder ist 'null'
     * (kein Ausgang).
     * @param norden  der Nordausgang
     * @param osten   der Ostausgang
     * @param sueden  der S�dausgang
     * @param westen  der Westausgang
     */
    // public void setzeAusgaenge(Raum norden, Raum osten,
    // Raum sueden, Raum westen) 
    // {
    // if(norden != null) {
    // ausgaenge.put("north", norden);
    // }
    // if(osten != null) {
    // ausgaenge.put("east", osten);
    // }
    // if(sueden != null) {
    // ausgaenge.put("south", sueden);
    // }
    // if(westen != null) {
    // ausgaenge.put("west", westen);
    // }
    // }
    public void setzeAusgang(String richtung, Raum nachbar)
    {
        ausgaenge.put(richtung, nachbar);
    }

    public void setzeGegenstand(String beschreibung, int gewicht)
    {
        gegenstand.erstelleGegenstand(beschreibung, gewicht);
    }

    public Raum gibAusgang(String richtung)
    {

        return ausgaenge.get(richtung);
    }
    //Aufgabe 8.7
    /**
     * Liefere eine Beschreibung der Ausg�nge dieses Raumes.
     * beispielsweise "Ausg�nge: north west".
     * @return eine Beschreibung der verf�gbaren Ausg�nge
     */
    public String gibAusgaengeAlsString()
    {
        // String ausgabeString = "Ausg�nge: ";
        // if(nordausgang != null)
        // {
        // ausgabeString = ausgabeString + "Norden ";
        // }
        // if(ostausgang != null)
        // {
        // ausgabeString = ausgabeString + "Osten ";
        // }        
        // if(suedausgang != null)
        // {
        // ausgabeString = ausgabeString + "S�den ";
        // }
        // if(westausgang != null)
        // {
        // ausgabeString = ausgabeString + "Westen ";
        // }
        // return ausgabeString;
        String ergebnis = "Ausg�nge:";
        Set<String> keys = ausgaenge.keySet();
        for(String ausgang : keys)
            ergebnis += " " + ausgang;
        return ergebnis;
    }

    /**
     * @return  die Beschreibung dieses Raums
     */
    public String gibBeschreibung()
    {
        return beschreibung;
    }
    //8.11
    /**
     * Liefere eine lange Beschreibung dieses Raums, in der Form:
     *     Sie sind in der K�che.
     *     Ausg�nge: north west
     * @return  eine lange Beschreibung dieses Raumes
     */
    public String gibLangeBeschreibung()
    {
        return "Sie sind " + beschreibung + ".\n" + gibAusgaengeAlsString()+" .\n"+ gegenstand.gibBeschreibungGegenstand();
    }
}
