import java.awt.Color;
import java.util.Random;

/**
 * Die Klasse ZeichnenDemo bietet einige kurze Demonstrationen, wie mithilfe der
 * Klasse Stift verschiedene Zeichnungen erzeugt werden können.
 *
 * @author Michael Kölling und David J. Barnes
 * @version 2016.02.29
 */

public class ZeichnenDemo
{
    private Canvas meineCanvas;
    private Random zufallsgenerator;

    /**
     * Die Demo vorbereiten. Es wird eine frische Canvas erzeugt und sichtbar gemacht.
     */
    public ZeichnenDemo()
    {
        meineCanvas = new Canvas("Zeichnen-Demo", 500, 400);
        zufallsgenerator = new Random();
    }

    /**
     * Zeichne ein Quadrat auf den Bildschirm.
     * Anfangs wird ein Stift-Objekt mit den Positionen x320 und y260 und
     *  und dem meineCanvas Objekt erstellt.
     *  Im Anschluss wird die Farbe des Stifts auf Blau gesetzt und dann wird mit 
     *  Hilfe dieses Stiftes die Methode quadrat aufgerufen.
     */
    public void zeichneQuadrat()
    {
        Stift stift = new Stift(320, 260, meineCanvas);
        stift.setzeFarbe(Color.BLUE);
        quadrat(stift);
    }

    public void zeichneDreieck()
    {
        Stift stift = new Stift(100, 100, meineCanvas);
        stift.setzeFarbe(Color.GREEN);
        dreieck(stift);
    }

    public void zeichneFuenfeck()
    {
        Stift stift = new Stift(100, 100, meineCanvas);
        stift.setzeFarbe(Color.RED);
        fuenfeck(stift);
    }

    public void zeichnePolygon(int n)
    {
        Stift stift = new Stift(100, 100, meineCanvas);
        stift.setzeFarbe(Color.RED);
        polygon(n,stift);
    }

    public void zeichneSpirale()
    {
        Stift stift = new Stift(100, 100, meineCanvas);
        stift.setzeFarbe(Color.MAGENTA);
        spirale(stift);
    }

    /**
     * Zeichne ein Rad aus mehreren Quadraten.
     * Anfangs wird ein Stift-Objekt mit den Positionen x250 und y200 und
     * und dem meineCanvas Objekt erstellt.
     * Im Anschluss wird die Farbe des Stifts auf Rot gesetzt und dann wird mit 
     * Hilfe dieses Stiftes die Methode quadrat 36-mal aufgerufen und dabei wird der 
     * Stift jedesmal um 10° gedreht.
     */
    public void zeichneRad()
    {
        Stift stift = new Stift(250, 200, meineCanvas);
        stift.setzeFarbe(Color.RED);

        for (int i=0; i<36; i++) {
            quadrat(stift);
            stift.drehen(10);
        }
    }

    public void zeichneIrendwas()
    {
        meineCanvas.setBackgroundColor(Color.RED);   
        meineCanvas.fillCircle(100, 100, 50);
        meineCanvas.fillRectangle(250, 250, 50, 50);
    }

    /**
     * Zeichne ein Quadrat in der Farbe des Stifts und an der Position des Stifts.
     * Bei dieser Methode wird mit Hilfe eines Stift Objektes eine Quadrat gezeichnet.
     * Hierbei wird der Sift 100px in eine Richtung bwewegt und dann um 90° gedreht.
     * Dieser Vorgang wiederholt sich 4 mal.
     * 
     */
    private void quadrat(Stift stift)
    {
        for (int i=0; i<4; i++) {
            stift.bewegen(100);
            stift.drehen(90);
        }
    }

    private void dreieck(Stift stift)
    {
        for (int i=0; i<3; i++) {
            stift.bewegen(100);
            stift.drehen(120);
        }
    }

    private void fuenfeck(Stift stift)
    {
        for (int i=0; i<5; i++) {
            stift.bewegen(100);
            stift.drehen(72);
        }
    }

    private void polygon(int n, Stift stift)
    {
        for (int i=0; i<n; i++) {
            stift.bewegen(100);
            stift.drehen(360/n);
        }
    }    

    private void spirale(Stift stift)
    {
        for (int i=0; i<50; i++) {
            stift.bewegen(i);
            stift.drehen(90);
        }
    }

    /**
     * Zeichne einige zufällige Schnörkel in zufälligen Farben.
     * Anfangs wird ein Stift-Objekt mit den Positionen x250 und y200 und
     * und dem meineCanvas Objekt erstellt.
     * Im Anschluss werden die Farbe des Stifts auf einen zufälligen
     * ROt, Grün und Blau Wert gesetzt und dann wird mit 
     * Hilfe dieses Stiftes die Methode stift.zufaelligesGekritzel() aufgerufen.
     * Dieser Vorgang wiederholt sich 10 mal.
     */
    public void buntesGekritzel()
    {
        Stift stift = new Stift(250, 200, meineCanvas);

        for (int i=0; i<10; i++) {
            // wähle zufällig eine Farbe aus
            int rot = zufallsgenerator.nextInt(256);
            int gruen = zufallsgenerator.nextInt(256);
            int blau = zufallsgenerator.nextInt(256);
            stift.setzeFarbe(new Color(rot, gruen, blau));

            stift.zufaelligesGekritzel();
        }
    }

    /**
     * Leere die Zeichenfläche.
     */
    public void leeren()
    {
        meineCanvas.erase();
    }
}
