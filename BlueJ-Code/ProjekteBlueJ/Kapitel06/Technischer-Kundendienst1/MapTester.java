
import java.util.HashMap;

public class MapTester
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    HashMap<String, String> kontakte;

    /**
     * Konstruktor f�r Objekte der Klasse MapTester
     */
    public MapTester()
    {
        this.kontakte=new HashMap<>();
    }

    public void nummerEintragen(String name, String nummer){
        this.kontakte.put(name, nummer);
    }
    public String  nummerSuchen(String name){
        return this.kontakte.get(name);
    }
}
