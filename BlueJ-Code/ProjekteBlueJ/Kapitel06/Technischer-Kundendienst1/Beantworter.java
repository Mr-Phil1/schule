/**
 * Die Klasse Beantworter beschreibt Exemplare, die 
 * automatische Antworten generieren.
 * 
 * @author     Michael K�lling und David J. Barnes
 * @version    0.1 (2016.02.29)
 */
import java.util.Random;
import java.util.ArrayList;
public class Beantworter
{
    private int randNumber;
    private Random random;
    private ArrayList<String> antworten;
    /**
     * Erzeuge einen Beantworter - nichts Besonderes zu tun.
     */
    public Beantworter()
    {
        random = new Random();
        antworten=new ArrayList<>();
        antwortlistefuellen();
    }

    /**
     * Erzeuge eine Antwort.
     * @return  einen String, der die Antwort enth�lt
     */
    public String generiereAntwort(String wort)
    {
         //return "Das klingt interessant. Erz�hlen Sie mehr ...";
        return antworten.get(random.nextInt(antworten.size()));
    }

    private void antwortlistefuellen(){
        antworten.add("Das klingt interessant. Erz�hlen Sie mehr ...");
        antworten.add("Windows ist nicht toll, dass wissen wir.");
        antworten.add("Linux hat auch seine Vorteile.");
    }
}
