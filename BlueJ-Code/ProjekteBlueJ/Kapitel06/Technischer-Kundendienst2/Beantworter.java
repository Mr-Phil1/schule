import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
/**
 * Die Klasse Beantworter beschreibt Exemplare, die 
 * automatische Antworten generieren. Dies ist die zweite Version 
 * dieser Klasse. In dieser Version generieren wir Antworten, 
 * indem wir zuf�llig eine Antwortphrase aus einer Liste ausw�hlen.
 * 
 * @author     Michael K�lling und David J. Barnes
 * @version    0.2 (2016.02.29)
 */

public class Beantworter
{
    private int randNumber;
    private Random random;
    private ArrayList<String> antworten;
    private HashMap<String, String> antwortMap;
    private HashMap<String, String> synonyme;

    /**
     * Erzeuge einen Beantworter
     */
    public Beantworter()
    {
        random = new Random();
        antworten=new ArrayList<>();
        antwortMap=new HashMap<>();
        synonyme=new HashMap<>();
        standardantwortlisteFuellen();
        antwortMapBefuellen();
        synonymenListeBefuellen();
    }

    public void synonymenListeBefuellen(){
        synonyme.put("abst�rtzt", "absturz");
        synonyme.put("st�rzt", "absturz");
        synonyme.put("fehlerhaft", "fehler");
    }

    /**
     * Erzeuge eine Antwort.
     * @return  einen String, der die Antwort enth�lt
     */
    public String generiereAntwort(HashSet<String> woerter)
    {
        //String antwort = antwortMap.get(wort);
        //if(antwort != null){
        //    return antwort;
        //}else{
        //    return standardAntwort();
        //}
        for (String wort : woerter) {
            String antwort = antwortMap.get(wort);
            if(antwort != null){
                return antwort;
            } else {
                String syn = synonyme.get(wort);
                if(syn != null){
                    return antwortMap.get(syn);
                }
            }
        }
        return standardAntwort();
    }

    public String standardAntwort()
    {
        //return "Das klingt interessant. Erz�hlen Sie mehr ...";
        return antworten.get(random.nextInt(antworten.size()));
    }


    /**
     * Trage alle bekannten Stichw�rter mit ihren verkn�pften
     * Antworten in die Map 'antwortMap' ein.
     */
    private void antwortMapBefuellen()
    {
        antwortMap.put("absturz", 
            "Tja, auf unserem System kommt es nie zu einem Absturz. Das muss \n" +
            "an Ihrem System liegen. Welche Konfiguration haben Sie?");
        antwortMap.put("langsam", 
            "Ich vermute, dass das mit Ihrer Hardware zu tun hat. Ein Upgrade\n" +
            "f�r Ihren Prozessor sollte diese Probleme l�sen. Haben Sie ein\n" +
            "Problem mit unserer Software?");
        antwortMap.put("performance", 
            "Bei all unseren Tests war die Performance angemessen. Haben Sie\n" +
            "andere Prozesse, die im Hintergrund laufen?");
        antwortMap.put("fehler", 
            "Wissen Sie, jede Software hat Fehler. Aber unsere Entwickler arbeiten\n" +
            "sehr hart daran, diese Fehler zu beheben. K�nnen Sie das Problem ein\n" +
            "wenig genauer beschreiben?");
        antwortMap.put("windows", 
            "Das ist ein bekanntes Problem, das im Betriebssystem von Microsoft begr�ndet\n" +
            "ist. Bitte leiten Sie es an Microsoft weiter. Da k�nnen wir leider nichts \n" +
            "dran machen.");
        antwortMap.put("mac", 
            "Das ist ein bekanntes Problem, das im Betriebssystem des Mac begr�ndet\n" +
            "ist. Bitte leiten Sie es an Apple weiter. Da k�nnen wir leider nichts \n" +
            "dran machen.");
        antwortMap.put("teuer", 
            "Unsere Preise sind absolute Marktpreise. Haben Sie sich mal umgesehen\n" +
            "und wirklich unser Leistungsspektrum verglichen?");
        antwortMap.put("installation", 
            "Die installation ist wirklich sehr einfach. Wir haben haufenweise\n" +
            "Wizards, die die Arbeit f�r sie erledigen. Haben Sie die Installations-\n" +
            "anleitung gelesen?");
        antwortMap.put("speicher", 
            "Wenn sie die Systemanforderungen gr�ndlich lesen, werden Sie feststellen,\n" +
            "dass die Speicheranforderung 1,5 Gigabyte betr�gt. Sie sollten Ihren\n" +
            "Hauptspeicher unbedingt aufr�sten. K�nnen wir sonst noch etwas f�r Sie tun?");
        antwortMap.put("linux", 
            "Wir nehmen Linux sehr ernst. Aber es gibt da einige Probleme.\n" +
            "Die meisten h�ngen mit der inkompatiblen glibc-Version zusammen. K�nnen \n" +
            "Sie das Problem etwas pr�ziser beschreiben?");
        antwortMap.put("bluej", 
            "Ahhh, BlueJ, ja. Wir haben schon vor l�ngerer Zeit versucht, diese Leute \n" +
            "aufzukaufen, aber sie wollen nicht verkaufen... Sturk�pfe sind das. \n" +
            "Ich f�rchte, da k�nnen wir nichts dran �ndern.");
    }

    /**
     * Generiere eine Liste von Standardantworten, aus denen wir eine
     * ausw�hlen k�nnen, wenn wir keine bessere Antwort wissen.
     */
    private void standardantwortlisteFuellen()
    {
        antworten.add("Das klingt seltsam. K�nnen Sie das ausf�hrlicher beschreiben?");
        antworten.add("Bisher hat sich noch kein Kunde dar�ber beschwert. \n" +
            "Welche Systemkonfiguration haben Sie?");
        antworten.add("Da brauche ich etwas ausf�hrlichere Angaben.");
        antworten.add("Haben Sie gepr�ft, ob Sie einen Konflikt mit einer DLL haben?");
        antworten.add("Das steht im Handbuch. Haben Sie das Handbuch gelesen?");
        antworten.add("Das klingt alles etwas Wischi-Waschi. Haben Sie einen Experten\n" +
            "in der N�he, der das besser beschreiben kann?");                     
        antworten.add("Das ist kein Fehler, das ist eine Systemeigenschaft!");
        antworten.add("K�nnten Sie es anders erkl�ren?");
        antworten.add("Haben Sie versucht, die App auf Ihrem Handy auszuf�hren?");
        antworten.add("Ich habe gerade StackOverflow �berpr�ft - das ist auch keine L�sung.");
    }
    
}
