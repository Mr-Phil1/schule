import java.util.Scanner;
import java.util.HashSet;

/**
 * Ein Eingabeleser liest eingetippten Text von der Konsole.
 * Dieser Text wird dann in W�rter zerlegt, die in einer Menge
 * zur�ckgeliefert werden.
 * 
 * @author     Michael K�lling und David J. Barnes
 * @version    0.2 (2016.02.29)
 */
public class Eingabeleser
{
    private Scanner scanner;

    /**
     * Erzeuge einen neuen Eingabeleser, der Text von der Konsole
     * einliest.
     */
    public Eingabeleser()
    {
        scanner = new Scanner(System.in);;
    }

    /**
     * Lies eine Zeile von der Konsole und liefere sie als String.
     *
     * @return  eine Zeichenkette, die vom Benutzer eingetippt wurde
     */
    public String gibEingabe1() 
    {
        System.out.print("> ");   // Eingabebereitschaft anzeigen
        String eingabezeile = scanner.nextLine();

        return eingabezeile;
    }

    public HashSet<String> gibEingabe() 
    {
        System.out.print("> ");   // Eingabebereitschaft anzeigen
        String eingabezeile = scanner.nextLine().trim().toLowerCase();
        String[] wortArray= eingabezeile.split(" ");

        HashSet<String> woerter = new HashSet<>();
        for(String wort : wortArray){
            woerter.add(wort);
        }
        return woerter;
    }

}
