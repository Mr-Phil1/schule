
/**
 * Beschreiben Sie hier die Klasse ZufallszehlenTester.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
import java.util.Random;
import java.util.ArrayList;
public class ZufallszehlenTester
{   private int randNumber;
    private Random random;
    private ArrayList<String> antworten;
    public ZufallszehlenTester(){
        random = new Random();
        antworten=new ArrayList<>();
        antwortlistefuellen();
    }

    public void eineZufallszahlAusgeben(){
        System.out.println(random.nextInt(100));
    }

    public void mehrereZufallszahlenAusgeben(int anzahl){
        if (anzahl > 0){
            for (int i = 1; i <= anzahl; i++){
                randNumber = random.nextInt(100);
                System.out.println("Zahl "+i+" ist: "+randNumber); 
            }
        } else{
            System.out.println(0); 
        }
    }

    public void wuerfeln(){
        System.out.println(random.nextInt(6)+1);
    }

    public void gibAntwort(){
        randNumber = random.nextInt(3)+1;
        switch(randNumber){
            case 1:
            System.out.println("ja");
            break;
            case 2:
            System.out.println("nein");
            break;
            case 3:
            System.out.println("vielleicht");
            break;
        }
    }

    public String gibAntwort2(){
        return antworten.get(random.nextInt(antworten.size()));
    }

    public void gibMaxZahlen(int max){
        randNumber = random.nextInt(max)+1;
        System.out.println("Die Zufallszahl zwischen 1 bis "+max+ " ist: "+randNumber);
    }

    public void gibMinMaxZahlen(int min, int max){
        randNumber = random.nextInt(max+1 - min)+min;
        System.out.println("Die Zufallszahl zwischen"+ min+ " bis "+max+ " ist: "+randNumber);
    }

    private void antwortlistefuellen(){
        antworten.add("Ja");
        antworten.add("Nein");
        antworten.add("Vielleicht");
    }
}
