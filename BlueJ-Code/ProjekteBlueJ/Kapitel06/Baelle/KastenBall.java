import java.awt.*;
import java.awt.geom.*;
/**
 * Beschreiben Sie hier die Klasse KastenBall.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class KastenBall
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    //private BallDemo demo;
    private static final int gravitation = 3;  // Einfluss der Gravitation

    private int bremsfaktor = 2;  // simuliert den Luftwiderstand
    private Ellipse2D.Double kreis;
    private Color farbe;
    private int durchmesser;
    private int xPosition;
    private int yPosition;
    private final int bodenhoehe;      // y-Position des Bodens
    private Canvas leinwand;
    private int yGeschwindigkeit = 1;   // anf�ngliche Abw�rtsgeschwindigkeit


    /**
     * Konstruktor f�r Objekte der Klasse KastenBall
     */
     public KastenBall(int xPos, int yPos, int balldurchmesser, Color ballfarbe,int bodenPosition, Canvas zeichengrund)
    {
       xPosition = xPos;
        yPosition = yPos;
        farbe = ballfarbe;
        durchmesser = balldurchmesser;
        bodenhoehe = bodenPosition;
        leinwand = zeichengrund;
    }
    public void zeichnen()
    {
        leinwand.setForegroundColor(farbe);
        leinwand.fillCircle(xPosition, yPosition, durchmesser);
    }
         /**
     * L�sche diesen Ball an seiner aktuellen Position.
     **/
    public void loeschen()
    {
        leinwand.eraseCircle(xPosition, yPosition, durchmesser);
    }    

    /**
     * Bewege diesen Ball entsprechend seiner Position und 
     * Geschwindigkeit und zeichne ihn erneut.
     **/
    public void bewegen()
    {
        // An der aktuellen Position von der Leinwand entfernen.
        loeschen();

        // Neue Position berechnen.
        yGeschwindigkeit += gravitation;
        yPosition += yGeschwindigkeit;
        xPosition +=2;

        // Pr�fen, ob der Boden erreicht ist.
        if(yPosition >= (bodenhoehe - durchmesser) && yGeschwindigkeit > 0) {
            yPosition = (int)(bodenhoehe - durchmesser);
            yGeschwindigkeit = -yGeschwindigkeit + bremsfaktor; 
        }

        // An der neuen Position erneut zeichnen.
        zeichnen();
    }    

    /**
     * Liefere die horizontale Position dieses Balls.
     */
    public int gibXPosition()
    {
        return xPosition;
    }

    /**
     * Liefere die vertikale Position dieses Balls.
     */
    public int gibYPosition()
    {
        return yPosition;
    }
}
