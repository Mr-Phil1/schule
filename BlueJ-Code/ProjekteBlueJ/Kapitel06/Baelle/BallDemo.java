import java.awt.*;
import java.util.*;

/**
 * Die Klasse BallDemo bietet eine kurze Demonstration, wie die
 * Klasse Canvas benutzt werden kann.
 *
 * @author Michael K�lling und David J. Barnes
 * @version 2016.02.29
 */

public class BallDemo   
{
    private Canvas leinwand;

    /**
     * Erzeuge ein Exemplar von BallDemo.
     * Es wird ein frischer Canvas erzeugt und sichtbar gemacht.
     */
    public BallDemo()
    {
        leinwand = new Canvas("Ball Demo", 600, 500);
    }

    /**
     * Simuliere zwei springende B�lle.
     */
    public void springenLassen()
    {
        int boden = 400;   // Position der Bodenlinie

        leinwand.setVisible(true);

        // Den Boden zeichnen.
        leinwand.setForegroundColor(Color.BLACK);
        leinwand.drawLine(50, boden, 550, boden);

        // Die B�lle erzeugen und anzeigen.
        Ball ball = new Ball(50, 50, 16, Color.BLUE, boden, leinwand);
        ball.zeichnen();
        Ball ball2 = new Ball(70, 80, 20, Color.RED, boden, leinwand);
        ball2.zeichnen();

        // Die B�lle springen lassen.
        boolean fertig =  false;
        while(!fertig) {
            leinwand.wait(50);           // kurze Pause
            ball.bewegen();
            ball2.bewegen();
            // Stoppen, wenn die B�lle weit genug gesprungen sind.
            if(ball.gibXPosition() >= 550 || ball2.gibXPosition() >= 550) {
                fertig = true;
            }
        }
    }

    public void springenLassen2(int anzahl)
    {
        int boden = 400;
        leinwand.setVisible(true);
        leinwand.setForegroundColor(Color.BLACK);
        leinwand.drawLine(50, boden, 500, boden);
        HashSet<Ball> baelle = new HashSet<Ball>();
        for(int i = 0; i < anzahl; i++){
            Ball ball = new Ball(50+32*i,50,20, Color.RED, boden, leinwand);
            baelle.add(ball);
            ball.zeichnen();
        }

        boolean fertig = false;
        while(!fertig){
            leinwand.wait(50);
            for(Ball ball : baelle){
                ball.bewegen();
                if(ball.gibXPosition() >= 500 +32*anzahl){
                    fertig = true;
                }
            }
        }
        for(Ball ball : baelle){
            ball.loeschen();
        }
    }

    public void springenLassen3(int anzahl)
    {
        int boden = 400;
        leinwand.setVisible(true);
        leinwand.setForegroundColor(Color.BLACK);
        leinwand.drawLine(50, boden, 500, boden);
        Random random = new Random();
        Dimension size = leinwand.getSize();

        HashSet<Ball> baelle = new HashSet<Ball>();
        for(int i = 0; i < anzahl; i++){
            int x = random.nextInt(size.width);
            int y = random.nextInt(size.height);
            Ball ball = new Ball(x,y,20, Color.RED, boden, leinwand);
            baelle.add(ball);
            ball.zeichnen();
        }

        boolean fertig = false;
        while(!fertig){
            leinwand.wait(50);
            for(Ball ball : baelle){
                ball.bewegen();
                if(ball.gibXPosition() >= 500 +32*anzahl){
                    fertig = true;
                }
            }
        }
        for(Ball ball : baelle){
            ball.loeschen();
        }
    }

    public void kastenSpringen(int anzahl){

        Random random = new Random();
        leinwand.fillRectangle(200, 200, 200, 150);
        HashSet<KastenBall> baelle = new HashSet<KastenBall>();
        for(int i = 0; i < anzahl; i++){
            float r = random.nextFloat();
            float g = random.nextFloat();
            float b = random.nextFloat();
            Color randomColor = new Color(r,g,b);
            KastenBall kBall= new KastenBall(200+20*i,200,20, randomColor, 370, leinwand);
            baelle.add(kBall);
            kBall.zeichnen();
        }
        boolean fertig = false;
        while(!fertig){
            leinwand.wait(50);
            for(KastenBall kBall : baelle){
                kBall.bewegen();
                if(kBall.gibXPosition() >= 500 +32*anzahl){
                    fertig = true;
                }
            }
        }
        for(KastenBall kBall : baelle){
            kBall.loeschen();
        }
    }
}
