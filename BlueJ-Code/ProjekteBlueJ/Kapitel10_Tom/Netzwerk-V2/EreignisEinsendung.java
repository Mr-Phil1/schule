
/**
 * Beschreiben Sie hier die Klasse EreignisEinsendung.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class EreignisEinsendung extends Einsendung
{
    //10.8
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String ereignisTyp;

    /**
     * Konstruktor für Objekte der Klasse EreignisEinsendung
     */
    public EreignisEinsendung(String autor, String typ)
    {
      super(autor);
      this.ereignisTyp = typ;
    }
    public String gibEreignisTyp()
    {
        return ereignisTyp;
    }
}
