
/**
 * Beschreiben Sie hier die Klasse Baum.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Baum
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private Dreieck blaetter;
    private Quadrat stamm;

    /**
     * Konstruktor f�r Objekte der Klasse Baum
     */
    public Baum()
    {
        blaetter = new Dreieck();
        stamm = new Quadrat();
        stamm.horizontalBewegen(-5);
        stamm.vertikalBewegen(0);
        stamm.groesseAendern(20);
        stamm.farbeAendern("schwarz");

        blaetter.groesseAendern(100, 100);
        blaetter.horizontalBewegen(105);
        blaetter.vertikalBewegen(-120);
        blaetter.farbeAendern("gruen");
    }

    /**
     * Mache diesen Kreis sichtbar. Wenn er bereits sichtbar ist, tue nichts.
     */
    public void einrichten() {
        stamm.sichtbarMachen();
        blaetter.sichtbarMachen();
    }
}
