
/**
 * Beschreiben Sie hier die Klasse Drucker.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Drucker
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int status;
    /**
     * Konstruktor für Objekte der Klasse Drucker
     */
    public Drucker()
    {
        // Instanzvariable initialisieren
        status = 0;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public int gibStatus(int wartezeit)
    {
        // tragen Sie hier den Code ein
        status = wartezeit;
        return status;
    }
     public void drucke(String dateiname, boolean doppelseitig)
    {
        System.out.println("Datei: "+dateiname+", wird doppelseitig gedruckt: "+doppelseitig);
    }
}
