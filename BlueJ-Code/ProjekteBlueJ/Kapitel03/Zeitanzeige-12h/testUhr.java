
/**
 * Beschreiben Sie hier die Klasse testUhr.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class testUhr
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int x;

    /**
     * Konstruktor f�r Objekte der Klasse testUhr
     */
    public testUhr()
    {
    }

    public void test()
    {
        Uhrenanzeige uhr = new Uhrenanzeige();
        uhr.setzeUhrzeit(22, 30);
        System.out.println(uhr.gibUhrzeit());
        System.out.println(uhr.gibUhrzeit12h());
        uhr.setzeUhrzeit(10, 30);
        System.out.println(uhr.gibUhrzeit());
        uhr.setzeUhrzeit(10, 30);
        System.out.println(uhr.gibUhrzeit12h());
        uhr.setzeUhrzeit(0, 0);
        System.out.println(uhr.gibUhrzeit());
        uhr.setzeUhrzeit(0, 0);
        System.out.println(uhr.gibUhrzeit12h());
        uhr.setzeUhrzeit(12, 56);
        System.out.println(uhr.gibUhrzeit());
        uhr.setzeUhrzeit(12, 56);
        System.out.println(uhr.gibUhrzeit12h());
        System.out.println();
    }
}
