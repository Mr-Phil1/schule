/**
 * Eine Klasse, die das Protokoll eines Webservers
 * in Hinsicht auf Zugriffe pro Stunde auswertet.
 * 
 * @author David J. Barnes und Michael K�lling
 * @version 2016.02.29
 */
public class ProtokollAuswerter
{
    // Hier werden die Zugriffe f�r die Stunden berechnet.
    private int[] zugriffeInStunde;
    //private Person[] leute;
    private Boolean[] verfuegbar;
    // Verwendung eines LogdateiLesers.
    private LogdateiLeser leser;

    /**
     * Erzeuge ein Objekt, das die Zugriffe in den
     * Stunden eines Tages z�hlt.
     */
    public ProtokollAuswerter()
    { 
        // Das Array-Objekt erzeugen, das die Zugriffe
        // in den einzelnen Stunden eines Tages z�hlt.
        zugriffeInStunde = new int[24];
        // Den Leser f�r die Logdatei erzeugen.
        leser = new LogdateiLeser();
    }
    public ProtokollAuswerter(String name)
    { 
        // Das Array-Objekt erzeugen, das die Zugriffe
        // in den einzelnen Stunden eines Tages z�hlt.
        zugriffeInStunde = new int[24];
        // Den Leser f�r die Logdatei erzeugen.
        leser = new LogdateiLeser(name);
    }
    /**
     * Analysiere die in der Logdatei erfassten Zugriffsdaten.
     */
    public void analysiereStundendaten()
    {
        while(leser.hasNext()) {
            Logeintrag eintrag = leser.next();
            int stunde = eintrag.gibStunde();
            zugriffeInStunde[stunde]++;
        }
    }

    /**
     * Gib die Anzahl der Zugriffe in den Stunden eines
     * Tages nach Stunden sortiert auf der Konsole aus.
     * Diese Werte sollten zuerst mit einem Aufruf von 
     * 'analysiereStundendaten' berechnet werden.
     */
    public void stundendatenAusgeben()
    {
        System.out.println("Stunde: Zugriffe");
        for(int stunde = 0; stunde < zugriffeInStunde.length; stunde++) {
            System.out.println(stunde + ": " + zugriffeInStunde[stunde]);
        }
    }

    public void stundendatenAusgeben2()
    {
        int stunde=0;
        System.out.println("Stunde: Zugriffe");
        while (stunde < zugriffeInStunde.length){
            System.out.println(stunde + ": " + zugriffeInStunde[stunde]);
            stunde++;
        }

    }

    public int anzahlZugriffe()
    {
        int gesamt=0;
        for(int stunde = 0; stunde < zugriffeInStunde.length; stunde++) {
            gesamt= gesamt + zugriffeInStunde[stunde];
        }
        return gesamt;
    }
    /**
     * @return Liefert die Stunde mit den meisten Zugriffen
     */
    public int aktivsteStunde()
    {
        int aktivsteStd=0;
        for(int stunde=0; stunde < zugriffeInStunde.length;stunde++)
        {
            if(zugriffeInStunde[stunde] > zugriffeInStunde[aktivsteStd])
            {
                aktivsteStd = stunde;
            }
        } 
        return aktivsteStd;
    }

    /**
     * @return Liefert die Stunde mit den wenigsten Zugriffen
     */
    public int ruhigsteStunde()
    {
        int ruhigsteStd=0;
        for(int stunde=0; stunde < zugriffeInStunde.length;stunde++)
        {
            if(zugriffeInStunde[stunde] < zugriffeInStunde[ruhigsteStd])
            {
                ruhigsteStd = stunde;
            }
        } 
        return ruhigsteStd;
    }

    /**
     * @return Liefert die zweist�ndigste Stunde mit den meisten Zugriffen
     */
    public int aktivsteZweiStuendige()
    {
        int aktivsteStd=0;
        int aktivsteStdZaehler=0;
        for(int stunde=0; stunde < zugriffeInStunde.length-1;stunde++)
        {
            int anzahlZaehler = zugriffeInStunde[stunde] + zugriffeInStunde[stunde+1];
            if(anzahlZaehler > aktivsteStdZaehler)
            {
                aktivsteStd = stunde;
                aktivsteStdZaehler = anzahlZaehler;
            }
        } 
        return aktivsteStd;
    }

    /**
     * Gib die Zeilen der Logdatei auf der Konsole aus.
     */
    public void logdateiAusgeben()
    {
        leser.datenAusgeben();
    }
}
