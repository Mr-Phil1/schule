import java.util.Arrays;

/**
 * Modelliert einen eindimensionalen elementaren zellulären Automaten.
 * 
 * @author David J. Barnes und Michael Kölling
 * @version  2016.02.29 - Version 1
 */
public class Automat
{
    // Die Anzahl der Zellen.
    private final int anzahlZellen;
    // Der Zustand der Zellen.
    private int[] zustand;

    /**
     * Erzeuge einen eindimensionalen Automaten, der aus der angegebenen
     * Anzahl an Zellen besteht.
     * @param anzahlZellen  die Anzahl der Zellen im Automaten
     */
    public Automat(int anzahlZellen)
    {
        this.anzahlZellen = anzahlZellen;
        zustand = new int[anzahlZellen];
        // den Automaten einrichten mit einer einzelnen 'an'-Zelle in der Mitte
        zustand[anzahlZellen/2] = 1;
        zustand[anzahlZellen/4] = 1;
        zustand[anzahlZellen/6] = 1;
        zustand[anzahlZellen/8] = 1;
    }

    /**
     * Gib den akutellen Zustand des Automaten aus.
     */
    public void ausgeben()
    {
        for(int zellenwert : zustand) {
            if(zellenwert == 1) {
                System.out.print("*");
            }
            else {
                System.out.print(" ");
            }
        }
        System.out.println();
    }   

    /**
     * Aktualisiere den Automaten auf seinen nächsten Zustand.
     */
    public void aktualisieren()
    {
        // Baut den neuen Zustand in einem anderen Array auf.
        int[] naechsterZustand = new int[zustand.length];
        // Aktualisiert den Zustand jeder Zelle, 
        // ausgehend von den Zuständen seiner beiden Nachbarn.
        for(int i = 0; i < zustand.length; i++) {
            int links, zentrum, rechts;
            if(i == 0) {
                links = 0;
            }
            else {
                links = zustand[i - 1];
            }

            zentrum = zustand[i];
            if(i + 1 < zustand.length) {
                rechts = zustand[i + 1];
            }
            else {
                rechts = 0;
            }
            naechsterZustand[i] = (links + zentrum + rechts) % 2;
        }
        zustand = naechsterZustand;
    }

    /**
     * Setze den Automaten zurück.
     */
    public void zuruecksetzen()
    {
        Arrays.fill(zustand, 0);
        // den Automaten einrichten mit einer einzelnen 'an'-Zelle.
        zustand[anzahlZellen / 2] = 1;

    }

    public void aktualisieren2()
    {
        int[] naechsterZustand = new int[zustand.length];
        for(int i = 0; i < zustand.length; i++)
        {
            int links, zentrum, rechts;
            links = i == zustand.length ? 0 : zustand[i - 1];

            zentrum = zustand[i];

            rechts = i + 1 < zustand.length ? zustand[i+1] : 0;
            naechsterZustand[i] = berechneNaechstenZustand(links, zentrum, rechts);
        }
        zustand = naechsterZustand;
    }

    public void aktualisieren3()
    {
        int[] naechsterZustand = new int[zustand.length];
        int links = 0;
        int zentrum = zustand[0];
        for(int i = 0; i < zustand.length; i++)
        {
            int rechts = i + 1 < zustand.length ? zustand[i+1] : 0;
            naechsterZustand[i] = berechneNaechstenZustand(links, zentrum, rechts);
            links = zentrum;
            zentrum = rechts;
        }
    }
   
    public void aktualisieren4()
    {
        int[] naechsterZustand = new int[zustand.length];
        int links = 0;
        int zentrum = zustand[0];
        for(int i = 0; i < anzahlZellen; i++)
        {
            int rechts = zustand[i+1];
            naechsterZustand[i] = berechneNaechstenZustand(links, zentrum, rechts);
            links = zentrum;
            zentrum = rechts;
        }
    }

    /**
     * Setze den Automaten zurück.
     */
    public void zuruecksetzen1()
    {
        Arrays.fill(zustand, 0);
        // den Automaten einrichten mit einer einzelnen 'an'-Zelle.
        zustand[anzahlZellen / 2] = 1;

    }


    public int berechneNaechstenZustand(int links,int zentrum, int rechts)
    {   
        return (links + zentrum + rechts) % 2;
    }

    public int berechneNaechstenZustand2(int links,int zentrum, int rechts)
    {   
        return (links + rechts) % 2;
    }

    public int berechneNaechstenZustand3(int links,int zentrum, int rechts)
    {   
        return (zentrum + rechts + zentrum * rechts + links *zentrum * rechts) % 2;
    }
}
