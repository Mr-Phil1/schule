import java.util.ArrayList;

/**
 * Ein einfaches Modell einer Auktion.
 * Ein Exemplar dieser Klasse h�lt eine Liste von Posten,
 * die beliebig lang werden kann.
 * 
 * @author David J. Barnes und Michael K�lling
 * @version 2016.02.29
 */
public class Auktion
{
    // eine Liste der Posten dieser Auktion
    private ArrayList<Posten> postenliste;
    // die Nummer, die an den n�chsten Posten vergeben wird,
    // der f�r diese Auktion angemeldet wird
    private int naechstePostennummer;

    /**
     * Erzeuge eine neue Auktion.
     */
    public Auktion()
    {
        postenliste = new ArrayList<>();
        naechstePostennummer = 1;
    }

    /**
     * Melde einen Posten f�r diese Auktion an.
     * @param beschreibung  die Beschreibung des Postens
     */
    public void postenAnmelden(String beschreibung)
    {
        postenliste.add(new Posten(naechstePostennummer, beschreibung));
        naechstePostennummer++;
    }

    /**
     * Zeige die komplette Liste der Posten dieser Auktion.
     */
    public void zeigePostenliste()
    {
        for (Posten posten : postenliste) {
            System.out.println(posten.toString());
        }
    }

    /**
     * Gebe f�r einen Posten ein Angebot ab.
     * Es erfolgt eine Ausgabe, ob das Gebot erfolgreich war oder nicht.
     * @param postennummer  der Posten, f�r den geboten wird
     * @param bieter  die Person, die bietet
     * @param betrag  die H�he des Gebots
     */
    public void gibGebotAb(int postennummer, Person bieter, long betrag)
    {
        Posten gewaehlterPosten = gibPosten(postennummer);
        if(gewaehlterPosten != null) {

            boolean erfolgreich = gewaehlterPosten.hoeheresGebot(new Gebot(bieter, betrag));
            if(erfolgreich) {
                System.out.println("Das Gebot f�r Posten Nummer " +
                    postennummer + " war erfolgreich.");
            }
            else {
                // Melden, welches Gebot h�her ist.
                Gebot hoechstesGebot = gewaehlterPosten.gibHoechstesGebot();
                System.out.println("Posten Nummer " + postennummer +
                    ": Gebot steht bereits bei " +
                    hoechstesGebot.gibHoehe());
            }
        }
    }

    /**
     * Liefere den Posten mit der angegebenen Nummer.
     * Liefere 'null', wenn ein Posten mit dieser Nummer
     * nicht existiert.
     * @param nummer  die Nummer des Postens, der geliefert
     *                werden soll
     */
    public Posten gibPosten(int nummer)
    {
        if((nummer >= 1) && (nummer < naechstePostennummer)) {
            // die Nummer scheint g�ltig zu sein.
            Posten gewaehlterPosten = postenliste.get(nummer-1);
            // ein Sicherheitscheck, ob wir auch den richtigen
            // Posten haben:
            if(gewaehlterPosten.gibNummer() != nummer) {
                System.out.println("Interner Fehler: Posten Nummer " +
                    gewaehlterPosten.gibNummer() +
                    " wurde geliefert anstelle von " +
                    nummer);
            }
            return gewaehlterPosten;
        }
        else {
            System.out.println("Einen Posten mit der Nummer: " + nummer +
                " gibt es nicht.");
            return null;
        }
    }

    public void beenden()
    {
        for (Posten posten : postenliste)
        {
            System.out.println(posten.gibNummer()+ ": " + posten.gibBeschreibung());
            Gebot hoechstesGebot = posten.gibHoechstesGebot();
            if(hoechstesGebot != null)
            {
                System.out.println("    Bieter: " + hoechstesGebot.gibBieter().gibName());
                System.out.println("    Gebot: " + hoechstesGebot.gibHoehe());
            }
            else{
                System.out.println("    Nicht verkauft");
            }
        }

    }

    public ArrayList<Posten> gibUnverkaufte()
    {
        ArrayList<Posten> nichtVerkauft = new ArrayList<>();
        for(Posten posten : postenliste)
        {
            Gebot hoechstesGebot = posten.gibHoechstesGebot();
            if(hoechstesGebot == null) 
            {
                nichtVerkauft.add(posten); 
            }
        }
        return nichtVerkauft;
    }

    public Posten entfernePosten(int nummer)
    {
        Posten posten = gibPosten(nummer);
        if(posten != null)
        {
            postenliste.remove(posten);
        }
        System.out.println("Der Posten: "+gibPosten(nummer) + " wurde gel�scht!");
        return posten;
    }

    public Posten gibPostenNeu(int nummer)
    {
        int naechsterIndex = 0;
        while(naechsterIndex < postenliste.size() && postenliste.get(naechsterIndex).gibNummer() != nummer) {
            naechsterIndex++;
        }
        if (naechsterIndex == postenliste.size())
        {
            System.out.println("Postennummer: " + nummer + " existiert nicht.");
            return null;
        }
        else
        {
            return postenliste.get(naechsterIndex);
        }
    }
}
