import java.util.ArrayList;

/**
 * Verwalte den Bestand eines Unternehmens.
 * Der Bestand ist beschrieben durch einen oder
 * mehrere Artikel.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Bestandsverwalter
{
    // Das Lager mit den Artikeln
    private ArrayList<Artikel> lager;

    /**
     * Initialisiere den Bestandsverwalter.
     */
    public Bestandsverwalter()
    {
        lager = new ArrayList<Artikel>();
    }

    public void test1(Artikel artikel){
        if(!lager.contains(findeArtikel(artikel.gibName()))) { // Methode findeArtikel ist zu verwenden!
            System.out.println("nicht da");
        }else{
             System.out.println("da");
             lager.equals(_o_)
        }
    }

    /**
     * F�hre einen neuen Artikel im Lager ein.
     * @param artikel  der Artikel, der neue eingef�hrt werden soll
     */
    public void neuerArtikel(Artikel artikel)
    {
        if(!lager.contains(findeArtikel(artikel.gibName()))) { // Methode findeArtikel ist zu verwenden!
            lager.add(artikel);
        }
    }

    /**
     * Nimm eine Lieferung eines Artikels in das Lager auf.
     * Erh�he den Lagerbestand um die angegebene Menge.
     * @param nummer  die Artikelnummer des Artikels
     * @param menge   die angelieferte Menge
     */
    public void aufnehmen(int nummer, int menge)
    {  
        Artikel artikel = findeArtikel(nummer);
        if(artikel != null) {
            artikel.erhoeheBestand(menge);
        } 
    }

    /**
     * Versuche einen Artikel mit der angegebenen Nummer im
     * Bestand zu finden.
     * @param nummer  die Nummer des zu findenden Artikels.
     * @return        den gefundenen Artikel oder null, falls kein
     *                passender Artikel gefunden wird.
     */
    public Artikel findeArtikel(int nummer)
    {
        boolean gefunden = false;
        int index = 0;
        while(index<lager.size() && !gefunden) {
            if(lager.get(index).gibNummer()==nummer) {                
                gefunden=true;
            }
            else {
                index++;
            }
        }
        if(gefunden) {
            return lager.get(index);
        }
        else {
            return null;
        }
    }

    
    /**
     * Finde einen Artikel mit der angegebenen Nummer und
     * liefere seine aktuelle Menge im Bestand.
     * Wenn die Nummer auf keinen Artikel passt, wird
     * Null zur�ckgeliefert.
     * @param nummer  die Nummer des Artikels
     * @return        die Menge des Artikels im Bestand
     */
    public int mengeImBestand(int id)
    {
        Artikel artikel = findeArtikel(id);
        if(artikel!=null) {
            return artikel.gibBestand();
        } 
        else {
            return 0;
        }
    }

    /**
     * Informationen �ber alle Artikel ausgeben.
     */
    public void alleArtikelAnzeigen()
    {
        for( Artikel produkt : lager)
        {
            System.out.println(produkt.toString());
        }
    }

    public void niedrigerProduktstand(int grenze)
    {
        for(Artikel artikel : lager) {
            if(artikel.gibBestand()<grenze) {
                System.out.println(artikel.toString());
            }
        }
    }

    public Artikel findeArtikel(String name)
    {
        boolean found = false;
        int index = 0;
        while(index < lager.size() && !found) {
            if(lager.get(index).gibName().equals(name)) {
                found=true;
            }
            else {     
                index++;
            }
        }
        if(found) {
            return lager.get(index);
        }
        else {
            return null;
        }

    }
}
