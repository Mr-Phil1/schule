/**
 * Speichert Details �ber Vereinsmitgliedschaften.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
import java.util.ArrayList;
import java.util.Iterator;
public class Verein
{
    ArrayList<Mitgliedschaft> mitglieder;

    /**
     * Konstruktor f�r Objekte der Klasse Verein
     */
    public Verein()
    {
        mitglieder = new ArrayList<Mitgliedschaft>();

    }

    /**
     * F�ge ein neues Mitglied in die Mitgliederliste ein.
     * @param mitglied  Infos �ber das einzuf�gende Mitglied
     */
    public void beitreten(Mitgliedschaft mitglied)
    {
        mitglieder.add(mitglied);
    }

    /**
     * @return  die Anzahl der Mitglieder (Mitgliedschaft-Objekte)
     *          in diesem Verein
     */
    public int anzahlMitglieder()
    {
        return mitglieder.size();
    }

    /**
     * Liefere die Anzahl der Mitglieder, die im angegebenen Monat
     * Mitglied geworden sind.
     * @param monat der Monat des Beitritts.
     * @return die Anzahl der Mitglieder, die im Monat beigetreten sind.
     */
    public int beigetretenInMonat(int monat)
    {
        int count = 0;
        if(monat <1 || monat > 12)
        {
            System.out.println("Monatsangabe " + monat +" im ung�ltigen Bereich. Angabe muss zwischen 1 ... 12 sein!");
        }
        else{
            for(Mitgliedschaft mitglied : mitglieder)
            {
                if(mitglied.gibMonat() == monat)
                {
                    count++;
                }
            }
        }
        return count;
    }
   public ArrayList<Mitgliedschaft> entfernen(int monat, int jahr)
   {
        ArrayList<Mitgliedschaft> entfernen = new ArrayList<>();
        if(monat < 1 || monat > 12) {
            System.out.println("Monatsangabe " + monat +
                               " out of range. " +
                               "It must be in the range 1 ... 12");
        }
        else {
            Iterator<Mitgliedschaft> it = mitglieder.iterator();
            while(it.hasNext()) {
                Mitgliedschaft mitglied = it.next();
                if(mitglied.gibMonat() == monat &&
                   mitglied.gibJahr() == jahr) {
                    it.remove();
                    entfernen.add(mitglied);              
                }
            }
        }
        return entfernen;
    }

}
