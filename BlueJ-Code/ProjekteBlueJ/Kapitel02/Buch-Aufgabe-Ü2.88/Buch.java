/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private String refnummer;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel)
    {
        autor = buchautor;
        titel = buchtitel;
        refnummer ="" ;
    }

    public void setzteRefNummer(String ref){
        refnummer=ref;
    }
    
    public String gibRefNummer(){
        return refnummer;
    }
}
