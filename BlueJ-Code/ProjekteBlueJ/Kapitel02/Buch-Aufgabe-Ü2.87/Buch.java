/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private int seiten;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel, int buchseiten)
    {
        autor = buchautor;
        titel = buchtitel;
        seiten = buchseiten;
    }

    public void detailAusgeben(){
        System.out.println("Buch-Details: \n\tTitel: \t"+titel+"\n\tAutor: \t"+autor+"\n\tSeiten: "+seiten);
    }
}
