
/**
 * Beschreiben Sie hier die Klasse Heizung.
 * 
 * @author Mr-Phil
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Heizung
{
  
    private double temperatur;

    /**
     * Konstruktor für Objekte der Klasse Heizung
     */
    public Heizung()
    {
        temperatur = 15.0;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public void waermer()
    {
        // tragen Sie hier den Code ein
        temperatur = temperatur + 5.0;
    }
    public void kuehler()
    {
        // tragen Sie hier den Code ein
        temperatur = temperatur - 5.0;
    }
    public double temperaturAusgabe()
    {
        return temperatur;
    }
}
