/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private int seiten,ausgeliehen;
    private String refnummer;
    private boolean kursText;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel, int buchseiten, boolean fachbuch)
    {
        autor = buchautor;
        titel = buchtitel;
        seiten = buchseiten;
        refnummer ="" ;
        kursText = fachbuch;
    }
    public void setzteRefNummer(String ref){
        if (ref.length()>=3)
        {
            refnummer=ref;
        }
        else
        {
            System.out.println("Kollege du hast Fehler gemacht, die Nummer musst mindesten 3 Zeichen haben!");
        }  

    }
    public void ausleihen(){
        ausgeliehen++;
    }
    public int gibAusgeliehen(){
        return ausgeliehen;
    }
    public String gibRefNummer(){
        return refnummer;
    }
    public boolean istKursText()
    {
        return kursText;
    }
    public void detailAusgeben(){
        System.out.println("Buch-Details: ");
        System.out.println("\tTitel: \t"+titel);
        System.out.println("\tAutor: \t"+autor);
        System.out.println("\tSeiten: "+seiten);
        System.out.println("\tDas Buch wurde bisher "+ausgeliehen+"x ausgehliehen");
        System.out.println("\tDieses Buch wird als Fachbuch verwendet: "+kursText);
        if (refnummer.length()>0)
        {
            System.out.println("\tRefNummer: "+refnummer);
        }
        else
        {
            System.out.println("\tZZZ");
        }        
        System.out.println();
    }

}
