/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel)
    {
        autor = buchautor;
        titel = buchtitel;
    }

    /**
     * Antwort auf die Frage:
     * Die Buch-Objekte sind bis jetzt unver�nderlich. 
     * Da man zu diesem Zeitpunkt noch keine Methode 
     * f�r ver�nderung geschriben bzw. erstellt hat.
     * 
     */
}
