
/**
 * Beschreiben Sie hier die Klasse Heizung.
 * 
 * @author Mr-Phil
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Heizung
{

    private double temperatur, minimum, maximum;
    private int schrittweite;
    /**
     * Konstruktor für Objekte der Klasse Heizung
     */
    public Heizung(double min, double max)
    {
        temperatur = 15.0;
        minimum = min;
        maximum= max;
        schrittweite = 5;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public void waermer()
    {
        if ((temperatur+schrittweite)<=maximum)
        {
            temperatur = temperatur + schrittweite;
        }
        else
        {
            System.out.println("Maximal Temperatur darf nicht überschritten werden.");
        }
    }

    public void kuehler()
    {
        if ((temperatur-schrittweite)>=minimum)
        {
            temperatur = temperatur - schrittweite;
        }
        else
        {
            System.out.println("Minimal Temperatur darf nicht unterschritten werden.");
        }
    }

    public double temperaturAusgabe()
    {
        return temperatur;
    }

    public void setzteSchrittweit(int input){
       if (input>0)
       {
           schrittweite = input;
       }
       else
       {
           System.out.println("Sie müssen einen positiven Wert eingeben");
       }
    }
}
