/**
 * Eine Klasse, deren Objekte Informationen �ber ein Buch halten.
 * Dies k�nnte Teil einer gr��eren Anwendung sein, einer
 * Bibliothekssoftware beispielsweise.
 *
 * @author (Ihren Namen hier eintragen.)
 * @version (das heutige Datum eintragen.)
 */
class Buch
{
    // Die Datenfelder
    private String autor;
    private String titel;
    private int seiten;
    private String refnummer;

    /**
     * Setze den Autor und den Titel, wenn ein Objekt erzeugt wird.
     */
    public Buch(String buchautor, String buchtitel, int buchseiten)
    {
        autor = buchautor;
        titel = buchtitel;
        seiten = buchseiten;
        refnummer ="" ;
    }

    public void setzteRefNummer(String ref){
        refnummer=ref;
    }

    public String gibRefNummer(){
        return refnummer;
    }

    public void detailAusgeben(){
        System.out.println("Buch-Details: ");
        System.out.println("\tTitel: \t"+titel);
        System.out.println("\tAutor: \t"+autor);
        System.out.println("\tSeiten: "+seiten);
        if (refnummer.length()>0)
        {
            System.out.println("\tRefNummer: "+refnummer);
        }
        else
        {
            System.out.println("\tZZZ");
        }        
    }

}
