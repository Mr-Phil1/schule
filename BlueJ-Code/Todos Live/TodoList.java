import java.util.ArrayList;
import java.util.Iterator;
import java.util.GregorianCalendar;

public class TodoList {

    /**********************************************************
     * Datenfelder
     **********************************************************/


    /**********************************************************
     * Konstruktor
     **********************************************************/
    

    /**********************************************************
     * Methoden
     **********************************************************/
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Wrapped ArrayList-Methods: size, add, get, remove
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**
     * returns the size of the ToDo-List
     * @return size of the list
     */
    
    
    /**
     * add an element to the ToDo-List
     * @param: element to be added
     */ 

    
    /**
     * returns true if the given index is a valid index for the ToDo-List
     */


    /**
     * returns the element in the list by the given index
     * @param index of the element to be returned
     * @return element with the given index
     */


    /**
     * removes the element in the list by the given index
     * @param index of the element to be returned
     * @return element with the given index
     */

    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * for-each
     * 
     * - certain Iteration (bestimmte Iteration)
     * - no index
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**
     * print out all Todos in the list
     */

    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Search for Elements with given Property (while, for)
     * 
     * - indefinite Iteration (unbestimmte Iteration)
     * - index needed
     * - good for searching
     * - not good for deleting
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    /**
     * Searches for the first element in the list with the 
     * given ID (attribute of a Todo-Object) and returns it.
     * @param id of the element to be returned
     * @return element with the given id-attribute
     */


    /**
     * Searches for the first element in the list with the 
     * given Title (attribute of a Todo-Object) and returns it.
     * @param title of the element to be returned
     * @return element with the given id-attribute
     */


    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Iterator
     * 
     * - indefinite Iteration (unbestimmte Iteration)
     * - no index
     * - best for deleting one or more elements by properties
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
     
    /**
     * removes all elements in the list with the given title
     * @param title of the list-element to be removed
     */
  
    
    /**
     * removes all elements in the list with a title that contains the given searchString
     * @param title of the list-element to be removed
     */

    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Search for Elements with given Property (for-each)
     * 
     * - definite Iteration (bestimmte Iteration)
     * - no index
     * - good for searching
     * - not good for deleting
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    
    /**
     * returns a list of Todos that contain the given searchstring in their title
     * @param searchString that should be contained by the title of the Todo
     * @return list of fitting Todos
     */ 


    /**
     * returns a list of Todos that should have been done by now (time overdrawn)
     * @return list of fitting Todos
     */ 

}
