import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Todo {
    
    /**********************************************************
     * Datenfelder
     **********************************************************/
    private long id;
    private String title;
    private String remarks;
    private GregorianCalendar deadline;

    /**********************************************************
     * Konstruktor
     **********************************************************/
    public Todo(long id, String title, String remarks, GregorianCalendar deadline) {
        this.id = id;
        this.title = title;
        this.remarks = remarks;
        this.deadline = deadline;
    }

    /**********************************************************
     * Methoden
     **********************************************************/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public GregorianCalendar getDeadline() {
        return deadline;
    }

    public void setDeadline(GregorianCalendar deadline) {
        if (deadline != null) 
        {
            this.deadline = deadline;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", remarks='" + remarks + '\'' +
                ", deadline=" + new SimpleDateFormat("dd. MMMM YYYY").format(deadline.getTime()) +
                '}';
        
    }
}
