import java.util.GregorianCalendar;
import java.util.ArrayList;

/**
 * Diese Klasse testet die beiden Klassen TodoList und Todo.
 * 
 * @author Dominik Moser 
 * @version 12.01.2020
 */
public class Main
{
    public static void main(String[] args)
    {
        TodoList list = new TodoList();
/*
        // Listenelemente erstellen
        Todo t1 = new Todo(1L,"POS1 HÜ", "BlueJ-Buch-Kapitel 4 fertig machen.", new GregorianCalendar(2021,1,12));
        Todo t2 = new Todo(2L,"TINIP Award", "Beschreibung", new GregorianCalendar(2021,5,4));
        Todo t3 = new Todo(3L,"Titel", "Das sind meine Bemerkungen dazu.", new GregorianCalendar(2020,10,1));
        Todo t4 = new Todo(4L,"Titel", "Das sind meine Bemerkungen dazu.", new GregorianCalendar(2020,6,19));

        // Elemente zur Liste hinzufügen
        list.add(t1);
        list.add(t2);
        list.add(t3);
        list.add(t4);

        // ToDo-Liste ausgeben
        System.out.println("\n# Print ToDo-List");
        list.printAllTodosOnCommandline();
        
        System.out.println("\n# Get ToDo on index \"3\"");
        System.out.println(list.get(3));

        System.out.println("\n# Get ToDos with ID \"3\"");
        System.out.println(list.getByID(3));
        
        System.out.println("\n# Get ToDos with Title \"TINIP Award\"");
        System.out.println(list.getByTitle("TINIP Award"));
        
        System.out.println("\n# Overdrawn Todos: " + list.getLateTodos().size());
        ArrayList<Todo> lateTodos = list.getLateTodos();
        System.out.println(lateTodos);
        
        System.out.println("\n# Get list of Todos with \"OS\" in the title: ");
        ArrayList<Todo> anotherList = list.searchInTitle("OS");
        System.out.println(anotherList);
        System.out.println("size = " + anotherList.size());
        
        System.out.println("\n# Remove all Todos with \"Ti\" in the title: ");
        list.removeAllWithStringInTitle("Ti");
        list.printAllTodosOnCommandline();
*/
    }
}
