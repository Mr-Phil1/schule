
import java.util.HashMap;
import java.util.Map;

public class Triebfahrzeugliste
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private HashMap<String, String> liste;

    /**
     * Konstruktor für Objekte der Klasse Triebfahrzeugliste
     */
    public Triebfahrzeugliste()
    {
        this.liste=new HashMap<>();
    }

    public void neuesTfzHinzufuegen(String modellnummer, String modelname){
        this.liste.put(modellnummer, modelname);
    }

    public void tfzModdellnamenAusgebenMitModellnummer(String modellnummer){
        System.out.println(liste.get(modellnummer));
    }

    public void alleTfzAusgeben(){
        for(Map.Entry<String,String> pair : liste.entrySet()){
            System.out.println(pair.getKey()+ " : "+pair.getValue());
        }
    }

    public void alleTfzAusgeben2(){
        this.liste.forEach((key, value)->{
                System.out.println(key+ " : "+value);
            });
    }

    public void alleTfzModdellnamenAusgeben(){
        for(Map.Entry s : liste.entrySet()){
            System.out.println(s.getKey());
        }
    }

    public boolean existiertDerSchluessel(String s){
        return this.liste.containsKey(s);
    }

    public boolean existiertDerSchluessel2(String s){
        for(String key : liste.keySet()){
            return true;
        }
        return  false;
    }

    public boolean existiertDerSchluessel3(String s){
        return liste.get(s)==null ?  false : true;
    }

    public boolean existiertDerSchluessel4(String s){
        if(liste.get(s)==null){
            return false;
        }else{
            return true;
        }
    }
}
