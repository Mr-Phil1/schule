﻿ cls
Get-ChildItem  -File $env:USERPROFILE -Recurse -ErrorAction SilentlyContinue | Where-Object { $_.Extension -match '\.(txt|ini|pdf)$' }| `
Sort-Object -Property Length -Descending | Where-Object -Property Length -GT 1kb | Select-Object -Property  Name , Length, CreationTime ,LastAccessTime | Out-GridView