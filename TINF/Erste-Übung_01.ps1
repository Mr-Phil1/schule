﻿cls
Get-ChildItem  -File $env:USERPROFILE -Recurse -ErrorAction SilentlyContinue -Filter "*.txt"  | `
Sort-Object -Property Length -Descending | Select-Object -Property  Name , Length, CreationTime ,LastAccessTime -First 5
