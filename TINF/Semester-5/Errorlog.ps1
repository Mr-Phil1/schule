﻿function get-errrorlog{
<#
        .SYNOPSIS
        Speichert sämtliche Fehlermeldungen

        .DESCRIPTION
        Holt sämtliche Fehlermeldungen, und speichert diese auf dem Desktop als CSV und als HTML Datei.

        .PARAMETER Anzahl
        Legt die Anzahl der gewünschten Fehlermeldungen fest. Default: 5

        .PARAMETER Typ
        Legt die Protokollart der Fehlermeldung fest. Muss angegeben werden.

        .PARAMETER Warnung
        Legt fest ob Warnungen angezeigt werden sollen, oder nicht.

        .EXAMPLE
        get-errrorlog -Typ System -Anzahl 1

        .EXAMPLE
        get-errrorlog -Typ System
        
        .EXAMPLE
        get-errrorlog -Typ Application -Warnung
    #>
param([int]$Anzahl=5, [parameter(Mandatory=$true)][string]$Typ, [switch]$Warnung)

    $dateTime=Get-Date -Format "yyyy-MM-dd_hhm"
    
    if ($Warnung){
    $errorLog= Get-EventLog -LogName $Typ -Newest $Anzahl -EntryType Warning
    }
    else {
    $errorLog= Get-EventLog -LogName $Typ -Newest $Anzahl -EntryType Error
    }
    
    $errorLog | Select-Object -Property  Index , Time, EntryType ,Source ,InstanceID, Message | `
    Export-Csv -Path $env:USERPROFILE\Desktop\Maar-Philipp-Errorlog_$dateTime.csv -Delimiter "," -NoTypeInformation 
    
   
    $errorLog | Select-Object -Property  Index , Time, EntryType ,Source ,InstanceID, Message | `
    ConvertTo-Html -Title "TestAusgabeHTML" -PreContent "<h1>Liste aller Fehlermeldungen</h1>" | `
    Out-File $env:USERPROFILE\Desktop\Maar-Philipp-Errorlog_$dateTime.html
}