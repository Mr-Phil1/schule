﻿<#
.Synopsis
Übersicht über die Laufwerksbelegung
.Descriptions
Gibt eine detaillierte Übersicht über den Laufwerksfüllstand in EXCEL aus
#>

$dateTime=Get-Date -Format "yyyy-MM-dd_hh-mm-ss"
$excel = New-Object -ComObject Excel.Application
$excel.visible = $false
$wb = $excel.Workbooks.add()
$ws = $wb.Worksheets.Item(1)

$ws.cells(1,1)="Laufwerk"
$ws.cells(1,1).font.bold=$true

$ws.cells(1,2)="Name"
$ws.cells(1,2).font.bold=$true

$ws.cells(1,3)="Größe (GB)"
$ws.cells(1,3).font.bold=$true

$ws.cells(1,4)="Belegt(GB)"
$ws.cells(1,4).font.bold=$true

$ws.cells(1,5)="Frei (GB)"
$ws.cells(1,5).font.bold=$true

$ws.cells(1,6)="Frei (%)"
$ws.cells(1,6).font.bold=$true

$ws.cells(1,7)="Dateisystem"
$ws.cells(1,7).font.bold=$true


$laufwerke= Get-CimInstance -ClassName Win32_LogicalDisk -Filter "DriveType = 3"
$counter=2

foreach ($lw in $laufwerke) {
$ws.cells($counter,1)=$lw.DeviceID

$ws.cells($counter,2)=$lw.VolumeName

$ws.cells($counter,3)=$lw.Size/1GB
$ws.cells($counter,3).numberformat="0,00"

$ws.cells($counter,4)=($lw.Size-$lw.FreeSpace)/1GB
$ws.cells($counter,4).numberformat="0,00"

$ws.cells($counter,5)=$lw.FreeSpace/1GB
$ws.cells($counter,5).numberformat="0,00"

$ws.cells($counter,6)=($lw.FreeSpace/$lw.Size)
$ws.cells($counter,6).numberformat="0,00%"

$ws.cells($counter,7)=$lw.FileSystem

$counter+=1
}

$wb.saveas("$env:USERPROFILE\Desktop\$dateTime-Laufwerksauslastung.xlsx")
$excel.quit()