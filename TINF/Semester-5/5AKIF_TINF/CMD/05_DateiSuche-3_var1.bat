@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
cls

if !%1! == !! (echo Sie haben keinen Parameter angegeben! && goto :EOF
) else (

dir /B /S "%USERPROFILE%"\*.%1 > "%USERPROFILE%"\Desktop\Ergbnis-%1.txt
echo. 
echo *****************************************************
echo Anzahl der gefundenen %1-Dateien 
find /C ".%1" < "%USERPROFILE%"\Desktop\Ergbnis-%1.txt
echo *****************************************************
echo. 
)