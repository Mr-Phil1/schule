@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
cls

if !%1! == !! goto :Fehler1
if !%2! == !! goto :Fehler1
if not exist %2 goto :Fehler2

dir /B /S "%2\*.%1" > "%USERPROFILE%"\Desktop\Ergbnis-%1.txt
echo. 
echo *****************************************************
echo Anzahl der gefundenen %1-Dateien 
find /C ".%1" < "%USERPROFILE%"\Desktop\Ergbnis-%1.txt
echo *****************************************************
echo. 
goto :EOF

:Fehler1
echo. 
echo *****************************************************
echo Das Skript %0 muss mit zwei Argumentn aufgerufen werden.
echo %0 [Dateityp] [Dateipfad]
echo Beende.....
goto :EOF

:Fehler2
echo. 
echo *****************************************************
echo Das Verzeichnis %2 existiert nicht
echo Beende.....