@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
setlocal

:Start
cls
set bn=
set /P bn="Benutzername: "
if not defined bn goto :Fehler
echo.

net user %bn% 1>NUL 2>1
if %errorlevel% EQU 0 echo "Der Benutzer %bn% existiert" && goto :Frage

net user %bn% /delete 1>NUL 2>1

if %errorlevel% EQU 0 ( 
echo "Der Benutzer %bn% wurde erfolgreich gel�scht." 
goto :Frage
) else (
echo "Der Benutzer %bn% konnte nicht gel�scht werden." 
goto :Frage
)


:Fehler
echo. 
REM echo ********************************************************
echo Das Skript %0 ben�tigt ein Benutzername und ein Kennwort
REM echo ********************************************************
goto :EOF

:Frage
REM SOll das Skript wiederholt werden?
echo.
choice /T 10 /D N /M "Wollen Sie das Skript wiederholen? "
if %errorlevel% EQU 1 ( 
goto :Start
)

endlocal