@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
setlocal

:Start
set bn=
set kw=
cls
set /P bn="Benutzername: "
if not defined bn goto :Fehler
set /P kw="Kennwort: "
if not defined kw goto :Fehler

net user %bn% 1>NUL 2>1
if %errorlevel% EQU 0 echo "Der Benutzer %bn% existiert bereits" && goto :Frage

net user %bn% %kn% /add 1>NUL 2>1

if %errorlevel% EQU 0 ( 
echo "Der Benutzer %bn% wurde erfolgreich angelegt " 
goto :Frage
) else (
echo "Der Benutzer %bn% konnte nicht angelegt werden" 
goto :Frage
)


:Fehler
echo. 
REM echo ********************************************************
echo Das Skript %0 ben�tigt ein Benutzername und ein Kennwort
REM echo ********************************************************
goto :EOF

:Frage
REM SOll das Skript wiederholt werden?
choice /T 5 /D N /M "Wollen Sie das Skript wiederholen? "
if %errorlevel% EQU 1 ( 
goto :Start
)

endlocal