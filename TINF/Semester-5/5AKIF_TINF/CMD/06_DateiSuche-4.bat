@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
cls
setlocal

set /P dateityp="Bitte einen Dateityp angegeben: "
if not defined dateityp goto :Fehler
set /P suchpfad="Bitte einen Suchpfad angegeben: "
if not defined suchpfad goto :Fehler
if not exist %suchpfad% goto :Fehler1

dir /B /S "%suchpfad%"\*.%dateityp% > "%USERPROFILE%"\Desktop\Ergbnis-%1.txt
echo. 
echo *****************************************************
echo Anzahl der gefundenen %dateityp%-Dateien (Var 2): 
find /C ".%dateityp%" < "%USERPROFILE%"\Desktop\Ergbnis-%dateityp%.txt
echo *****************************************************
echo. 
endlocal
goto :EOF

:Fehler
echo. 
echo *****************************************************
echo Sie m�ssen zwingend eine g�ltige Eingabe t�tigen!
echo *****************************************************
goto :EOF

:Fehler1
echo. 
echo *****************************************************
echo Das Verzeichnis %suchpfad% existiert nicht
echo *****************************************************
goto :EOF