@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
cls
dir /B /S "%USERPROFILE%"\*.txt > "%USERPROFILE%"\Desktop\Ergbnis-txt.txt
dir /B /S "%USERPROFILE%"\*.bat > "%USERPROFILE%"\Desktop\Ergbnis-bat.txt

REM type "%USERPROFILE%"\Desktop\Ergbnis-txt.txt
REM type "%USERPROFILE%"\Desktop\Ergbnis-bat.txt

echo. 
echo *****************************************************
echo Anzahl der gefundenen BAT-Dateien (Var 1): 
find /C "%USERPROFILE%"\Desktop\Ergbnis-bat.txt".bat"
echo *****************************************************
echo. 

echo. 
echo *****************************************************
echo Anzahl der gefundenen BAT-Dateien (Var 2): 
find /C ".bat" < "%USERPROFILE%"\Desktop\Ergbnis-bat.txt
echo *****************************************************
echo. 

echo. 
echo *****************************************************
echo Anzahl der gefundenen BAT-Dateien (Var 3): 
type "%USERPROFILE%"\Desktop\Ergbnis-bat.txt | find /C ".bat" 
echo *****************************************************
echo. 

echo. 
echo *****************************************************
echo Anzahl der gefundenen BAT-Dateien (Var 4): 
dir /B /S "%USERPROFILE%"\*.bat  | find /C ".bat" 
echo *****************************************************
echo. 