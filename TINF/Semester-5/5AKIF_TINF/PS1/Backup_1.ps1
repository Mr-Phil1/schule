﻿<#
.SYNOPSIS
Archiviert Dateien
.DESCRIPTION
Fasst alle Dateien vom übergebenen Dateityp in einem ZIP-Container zusammen
#>
"++++++++++++++++++++++++++++++++++"
$eingabe=Read-Host -Prompt "Bitte geben Sie einen Dateityp an"
$dateTime=Get-Date -Format "yyyy-MM-dd_hhm"
"++++++++++++++++++++++++++++++++++"
if($eingabe -ne ""){
"----------------------------------"
"Sie suchen nach $eingabe Dateien!"
"----------------------------------"
Get-ChildItem -Path $HOME -File -Recurse -Filter "*.$eingabe" |`
Compress-Archive -DestinationPath $env:USERPROFILE\Desktop\$dateTime-$eingabe-archive.zip -Verbose
"-----------------------------------------------------------------------------------------------"
"Die Archivierung von $eingabe in das Archive $dateTime-$eingabe-archive.zip ist beendet!"
"-----------------------------------------------------------------------------------------------"

} else{
"----------------------------------"
"Sie müssen einen Dateityp angeben!"
"----------------------------------"
}
