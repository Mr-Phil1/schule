﻿<#
.SYNOPSIS
Archiviert Dateien
.DESCRIPTION
Fasst alle Dateien vom übergebenen Dateityp in einem ZIP-Container zusammen
#>
"++++++++++++++++++++++++++++++++++"
[string[]]$eingabe =(Read-Host -Prompt "Bitte geben Sie die Dateitypen an (typ1,typ2,..)").Split(",") | %{$_.Trim()}
"++++++++++++++++++++++++++++++++++"

function New-Backup{
param([string[]]$Filetype, [switch]$Recursive)
    $dateTime=Get-Date -Format "yyyy-MM-dd_hhm"
    if($Filetype -ne ""){
        foreach($typ in $Filetype){
            "----------------------------------"
            "Sie suchen nach $typ Dateien!"
            "----------------------------------"
            Get-ChildItem -Path $HOME -File -Recurse:$Recursive -Filter "*.$typ" | Compress-Archive -DestinationPath $env:USERPROFILE\Desktop\$dateTime-$typ-archive.zip -Verbose
            "-----------------------------------------------------------------------------------------------"
            "Die Archivierung von $typ in das Archive $dateTime-$typ-archive.zip ist beendet!"
            "-----------------------------------------------------------------------------------------------"
        }
    }
    else {
        "----------------------------------"
        "Sie müssen einen Dateityp angeben!"
        "----------------------------------"
    }
}

New-Backup -Filetype $eingabe -Recursive
New-Backup -Filetype $eingabe 