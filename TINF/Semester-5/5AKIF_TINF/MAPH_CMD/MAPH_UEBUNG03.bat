@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
setlocal

dir /A /D /B /S "%USERPROFILE%"\*python*   1>NUL 2>1

if %errorlevel% EQU 1 (
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
echo "Es sind keine Verzeichnise mit "python" im Namen vorhanden!" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
)

if %errorlevel% EQU 0 ( 
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
echo "Folgende Verzeichnise mit "python" im Namen sind vorhanden:" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
echo.
dir /A /D /B /S "%USERPROFILE%"\*python* 1>NUL 2>1  >> "%USERPROFILE%"\Desktop\MAPH_BERICHT2.txt"
)