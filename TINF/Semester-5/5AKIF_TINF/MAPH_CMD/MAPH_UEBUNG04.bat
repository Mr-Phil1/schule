@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
setlocal

if !%1! == !! goto :Fehler1

dir /A /D /B /S "%USERPROFILE%"\*%1*   1>NUL 2>1

if %errorlevel% EQU 1 (
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo "Es sind keine Verzeichnise mit "%1" im Namen vorhanden!" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
)

if %errorlevel% EQU 0 ( 
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo "Folgende Verzeichnise mit "%1" im Namen sind vorhanden:" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo.
dir /A /D /B /S "%USERPROFILE%"\*%1* 1>NUL 2>1  >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
)
goto :EOF

:Fehler1
echo. 
echo *****************************************************
echo Das Skript %0 muss mit zwei Argumenten aufgerufen werden.
echo %0 [Ordnername]
echo Beende.....
goto :EOF

:Fehler2
echo. 
echo *****************************************************
echo Das Skript %0 darf keinen nummerischen Wert enthalten
echo %0 [Ordnername]
echo Beende.....
goto :EOF
