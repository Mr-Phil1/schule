@echo off
REM �ndert die Codepage 850 in die Codepage 1252
chcp 1252 > nul
setlocal
if !%1! == !! goto :Fehler1
REM Quelle nummerische Parameter �berpr�fung
REM https://qastack.com.de/superuser/404338/check-for-only-numerical-input-in-batch-file
set param=%1

echo %param%| findstr /r "^[1-9][0-9]*$" >nul

if %errorlevel% equ 0 goto :Fehler2

dir /A /D /B /S "%USERPROFILE%"\*%param%*   1>NUL 2>1

if %errorlevel% EQU 1 (
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3_1.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3_1.txt"
echo "Es sind keine Verzeichnise mit "%param%" im Namen vorhanden!" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3_1.txt"
)

if %errorlevel% EQU 0 ( 
echo Bericht vom %date% um %time% >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo --------------------------------------- >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3.txt"
echo "Folgende Verzeichnise mit "%param%" im Namen sind vorhanden:" >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3_1.txt"
echo.
dir /A /D /B /S "%USERPROFILE%"\*%param%* 1>NUL 2>1  >> "%USERPROFILE%"\Desktop\MAPH_BERICHT3_1.txt"
)
goto :EOF

:Fehler1
echo. 
echo *****************************************************
echo Das Skript %0 muss mit einem Argumenten aufgerufen werden.
echo %0 [Ordnername]
echo Beende.....
goto :EOF

:Fehler2
echo. 
echo *****************************************************
echo Das Skript %0 darf keinen nummerischen Wert enthalten
echo %0 [Ordnername]
echo Beende.....
goto :EOF

endlocal