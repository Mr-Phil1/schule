﻿Get-ChildItem  -File $env:USERPROFILE -Recurse -ErrorAction SilentlyContinue | Where-Object  CreationTime -GT "10/01/2021" | `
Sort-Object -Property LastAccessTime -Descending | Select-Object -Property Name , Length, CreationTime ,LastAccessTime
echo "#######################################################################################################"
# Get-ChildItem  -File $env:USERPROFILE -Recurse -ErrorAction SilentlyContinue | Where-Object  -FilterScript {$_.CreationTime -gt "10/01/2021"} | `
Sort-Object -Property LastAccessTime -Descending | Select-Object -Property Name , Length, CreationTime ,LastAccessTime

Get-ChildItem -File -Recurse | Where-Object {$_.Length -eq 0} | ForEach-Object {Remove-Item $_.FullName -WhatIf -ErrorAction SilentlyContinue } 