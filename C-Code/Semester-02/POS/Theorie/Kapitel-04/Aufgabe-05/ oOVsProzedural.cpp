#include <iostream>
using namespace std;

// Koordinaten

struct koordinate
{
    int x;
    int y;
} koordinate1;

void set(koordinate *k, int x, int y)
{
    k->x = x;
    k->y = y;
}

void toString(struct koordinate k)
{
    cout << "\tX-Koordinate: " << k.x << endl;
    cout << "\tY-Koordinate: " << k.y << endl;
}

//Kreis

struct kreis
{
    struct koordinate mitte;
    int radius;
} kreis;

void setKreis(struct kreis *k, int x, int y, int radius)
{
    k->mitte.x = x;
    k->mitte.y = y;
    k->radius = radius;
}

void toStringKreis(struct kreis k)
{
    toString(k.mitte);
    cout << "\tRadius: " << k.radius << endl;
}

void strich()
{
    cout << "----------------------------------------------------" << endl;
}

int main()
{
    strich();
    cout << "Koordinate: " << endl;
    set(&koordinate1, 17, 33);
    toString(koordinate1);
    strich();
    cout << "Kreis: " << endl;
    setKreis(&kreis, 3, 9, 27);
    toStringKreis(kreis);
    strich();
}