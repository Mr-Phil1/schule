#include <iostream>
using namespace std;

struct Student
{
    int *ptr;
    char *name;
} s1;

int main()
{
    int roll = 20;
    s1.ptr = &roll;
    s1.name = (char *)"Pritesh";

    printf("\nRoll Number of Student        : %d", *s1.ptr);
    printf("\nName of Student               : %s", s1.name);
    printf("\n-----------------------------------------------");
 //  *(s1.name + 1) = 'a';
    printf("\nName of Student               : %s", s1.name);
    printf("\n-----------------------------------------------");
    printf("\nMemory-Address from the name  : %p", &s1.name);
    printf("\nMemory-Address from the ptr   : %p", &s1.ptr);
    cout << "\nMemory-Address from the s1    : " << &s1 << endl;
    return 0;
}