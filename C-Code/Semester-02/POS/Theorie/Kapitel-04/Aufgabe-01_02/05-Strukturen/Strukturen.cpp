#include <iostream>
using namespace std;
struct Student1
{
    int *ptr;
    char *name;
} s1;
// int Pointer
// char Pointer (String)
int main()
{
    int roll = 20;
    s1.ptr = &roll;
    s1.name = (char *)"Pritesh";
    printf("\nRoll Number of Student : %d", *s1.ptr);
    printf("\nName of Student: %s", s1.name);
    return 0;
}