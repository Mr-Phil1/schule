#include <iostream>
using namespace std;
int main()
{
    // Eingabe der Anzahl der zu speichernden Zahlen
    int n;
    cout << "Bitte geben Sie die Anzahl der zu speichernden Zahlen ein: ";
    cin >> n;
    cout << endl;
    // NICHT MÖGLICH IST FOLGENDES:
    // double a[n];
    // Deklaration des dynamischen Arrays: Allokation von Speicher
    double *a;
    a = new double[n];
    // Speicher für n Double-Zahlen wird reserviert und
    // die Startadresse dieses Speicherblocks dem
    // Zeiger a zugewiesen

    // Eingaben der n Double-Zahlen
    for (int i = 0; i < n; i++)
    {
        /* code */
    }
    
}