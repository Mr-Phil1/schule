#include <iostream>

using namespace std;

// Aufgabe 1

int quotient, rest;
void division1(int dividend1, int divisor1)
{
    quotient = dividend1 / divisor1;
    rest = dividend1 % divisor1;
}

// Aufgabe 2
struct Rechnung
{
    int quotient;
    int rest;
} loesung2;

struct Rechnung division2(int dividend2, int divisor2)
{
    loesung2.quotient = dividend2 / divisor2;
    loesung2.rest = dividend2 % divisor2;
   return loesung2;
};

// Aufgabe 3

void division3(int dividend3, int divisor3, int *q, int *r)
{
    *q = dividend3 / divisor3;
    *r = dividend3 % divisor3;
}
void strich()
{
    cout << "----------------------------------------------------" << endl;
}

int main()
{
    int dividend = 9, divisor = 2, q, r;
    strich();
    // Aufgabe 1
    division1(dividend, divisor);
    cout << "Aufgabe 1 (Lösung):" << endl;
    cout << "\tQuotient:  " << quotient << "\n\tRest:      " << rest << endl;
    strich();
    // Aufgabe 2
    struct Rechnung loesung2;
    loesung2 = division2(dividend, divisor);
    cout << "Aufgabe 2 (Lösung):" << endl;
    cout << "\tQuotient:  " << loesung2.quotient << "\n\tRest:      " << loesung2.rest << endl;
    strich();
    // Aufgabe 3
    division3(dividend, divisor, &q, &r);
    cout << "Aufgabe 3 (Lösung):" << endl;
    cout << "\tQuotient:  " << q << "\n\tRest:      " << r << endl;
    strich();


float ans = (float)dividend / (float)dividend;
    return 0;
}
