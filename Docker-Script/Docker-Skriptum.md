---
title: Skriptum
subtitle: Docker Installation + Ausführung + Beispiele + Problemlösungen
author: Mr. Phil
rights: CC BY-NC-SA
language: de-AT
keywords: [TINF, Linux, Docker]
titlepage: true
titlepage-color: "36A1F0"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
page-background: "background.pdf"
header-right: "Docker"
logo: "Skriptum-Bilder/Moby-logo.png"
---
# Allgemein
## Unterschied Hypervisor <-> Container
![Aufbau von Hypervisor und Container (Ersteller: Stolz Stefan)][Hypervisor_und_Container]

## Was ist Docker [WIP]
* Docker wurde am 2013-03-20 von der Docker, Inc relased
* 

\pagebreak
## Docker Must-Have Kommandobefehle

### Docker Alias setzen
  #. `nano ${HOME}/.bashrc`
  #. `alias docker='sudo docker'`
  #. `alias docker-compose='sudo docker-compose'`
  #. `clear && . ${HOME}/.bashrc` ausführen [Dieser Befehl ladet die bash config neu ein]

![Definierte docker Alias (Ersteller: Mr.Phil)][docker_Alias]

#### Hinweis zur .bashrc-Datei: 

* Bis ans Ende der Datei scrollen (mit den Pfeiltasten nach unten) und dort die aliase setzen
*  **Bitte nicht schocken lassen von der Fülle der Datei**


### Docker-Container auflisten
#### **Langschreibweise**
| Befehl                   | Erkläung                                                     |
| :----------------------- | :----------------------------------------------------------- |
| `docker container ls`    | Listet alle aktiven Container auf                            |
| `docker container ls -a` | Listet alle Container auf                                    |

#### **Kurzschreibweise**
| Befehl         | Erkläung                                                     |
| :------------- | :----------------------------------------------------------- |
| `docker ps`    | Listet alle aktiven Container auf                            |
| `docker ps -a` | Listet alle installierten Container auf, auch die gestoppten |

![docker-ps_a list (Ersteller: Mr. Phil)][docker_ps_-a]

### Docker-Container löschen
#### **Langschreibweise**
| Befehl                                    | Erkläung                                                                      |
| :---------------------------------------- | :---------------------------------------------------------------------------- |
| `docker container rm [CONTAINER_ID]`      | entfernt einen gestoppten Container mit dessen ID                             |
| `docker container rm [CONTAINER_NAME]`    | entfernt einen gestoppten Container mit dessen NAME                           |
| `docker container rm -f [CONTAINER_ID]`   | entfernt einen gestoppten bzw. auch einen laufenden Container mit dessen ID   |
| `docker container rm -f [CONTAINER_NAME]` | entfernt einen gestoppten bzw. auch einen laufenden Container mit dessen NAME |

#### **Kurzschreibweise**
| Befehl                          | Erkläung                                                                      |
| :------------------------------ | :---------------------------------------------------------------------------- |
| `docker rm [CONTAINER_ID]`      | entfernt einen gestoppten Container mit dessen ID                             |
| `docker rm [CONTAINER_NAME]`    | entfernt einen gestoppten Container mit dessen NAME                           |
| `docker rm -f [CONTAINER_ID]`   | entfernt einen gestoppten bzw. auch einen laufenden Container mit dessen ID   |
| `docker rm -f [CONTAINER_NAME]` | entfernt einen gestoppten bzw. auch einen laufenden Container mit dessen NAME |

\pagebreak
### Docker Hilfe anzeigen
| Befehl            | Erkläung                              |
| :---------------- | :------------------------------------ |
| `docker`          | Listet die Docker Hilfe auf           |
| `docker --help`   | Listet auch die Docker Hilfe auf      |
| `man docker`      | Listet die Manpage für Docker auf     |
| `man docker run ` | Listet die Manpage für Docker run auf |


### Docker Images auflisten
* `docker images`

| Befehl          | Erkläung                                                                                                                          |
| :-------------- | :-------------------------------------------------------------------------------------------------------------------------------- |
| `docker images` | Listet sämtliche Docker-Images, welche auf dem System vorhanden sind, mit Namen, Version, Erstellungsdatum und der Dateigröße auf |

![docker-image list (Ersteller: Mr. Phil)][docker-image]

### Docker Images löschen
* `docker rmi [IMAGE ID]`
* `docker rmi -f [IMAGE ID]`

| Befehl                     | Erkläung                                                          |
| :------------------------- | :---------------------------------------------------------------- |
| `docker rmi [IMAGE ID]`    | Löscht ein Docker-Image mit dessen IMAGE-ID                       |
| `docker rmi -f [IMAGE ID]` | Löscht ein Docker-Image mit dessen IMAGE-ID mit einem force Stopp |


\pagebreak
# Beispiele aus dem Unterricht

## Docker httpd Container (Apache2 Webserver)

### Docker Pull Befehl
`docker pull httpd:2.4.46`

#### Parameter erklärung
| Befehl         | Erkläung                                                         |
| :------------- | :--------------------------------------------------------------- |
| `docker pull`  | Ladet sich das jeweiligen Docker-Image aus der Registry herunter |
| `httpd:2.4.46` | Der Name bzw. der Tag des **httpd Image**                        |

### Docker Run Befehl
`docker run --rm -p[8222:]80 -v [HOST-DIR:]/usr/local/apache2/htdocs/ httpd:2.4.46`

#### Parameter erklärung
| Befehl                                 | Erkläung                                                                                           |
| :------------------------------------- | :------------------------------------------------------------------------------------------------- |
| `docker run`                           | Starte den Docker-Container                                                                        |
| `--rm`                                 | entfernt den Container nach der Benützung wieder                                                   |
| `-p[8222:]80`                          | bindet den **Host port 8222** an den **Container port 80**                                         |
|                                        | *Wenn kein Host-Port angegeben wird ein zufälliger verwendet*                                      |
| `-v ${PWD}:/usr/local/apache2/htdocs/` | bindet das **aktuelle Verzeichnis** an das **/usr/local/apache2/htdocs/** Verzeichnis im Container |
| `httpd:2.4.46`                         | Der Name bzw. die Version des **httpd Docker-Image**                                               |


\pagebreak
## Eigenes Docker Image schreiben

### Webserver Image (Apache httpd:2.4)
  1. Textdatei mit dem Namen **Dockerfile** erstellen
  2. Der Inhalt für das Dockerfile ist nach dem Punkt 4 zufinden
  3. Mit Hilfe des Dockerfiles ein Image erstellen
      * `docker build -t  tmp/my_webserver .`
  4. Dockercontianer mit dem soeben erstellten Image starten
      * `docker run --rm -p8333:80  tmp/my_webserver`

#### Erklärung des Parameter für das Erstellen
| Befehl             | Erkläung                                                                                                              |
| :----------------- | :-------------------------------------------------------------------------------------------------------------------- |
| `docker build`     | Build an image from a Dockerfile                                                                                      |
| ` -t`              | Dieser Schalter erlaubt eine Tag auf das Docker-Image zu schreiben                                                    |
| `tmp/my_webserver` | Der Tag des Docker-Image                                                                                              |
| `.`                | Hier wird der Pfad zu dem Dockerfile angegeben (In diesem Fall befinden wir uns im selbern Ordern wie das Dockerfile) |

![Docker build Terminal ausgabe][build_ausgabe]

\pagebreak
#### Inhalt des Dockerfiles (httpd:2.4)
  * Ohne benutzerdefinierter apache-config
```Dockerfile
FROM httpd:2.4

COPY ./www/ /usr/local/apache2/htdocs/
```
  * Mit benutzerdefinierter apache-config
```Dockerfile
FROM httpd:2.4

COPY ./www/ /usr/local/apache2/htdocs/
COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf
```
#### Die httpd.conf aus einem container rausspeichern
* Dieser Befehl öffent die httpd.conf Datei, pipe diese dann aus dem Cointainer raus und speichert sie dann in dem aktullen Ordner ab.
  
`docker run --rm httpd:2.4 cat /usr/local/apache2/conf/httpd.conf > my-httpd.conf`


##### Erklärung des Dockerfiles

* **Hinweis:**
Für die Verkürzung wird der Pfad */usr/local/apache2/* als */u/l/apache2/* angezeigt.!

| Befehl                                              | Erkläung                                                                    |
| :-------------------------------------------------- | :-------------------------------------------------------------------------- |
| `FROM httpd:2.4`                                    | Nimmt das httpd:2.4 Image als Basis                                         |
| `COPY ./www/ /u/l/apache2/htdocs/`                  | kopiert denn Inhalt von ./www/ in den Containerpfad /u/l/apache2/htdocs/    |
| `COPY ./my-httpd.conf /u/l/apache2/conf/httpd.conf` | kopiert die my-httpd.conf in den Containerpfad /u/l/apache2/conf/httpd.conf |


\pagebreak
## Mittels CLI in den Container mit einer neue BASH Sitzung einsteigen
 `docker exec -ti [CONTAINER_NAME] bash`

 `docker exec -ti [CONTAINER_ID] bash`

### Parameter erklärung
| Befehl             | Erkläung                                                                                                           |
| :----------------- | :----------------------------------------------------------------------------------------------------------------- |
| `docker exec`      | Führt einen neuen Befehl in einem laufenden Container aus                                                          |
| `-ti`              | Dieser Schalter öffent ein Terminal zum Container und                                                              |
|                    | leitet sämtliche Eingaben von stdin in den Container weiter.                                                       |
| `-t`               | öffnet eine Pseudo TTY Verbindung                                                                                  |
| `-i`               | hält den stdin offen                                                                                               |
| `[CONTAINER_NAME]` | Der Name des zu benützenden Containers                                                                             |
| `[CONTAINER_ID]`   | Die ID des zu benützenden Containers                                                                               |
| `bash`             | Startet einen neue BASH Sitzung                                                                                    |


\pagebreak

## Docker Logs auslesen [WIP]

\pagebreak
## Docker-Compose Verwenden
**Wird in der Sprache YAML geschrieben**

### Docker-Compose Befehle 

**Hinweis:** Diese Befehle verwenden die docker-compose.yml Datei als Basis


| Befehl                 | Erkläung                                                    |
| :--------------------- | :---------------------------------------------------------- |
| `docker-compose up`    | Startet alle Container einer Docker Compose Datei           |
| `docker-compose up -d` | Startet alle Container einer Docker Compose Datei als demon |
| `docker-compose down`  | Beendet alle Container und löscht sie zugleich              |
| `docker-compose start` | Startet alle Container                                      |
| `docker-compose stop`  | Stoppt alle Container                                       |

**Hinweis:** Möchte man einen andere Compose Datei verwenden oder möchte man eine Copmpose Datei von einem anderem Ort aus starten muss man folgende Befehl verwenden

| Befehl                                     | Erkläung                                                    |
| :----------------------------------------- | :---------------------------------------------------------- |
| `docker-compose -f (Pfad/zur/Datei) up`    | Startet alle Container einer Docker Compose Datei           |
| `docker-compose -f (Pfad/zur/Datei) up -d` | Startet alle Container einer Docker Compose Datei als demon |
| `docker-compose -f (Pfad/zur/Datei) down`  | Beendet alle Container und löscht sie zugleich              |
| `docker-compose -f (Pfad/zur/Datei) start` | Startet alle Container  einer Docker Compose Datei          |
| `docker-compose -f (Pfad/zur/Datei) stop`  | Stoppt alle Container einer Docker Compose Datei            |

\pagebreak
### Auf Basis eines Apache2-Webservers
#. Textdatei mit dem Namen **docker-compose.yml** erstellen
#. Den Inhalt des unten angefügten Codeblockes in diese soeben erstellte Datei einfügen
#. Danach den Befehl `docker-compose up` oder `sudo docker-compose up` in dem selbern Ordner ausführen in dem die docker-compose.yml Datei liegt


#### Beispielinhalt der docker-compose Datei [WIP]

* Inhalt für die oben genannte docker-compose.yml Datei
  * Für einen Apache-Webserver mit einer benutzerdefinierten Apache2-config und der dynamischen-Einbindung des www Ordners
  * Sollte das *my_webserver_images* Image nicht auf dem System vorhanden sein wird diese mit Hilfe des Dockerfile's erstellt welches sich auch im selbem Ordner befinden muss

```yaml
version: '2'

services: 

  webserver:
    image: my_webserver_images
    container_name: my_webserver
    ports: 
      - 8099:80
    volumes:
      - ./www/:/usr/local/apache2/htdocs/
      - ./my-httpd.conf:/usr/local/apache2/conf/httpd.conf
    build: 
      dockerfile: ./Dockerfile
      context: ./
```
* Inhalt des Dockerfile's
```dockerfile
FROM httpd:2.4

COPY ./www/ /usr/local/apache2/htdocs/
COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf
```
\pagebreak
## Datenbank docker-compose aufsetzten
**Warnung:** Ich übernehmen keine Garantie für die vollste Funktionaltität der dbDocker-compose Datei unter *Docker für Windows unter WSL2* **Warnung-Ende!**

### Erstellung

1. Folgende Webseite aufrufen: 
   * [https://gitlab.com/st.stolz/Skills-Project/tree/master/dockerSkills/dbDocker](https://gitlab.com/st.stolz/Skills-Project/tree/master/dockerSkills/dbDocker)
  
2. In der URL Leiste musst die URL mit dem unten gezeigten Bild übereinstimmen

![dbdocker_url (Ersteller: Mr. Phil)][dbdocker_url]

3. ReadMe durchlesen
4. Docker Verzeichnis  anlegen
   1. Im Home Verzeichnis einen docker Ordner erstellen
   2. In diesem dann einen dbdocker Ordner erstellen
   3. In dem dbdocker Ordner einmal einen www und einen mysqlBackup Ordner erstellen
5. Danach die docker-compose.yml Datei öffnen, siehe Bild

![dbdocker_docker-compoes.yml Datei (Ersteller: Mr. Phil)][dbdocker_docker-compose]


6. Danach in der Leiste mit den drei Icons auf das mittlere klicken, um die rohe docker-compose.yml anzuzeigen. Siehe unteres Bild.

![dbdocker_docker-compose.yml_open-raw (Ersteller: Mr. Phil)][dbdocker_raw]

7. Die nun folgende URL kopieren. URL siehe Bild.
   
![dbdocker_url_dbdocker-raw (Ersteller: Mr. Phil)][dbdocker_url-raw]


8. Sollte Docker-Compose auf dem Systemnoch nicht vorhanden sein, sollte man diese mit diesem Befehl installieren.
 * `curl -s hydra.mr-phil.eu/Schule/tinf/install_docker-compose.sh | bash`
   
9.  Im Anschluss wieder in das dbdocker Verzeichnis wechseln und folgende Befehle ausühren
   * `wget {Hier die kopierte URL einfügen} -O ./docker-compose.yml`
10. Danach kann man diese Komposition mittels `sudo docker-compose up` starten
11. Beim ersten Start dieser Komposition kann es ein paar Minuten dauern bis alles heruntergeladen und gestartet ist.
12.  Für die Vereinfachung empfiehlt sich einen Alias in der .bashrc für die Komposition anzulegen
     1. `nano ${HOME}/.bashrc`
     2. `alias dbdocker='sudo docker-compose -f {pfad zu der dbdocker-compose.yml Datei}'`
     3. `clear && . ${HOME}/.bashrc` ausführen [Dieser Befehl ladet die bash config neu ein]
13. **Hinweis**: Diese docker-compose wird bei jedem Systemstart mit gestartet wenn diese nicht gestopt wurde. 


\pagebreak
### Info
#### Mittels einer Teminal-Sitzung in den mysql Bereich anmelden
* `sudo docker exec -ti mysql mysql -uroot -p123` 
   * `sudo docker exec -ti mysql` 
     * Öffnet eine Terminal Sitzung in den mysql Container
   * `mysql -uroot -p123` 
     * Hier mit öffnet man eine mysql-Sitzung mit dem Username *root* und dem Passwort *123*
* `docker exec -i mysql mysql -t -uroot -p123 < besispieldatei.sql`
  * Dieseer Befehl führt eine Beispieldatei.sql in dem mysql-Docker Container aus
  
#### Inhalt einer SQL-Beispieldatei
* So könnte eine SQL-Datei, wie sie über Docker ausgeführt werden kann, ausschauen.

```sql
CREATE DATABASE IF NOT EXISTS `sample-Database`;

USE `sample-Database`;

CREATE TABLE Teacher (
    id int NOT null AUTO_INCREMENT,
    LastName varchar(255),
    -- Wenn man für ShortName varchar verwenden würde, würde jeder 5 statt 4 Bytes verbrauchen 
    ShortName char(4),
    PRIMARY KEY(id)
);

INSERT INTO Teacher(LastName, ShortName) VALUES('Scharmer', 'SALE'), ('Stolz', 'STOL'), ('Landerer', 'LAND');
```
\pagebreak
#### Verwendete Docker-Images und deren Version
| Image                         | Version              |
| :---------------------------- | :------------------- |
| **bitnami/postgresql**        | 9.6.10               |
| **mysql**                     | latest               |
| **bitnami/mongodb**           | 3.6.8                |
| **thecodingmachine/php**      | 7.2-v2-apache-node10 |
| **adminer**                   | 4.7.7                |
| **mongo-express**             | 0.49                 |
| **bitnami/phpmyadmin**        | 5.0.2                |
| **fradelg/mysql-cron-backup** | latest               |

#### Port-Übersicht
| Service                         | Port      |
| :------------------------------ | :-------- |
| **Apache2 Webserver mit php**   | 80        |
| **Adminer Frontend**            | 8080      |
| **Mongo-Express**               | 8081      |
| **PHPMyAdmin Frontend**         | 8082      |
| **Postgres SQL**                | 5432      |
| **MariaDB**                     | 3306      |
| **MongoDB**                     | 27017     |

![dbdocker-ps_Kommandozeilen Ausgabe](Skriptum-Bilder/ddbdocker_ps.png)

\pagebreak
## Java Programm mit Hilfe von Docker kompilieren
### Händisch
1. Java Programm kompilieren: `docker run --rm -v ${PWD}/src/:/usr/src/myapp -w /usr/src/myapp openjdk:7 javac Main.java`
2. Java Programm ausführen: `docker run --rm -v ${PWD}/src/:/usr/src/myapp -w /usr/src/myapp openjdk:7 java Main`

### Mit Hilfe eines Dockerfiles
```dockerfile
FROM openjdk:7

COPY ./src/ /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac Main.java
CMD ["java", "Main"]
```

#### Dockerfile-Erklärung
1. Zunächst wird für unser neues Image die Version 7 des OpenJDK Imager zu Hand genommen
2. Danach wird dann der Inhalt des **src** Verzeichnisses in das Container-Verzeichniss **/usr/src/myapp** kopiert.
3. Dieses wird dann als WORKDIR gesetzt
4. Während der Image-Erstellung wird dann mit Hilfe des Befehls `javac Main.java` das Java Programm kompiliert.
5. Wenn dann ein Docker-Container mit diesem Image gestartet wird, wird dann das Java-Programm augeführt.

### Inhalt der Main.java
* So könnte die Main.java Datei ausschauen.
```java
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");
        for(int i =0; i <= 20; i++ ){
        System.out.println("Number: "+i);
        }
    }
}
```

\pagebreak
# Problemlösung
## Probleme bei der Aktivierung des ssh-Servers
#. Zunächst sollte man mit `journalctl -u ssh.service` überprüfen ob die Fehlermeldung **sshd: no hostkeys available -- exiting** erscheint
#. Sollte dieser Fall eingetreten sein so sollte man zunächst in den Ordner */etc/ssh* wechseln
#. In diesem Ordner dann den Befehl `sudo ssh-keygen -A` 
#. Nach der Ausführung dieses Befehl sollte man das System neustarten
#. Ab diesem Zeitpunkt sollte nun die  Verbindung über ssh wieder funktionieren

## Probleme beim Stoppen eines docker-containers
`sudo aa-remove-unknown`

## Probleme bei der Ausführung von docker-compose
Sollte die Fehlermeldung **cannot change profile for the next exec call: No such file or directory** auftauchen so empfielt sich eine der nun folgenden Varianten auszuführen

### Variante 1 über App-Armor
`sudo apparmor_parser -r /var/lib/snapd/apparmor/profiles/*`

### Variante 2 über eine neue Docker-compose Installation
Hier wird die aktuellste Docker-Compose Version installiert und im Anschuss wird dann in der .bashrc der Alias **docker-compose="sudo docker-compose"** gesetzt.

```bash
curl http://hydra.mr-phil.eu/Schule/tinf/install_docker-compose.sh | bash
```

\pagebreak
# Literatur und Quellenverzeichniss

## Literaur: 
* **Ersteller**:
  * [Github_Mr-Phil1](https://github.com/Mr-Phil1/)
  * [Gitlab_Mr-Phil1](https://gitlab.com/Mr-Phil1/)

* **GitLab**:
  * [WIKI-Seite_st.stolz](https://gitlab.com/school-edu/betriebssysteme-skriptum/-/wikis/Serverdienste/Docker-Installation)
  * [dbDocker-docker-compose_st.stolz](https://gitlab.com/st.stolz/Skills-Project/-/blob/master/dockerSkills/dbDocker/docker-compose.yml)

* **Docker-Hub**:
  * [Main-Seite](https://hub.docker.com/)
  * [httpd-Seite](https://hub.docker.com/_/httpd)

* **Wikipedia**:
  * [Docker (Software), English](https://en.wikipedia.org/wiki/Docker_%28software%29)
  * [Docker (Software), Deutsch](https://de.wikipedia.org/wiki/Docker_(Software))

## Quellen:
* [Docker Logos](https://www.docker.com/company/newsroom/media-resources)
  * [Docker-Moby_logo](https://www.docker.com/sites/default/files/d8/2019-07/Moby-logo.png)

---
Copyright (CC BY-NC-ND) 2021 Copyright Mr-Phil1 All Rights Reserved.

[Hypervisor_und_Container]: ./Skriptum-Bilder/Hypervisors_and_Containers.png
[docker_Alias]: ./Skriptum-Bilder/docker-alias.png {height=200px width=250px}
[docker_ps_-a]: ./Skriptum-Bilder/docker-ps_a.png {height=400px width=500px}
[docker-image]: ./Skriptum-Bilder/docker-images.png {height=400px width=500px}
[build_ausgabe]: ./Skriptum-Bilder/docker-build_ausgabe.png
[dbdocker_url]: Skriptum-Bilder/dbdocker_url.png
[dbdocker_docker-compose]: Skriptum-Bilder/ddbdocker_docker-compose.png
[dbdocker_url-raw]: Skriptum-Bilder/dbdocker_url-raw.png
[dbdocker_raw]: Skriptum-Bilder/db-raw.png