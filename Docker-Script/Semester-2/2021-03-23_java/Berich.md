---
title: Arbeitsbericht 
subtitle: Für eine JAVA-Kompilierung mit Hilfe eines Docker-Containers
author: Mr. Phil
rights: Nah
language: de-AT
keywords: TINF; Linux;
titlepage: true
titlepage-color: "36A1F0"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---
# Einleitung
In diesem Bericht wird erklärt wie man ein Programm welches in der Sprache Java geschrieben wurde, mit Hilfe von Docker kompilieren und ausführen kann.

# Verwendete Technologien
Technologien-Name | Verwendete Version 
------------ | ------------- | -------------
Ubuntu |Ubuntu 20.04 LTS 
Docker  | Docker version 20.10.5, build 55c4c88 
Docker OpenJDK Image | openjdk:7

# Durchführung

## docker image build
`sudo docker build -t mrphil/java .`

Mit diesem Befehl wird mit Hilfe des zu zuvor erstellten Dockerfile ein Image erstellt.

## docker image run
`sudo docker run --rm mrphil/java`

Im Anschluss wird dann ein Docker-Container mit dem zuvor erstellte Image gestartet, dieser wird nach der Ausführung gelöscht. Sollte man nach einer Ausführung ander Java Datei etwas geändert haben, so muss man das Docker-Image neu bauen. 


\pagebreak
## Dockerfile

### Dockerfile-Inhalt
```dockerfile
FROM openjdk:7

COPY ./src/ /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac Main.java
CMD ["java", "Main"]
```
### Dockerfile-Erklärung
1. Zunächst wird für unser neues Image die Version 7 des OpenJDK Imager zu Hand genommen
2. Danach wird dann der Inhalt des **src** Verzeichnisses in das Container-Verzeichniss **/usr/src/myapp** kopiert.
3. Dieses wird dann als WORKDIR gesetzt
4. Während der Image-Erstellung wird dann mit Hilfe des Befehls `javac Main.java` das Java Programm kompiliert.
5. Wenn dann ein Docker-Container mit diesem Image gestartet wird, wird dann das Java-Programm augeführt.

## Javafile
### Inhalt der Main.java
```java
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");
        for(int i =0; i <= 20; i++ ){
        System.out.println("Number: "+i);
        }
    }
}
```
# Diskussion
OpenJDK ist eine freie und OpenSource Implementation von der **Java Standart Edition (Java SE)**, welche unter der **GNU General Public (GNU GPL) 2** lizenzsiert ist. Seit der Version 7 der Java SE ist OpenJDK die offizielle Referenz Implementation. 

# Literaturverzeichnis
* [Docker-Hub](https://hub.docker.com/)
  * [OpenJDK](https://hub.docker.com/_/openjdk)
* [Wikipedia Deutsch](https://de.wikipedia.org/)
  * [OpenJDK Wikipedia Deutsch](https://de.wikipedia.org/wiki/OpenJDK)
* [Wikipedia English](https://de.wikipedia.org/)
  * [OpenJDK Wikipedia Englisch](https://en.wikipedia.org/wiki/OpenJDK)