---
title: Arbeitsbericht
subtitle: Docker httpd Installation + Ausführungs Vereinfachung
author: Mr. Phil
rights: Nah
language: de-AT
keywords: TINF; Linux; Docker
titlepage: true
titlepage-color: "36A1F0"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---
# Einleitung
Der folgende Bericht handelt davon, wie man automatisiert einen Apache2 httpd Docker-Container herunterladet und personalisiert startet. Man könnte zwar die jeweilgen Kommandobefehle wie z.B.:  `docker pull httpd` oder `docker run --rm -p8222:80 -v /home/debian/:/usr/local/apache2/htdocs/ httpd` immer wieder von Hand eingeben, der einzige Nachteil bei diesen Vefahren ist jedoch dass man bei der Ordnermitgabe nicht mit der Autovervollständigung arbeiten kann. Aus diesem Grund habe ich ein Script für die Vereinfachung geschrieben.


# Verwendete Technologien
Technologien-Name | Verwendete Version | Containername
------------ | ------------- | -------------
Debian | Debian GNU/Linux 10 (buster) |
Windows 10 | 2004 (Build 19041.746) |
Windows Subsystem for Linux  | WSLv2 |
Docker Desktop | 3.10.0 (51484) |
Apache2 Webserver | latest | httpd


# Durchführung
Für dieses Script ist eine Mitgabe von Argumenten zwingend erforderlich. Diese Tabelle veranschaulicht, welches Argument wann und für was benötigt wird.

Namen der Variablen | Speicherort für: | Arg. Nr | Beispiel Eingaben
------------  | :-------------: | :-------------: | :-------------:
**hostPort**| *Der localhost Port für die Durchreichung* | **1** | *8222*
**directoryPath** | *Das Verzeichnis für die Durchreichung* | **2** | */home/debian/*

Das BackupScript kann folgendermaßen aufgerufen werden:

* `bash docker-install_run-httpd.sh 8222 /home/debian/`

\pagebreak
## Quellcode
```bash
###########args###########
hostPort="$1"
directoryPath="$2"
#########################
#this is the auto-install routine

if ! docker images --format '{{.Repository}}' | grep 'httpd' >/dev/null 2>&1; then
  clear
  echo "--------------------------------------------------------------------------"
  echo "                  Welcome to the Mr. Phil1 Docker-Script"
  echo "                            Apache2 Webserver"
  echo "--------------------------------------------------------------------------"
  echo " Es wird nun ein System-Update gemacht."
  echo "--------------------------------------------------------------------------"
  sudo apt-get update -y > /dev/null
  echo "--------------------------------------------------------------------------"
  echo " Nun wird der aktuellste httpd-Container heruntergeladen."
  echo "--------------------------------------------------------------------------"
  docker pull httpd
fi

clear
echo "--------------------------------------------------------------------------"
echo "                  Welcome to the Mr. Phil1 Docker-Script"
echo "                            Apache2 Webserver"
echo "--------------------------------------------------------------------------"
if [[ "$2" == '' ]] ; then
  echo "  Sie haben zu wenigen Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi

if [ $# -eq 3 ] ; then
  echo "  Sie haben zu viele Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi

if  [[ $hostPort =~ [a-zA-Z]+ ]] ; then
  echo "  Sie haben eine ungültige Portnummer mitgegeben!"
  echo "  Sie müssen bsp.: 8080 eingeben."
  echo "--------------------------------------------------------------------------"
  exit 1
fi
echo " Ihre Eingaben lauten:"
echo "  hostPort:        ${hostPort}"
echo "  directoryPath:   ${directoryPath}"
echo "--------------------------------------------------------------------------"

echo "  Die httpd-Container wird nun gestartet!"
echo "--------------------------------------------------------------------------"
docker run --rm -p${hostPort}:80 -v ${directoryPath}:/usr/local/apache2/htdocs/ httpd >/dev/null 2>&1
echo "  Vielen Herzlichen Dank für Ihr Vertrauen."
echo "--------------------------------------------------------------------------"
```
\pagebreak
## Erklärung des Quellcodes

### Auto-Installations Routine:
In diesem Bereich wird überprüft, ob das Apache2 httpd Docker-Image auf dem Gerät nicht nicht verfügtbar ist, wenn das zutrifft wird zuerst ein System-Update gemacht und im Anschluss wird dann das aktuellste Image heruntergeladen.
```bash
if ! docker images --format '{{.Repository}}' | grep 'httpd' >/dev/null 2>&1; then
  clear
  echo "--------------------------------------------------------------------------"
  echo "                  Welcome to the Mr. Phil1 Docker-Script"
  echo "                            Apache2 Webserver"
  echo "--------------------------------------------------------------------------"
  echo " Es wird nun ein System-Update gemacht."
  echo "--------------------------------------------------------------------------"
  sudo apt-get update -y > /dev/null
  echo "--------------------------------------------------------------------------"
  echo " Nun wird der aktuellste httpd-Container heruntergeladen."
  echo "--------------------------------------------------------------------------"
  docker pull httpd
fi
```
### Erste Argument-Überprüfung:
In diesem Bereich wird überprüft, ob man dem Script weniger als zwei Argumente mitgegeben hat. Wenn dieser Fall zutrifft, gibt das Programm eine Fehlermeldung aus und beendet dieses.
```bash
if [[ "$2" == '' ]] ; then
  echo "  Sie haben zu wenigen Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi
```
### Zweite Argument-Überprüfung:
In diesem Bereich wird überprüft, ob man dem Script mehr als zwei Argumente mitgegeben hat. Wenn dieser Fall zutrifft, gibt das Programm eine Fehlermeldung aus und beendet dieses.
```bash
if [ $# -eq 3 ] ; then
  echo "  Sie haben zu viele Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi
```
### Portnummer-Überprüfung
In diesem Bereich wird überprüft, ob es sich um eine ungültige Portnummer handelt. Wenn dieser Fall zutrifft, gibt das Programm eine Fehlermeldung und eine Hinweismeldung aus und beendet dieses, sollte es sich doch um eine gültige Portnummer handeln wird dieser Bereich übersprungen.
```bash
if  [[ $hostPort =~ [a-zA-Z]+ ]] ; then
  echo "  Sie haben eine ungültige Portnummer mitgegeben!"
  echo "  Sie müssen bsp.: 8080 eingeben."
  echo "--------------------------------------------------------------------------"
  exit 1
fi
```

### Container start
In diesem Bereich werden zuerst die mitgegeben Argumenten ausgegeben, im Anschluss wird dann der selbstzerstörende httpd-Container mit denn mitgegeben Argumenten gestartet.
```bash
echo " Ihre Eingaben lauten:"
echo "  hostPort:        ${hostPort}"
echo "  directoryPath:   ${directoryPath}"
echo "--------------------------------------------------------------------------"

echo "  Die httpd-Container wird nun gestartet!"
echo "--------------------------------------------------------------------------"
docker run --rm -p${hostPort}:80 -v ${directoryPath}:/usr/local/apache2/htdocs/ httpd >/dev/null 2>&1
echo "  Vielen Herzlichen Dank für Ihr Vertrauen."
echo "--------------------------------------------------------------------------"
```
\pagebreak
# Literatur

* Docker-Hub:
  * [Main-Seite](https://hub.docker.com/)
  * [httpd-Seite](https://hub.docker.com/_/httpd)
* stackoverflow:
  * [Main-Seite](https://stackoverflow.com/)
  * [How to remove docker images based on name? (Hilfe Stellung für die Docker-Image suche mit dem Namen)](https://stackoverflow.com/questions/40084044/how-to-remove-docker-images-based-on-name)

---

Copyright (c) 2021 Copyright mr-phil1 All Rights Reserved.
