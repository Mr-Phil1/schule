#!/bin/bash
###########args###########
hostPort="$1"
directoryPath="$2"
#########################
#this is the auto-install routine 

if !docker images --format '{{.Repository}}' | grep 'httpd' >/dev/null 2>&1; then
  clear
  echo "--------------------------------------------------------------------------"
  echo "                  Welcome to the Mr. Phil1 Docker-Script"
  echo "                            Apache2 Webserver"
  echo "--------------------------------------------------------------------------"
  echo " Es wird nun ein System-Update gemacht."
  echo "--------------------------------------------------------------------------"
  sudo apt-get update -y > /dev/null
  echo "--------------------------------------------------------------------------"
  echo " Nun wird der aktuellste httpd-Container heruntergeladen."
  echo "--------------------------------------------------------------------------"
  docker pull httpd:2.4.46
fi

clear
echo "--------------------------------------------------------------------------"
echo "                  Welcome to the Mr. Phil1 Docker-Script"
echo "                            Apache2 Webserver"
echo "--------------------------------------------------------------------------"
if [[ "$2" == '' ]] ; then
  echo "  Sie haben zu wenigen Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi

if [ $# -eq 3 ] ; then
  echo "  Sie haben zu viele Argumente mitgegeben!"
  echo "  Sie müssen hostPort und directoryPath angegeben"
  echo "--------------------------------------------------------------------------"
  exit 1
fi

if  [[ $hostPort =~ [a-zA-Z]+ ]] ; then
  echo "  Sie haben eine ungültige Portnummer mitgegeben!"
  echo "  Sie müssen bsp.: 8080 eingeben."
  echo "--------------------------------------------------------------------------"
  exit 1
fi
echo " Ihre Eingaben lauten:"
echo "  hostPort:        ${hostPort}"
echo "  directoryPath:   ${directoryPath}"
echo "--------------------------------------------------------------------------"

echo "  Die httpd-Container wird nun gestartet!"
echo "--------------------------------------------------------------------------"
sudo docker run --rm -p${hostPort}:80 -v ${directoryPath}:/usr/local/apache2/htdocs/ httpd >/dev/null 2>&1
echo "  Vielen Herzlichen Dank für Ihr Vertrauen."
echo "--------------------------------------------------------------------------"

#Copyright (c) 2021 Copyright mr-phil1 All Rights Reserved.
