function init1() {
    document.getElementById('outputSum').innerHTML = 'Seite init';
    document.getElementById('outputFahrenheit').innerHTML = 'Seite init';

}

function toFahrenheit(celsius) {
// F = C 1.8 + 32
    return celsius * 1.8 + 32;
}

function toFahrenheitOutput() {
    var celsius = document.getElementById("inputCelsius").value;
    if (celsius != '') {
        document.getElementById("outputFahrenheit").innerHTML = toFahrenheit(celsius) + ' °F';
    } else {
        document.getElementById("outputFahrenheit").innerHTML = 'Please enter a value!';
    }
}

function mySum(num1, num2) {
    var sum = parseInt(num1) + parseInt(num2);
    return sum;
}

function inputTwoDigitSum() {
    var num1 = document.getElementById("digitOne").value;
    var num2 = document.getElementById("digitTwo").value;
    if (num1 != '' && num2 != '') {
        document.getElementById("outputSum").innerHTML = mySum(num1, num2);
    } else {
        document.getElementById("outputSum").innerHTML = 'Please enter a value!';
    }
}