<!doctype html>
<html>
<head>
    <title>BMI Calulator</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="post" action="index.php">
    Height (cm): <input type="number" name="height"><br>
    Weight (kg): <input type="number" name="weight"><br>
    <input type="submit" value="Calculate BMI">
</form>

<?php
if (!empty($_POST)) {
    $height=$_POST["height"]/100;
    $weight=$_POST["weight"];
    echo "Your BMI is ".calcBMI($height, $weight);
}
function calcBMI($height, $weight){
    $bmi = $weight/pow($height,2);
    return round($bmi, 2);
}
?>
</body>
</html>